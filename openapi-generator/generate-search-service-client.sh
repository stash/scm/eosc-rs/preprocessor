cd "${0%/*}"

if [ -z "$1" ]
  then
    echo "Nie podano ścieżki lub adresu url do pliku swagger.json"
    exit 1
fi

if [ ! -f openapi-generator-cli-5.0.1.jar ]; then
    echo "Pobieranie pliku openapi-generator-cli-5.0.1.jar"
    wget https://repo1.maven.org/maven2/org/openapitools/openapi-generator-cli/5.0.1/openapi-generator-cli-5.0.1.jar
fi

cd ..

java -jar openapi-generator/openapi-generator-cli-5.0.1.jar generate \
   -i ${1} \
   -g java \
   --library resttemplate \
   --api-package pl.psnc.eosc.rs.preprocessor.common.pitocr.api \
   --model-package pl.psnc.eosc.rs.preprocessor.common.pitocr.model \
   --invoker-package pl.psnc.eosc.rs.preprocessor.common.pitocr.invoker \
   --group-id pl.psnc.eosc.rs.preprocessor \
   --artifact-id search-service-client-generated \
   -p dateLibrary=java8-localdatetime \
   --artifact-version latest \
   -o search-service-client-generated \
   -p java8=true \
   -p useAbstractionForFiles=true \
   -p parentArtifactId=preprocessor-parent \
   -p parentGroupId=pl.psnc.eosc.rs.preprocessor \
   -p parentVersion=latest
