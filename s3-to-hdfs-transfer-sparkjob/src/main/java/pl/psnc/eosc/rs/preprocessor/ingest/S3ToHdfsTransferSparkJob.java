package pl.psnc.eosc.rs.preprocessor.ingest;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.FileUtil;
import org.apache.hadoop.fs.Path;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.sql.SparkSession;

import java.io.IOException;

public class S3ToHdfsTransferSparkJob {

    public static void main(String[] args) throws IOException {
        if(args.length < 3) throw new IllegalArgumentException("At least 2 parameters required! [srcFiles..., destDir, mode(1)] or [srcFiles..., destPaths..., mode(2)]");

        final SparkConf sparkConf = new SparkConf()
                .setAppName("S3ToHdfsTransfer");

        final JavaSparkContext javaSparkContext = new JavaSparkContext(sparkConf);

        final SparkSession sparkSession = SparkSession
                .builder()
                .sparkContext(javaSparkContext.sc())
                .appName("S3ToHdfsTransfer")
                .getOrCreate();


        Path firstSrcFilePath = new Path(args[0]);
        int mode = Integer.parseInt(args[args.length-1]);

        sparkSession.sparkContext().hadoopConfiguration().set("mapreduce.fileoutputcommitter.algorithm.version", "2");
        Configuration hadoopConfig = sparkSession.sparkContext().hadoopConfiguration();
        FileSystem srcFs = FileSystem.get(firstSrcFilePath.toUri(), hadoopConfig);
        FileSystem dstFs = FileSystem.get(new Path(args[args.length-2]).toUri(), hadoopConfig);

        if(mode == 1){
            Path dstDirPath = new Path(args[args.length-2]);
            int numOfSrcFiles = args.length-2;

            for(int i = 0; i < numOfSrcFiles; i++){
                Path srcFilePath = new Path(args[i]);
                copy(srcFs,srcFilePath,dstFs,new Path(dstDirPath,srcFilePath.getName()),hadoopConfig);
            }
        } else if(mode == 2) {
            if((args.length-1)%2 != 0) throw new RuntimeException("The number of src files and dest paths must be equal");
            int numOfSrcFiles = (args.length-1)/2;

            for(int i = 0; i < numOfSrcFiles; i++) {
                Path srcFilePath = new Path(args[i]);
                Path destPath = new Path(args[i+numOfSrcFiles]);
                copy(srcFs,srcFilePath,dstFs,destPath,hadoopConfig);
            }

        } else {
            throw new RuntimeException("Invalid mode");
        }
    }

    public static void copy(FileSystem srcFS, Path src,
                      FileSystem dstFS, Path dst,  Configuration hadoopConf) throws IOException {

        FileStatus srcFileStatus = srcFS.getFileStatus(src);

        if(srcFileStatus.isDirectory()){
            if(dstFS.exists(dst) && !dstFS.getFileStatus(dst).isDirectory()) dstFS.delete(dst,false);
            dstFS.mkdirs(dst);
            for (FileStatus srcFileStatusNested : srcFS.listStatus(srcFileStatus.getPath())) {
                copy(srcFS, srcFileStatusNested.getPath(), dstFS, new Path(dst,srcFileStatusNested.getPath().getName()), hadoopConf);
            }
        } else {
            dstFS.delete(dst,false);
            FileUtil.copy(srcFS, src, dstFS, dst, false, true, hadoopConf);
        }
    }

}
