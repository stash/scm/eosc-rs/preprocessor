CREATE DATABASE test;
USE test;

CREATE TABLE testTable
(
    id INTEGER AUTO_INCREMENT,
    name TEXT,
    PRIMARY KEY (id)
) COMMENT='this is the test table';