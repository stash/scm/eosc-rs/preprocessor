package pl.psnc.eosc.rs.preprocessor.job.psql.util;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hdfs.MiniDFSCluster;
import org.apache.hadoop.test.PathUtils;

import java.io.File;
import java.io.IOException;

public class HdfsCluster {

    MiniDFSCluster cluster;

    public MiniDFSCluster start() throws IOException {
        File baseDir = new File(PathUtils.getTestDir(getClass()), "miniHDFS");

        cluster = new MiniDFSCluster.Builder(hadoopConf(baseDir))
//                .nameNodePort(9000)
                .manageNameDfsDirs(true)
                .manageDataDfsDirs(true)
                .format(true)
                .build();
        cluster.waitClusterUp();
        return cluster;
    }

    public void stop() {
        cluster.shutdown();
    }

    public Configuration hadoopConf(File baseDir) {
        Configuration conf = new Configuration();
        conf.set(MiniDFSCluster.HDFS_MINIDFS_BASEDIR, baseDir.getAbsolutePath());
        return conf;
    }
}
