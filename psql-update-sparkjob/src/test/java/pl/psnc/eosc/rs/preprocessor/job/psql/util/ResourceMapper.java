package pl.psnc.eosc.rs.preprocessor.job.psql.util;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ResourceMapper {

    public static List<ResourcePojo> mapResultToResourceList(ResultSet rs) throws SQLException {
        List<ResourcePojo> result = new ArrayList();
        while(rs.next())
        {
            result.add(mapResourceRow(rs));
        }
        return result;
    }

    private static ResourcePojo mapResourceRow(ResultSet rs) throws SQLException {
        ResourcePojo resource = new ResourcePojo();
        resource.setResourceType(rs.getString("resource_type"));
        resource.setResourceId(rs.getString("resource_id"));
        resource.setVisits(rs.getInt("visits"));
        resource.setOrders(rs.getInt("orders"));
        return resource;
    }
}
