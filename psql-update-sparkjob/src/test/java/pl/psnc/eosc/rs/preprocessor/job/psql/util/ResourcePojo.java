package pl.psnc.eosc.rs.preprocessor.job.psql.util;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class ResourcePojo {

    private  String resourceType;
    private String resourceId;
    private Integer visits;
    private Integer orders;

    public ResourcePojo(String resourceType, String resourceId) {
        this.resourceType=resourceType;
        this.resourceId=resourceId;
    }
}
