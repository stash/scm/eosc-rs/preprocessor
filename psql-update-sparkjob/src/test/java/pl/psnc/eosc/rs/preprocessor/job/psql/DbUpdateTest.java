package pl.psnc.eosc.rs.preprocessor.job.psql;

import lombok.extern.slf4j.Slf4j;
import org.apache.hadoop.hdfs.MiniDFSCluster;
import org.apache.spark.sql.SparkSession;
import org.apache.hadoop.fs.Path;
import org.junit.ClassRule;
import org.junit.Test;
import org.junit.jupiter.api.Disabled;
import org.testcontainers.containers.PostgreSQLContainer;
import pl.psnc.eosc.rs.preprocessor.shared.ResourceType;
import pl.psnc.eosc.rs.preprocessor.job.psql.util.DbConnection;
import pl.psnc.eosc.rs.preprocessor.job.psql.util.HdfsCluster;
import pl.psnc.eosc.rs.preprocessor.job.psql.util.PostgresContainer;
import pl.psnc.eosc.rs.preprocessor.job.psql.util.ResourcePojo;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.google.common.truth.Truth.assertThat;


public class DbUpdateTest {

    @ClassRule
    public static PostgreSQLContainer postgreSQLContainer = PostgresContainer.getInstance();

    @Disabled // TODO due to Jenkins issue, run 165.
//    @Test
    public void initProcessing() throws IOException, SQLException {
        HdfsCluster hdfsCluster = new HdfsCluster();
        MiniDFSCluster cluster = hdfsCluster.start();
        ClassLoader classLoader = getClass().getClassLoader();
        File file = new File(classLoader.getResource("diff_dump").getFile());
        String absolutePath = file.getAbsolutePath();
        SparkSession sparkSession = SparkSession.builder().master("local").getOrCreate();
        String uri = cluster.getFileSystem().getUri().toString();
        cluster.getFileSystem().copyFromLocalFile(new Path(absolutePath),new Path("diff_dump"));
        String url = new Path(uri).getFileSystem(cluster.getConfiguration(0)).getHomeDirectory().toString();
        sparkSession.sparkContext().hadoopConfiguration().set("fs.defaultFS",url);

        DbConnection db = new DbConnection(postgreSQLContainer);
        // get initial records
        List<ResourcePojo> initialState = db.getAllRecords(sparkSession.sparkContext().conf().get("spark.db.table"));

        // expected changes
        String resourceType = ResourceType.PUBLICATION.getId().toString();
        List<ResourcePojo> expectedAddedResources = List.of(
                new ResourcePojo(resourceType,"10000003",0,0),
                new ResourcePojo(resourceType,"10000006",0,0)
        );
        List<ResourcePojo> expectedDeletedResources = List.of(
                new ResourcePojo(resourceType,"12"),
                new ResourcePojo(resourceType,"13")
        );

        //when
        PsqlUpdateSparkJob.main(Stream.of(url+"/diff_dump", "PUBLICATION").toArray(String[]::new));
        // get records after executing the Job
        List<ResourcePojo> result = db.getAllRecords(sparkSession.sparkContext().conf().get("spark.db.table"));

        // then
        checkDbRecords(initialState,result,expectedAddedResources,expectedDeletedResources);
    }

    private void checkDbRecords(
            List<ResourcePojo> initialState,
            List<ResourcePojo> result,
            List<ResourcePojo> added,
            List<ResourcePojo> deleted){
        List<ResourcePojo> expectedResult;
        expectedResult = initialState.stream().filter(
                res -> !containsResource(deleted, res.getResourceType(), res.getResourceId()))
                .collect(Collectors.toList());
        added.forEach(resource ->
                expectedResult.add(new ResourcePojo(
                        resource.getResourceType(),
                        resource.getResourceId(),
                        resource.getVisits(),
                        resource.getOrders())));
        // assertions
        assertThat(result.size()).isEqualTo(expectedResult.size());
        result.forEach(
                resource -> assertThat(
                containsResource(expectedResult,
                        resource.getResourceType(),
                        resource.getResourceId()))
                .isTrue());
    }

    private boolean containsResource(final List<ResourcePojo> list, final String resourceType, final String resourceId){
        return list.stream().anyMatch(o ->
                o.getResourceType().equals(resourceType) &&
                o.getResourceId().equals(resourceId));
    }
}