package pl.psnc.eosc.rs.preprocessor.job.psql.util;

import org.apache.spark.SparkContext;
import org.apache.spark.sql.SparkSession;
import org.testcontainers.containers.PostgreSQLContainer;
import org.testcontainers.utility.MountableFile;

public class PostgresContainer extends PostgreSQLContainer<PostgresContainer> {
    private static final String IMAGE_VERSION = "postgres:11.1";
    private static PostgresContainer container;

    private PostgresContainer() {
        super(IMAGE_VERSION);
    }

    public static PostgresContainer getInstance() {
        if (container == null) {
            container = new PostgresContainer();
            container.withCopyFileToContainer(
                    MountableFile.forClasspathResource(
                            "init_db.sql"),
                    "/docker-entrypoint-initdb.d/schema.sql");
        }
        return container;
    }

    @Override
    public void start() {
        super.start();
        SparkContext sc = SparkSession.builder().master("local").getOrCreate().sparkContext();
        sc.conf().set("spark.db.url", container.getJdbcUrl());
        sc.conf().set("spark.db.table", "base.stats_total_table");
        sc.conf().set("spark.db.user", container.getUsername());
        sc.conf().set("spark.db.password", container.getPassword());
        sc.conf().set("spark.db.batch.size", "50");
        System.setProperty("spring.datasource.url", container.getJdbcUrl());
        System.setProperty("spring.datasource.username", container.getUsername());
        System.setProperty("spring.datasource.password", container.getPassword());
    }

    @Override
    public void stop() {
        //do nothing, JVM handles shut down
    }
}