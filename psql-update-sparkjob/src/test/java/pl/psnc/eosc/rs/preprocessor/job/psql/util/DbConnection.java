package pl.psnc.eosc.rs.preprocessor.job.psql.util;

import lombok.extern.slf4j.Slf4j;
import org.testcontainers.containers.PostgreSQLContainer;

import java.sql.*;
import java.util.List;
import java.util.Properties;

@Slf4j
public class DbConnection {

    Connection conn;
    public DbConnection(PostgreSQLContainer container) throws SQLException {
            Properties props = new Properties();
            String jdbcUrl = container.getJdbcUrl();
            props.setProperty("user", container.getUsername());
            props.setProperty("password", container.getPassword());
            conn = DriverManager.getConnection(jdbcUrl, props);
    }

    public List<ResourcePojo> getAllRecords(String tableName) throws SQLException {
        PreparedStatement ps = conn.prepareStatement("select * from "+tableName);
        ResultSet rs=ps.executeQuery();
        return ResourceMapper.mapResultToResourceList(rs);
    }
}
