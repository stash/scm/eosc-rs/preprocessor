package pl.psnc.eosc.rs.preprocessor.job.psql;

import org.apache.spark.api.java.function.ForeachPartitionFunction;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.util.CollectionAccumulator;
import pl.psnc.eosc.rs.preprocessor.shared.ResourceType;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.Iterator;
import java.util.Properties;

public class SqlDeleteByPartition {

    static String deleteQuery(String table) {
        return "DELETE FROM " + table + "  WHERE resource_type=? AND resource_id=?";
    }

    static void completeDeleteStatement(PreparedStatement ps, Short resourceType, String resourceId) throws SQLException {
        ps.setShort(1, resourceType);
        ps.setString(2, resourceId);
    }

//    public static Integer deleteIds(Dataset<Row> jdbcDS, ResourceType resourceType) {
//        SparkSession sparkSession = SparkSession.getDefaultSession().get();
//        CollectionAccumulator<Integer> acc = sparkSession.sparkContext().collectionAccumulator("deleteCounter");
//        Properties props = new Properties();
//        String jdbcUrl = sparkSession.sparkContext().getConf().get("spark.db.url");
//        String table = sparkSession.sparkContext().getConf().get("spark.db.table");
//        props.setProperty("user", sparkSession.sparkContext().getConf().get("spark.db.user"));
//        props.setProperty("password", sparkSession.sparkContext().getConf().get("spark.db.password"));
//        Integer batchSize = Integer.valueOf(sparkSession.sparkContext().getConf().get("spark.db.batch.size"));
//        jdbcDS.foreachPartition((ForeachPartitionFunction<Row>) iterator -> {
//            Connection conn = DriverManager.getConnection(jdbcUrl, props);
//            conn.setAutoCommit(false);
//            String sqlQuery = "DELETE FROM " + table + "  WHERE resource_type=? AND resource_id=?";
//
//            PreparedStatement preparedStatement = conn.prepareStatement(sqlQuery);
//            Integer rowCounter = 0;
//            for (Iterator<Row> it = iterator; it.hasNext(); ) {
//                rowCounter++;
//                Row row = it.next();
//                preparedStatement.setShort(1, resourceType.getId()); //resourceType
//                preparedStatement.setString(2, row.getAs("id")); //resourceId
//                preparedStatement.addBatch();
//                if (rowCounter >= batchSize) {
//                    int[] result = preparedStatement.executeBatch();
//                    acc.add(Arrays.stream(result).filter(p->p>0).sum());
//                    preparedStatement.clearBatch();
//                    rowCounter = 0;
//                }
//            }
//            if(rowCounter>0){
//                int[] result = preparedStatement.executeBatch();
//                acc.add(Arrays.stream(result).filter(p->p>0).sum());
//                preparedStatement.clearBatch();
//            }
//            conn.commit();
//            conn.close();
//        });
//        return acc.value().stream().reduce(0, Integer::sum);
//    }
}
