package pl.psnc.eosc.rs.preprocessor.job.psql;

import org.apache.spark.api.java.function.ForeachPartitionFunction;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.util.CollectionAccumulator;
import pl.psnc.eosc.rs.preprocessor.shared.ResourceType;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.Iterator;
import java.util.Properties;

import static pl.psnc.eosc.rs.preprocessor.job.psql.SqlDeleteByPartition.completeDeleteStatement;
import static pl.psnc.eosc.rs.preprocessor.job.psql.SqlDeleteByPartition.deleteQuery;
import static pl.psnc.eosc.rs.preprocessor.job.psql.SqlInsertByPartition.completeInsertStatement;
import static pl.psnc.eosc.rs.preprocessor.job.psql.SqlInsertByPartition.insertQuery;
import static pl.psnc.eosc.rs.preprocessor.job.psql.SqlUpdateByPartition.completeUpdateStatement;
import static pl.psnc.eosc.rs.preprocessor.job.psql.SqlUpdateByPartition.updateQuery;

public class SqlByPartition {

    public enum Type {
        INSERT, UPDATE, DELETE
    }

    private static Connection configureConnection() throws SQLException {
        SparkSession sparkSession = SparkSession.getDefaultSession().get();
        Properties props = new Properties();
        String jdbcUrl = sparkSession.sparkContext().getConf().get("spark.db.url");
        props.setProperty("user", sparkSession.sparkContext().getConf().get("spark.db.user"));
        props.setProperty("password", sparkSession.sparkContext().getConf().get("spark.db.password"));
        Connection conn = DriverManager.getConnection(jdbcUrl, props);
        conn.setAutoCommit(false);
        return conn;
    }

    private static PreparedStatement createPreparedStatement(Connection conn, Type type) throws SQLException {
        SparkSession sparkSession = SparkSession.getDefaultSession().get();
        String table = sparkSession.sparkContext().getConf().get("spark.db.table");
        String sqlQuery = null;
        switch(type){
            case INSERT:
                sqlQuery = insertQuery(table);
                break;
            case UPDATE:
                sqlQuery = updateQuery(table);
                break;
            case DELETE:
                sqlQuery = deleteQuery(table);
                break;
        }
        return conn.prepareStatement(sqlQuery);
    }

//    private static void completeDeleteStatement(PreparedStatement ps, Short resourceType, String resourceId) throws SQLException {
//        ps.setShort(1, resourceType);
//        ps.setString(2, resourceId);
//    }

//    private static void completeInsertStatement(PreparedStatement ps, Short resourceType, String resourceId, Integer visitCounter, Integer orderCounter) throws SQLException {
//        ps.setShort(1, resourceType);
//        ps.setString(2, resourceId);
//        ps.setInt(3, orderCounter); //orders
//        ps.setInt(4, visitCounter); //visits
//    }

//    private static void completeUpdateStatement(PreparedStatement ps, Short resourceType, String resourceId, Integer visitCounter, Integer orderCounter) throws SQLException {
//        ps.setInt(1, visitCounter); //visits
//        ps.setInt(2, orderCounter); //orders
//        ps.setShort(3, resourceType);
//        ps.setString(4, resourceId);
//    }

    public static Integer reflectChangesToDb(Dataset<Row> jdbcDS, ResourceType resourceType, Type type) {
        SparkSession sparkSession = SparkSession.getDefaultSession().get();
        CollectionAccumulator<Integer> acc = sparkSession.sparkContext().collectionAccumulator("deleteCounter");
        Integer batchSize = Integer.valueOf(sparkSession.sparkContext().getConf().get("spark.db.batch.size"));
        jdbcDS.foreachPartition((ForeachPartitionFunction<Row>) iterator -> {
            Connection conn = configureConnection();
            PreparedStatement preparedStatement = createPreparedStatement(conn,type);
            Integer rowCounter = 0;
            for (Iterator<Row> it = iterator; it.hasNext(); ) {
                rowCounter++;
                Row row = it.next();
                switch(type){
                    case INSERT:
                        completeInsertStatement(preparedStatement,resourceType.getId(),row.getAs("id"),
                                row.getAs("visitCounter"),row.getAs("orderCounter"));
                        break;
                    case UPDATE:
                        completeUpdateStatement(preparedStatement,resourceType.getId(),row.getAs("id"),
                                row.getAs("visitCounter"),row.getAs("orderCounter"));
                        break;
                    case DELETE:
                        completeDeleteStatement(preparedStatement,resourceType.getId(),row.getAs("id"));
                        break;
                }
                if (rowCounter >= batchSize) {
                    int[] result = preparedStatement.executeBatch();
                    acc.add(Arrays.stream(result).filter(p->p>0).sum());
                    preparedStatement.clearBatch();
                    rowCounter = 0;
                }
            }
            if(rowCounter>0){
                int[] result = preparedStatement.executeBatch();
                acc.add(Arrays.stream(result).filter(p->p>0).sum());
                preparedStatement.clearBatch();
            }
            conn.commit();
            conn.close();
        });
        return acc.value().stream().reduce(0, Integer::sum);
    }

//    public static Integer deleteIds(Dataset<Row> jdbcDS, ResourceType resourceType, String sqlQuery) {
//        SparkSession sparkSession = SparkSession.getDefaultSession().get();
//        CollectionAccumulator<Integer> acc = sparkSession.sparkContext().collectionAccumulator("deleteCounter");
//        Properties props = new Properties();
//        String jdbcUrl = sparkSession.sparkContext().getConf().get("spark.db.url");
//        String table = sparkSession.sparkContext().getConf().get("spark.db.table");
//        props.setProperty("user", sparkSession.sparkContext().getConf().get("spark.db.user"));
//        props.setProperty("password", sparkSession.sparkContext().getConf().get("spark.db.password"));
//        Integer batchSize = Integer.valueOf(sparkSession.sparkContext().getConf().get("spark.db.batch.size"));
//        jdbcDS.foreachPartition((ForeachPartitionFunction<Row>) iterator -> {
//            Connection conn = DriverManager.getConnection(jdbcUrl, props);
//            conn.setAutoCommit(false);
//            String sqlQuery = "DELETE FROM " + table + "  WHERE resource_type=? AND resource_id=?";
//
//            PreparedStatement preparedStatement = conn.prepareStatement(sqlQuery);
//            Integer rowCounter = 0;
//            for (Iterator<Row> it = iterator; it.hasNext(); ) {
//                rowCounter++;
//                Row row = it.next();
//                preparedStatement.setShort(1, resourceType.getId()); //resourceType
//                preparedStatement.setString(2, row.getAs("id")); //resourceId
//                preparedStatement.addBatch();
//                if (rowCounter >= batchSize) {
//                    int[] result = preparedStatement.executeBatch();
//                    acc.add(Arrays.stream(result).filter(p->p>0).sum());
//                    preparedStatement.clearBatch();
//                    rowCounter = 0;
//                }
//            }
//            if(rowCounter>0){
//                int[] result = preparedStatement.executeBatch();
//                acc.add(Arrays.stream(result).filter(p->p>0).sum());
//                preparedStatement.clearBatch();
//            }
//            conn.commit();
//            conn.close();
//        });
//        return acc.value().stream().reduce(0, Integer::sum);
//    }

}
