package pl.psnc.eosc.rs.preprocessor.job.psql;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import pl.psnc.eosc.rs.preprocessor.diff.CsvDigestReader;
import pl.psnc.eosc.rs.preprocessor.shared.ResourceType;
import scala.Option;

import static pl.psnc.eosc.rs.preprocessor.job.psql.SqlByPartition.reflectChangesToDb;

public class PsqlUpdateSparkJob {
    public static final String APP_NAME = "PsqlUpdate";

    public static void main(String[] args) {
        if(args.length != 2) throw new IllegalArgumentException(
                "Exactly 2 parameters required! [dumpDir, resourceType]");
        String dumpDir = args[0];
        ResourceType resourceType = ResourceType.valueOf(args[1].toUpperCase());

        Option<SparkSession> optionalSparkSession = SparkSession.getDefaultSession();
        if(!optionalSparkSession.isDefined()){
            final SparkConf sparkConf = new SparkConf()
                    .setAppName(APP_NAME);
            final JavaSparkContext javaSparkContext = new JavaSparkContext(sparkConf);
            SparkSession.builder()
                    .sparkContext(javaSparkContext.sc())
                    .appName(APP_NAME)
                    .getOrCreate();
        }

        updateDatabase(dumpDir,resourceType);
    }

    public static void updateDatabase(String dumpDir, ResourceType resourceType){
        String resourceDir = dumpDir+"/"+resourceType.getSubDir();
        System.out.println("Resource type: "+resourceType.getSubDir());
        System.out.println("Resource directory: "+resourceDir);
        Dataset<Row> addedIds = CsvDigestReader.readCsvAddedIdsStats(resourceDir);
        Integer addedCounter = reflectChangesToDb(addedIds, resourceType, SqlByPartition.Type.INSERT);
        System.out.println("Insert: "+addedCounter+" resources");
        Dataset<Row> deletedIds = CsvDigestReader.readCsvDeletedIds(resourceDir);
        Integer deletedCounter = reflectChangesToDb(deletedIds, resourceType, SqlByPartition.Type.DELETE);
        System.out.println("Delete: "+deletedCounter+" resources");
        Dataset<Row> updateCounterIds = CsvDigestReader.readCsvUpdateCounterIdsStats(resourceDir);
        Integer updatedCounter = reflectChangesToDb(updateCounterIds, resourceType, SqlByPartition.Type.UPDATE);
        System.out.println("Update: "+updatedCounter+" resources");
    }
}