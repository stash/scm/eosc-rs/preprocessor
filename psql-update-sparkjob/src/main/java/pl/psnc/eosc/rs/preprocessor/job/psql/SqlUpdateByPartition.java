package pl.psnc.eosc.rs.preprocessor.job.psql;

import java.sql.PreparedStatement;
import java.sql.SQLException;

public class SqlUpdateByPartition {
    
    static String updateQuery(String table){
        return "UPDATE " + table + " SET " +
                "visits = visits + ?, " +
                "orders = orders + ? " +
                " WHERE resource_type=? AND resource_id=?";
    };

    static void completeUpdateStatement(PreparedStatement ps, Short resourceType, String resourceId, Integer visitCounter, Integer orderCounter) throws SQLException {
        ps.setInt(1, visitCounter); //visits
        ps.setInt(2, orderCounter); //orders
        ps.setShort(3, resourceType);
        ps.setString(4, resourceId);
    }
}
