#!/usr/bin/env bash
set -e

echo "INFO: Loading configuration from: settings.sh"
. settings.sh

mvn clean package spring-boot:start

sleep 2
BAR="*************************************"
echo -e "\n${BAR}\n\nThe Preprocessor is running.\n\n${BAR}\n"

