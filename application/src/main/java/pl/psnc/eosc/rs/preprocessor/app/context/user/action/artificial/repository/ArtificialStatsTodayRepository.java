package pl.psnc.eosc.rs.preprocessor.app.context.user.action.artificial.repository;

import org.springframework.stereotype.Repository;
import pl.psnc.eosc.rs.preprocessor.app.context.user.action.artificial.model.entity.ArtificialTodayStatsEntity;
import pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular.repository.AbstractStatsTodayRepository;

@Repository
public interface ArtificialStatsTodayRepository extends AbstractStatsTodayRepository<ArtificialTodayStatsEntity> { }
