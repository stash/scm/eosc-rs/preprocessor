package pl.psnc.eosc.rs.preprocessor.app.cloud.jms;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Profile;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;
import pl.psnc.eosc.rs.preprocessor.app.context.user.action.generator.GeneratorManagerImpl;

import java.util.ArrayList;
import java.util.List;

/**
 * The JMS consumer is used to receive messages created by User Action Generator {@link GeneratorManagerImpl}
 */
@Profile({"!jms-disabled & artificial-flow"})
@Slf4j
@Component
@Getter
public class ArtificialConsumer implements MessageBroadcaster {

    List<Subscriber> userActionSubscribers = new ArrayList<>();

    public void subscribe(Subscriber subscriber) {
        userActionSubscribers.add(subscriber);
    }

    @JmsListener(destination = "${jms.topic.artificialUserActions.name}", containerFactory = "artificialFactory")
    public void userA0(String message) {
        log.debug("Received artificial UA:\n"+message);
        try {
            userActionSubscribers.forEach(subscriber -> subscriber.processMessage(message));
        } catch (Exception e) {
            log.warn(e.getMessage());
        }
    }
}
