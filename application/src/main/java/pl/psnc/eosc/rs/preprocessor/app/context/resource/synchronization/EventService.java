package pl.psnc.eosc.rs.preprocessor.app.context.resource.synchronization;

import lombok.AllArgsConstructor;
import pl.psnc.eosc.rs.preprocessor.app.cloud.jms.MessageBroadcaster;
import pl.psnc.eosc.rs.preprocessor.app.cloud.jms.Subscriber;
import pl.psnc.eosc.rs.preprocessor.app.context.resource.converter.ResourceEvent;

import javax.annotation.PostConstruct;

/**
 * Abstract class for handling specific types of {@link ResourceEvent}.
 */
@AllArgsConstructor
public abstract class EventService implements Subscriber {

    MessageBroadcaster broadcaster;

    @PostConstruct
    protected void subscribeJms() {
        broadcaster.subscribe(this);
    }

    @Override
    public abstract void processMessage(String text);

    protected boolean isModifyEvent(ResourceEvent resourceEvent){
        return resourceEvent.getCud().equals("create") ||
                resourceEvent.getCud().equals("update");
    }

    protected boolean isRemoveEvent(ResourceEvent resourceEvent){
        return resourceEvent.getCud().equals("delete");
    }
}
