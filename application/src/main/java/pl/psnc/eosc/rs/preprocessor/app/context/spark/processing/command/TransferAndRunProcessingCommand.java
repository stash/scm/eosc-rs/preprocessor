package pl.psnc.eosc.rs.preprocessor.app.context.spark.processing.command;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.experimental.SuperBuilder;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.spark.launcher.SparkAppHandle;
import pl.psnc.eosc.rs.preprocessor.app.commons.spark.SparkJobLauncherAbstract;
import pl.psnc.eosc.rs.preprocessor.app.context.oag.job.S3toHdfsTransferJobLauncher;

import java.util.Map;

@Slf4j
@SuperBuilder
@EqualsAndHashCode
public class TransferAndRunProcessingCommand extends Command {

    String sourceDir;
    @Getter
    String destDir;
    @Getter
    String previousDumpDir;

    @Override
    public void execute(SparkJobLauncherAbstract sparkJobLauncherAbstract,
                        SparkAppHandle.Listener... listeners) {
        if (sparkJobLauncherAbstract instanceof S3toHdfsTransferJobLauncher) {
            if(StringUtils.isNotBlank(sourceDir) && StringUtils.isNotBlank(destDir)) {
                log.error("transfer and process s: "+sourceDir);
                ((S3toHdfsTransferJobLauncher)sparkJobLauncherAbstract)
                        .runAsync(Map.of(sourceDir,"preproc/input/"+destDir), listeners);
            }
        }
        else{
            //TODO throw exception
            log.error("Improper Spark job launcher.");
        }
    }
}