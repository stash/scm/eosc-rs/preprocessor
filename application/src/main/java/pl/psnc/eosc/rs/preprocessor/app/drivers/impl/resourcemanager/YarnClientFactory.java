package pl.psnc.eosc.rs.preprocessor.app.drivers.impl.resourcemanager;

import org.apache.hadoop.yarn.client.api.YarnClient;

public interface YarnClientFactory {

    YarnClient create();

}
