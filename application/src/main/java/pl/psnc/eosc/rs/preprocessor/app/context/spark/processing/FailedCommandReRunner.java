package pl.psnc.eosc.rs.preprocessor.app.context.spark.processing;

public interface FailedCommandReRunner {

    void reExecuteFailedCommands();

}
