package pl.psnc.eosc.rs.preprocessor.app.context.spark.processing.listener;

import lombok.extern.slf4j.Slf4j;
import org.apache.spark.launcher.SparkAppHandle;

/**
 * The listener logs Spark Jobs state changes.
 */
@Slf4j
public class SparkJobLoggingListener implements SparkAppHandle.Listener {


    @Override
    public void stateChanged(SparkAppHandle handle) {
        log.info("Spark Job state changed! Id: "+" : "+handle.getAppId()+" state: "+handle.getState());
        if(handle.getError().isPresent()){
            log.warn("Spark Job id: "+" : "+handle.getAppId()+" error: "+handle.getError().toString());
        }
    }

    @Override
    public void infoChanged(SparkAppHandle handle) {
        log.info("Spark Job info changed! Id: "+" : "+handle.getAppId()+" state: "+handle.getState());
        if(handle.getError().isPresent()){
            log.warn("Spark Job id: "+" : "+handle.getAppId()+" error: "+handle.getError().toString());
        }
    }
}
