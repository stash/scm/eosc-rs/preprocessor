package pl.psnc.eosc.rs.preprocessor.app.context.user.action.generator;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.annotation.Profile;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular.model.input.future.UserActionRaw;

import javax.annotation.PostConstruct;

/**
 * Generate User Actions and publish to JMS.
 * These User Actions will be processed by artificial flow.
 */
@Slf4j
@Profile("ua-generator")
@Component
public class GeneratorManagerImpl implements GeneratorManager{

    @Qualifier("randomUaExistingResourceGenerator")
    @Autowired
    UserActionGenerator userActionGenerator;

    @Qualifier("userActionsPublisherImpl")
    @Autowired
    UserActionsPublisher userActionsPublisher;


    @Scheduled(fixedDelay=1000)
    @Override
    public void generateAndPublishUA() {
        UserActionRaw generatedUserAction = userActionGenerator.generate();
        userActionsPublisher.publish(generatedUserAction);
    }
}
