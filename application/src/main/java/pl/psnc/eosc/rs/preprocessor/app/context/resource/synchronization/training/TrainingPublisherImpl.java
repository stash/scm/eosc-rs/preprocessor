package pl.psnc.eosc.rs.preprocessor.app.context.resource.synchronization.training;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import pl.psnc.eosc.rs.preprocessor.app.cloud.kafka.Producer;
import pl.psnc.eosc.rs.preprocessor.app.context.resource.synchronization.service.model.ServiceEvent;
import pl.psnc.eosc.rs.preprocessor.app.context.resource.synchronization.service.service.ServiceChangesPublisher;

@Component
public class TrainingPublisherImpl implements TrainingChangesPublisher {

    private final String topic;

    Producer producer;

    Gson gson = new GsonBuilder()
            .setDateFormat("yyyy-MM-dd hh:mm:ss.S")
            .create();

    @Autowired
    public TrainingPublisherImpl(Producer producer,
                                 @Value(value = "${sync.training.kafka-topic}") String topic){
        this.producer = producer;
        this.topic = topic;
    }


    @Override
    public void publish(TrainingEvent trainingEvent) {
        String json = gson.toJson(trainingEvent);
        producer.publish(topic,trainingEvent.getId(),json);
    }
}
