package pl.psnc.eosc.rs.preprocessor.app.context.user.action.generator;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.io.ClassPathResource;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

@Slf4j
@Data
public class LineReader {

    List<String> records = new ArrayList<>();

    public LineReader(String file)  {
        try (
                InputStream resource = new ClassPathResource(
                        "generator/"+file).getInputStream();
                BufferedReader reader = new BufferedReader(
                        new InputStreamReader(resource))) {

            reader.lines().forEach(line -> records.add(line));

        } catch (IOException e) {
            //TODO
            log.error(e.getMessage());
        }

    }
}
