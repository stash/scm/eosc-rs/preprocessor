package pl.psnc.eosc.rs.preprocessor.app.context.user.action.generator.model;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.UUID;

@Data
@AllArgsConstructor
public class GeneratedUserSession {
    String userId;
    UUID sessionId;
}
