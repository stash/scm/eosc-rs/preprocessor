package pl.psnc.eosc.rs.preprocessor.app.commons.error;

public class IncorrectMessageTypeException extends Exception {

    public IncorrectMessageTypeException() {
        super();
    }
    public IncorrectMessageTypeException(String errorMessage) {
        super(errorMessage);
    }
}
