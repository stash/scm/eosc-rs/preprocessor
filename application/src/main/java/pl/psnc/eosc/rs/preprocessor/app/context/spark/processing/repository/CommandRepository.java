package pl.psnc.eosc.rs.preprocessor.app.context.spark.processing.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.List;

@Repository
public interface CommandRepository extends JpaRepository<CommandEntity, Long> {

    List<CommandEntity> findByStatusIn(Collection<String> status);

    CommandEntity getReferenceById(Long id);
}
