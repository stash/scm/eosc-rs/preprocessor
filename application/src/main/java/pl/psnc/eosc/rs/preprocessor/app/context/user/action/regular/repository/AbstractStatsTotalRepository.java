package pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.NoRepositoryBean;
import pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular.model.statistics.database.ResourceKey;
import pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular.model.statistics.database.TotalStats;


@NoRepositoryBean
public interface AbstractStatsTotalRepository<T extends TotalStats> extends JpaRepository<T, ResourceKey> {}
