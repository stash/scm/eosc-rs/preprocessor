package pl.psnc.eosc.rs.preprocessor.app.context.user.action.generator;

import pl.psnc.eosc.rs.preprocessor.app.commons.metrics.RateTracer;
import pl.psnc.eosc.rs.preprocessor.app.context.user.action.generator.model.GeneratedResource;
import pl.psnc.eosc.rs.preprocessor.app.context.user.action.generator.model.GeneratedUserSession;
import pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular.model.UserActionType;
import pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular.model.input.future.*;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Random;
import java.util.UUID;

import static pl.psnc.eosc.rs.preprocessor.app.context.user.action.generator.Commons.randomEnum;


public class UserActionGeneratorImpl implements UserActionGenerator {

    ResourceProvider resourceProvider;

    SessionProvider sessionProvider;

    private static final Random random = new Random();

    public UserActionGeneratorImpl(ResourceProvider resourceProvider, SessionProvider sessionProvider) {
        this.resourceProvider = resourceProvider;
        this.sessionProvider = sessionProvider;
    }

    //    @DurationTracer(registryName = "generator")
//    @RateTracer(registryName = "generator")
    @Override
    public UserActionRaw generate() {
        long currentTime = System.currentTimeMillis();
        GeneratedUserSession userSession = sessionProvider.getUserSession();
        UserActionRaw generatedUA = new UserActionRaw(
                null,
                null,
                userSession.getUserId(),
                userSession.getSessionId(),
                "searchservice",
                LocalDateTime.ofInstant(Instant.ofEpochMilli(currentTime),ZoneId.of("UTC")),
                generateSource(),
                generateTarget(),
                generateAction()
        );
        return generatedUA;
    }

    private RootRaw generateRoot(){
        UserActionType userActionType = randomEnum(UserActionType.class);
        GeneratedResource resource = resourceProvider.getResource();
        return new RootRaw(
                userActionType.getType(),
                userActionType==UserActionType.RECOMMENDATION_PANEL?"v1":null,
                userActionType==UserActionType.SORTED?random.nextInt(10):null,
                resource.getResourceId(),
                resource.getResourceType());
    }

    private SourceRaw generateSource(){
        UserActionType userActionType = randomEnum(UserActionType.class);
        GeneratedResource resource = resourceProvider.getResource();
        return new SourceRaw(
                UUID.randomUUID(),
                "services_catalogue_list",
                generateRoot());
    }

    private TargetRaw generateTarget(){
        UserActionType userActionType = randomEnum(UserActionType.class);
        GeneratedResource resource = resourceProvider.getResource();
        return new TargetRaw(
                UUID.randomUUID(),
                "random_not_relevant");
    }

    private ActionRaw generateAction(){
        UserActionType userActionType = randomEnum(UserActionType.class);
        GeneratedResource resource = resourceProvider.getResource();
        return new ActionRaw(
                "button",
                "Details",
                random.nextBoolean());
    }
}
