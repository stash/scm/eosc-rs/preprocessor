package pl.psnc.eosc.rs.preprocessor.app.context.user.action.generator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import pl.psnc.eosc.rs.preprocessor.app.context.user.action.generator.model.GeneratedUserSession;

import java.util.Random;
import java.util.UUID;

public class RandomSessionProvider implements SessionProvider{

    Integer sessionAliveCounter = 0;

    Integer userAliveCounter = 0;

    String currentUserId;

    UUID currentSessionId;

    Random random = new Random();

    UserProvider userPicker;

    public RandomSessionProvider(UserProvider userPicker) {
        this.userPicker = userPicker;
    }

    @Override
    public GeneratedUserSession getUserSession() {
        if(sessionAliveCounter <= 0){
            generateNewSession();
        }
        if(userAliveCounter <= 0 && decideIfUserSignIn()){
            generateNewUser();
        } else {
            currentUserId = null;
        }
        sessionAliveCounter -= 1;
        userAliveCounter -= 1;
        return new GeneratedUserSession(currentUserId,currentSessionId);
    }

    boolean decideIfUserSignIn() {
        return random.nextInt(5) == 0; // 20% chance
    }

    void generateNewSession() {
        sessionAliveCounter = random.nextInt(9) + 1; // [1;10]
        currentSessionId = UUID.randomUUID();
    }

    void generateNewUser() {
        userAliveCounter = random.nextInt(9) + 1; // [1;10]
        currentUserId = null;
        if(random.nextInt(5) < 1) currentUserId = userPicker.pickUser(); // 20% chance for non-anonymous User
    }
}
