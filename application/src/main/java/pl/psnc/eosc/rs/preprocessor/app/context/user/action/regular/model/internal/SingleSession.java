package pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular.model.internal;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.UUID;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class SingleSession {

    @NotNull
    private UUID sessionId;

    private String marketplaceId;

    private String aaiUid;

    @NotNull
    @Valid
    private UserAction userAction;

}
