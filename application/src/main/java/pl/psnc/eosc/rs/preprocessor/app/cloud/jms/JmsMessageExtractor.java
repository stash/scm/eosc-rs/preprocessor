package pl.psnc.eosc.rs.preprocessor.app.cloud.jms;


import org.springframework.messaging.Message;
import pl.psnc.eosc.rs.preprocessor.app.commons.error.IncorrectMessageTypeException;

public class JmsMessageExtractor {

    public static String extractMessage(Message message) throws IncorrectMessageTypeException {
        if(message.getPayload() instanceof String){
            return (String) message.getPayload();
        } else
            if(message.getPayload() instanceof byte[]) {
            return new String((byte[]) message.getPayload());
        } else {
            throw new IncorrectMessageTypeException();
        }
    }

}
