package pl.psnc.eosc.rs.preprocessor.app.context.trainings.job;

import lombok.Getter;
import org.apache.spark.launcher.SparkAppHandle;
import org.apache.spark.launcher.SparkLauncher;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import pl.psnc.eosc.rs.preprocessor.app.commons.spark.ResourceType;
import pl.psnc.eosc.rs.preprocessor.app.commons.spark.SparkJobLaunchConfigProperties;
import pl.psnc.eosc.rs.preprocessor.app.commons.spark.SparkJobLauncherAbstract;
import pl.psnc.eosc.rs.preprocessor.app.commons.spark.SparkLauncherConfigProperties;

import java.util.Map;

@Service
public class TrainingsPreprocJobLauncher extends SparkJobLauncherAbstract {

    public enum TrainingResourceType implements ResourceType {
        TRAINING("training");

        @Getter
        String name;

        TrainingResourceType(String name){
            this.name = name;
        }
    }

    public TrainingsPreprocJobLauncher(SparkLauncherConfigProperties launcherConfig, @Qualifier("trainingsPreprocJobLaunchConfig") SparkJobLaunchConfigProperties jobConfig) {
        super(launcherConfig, jobConfig,
                Map.of("spark.model.path",jobConfig.getLanguagePredictionPipelinePath()));
    }

    @Override
    protected void postInit(SparkLauncher sparkLauncher) {

    }

    public void runAsync(String previousDumpDir, String inputFiles, String outputDirectory,  SparkAppHandle.Listener... listeners){
        this.launchAsync(new String[]{previousDumpDir,inputFiles,outputDirectory}, listeners);
    }

}
