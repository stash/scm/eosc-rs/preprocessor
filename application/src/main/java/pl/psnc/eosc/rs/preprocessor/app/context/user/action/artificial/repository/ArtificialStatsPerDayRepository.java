package pl.psnc.eosc.rs.preprocessor.app.context.user.action.artificial.repository;

import org.springframework.stereotype.Repository;
import pl.psnc.eosc.rs.preprocessor.app.context.user.action.artificial.model.entity.ArtificialDayStatsEntity;
import pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular.repository.AbstractStatsPerDayRepository;

@Repository
public interface ArtificialStatsPerDayRepository extends AbstractStatsPerDayRepository<ArtificialDayStatsEntity> { }
