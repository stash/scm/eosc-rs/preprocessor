package pl.psnc.eosc.rs.preprocessor.app.context.spark.processing.command;

import lombok.Data;
import lombok.experimental.SuperBuilder;
import lombok.extern.slf4j.Slf4j;
import org.apache.spark.launcher.SparkAppHandle;
import pl.psnc.eosc.rs.preprocessor.app.commons.spark.SparkJobLauncherAbstract;
import pl.psnc.eosc.rs.preprocessor.app.context.oag.job.OagResourcesPreprocJobLauncher;

@Slf4j
@SuperBuilder
@Data
public class OagProcessingCommand extends Command {

    String inputFiles;

    String outputDir;

    String previousDumpDir;

    @Override
    public void execute(SparkJobLauncherAbstract sparkJobLauncherAbstract,
                        SparkAppHandle.Listener... listeners) {
        if (sparkJobLauncherAbstract instanceof OagResourcesPreprocJobLauncher) {
            ((OagResourcesPreprocJobLauncher)sparkJobLauncherAbstract)
                    .runAsync(previousDumpDir,inputFiles,outputDir,listeners);
        }
        else{
            //TODO throw exception
            log.error("Improper Spark job launcher.");
        }
    }
}
