package pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular.model.input.future;

import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class RootRaw {

    private String type;

    @JsonProperty("panel_id")
    private String panelId;

    private Integer position;

    @JsonProperty("resource_id")
    @JsonAlias("service_id")
    private String resourceId;

    @JsonProperty("resource_type")
    private String resourceType;

}
