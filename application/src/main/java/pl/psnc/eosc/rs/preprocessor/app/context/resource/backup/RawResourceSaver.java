package pl.psnc.eosc.rs.preprocessor.app.context.resource.backup;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;
import pl.psnc.eosc.rs.preprocessor.app.cloud.jms.MessageBroadcaster;
import pl.psnc.eosc.rs.preprocessor.app.context.user.action.backup.BaseBackup;

import javax.annotation.PostConstruct;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.Validator;
import java.time.Instant;
import java.util.Set;

/**
 * Save and validate (raw - non-deserialized string only) all messages from Marketplace database.
 */
@Slf4j
@Profile("backup-raw-jms")
@Component
public class RawResourceSaver implements BaseBackup {

    @Autowired
    RawResourceRepository rawResourceRepository;

    @Autowired
    @Qualifier("resourceEventBroadcaster")
    MessageBroadcaster broadcaster;

    @Autowired
    Validator validator;

    @PostConstruct
    public void subscribeJms() {
        broadcaster.subscribe(this);
    }

    @Override
    public void processMessage(String rawMessage) {
        log.debug("Backing up raw resource");
        save(rawMessage);
    }

    @Override
    public void save(String resourceEvent) {
        ResourceEventBackupEntity resourceEventEntity = new ResourceEventBackupEntity(Instant.now().getEpochSecond(),resourceEvent);
        validate(resourceEventEntity);
        rawResourceRepository.save(resourceEventEntity);
    }

    private void validate(ResourceEventBackupEntity resourceEventEntity){
        Set<ConstraintViolation<ResourceEventBackupEntity>> violations = validator.validate(resourceEventEntity);
        if (!violations.isEmpty()) {
            StringBuilder sb = new StringBuilder();
            for (var constraintViolation : violations) {
                sb.append(constraintViolation.getMessage());
            }
            throw new ConstraintViolationException("Error occurred: " + sb, violations);
        }
    }
}
