package pl.psnc.eosc.rs.preprocessor.app.commons.spark;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum ExtendedResourceType implements ResourceType{
    TRAINING("training");

    String name;

}
