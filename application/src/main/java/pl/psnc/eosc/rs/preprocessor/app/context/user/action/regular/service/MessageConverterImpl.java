package pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.json.JsonMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular.model.input.future.RootRaw;
import pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular.model.input.future.UserActionRaw;
import pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular.model.internal.Root;
import pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular.model.internal.SingleSession;
import pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular.model.internal.UserAction;

import java.util.UUID;

import static pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular.model.PageMapper.mapPageId;

@Slf4j
@Component
public class MessageConverterImpl implements MessageConverter {

    ObjectMapper mapper = JsonMapper.builder()
            .addModule(new JavaTimeModule())
            .build();

    @Override
    public SingleSession messageToSingleSession(String userActionJson) {
        UserActionRaw userActionRaw;
        try {
            userActionRaw = mapper.readValue(userActionJson, UserActionRaw.class);
        } catch (JsonProcessingException e) {
            log.error("Error occurred while converting JSON message "+e.getMessage());
            return null;
        }
        UserAction userAction = toUserAction(userActionRaw);
        UUID uniqueId = userActionRaw.getUniqueId();
        String marketplaceId = userActionRaw.getMarketplaceId();
        String aaiUid = userActionRaw.getAaiUid();
        return new SingleSession(
                uniqueId,marketplaceId,aaiUid,userAction);
    }

    public UserAction toUserAction(UserActionRaw userActionRaw) {
        String resourceId=userActionRaw.getSource().getRoot().getResourceId();
        if(resourceId==null){
            Integer serviceId = mapPageId(userActionRaw.getTarget().getPage_id());
            if(serviceId!=null)
                resourceId=serviceId.toString();

        }
        String resourceType = userActionRaw.getSource().getRoot().getResourceType();
        if(resourceId!=null &&
                resourceType==null)
            resourceType = "service";
        return new UserAction(
                userActionRaw.getReceiptTimestamp(),
                userActionRaw.getTimestamp(),
                userActionRaw.getSource().getPage_id(),
                resourceId,
                resourceType,
                userActionRaw.getTarget().getPage_id(),
                userActionRaw.getClientId(),
                userActionRaw.getAction().getType(),
                userActionRaw.getAction().getText(),
                userActionRaw.getAction().getOrder(),
                extractRoot(userActionRaw.getSource().getRoot()));
    }

    private static Root extractRoot(RootRaw root) {
        if(root==null)
            return null;
        return new Root(
                root.getType(),
                root.getPanelId(),
                root.getPosition()
        );
    }
}
