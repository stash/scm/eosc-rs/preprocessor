package pl.psnc.eosc.rs.preprocessor.app.cloud.jms;

import pl.psnc.eosc.rs.preprocessor.app.context.user.action.generator.ResourceEnum;

public interface ProviderComponentResourceSubscriber {

    enum OperationType {
        CREATE,
        UPDATE,
        DELETE
    }
    void processMessage(String text, OperationType operationType, ResourceEnum resourceType);
}
