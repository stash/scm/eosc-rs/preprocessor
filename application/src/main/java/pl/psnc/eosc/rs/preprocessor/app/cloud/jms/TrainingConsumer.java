package pl.psnc.eosc.rs.preprocessor.app.cloud.jms;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Profile;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.messaging.Message;
import org.springframework.messaging.handler.annotation.Headers;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;
import pl.psnc.eosc.rs.preprocessor.app.context.user.action.generator.ResourceEnum;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * The JMS consumer is used to receive messages regarding changes to trainings.
 */
@Profile("!jms-disabled")
@Primary
@Slf4j
@Component("trainingEventBroadcaster")
@Getter
public class TrainingConsumer implements ProviderComponentMessageBroadcaster {

    List<ProviderComponentResourceSubscriber> trainingEventSubscribers = new ArrayList<>();

    public void subscribe(ProviderComponentResourceSubscriber subscriber) {
        trainingEventSubscribers.add(subscriber);
    }

    @JmsListener(destination = "training_resource.create", containerFactory = "defaultFactory")
    public void trainingPublisherCreate(@Payload final Message message, @Headers final Map<String, Object> headers) {
        try{
            String messagePayload = JmsMessageExtractor.extractMessage(message);
            log.debug("Received training CREATE:\n"+messagePayload);
            trainingEventSubscribers.forEach(subscriber -> subscriber.processMessage(messagePayload,
                    ProviderComponentResourceSubscriber.OperationType.CREATE, ResourceEnum.TRAINING));
        } catch (Exception e) {
            log.warn(e.getMessage());
        }
    }

    @JmsListener(destination = "training_resource.update", containerFactory = "defaultFactory")
    public void trainingPublisherUpdate(@Payload final Message message, @Headers final Map<String, Object> headers) {
        try{
            String messagePayload = JmsMessageExtractor.extractMessage(message);
            log.debug("Received training UPDATE:\n"+messagePayload);
            trainingEventSubscribers.forEach(subscriber -> subscriber.processMessage(messagePayload,
                    ProviderComponentResourceSubscriber.OperationType.UPDATE,ResourceEnum.TRAINING));
        } catch (Exception e) {
            log.warn(e.getMessage());
        }
    }

    @JmsListener(destination = "training_resource.delete", containerFactory = "defaultFactory")
    public void trainingPublisherDelete(@Payload final Message message, @Headers final Map<String, Object> headers) {
        try{
            String messagePayload = JmsMessageExtractor.extractMessage(message);
            log.debug("Received training DELETE:\n"+messagePayload);
            trainingEventSubscribers.forEach(subscriber -> subscriber.processMessage(messagePayload,
                    ProviderComponentResourceSubscriber.OperationType.DELETE,ResourceEnum.TRAINING));
        } catch (Exception e) {
            log.warn(e.getMessage());
        }
    }
}
