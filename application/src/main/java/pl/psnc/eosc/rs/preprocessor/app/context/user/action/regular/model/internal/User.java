package pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular.model.internal;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.util.UUID;

@Builder
@Data
@AllArgsConstructor
public class User {

    private UUID sessionId;

    private String marketplaceId;

    private String aaiUid;

}
