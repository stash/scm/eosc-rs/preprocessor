package pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular.model.input.current;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.UUID;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserActionRaw {

    private long receiptTimestamp = System.currentTimeMillis();

    @JsonProperty("id")
    private String id;

    @JsonProperty("user_id")
    private String user;

    @JsonProperty("unique_id")
    private UUID unique_id;

    private LocalDateTime timestamp;

    private SourceRaw source;

    private TargetRaw target;

    private ActionRaw action;

}
