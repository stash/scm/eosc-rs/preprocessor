package pl.psnc.eosc.rs.preprocessor.app.commons.metrics;

import com.codahale.metrics.Meter;
import com.codahale.metrics.MetricRegistry;
import com.codahale.metrics.Timer;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression;
import org.springframework.stereotype.Component;

import java.lang.reflect.Method;

@Slf4j
@Aspect
@Component
@ConditionalOnExpression("${aspect.enabled:true}")
public class RateTracerAdvice {

    @Autowired
    MetricRegistry metricRegistry;

    @AfterReturning(pointcut = "@annotation(RateTracer)", returning = "object")
    private void log(JoinPoint joinPoint, Object object) {
        String registryName = getRegistryName(joinPoint);

        Meter meter = metricRegistry.meter(registryName);
        meter.mark();
    }

    private static String getRegistryName(JoinPoint joinPoint) {
        MethodSignature signature = (MethodSignature) joinPoint.getSignature();
        Method method = signature.getMethod();
        RateTracer aspectDurationTracer = method.getAnnotation(RateTracer.class);
        return aspectDurationTracer.registryName();
    }
}