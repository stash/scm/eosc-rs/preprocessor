package pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular.model.internal;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular.model.statistics.database.ResourceKey;

import java.util.Objects;

@Getter
@Setter
@AllArgsConstructor
public class Resource {

    private Short resourceType;

    private String resourceId;

    public ResourceKey toResourceKey(){
        return new ResourceKey(resourceType,resourceId);
    }

    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Resource))
            return false;
        Resource r = (Resource)other;
        return resourceId.equals(r.resourceId) && Objects.equals(resourceType, r.resourceType);
    }

    @Override
    public int hashCode() {
        return 31*(31*resourceId.hashCode()+resourceType.hashCode());
    }
}
