package pl.psnc.eosc.rs.preprocessor.app.drivers.api.searchservice;

import java.util.List;

public interface SearchServiceDriver {

    List<Dump> listDumps();

}
