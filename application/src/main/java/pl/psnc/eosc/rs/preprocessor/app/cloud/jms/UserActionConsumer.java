package pl.psnc.eosc.rs.preprocessor.app.cloud.jms;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Profile;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;
import pl.psnc.eosc.rs.preprocessor.app.commons.metrics.DurationTracer;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static com.google.common.base.Throwables.getStackTraceAsString;

/**
 * The JMS consumer is used to receive User Actions.
 */
@Profile("!jms-disabled")
@Slf4j
@Component("userActionBroadcaster")
@Getter
public class UserActionConsumer implements MessageBroadcaster {

    List<Subscriber> userActionSubscribers = new ArrayList<>();

    public void subscribe(Subscriber subscriber) {
        userActionSubscribers.add(subscriber);
    }

//    @RateTracer(registryName = "userAction")
//    @DurationTracer(registryName = "userAction")
    @JmsListener(destination = "${jms.topic.userActions.name}", containerFactory = "defaultFactory")
    public void userActionPublisher(@Payload final String message) {
        String messagePayload = message;
        // Transform to valid JSON
        if(messagePayload.matches("^\".*")) {
            messagePayload = messagePayload.replaceAll("\\\\", "");
            messagePayload = messagePayload.substring(1, messagePayload.length() - 1);
        }
        log.debug("Received regular UA:\n"+messagePayload);
        String finalMessageText = messagePayload;
        try{
            userActionSubscribers.forEach(subscriber -> subscriber.processMessage(finalMessageText));
        } catch (Exception e) {
            log.warn(e.getMessage());
        }
    }
}
