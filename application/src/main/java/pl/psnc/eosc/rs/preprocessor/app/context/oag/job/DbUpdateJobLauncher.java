package pl.psnc.eosc.rs.preprocessor.app.context.oag.job;

import org.apache.spark.launcher.SparkAppHandle;
import org.apache.spark.launcher.SparkLauncher;
import org.springframework.stereotype.Service;
import pl.psnc.eosc.rs.preprocessor.app.commons.spark.SparkJobLauncherAbstract;
import pl.psnc.eosc.rs.preprocessor.app.commons.spark.SparkLauncherConfigProperties;

import java.util.Map;

@Service
public class DbUpdateJobLauncher extends SparkJobLauncherAbstract {

    public DbUpdateJobLauncher(SparkLauncherConfigProperties launcherConfig, DbUpdateJobConfig jobConfig) {
        super(launcherConfig, jobConfig,
                Map.of("spark.db.user",jobConfig.getUser(),
                        "spark.db.password",jobConfig.getPassword(),
                        "spark.db.url",jobConfig.getServerUrl(),
                        "spark.db.table",jobConfig.getTableName(),
                        "spark.db.batch.size",jobConfig.getBatchSize()));
    }

    @Override
    protected void postInit(SparkLauncher sparkLauncher) {

    }

    public void runAsync(String dumpDir, String resourceType, SparkAppHandle.Listener... listeners){
        this.launchAsync(new String[]{dumpDir,resourceType}, listeners);
    }
}
