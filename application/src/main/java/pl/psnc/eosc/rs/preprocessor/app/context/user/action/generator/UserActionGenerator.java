package pl.psnc.eosc.rs.preprocessor.app.context.user.action.generator;

import pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular.model.input.future.UserActionRaw;

public interface UserActionGenerator {

    UserActionRaw generate();
}
