package pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular.model.input.current;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class RootRaw {

    private String type;

    private String service_id;

    private String panel_id;

}
