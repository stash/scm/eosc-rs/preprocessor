package pl.psnc.eosc.rs.preprocessor.app.commons.spark;

public interface ResourceType {

    String getName();
}
