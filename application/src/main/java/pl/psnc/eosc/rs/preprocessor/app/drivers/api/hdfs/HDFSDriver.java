package pl.psnc.eosc.rs.preprocessor.app.drivers.api.hdfs;

import org.apache.hadoop.fs.Path;

import java.util.List;

public interface HDFSDriver {

    List<HdfsFileStatus> listFiles(Path path);
    boolean delete(Path path, boolean recursive);

}
