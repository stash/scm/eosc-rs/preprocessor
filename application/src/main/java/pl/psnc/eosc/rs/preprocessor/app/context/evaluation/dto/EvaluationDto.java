package pl.psnc.eosc.rs.preprocessor.app.context.evaluation.dto;

import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import java.util.UUID;

@Slf4j
@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class EvaluationDto {

    @JsonIgnore
    private Long receiptTimestamp = System.currentTimeMillis();
    @JsonAlias("aai_uid")
    private String aaiUid;
    @JsonAlias("unique_id")
    private UUID uniqueId;
    @JsonAlias("client_id")
    private String clientId;
    @JsonAlias("timestamp")
    private LocalDateTime timestamp;
    @JsonProperty(value="actionType",access=JsonProperty.Access.READ_ONLY)
    private String actionType;
    @JsonProperty(value="visitId",access=JsonProperty.Access.READ_ONLY)
    private String visitId;
    @JsonProperty(value="resourceType",access=JsonProperty.Access.READ_ONLY)
    private String resourceType;
    @JsonProperty(value="resourceId",access=JsonProperty.Access.READ_ONLY)
    private String resourceId;
    @JsonProperty(value="suggestion",access=JsonProperty.Access.READ_ONLY)
    private String suggestion;
    @JsonProperty(value="action",access=JsonProperty.Access.READ_ONLY)
    private String action;
    @JsonProperty(value="reason",access=JsonProperty.Access.READ_ONLY)
    private List<String> reason;

    @JsonProperty("action")
    private void unpackActionTypeFromNestedObject(Map<String, String> action) {
        actionType = action.get("type");
    }

    @JsonProperty("source")
    private void unpackSourceFromNestedObject(Map<String, Object> source) {

        visitId = (String) source.get("visit_id");
        resourceType = (String) source.get("resource_type");
        resourceId = (String) source.get("resource_id");
        suggestion = (String) source.get("suggestion");
        action = (String) source.get("action");
        reason = (List<String>) source.get("reason");

    }

    public String toJson(){
        ObjectMapper mapper = new ObjectMapper();
        mapper.registerModule(new JavaTimeModule());
        mapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
        String json = null;
        try {
            json = mapper.writeValueAsString(this);
        } catch (JsonProcessingException e) {
            log.error("Error occurred while converting JSON message "+e.getMessage());
        }
        return json;
    }
    
}
