package pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular.model.internal;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserAction {

    @NotNull
    private Long receiptTimestamp;

    @NotNull
    private LocalDateTime timestamp;

    @NotBlank
    private String sourcePageId;

    @NotBlank
    private String resourceId;

    @NotBlank
    private String resourceType;

    @NotBlank
    private String targetPageId;

    private String clientId;

    @NotBlank
    private String actionType;

    private String actionText;

    @NotNull
    private Boolean isOrderAction;

    @NotNull
    @Valid
    private Root root;

}