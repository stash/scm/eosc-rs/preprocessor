package pl.psnc.eosc.rs.preprocessor.app.diagnostic;

import org.apache.kafka.clients.admin.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.actuate.health.AbstractHealthIndicator;
import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.actuate.health.HealthIndicator;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.kafka.core.KafkaAdmin;

import java.util.Set;

import static java.util.concurrent.TimeUnit.MILLISECONDS;

@Configuration
@Profile("!test & !integration")
public class KafkaHealthIndicatorConfig {

    @Autowired
    private KafkaAdmin kafkaAdmin;

    @Bean
    public AdminClient kafkaAdminClient() {
        return AdminClient.create(kafkaAdmin.getConfigurationProperties());
    }

    @Bean
    public HealthIndicator kafkaHealthIndicator(AdminClient kafkaAdminClient) {
        final DescribeClusterOptions options = new DescribeClusterOptions()
                .timeoutMs(1000);

        return new AbstractHealthIndicator() {
            @Override
            protected void doHealthCheck(Health.Builder builder) throws Exception {
                DescribeClusterResult clusterDescription = kafkaAdminClient.describeCluster(options);
                clusterDescription.clusterId().get();

                Set<String> topicsSet = kafkaAdminClient.listTopics(new ListTopicsOptions().timeoutMs(5000)).names().get(5000, MILLISECONDS);

                builder.up()
                        .withDetail("topics", topicsSet)
                        .build();
            }
        };
    }
}
