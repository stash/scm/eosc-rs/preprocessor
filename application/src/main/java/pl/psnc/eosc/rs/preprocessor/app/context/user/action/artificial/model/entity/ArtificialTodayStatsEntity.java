package pl.psnc.eosc.rs.preprocessor.app.context.user.action.artificial.model.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular.model.statistics.database.TodayStats;
import pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular.model.statistics.database.UserResourceDayKey;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name="artificial.artificial_today_table")
@NoArgsConstructor
@Getter
public class ArtificialTodayStatsEntity extends TodayStats {
    public ArtificialTodayStatsEntity(UserResourceDayKey id, Boolean visited, Boolean ordered) {
        super(id, visited, ordered);
    }
}