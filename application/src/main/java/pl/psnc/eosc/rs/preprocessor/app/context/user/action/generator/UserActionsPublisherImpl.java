package pl.psnc.eosc.rs.preprocessor.app.context.user.action.generator;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;
import pl.psnc.eosc.rs.preprocessor.app.cloud.jms.JmsProducer;
import pl.psnc.eosc.rs.preprocessor.app.cloud.jms.JmsTopicConfig;
import pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular.model.input.future.UserActionRaw;

@Slf4j
@Profile("artificial-flow")
@Component
public class UserActionsPublisherImpl implements UserActionsPublisher{

    @Autowired
    JmsProducer jmsProducer;

    @Autowired
    JmsTopicConfig jmsTopic;

    @Override
    public void publish(UserActionRaw userAction) {
        String  producedMessage = userAction.toJson();
        jmsProducer.sendMessage(jmsTopic.getArtificialUserActions(),producedMessage);
        log.debug("Publishing to "+jmsTopic.getArtificialUserActions().getName()+" msg: " + producedMessage);
    }
}
