package pl.psnc.eosc.rs.preprocessor.app.cloud.jms;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Profile;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.messaging.Message;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;
import pl.psnc.eosc.rs.preprocessor.app.commons.error.IncorrectMessageTypeException;

import java.util.ArrayList;
import java.util.List;

import static com.google.common.base.Throwables.getStackTraceAsString;

/**
 * The JMS consumer is used to receive messages regarding recommendation evaluation.
 */
@Profile("!jms-disabled")
@Slf4j
@Getter
@Component("evaluationEventBroadcaster")
public class EvaluationConsumer implements MessageBroadcaster {

    List<Subscriber> evaluationEventSubscribers = new ArrayList<>();

    public void subscribe(Subscriber subscriber) {
        evaluationEventSubscribers.add(subscriber);
    }

    @JmsListener(destination = "recommendations_evaluation", containerFactory = "defaultFactory")
    public void evaluationConsumer(@Payload final Message message) {
        try{
            String messagePayload = JmsMessageExtractor.extractMessage(message);
            log.debug("Received recommendation evaluation:\n"+messagePayload);
            evaluationEventSubscribers.forEach(subscriber -> subscriber.processMessage(messagePayload));
        } catch (Exception e) {
            log.warn(e.getMessage());
        }
    }
}
