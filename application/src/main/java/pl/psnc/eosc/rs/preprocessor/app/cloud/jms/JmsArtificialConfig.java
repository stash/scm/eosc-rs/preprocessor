package pl.psnc.eosc.rs.preprocessor.app.cloud.jms;

import org.fusesource.stomp.jms.StompJmsConnectionFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.jms.DefaultJmsListenerContainerFactoryConfigurer;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.jms.annotation.EnableJms;
import org.springframework.jms.config.DefaultJmsListenerContainerFactory;
import org.springframework.jms.config.JmsListenerContainerFactory;
import org.springframework.jms.core.JmsTemplate;

import javax.jms.ConnectionFactory;

/**
 * Configuration for artificial JMS Listener.
 */
@Profile("!integration & !test & !jms-disabled & artificial-flow")
@Configuration
@EnableJms
@EnableConfigurationProperties(value = JmsTopicConfig.class)
public class JmsArtificialConfig {

    @Value("${artificial.stomp.address}")
    private String artificialBrokerUrl;

    @Value(value = "${artificial.stomp.username}")
    private String artificialUsername;

    @Value(value = "${artificial.stomp.password}")
    private String artificialPassword;

    @Bean
    public ConnectionFactory artificialConnectionFactory() {
        StompJmsConnectionFactory connFactory = new StompJmsConnectionFactory();
        connFactory.setBrokerURI(artificialBrokerUrl);
        connFactory.setUsername(artificialUsername);
        connFactory.setPassword(artificialPassword);
        return connFactory;
    }

    @Bean
    public JmsTemplate artificialJmsTemplate() {
        JmsTemplate jmsTemplate = new JmsTemplate();
        jmsTemplate.setConnectionFactory(artificialConnectionFactory());
        return jmsTemplate;
    }

    @Bean
    public JmsListenerContainerFactory<?> artificialFactory(
            @Qualifier("artificialConnectionFactory") ConnectionFactory connectionFactory,
            DefaultJmsListenerContainerFactoryConfigurer configurer) {
        DefaultJmsListenerContainerFactory factory = new DefaultJmsListenerContainerFactory();
        configurer.configure(factory, connectionFactory);
        return factory;
    }
}
