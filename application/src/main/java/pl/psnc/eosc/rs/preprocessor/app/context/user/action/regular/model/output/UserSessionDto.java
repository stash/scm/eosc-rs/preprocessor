package pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular.model.output;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserSessionDto {

    private String Id;

    private Boolean isAnonymous;

    private SessionDto session;
}