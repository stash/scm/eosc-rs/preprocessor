package pl.psnc.eosc.rs.preprocessor.app.context.recommendation.entity;

import com.vladmihalcea.hibernate.type.array.ListArrayType;
import lombok.*;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.time.LocalDateTime;
import java.util.List;

@Entity
@Table(name="recommendation.recommendation_table")
@TypeDef(
        name = "list-array",
        typeClass = ListArrayType.class
)
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
public class RecommendationEntity {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    Long id;
    @Column(name="aai_uid")
    private String aaiUid;
    @Column(name="user_id")
    private String userId;
    @Column(name="unique_id")
    private String uniqueId;
    @Column(name="client_id")
    private String clientId;
    @Column
    private Long timestamp;
    @Column(name="panel_id")
    private String panelId;
    @Column(name="engine_version")
    private String engineVersion;
    @Type(type = "list-array")
    @Column(
            name="recommendations",
            columnDefinition = "text[]")
    private List<String> recommendations;
    @Column(name="raw_recommendation_event")
    String rawRecommendationEvent;
}
