package pl.psnc.eosc.rs.preprocessor.app.context.resource.synchronization.service.service;

import pl.psnc.eosc.rs.preprocessor.app.context.resource.synchronization.service.model.ServiceEvent;

public interface ServiceChangesPublisher {
    void publish(ServiceEvent service);
}
