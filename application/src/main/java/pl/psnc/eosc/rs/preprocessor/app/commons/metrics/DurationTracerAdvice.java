package pl.psnc.eosc.rs.preprocessor.app.commons.metrics;

import com.codahale.metrics.MetricRegistry;
import com.codahale.metrics.Timer;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression;
import org.springframework.stereotype.Component;

import java.lang.reflect.Method;

@Slf4j
@Aspect
@Component
@ConditionalOnExpression("${aspect.enabled:true}")
public class DurationTracerAdvice {

    @Autowired
    MetricRegistry metricRegistry;

    @Around("@annotation(DurationTracer)")
    public Object traceExecutionTime(ProceedingJoinPoint joinPoint) throws Throwable{
        String registryName = getRegistryName(joinPoint);
        final Timer.Context context = metricRegistry.timer(registryName).time();
        Object object = null;
        try {
            object = joinPoint.proceed();
        } finally {
            context.stop();
        }
        return object;
    }

    private static String getRegistryName(ProceedingJoinPoint joinPoint) {
        MethodSignature signature = (MethodSignature) joinPoint.getSignature();
        Method method = signature.getMethod();
        DurationTracer aspectDurationTracer = method.getAnnotation(DurationTracer.class);
        return aspectDurationTracer.registryName();
    }
}