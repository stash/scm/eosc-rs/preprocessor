package pl.psnc.eosc.rs.preprocessor.app.context.user.action.artificial.repository;

import org.springframework.stereotype.Repository;
import pl.psnc.eosc.rs.preprocessor.app.context.user.action.artificial.model.entity.ArtificialTotalStatsEntity;
import pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular.repository.AbstractStatsTotalRepository;

@Repository
public interface ArtificialStatsTotalRepository extends AbstractStatsTotalRepository<ArtificialTotalStatsEntity> {}
