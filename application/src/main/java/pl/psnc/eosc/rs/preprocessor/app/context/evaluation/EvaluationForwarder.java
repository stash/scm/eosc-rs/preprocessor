package pl.psnc.eosc.rs.preprocessor.app.context.evaluation;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.json.JsonMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import pl.psnc.eosc.rs.preprocessor.app.cloud.jms.JmsProducer;
import pl.psnc.eosc.rs.preprocessor.app.cloud.jms.JmsTopicConfig;
import pl.psnc.eosc.rs.preprocessor.app.cloud.jms.MessageBroadcaster;
import pl.psnc.eosc.rs.preprocessor.app.cloud.kafka.Producer;
import pl.psnc.eosc.rs.preprocessor.app.context.evaluation.dto.EvaluationDto;

import javax.annotation.PostConstruct;
import javax.validation.Validator;

@Slf4j
//@Profile("backup-raw-jms")
@Component
public class EvaluationForwarder implements EvaluationService {

    Producer producer;

    String topic;

    MessageBroadcaster broadcaster;

    Validator validator;

    ObjectMapper mapper = JsonMapper.builder()
            .addModule(new JavaTimeModule())
            .build();

    @Autowired
    public EvaluationForwarder(Producer producer,
                               @Value(value = "${recommendation.evaluation.kafka-topic}") String topic,
                               @Qualifier("evaluationEventBroadcaster") MessageBroadcaster broadcaster,
                               Validator validator) {
        this.producer = producer;
        this.topic = topic;
        this.broadcaster = broadcaster;
        this.validator = validator;
    }

    @PostConstruct
    public void subscribeJms() {
        broadcaster.subscribe(this);
    }

    @Override
    public void processMessage(String rawMessage) {
        handleMessage(rawMessage);
    }

    @Override
    public void handleMessage(String message) {
        try {
            EvaluationDto evaluationDto = mapper.readValue(message, EvaluationDto.class);
            forwardMessage(evaluationDto);
        } catch (JsonProcessingException e) {
            log.error("Error occurred while converting JSON message "+e.getMessage());
        }
    }

    private void forwardMessage(EvaluationDto evaluation){
        String  producedMessage = evaluation.toJson();
        producer.publish(topic,evaluation.getAaiUid(),producedMessage);
    }
}
