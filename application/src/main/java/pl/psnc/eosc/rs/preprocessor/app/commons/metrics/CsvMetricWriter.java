package pl.psnc.eosc.rs.preprocessor.app.commons.metrics;

import com.codahale.metrics.CsvReporter;
import com.codahale.metrics.MetricRegistry;

import java.io.File;
import java.util.concurrent.TimeUnit;

public class CsvMetricWriter {


    public static void add(MetricRegistry metricRegistry) {
        final File directory = new File("benchmark-csv/");
        directory.mkdirs();

        CsvReporter reporter = CsvReporter.forRegistry(metricRegistry)
                .convertRatesTo(TimeUnit.SECONDS)
                .convertDurationsTo(TimeUnit.MILLISECONDS)
                .withSeparator(";")
                .build(directory);
        reporter.start(1, TimeUnit.MINUTES);
    }
}
