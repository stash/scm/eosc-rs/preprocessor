package pl.psnc.eosc.rs.preprocessor.app.context.resource.synchronization.service.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@JsonIgnoreProperties(ignoreUnknown = true)
public class ServiceEvent {

    @JsonProperty(value="event_type",access=JsonProperty.Access.READ_ONLY)
    private String eventType;

    @JsonProperty("id")
    private String id;

    @JsonProperty("name")
    private String name;

    @JsonProperty("description")
    private String description;

    @JsonProperty("scientific_domains")
    private List<Integer> scientificDomainValues;

    @JsonProperty("categories")
    private List<Integer> categoryValues;

    @JsonProperty("related_services")
    private List<Integer> relatedServices;

    public ServiceEvent(String id, String name, String description, List<Integer> scientificDomainValues, List<Integer> categoryValues, List<Integer> relatedServices) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.scientificDomainValues = scientificDomainValues;
        this.categoryValues = categoryValues;
        this.relatedServices = relatedServices;
    }
}
