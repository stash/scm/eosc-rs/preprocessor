package pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular.service.user;

import pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular.model.internal.SingleSession;

public interface UserDataSupplier {
    void enrichUserProfile(SingleSession userAction);
}
