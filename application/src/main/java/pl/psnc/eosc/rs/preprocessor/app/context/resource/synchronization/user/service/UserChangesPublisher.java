package pl.psnc.eosc.rs.preprocessor.app.context.resource.synchronization.user.service;

import pl.psnc.eosc.rs.preprocessor.app.context.resource.synchronization.user.model.UserEvent;

public interface UserChangesPublisher {
    void publish(UserEvent user);
}
