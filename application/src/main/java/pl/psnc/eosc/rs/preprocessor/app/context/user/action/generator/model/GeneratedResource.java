package pl.psnc.eosc.rs.preprocessor.app.context.user.action.generator.model;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class GeneratedResource {

    private String resourceId;

    private String resourceType;

}
