package pl.psnc.eosc.rs.preprocessor.app.context.recommendation.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class RecommendationDto {

    @JsonIgnore
    private Long receiptTimestamp = System.currentTimeMillis();
    private String aaiUid;
    private String userId;
    private String uniqueId;
    private String clientId;
    private LocalDateTime timestamp;
    private String panelId;
    private String engineVersion;
    private List<String> recommendations;

    @JsonProperty("context")
    private void unpackContextFromNestedObject(Map<String, Object> source) {

        aaiUid = extractNotNullToString(source.get("aai_uid"));
        userId = extractNotNullToString(source.get("user_id"));
        uniqueId = extractNotNullToString(source.get("unique_id"));
        clientId = extractNotNullToString(source.get("client_id"));
        timestamp = LocalDateTime.parse(extractNotNullToString(source.get("timestamp")),DateTimeFormatter.ISO_DATE_TIME);
        panelId = extractNotNullToString(source.get("panel_id"));
        engineVersion = extractNotNullToString(source.get("engine_version"));
        if(source.get("recommendations")!=null) recommendations = extractRecommendations((List<Object>) source.get("recommendations"));
    }

    @JsonProperty("recommendations")
    private void unpackRecommendations(List<Object> source) {
        if(source!=null)
            recommendations = extractRecommendations((List<Object>) source);
    }

    @JsonProperty("response")
    private void unpackRecommendationsFromResponse(Map<String, Object> source) {
        if(source!=null && source.get("recommendations")!=null)
            recommendations = extractRecommendations((List<Object>) source.get("recommendations"));
    }

    private String extractNotNullToString(Object obj){
        if(obj!=null)
            if (obj instanceof Integer)
               return Integer.toString((Integer) obj);
            else if (obj instanceof String)
                return (String) obj;
        return null;
    }

    private List<String> extractRecommendations(List<Object> input){
        List<String> recommendations = new ArrayList<>();
        for (Object obj: input) {
            if (obj instanceof Integer)
                recommendations.add(Integer.toString((Integer) obj));
            else if (obj instanceof String)
                recommendations.add((String) obj);
        }
        return recommendations;
    }
}
