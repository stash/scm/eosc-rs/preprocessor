package pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular.model;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
public enum UserActionType {
    RECOMMENDATION_PANEL("recommendation_panel"),
    OTHER("other"),
    SORTED("sorted");

    @Getter
    private final String type;
}
