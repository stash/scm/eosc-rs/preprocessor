package pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular.service.session;

import pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular.model.internal.User;
import pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular.model.internal.UserAction;

public interface SessionManagerHandler {

    void addUserAction(User user, UserAction userAction);

}
