package pl.psnc.eosc.rs.preprocessor.app.context.marketplace;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import pl.psnc.eosc.rs.preprocessor.app.commons.spark.SparkJobLaunchConfigProperties;

@Configuration
@EnableConfigurationProperties
public class MarketplaceConfig {

    @Bean(name = "marketplaceResourcesPreprocJobLaunchConfig")
    @ConfigurationProperties(prefix = "spark.job.marketplace-resources")
    public SparkJobLaunchConfigProperties marketplaceResourcesPreprocJobLaunchConfig() {
        return new SparkJobLaunchConfigProperties();
    }

}
