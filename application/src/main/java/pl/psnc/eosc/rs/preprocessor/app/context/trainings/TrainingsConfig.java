package pl.psnc.eosc.rs.preprocessor.app.context.trainings;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import pl.psnc.eosc.rs.preprocessor.app.commons.spark.SparkJobLaunchConfigProperties;

@Configuration
@EnableConfigurationProperties
public class TrainingsConfig {

    @Bean(name = "trainingsPreprocJobLaunchConfig")
    @ConfigurationProperties(prefix = "spark.job.trainings")
    public SparkJobLaunchConfigProperties trainingsPreprocJobLaunchConfig() {
        return new SparkJobLaunchConfigProperties();
    }

}
