package pl.psnc.eosc.rs.preprocessor.app.context.spark.processing.command;

import lombok.Data;
import lombok.experimental.SuperBuilder;
import org.apache.spark.launcher.SparkAppHandle;
import pl.psnc.eosc.rs.preprocessor.app.commons.spark.ResourceType;
import pl.psnc.eosc.rs.preprocessor.app.commons.spark.SparkJobLauncherAbstract;

import java.io.Serializable;

@SuperBuilder
@Data
public abstract class Command implements Serializable {

    String processingId;

    SparkAppHandle.State status;

    ResourceType resourceType;

    public abstract void execute(SparkJobLauncherAbstract launcher, SparkAppHandle.Listener... listeners);

}
