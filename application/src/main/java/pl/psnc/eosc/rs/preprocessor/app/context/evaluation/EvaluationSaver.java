package pl.psnc.eosc.rs.preprocessor.app.context.evaluation;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import pl.psnc.eosc.rs.preprocessor.app.cloud.jms.MessageBroadcaster;
import pl.psnc.eosc.rs.preprocessor.app.context.evaluation.entity.EvaluationEntity;
import pl.psnc.eosc.rs.preprocessor.app.context.user.action.backup.BaseBackup;

import javax.annotation.PostConstruct;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.Validator;
import java.util.Set;
import java.time.Instant;

@Slf4j
//@Profile("backup-raw-jms")
@Component
public class EvaluationSaver implements BaseBackup {
    @Autowired
    EvaluationRepository evaluationRepository;

    @Autowired
    @Qualifier("evaluationEventBroadcaster")
    MessageBroadcaster broadcaster;

    @Autowired
    Validator validator;

    @PostConstruct
    public void subscribeJms() {
        broadcaster.subscribe(this);
    }

    @Override
    public void processMessage(String rawMessage) {
        save(rawMessage);
    }

    @Override
    public void save(String message) {
        EvaluationEntity evaluationEntity = new EvaluationEntity(Instant.now().getEpochSecond(),message);
        validate(evaluationEntity);
        evaluationRepository.save(evaluationEntity);
    }

    private void validate(EvaluationEntity evaluationEntity){
        Set<ConstraintViolation<EvaluationEntity>> violations = validator.validate(evaluationEntity);
        if (!violations.isEmpty()) {
            StringBuilder sb = new StringBuilder();
            for (var constraintViolation : violations) {
                sb.append(constraintViolation.getMessage());
            }
            throw new ConstraintViolationException("Error occurred: " + sb, violations);
        }
    }
}
