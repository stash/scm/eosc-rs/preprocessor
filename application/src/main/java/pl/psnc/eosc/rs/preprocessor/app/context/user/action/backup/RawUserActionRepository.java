package pl.psnc.eosc.rs.preprocessor.app.context.user.action.backup;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RawUserActionRepository extends JpaRepository<UserActionBackupEntity, Long> { }
