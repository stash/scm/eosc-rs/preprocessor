package pl.psnc.eosc.rs.preprocessor.app.context.user.action.generator;

import pl.psnc.eosc.rs.preprocessor.app.context.user.action.generator.model.GeneratedResource;

public interface ResourceProvider {
    GeneratedResource getResource();
}
