package pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular.model.statistics.database;

import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name="base.today_table")
@NoArgsConstructor
@Getter
public class TodayStatsEntity extends TodayStats{

    public TodayStatsEntity(UserResourceDayKey id, Boolean visited, Boolean ordered) {
        super(id, visited, ordered);
    }

}
