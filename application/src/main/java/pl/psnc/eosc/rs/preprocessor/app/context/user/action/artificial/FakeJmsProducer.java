package pl.psnc.eosc.rs.preprocessor.app.context.user.action.artificial;

import lombok.extern.slf4j.Slf4j;
import pl.psnc.eosc.rs.preprocessor.app.cloud.jms.JmsProducerImpl;
import pl.psnc.eosc.rs.preprocessor.app.cloud.jms.JmsTopic;

@Slf4j
public class FakeJmsProducer extends JmsProducerImpl {
    @Override
    public void sendMessage(JmsTopic topic, String message) {
        log.debug("Fake JMS Producer - the message wasn't send");
    }
}
