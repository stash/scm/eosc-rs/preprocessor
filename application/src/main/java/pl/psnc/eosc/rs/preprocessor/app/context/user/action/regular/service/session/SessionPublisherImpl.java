package pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular.service.session;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;
import pl.psnc.eosc.rs.preprocessor.app.cloud.kafka.Producer;

@Slf4j
@Primary
@Component
public class SessionPublisherImpl implements SessionPublisher {

    private String topic;

    Producer producer;

    @Autowired
    public SessionPublisherImpl(Producer producer,
                                @Value(value = "${user-action.session.kafka-topic}") String topic){
        this.producer = producer;
        this.topic = topic;
    }

    @Override
    public void publish(String key, String message) {
        producer.publish(topic,key,message);
    }
}
