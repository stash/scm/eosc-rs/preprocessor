package pl.psnc.eosc.rs.preprocessor.app.context.spark.processing.listener;

import lombok.extern.slf4j.Slf4j;
import org.apache.spark.launcher.SparkAppHandle;
import pl.psnc.eosc.rs.preprocessor.app.commons.spark.ResourceType;
import pl.psnc.eosc.rs.preprocessor.app.context.spark.processing.ProcessingBroker;
import pl.psnc.eosc.rs.preprocessor.app.context.spark.processing.command.DbUpdateCommand;

import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * The listener to catch the positive completion of Spark Job responsible for file transfer.
 * Used to trigger the launch of specific resources processing.
 */
@Slf4j
public class SparkJobUpdaterTrigger implements SparkAppHandle.Listener {

    Long commandId;
    String dumpDir;
    ResourceType resourceType;
    ProcessingBroker broker;

    public SparkJobUpdaterTrigger(ProcessingBroker broker, Long commandId, String dumpDir, ResourceType resourceType) {
        this.broker=broker;
        this.commandId=commandId;
        Path path = Paths.get(dumpDir);
        Path parentDir = path.getParent();
        this.dumpDir=parentDir.toString();
        this.resourceType=resourceType;
    }

    @Override
    public void stateChanged(SparkAppHandle handle) {
        if(handle.getState()==SparkAppHandle.State.FINISHED && handle.getError().isEmpty()){
            broker.execute(prepareUpdaterCommand(resourceType),
                    false,commandId);
        }
    }

    @Override
    public void infoChanged(SparkAppHandle handle) { }

    private DbUpdateCommand prepareUpdaterCommand(ResourceType resourceType){
        return DbUpdateCommand.builder()
                .dumpDir(dumpDir)
                .resourceType(resourceType)
                .build();
    }
}
