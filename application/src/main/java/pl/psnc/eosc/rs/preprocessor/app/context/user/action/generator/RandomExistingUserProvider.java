package pl.psnc.eosc.rs.preprocessor.app.context.user.action.generator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import pl.psnc.eosc.rs.preprocessor.app.context.resource.synchronization.user.repository.UserRepository;

import java.util.List;
import java.util.Random;

@Component
public class RandomExistingUserProvider implements UserProvider {

    @Autowired
    UserRepository userRepository;

    List<String> cachedUsers = null;

    Random random = new Random();

    @Override
    public String pickUser() {
        if (cachedUsers == null) {
            cachedUsers = userRepository.getAllIds();
        }
        int userIdUpperBound = cachedUsers.size();
        final int userId = random.nextInt(userIdUpperBound);
        return cachedUsers.get(userId);
    }
}
