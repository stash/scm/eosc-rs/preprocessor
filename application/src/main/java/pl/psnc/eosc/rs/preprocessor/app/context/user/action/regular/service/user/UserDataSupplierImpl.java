package pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular.service.user;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;
import pl.psnc.eosc.rs.preprocessor.app.context.resource.synchronization.user.model.UserEntity;
import pl.psnc.eosc.rs.preprocessor.app.context.resource.synchronization.user.repository.UserRepository;
import pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular.model.internal.SingleSession;
import pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular.service.AbstractUserActionHandler;

import java.util.Optional;

@Slf4j
@Component
@Primary
public class UserDataSupplierImpl extends AbstractUserActionHandler implements UserDataSupplier {

    @Autowired
    UserRepository userRepository;

    @Override
    protected SingleSession processUserAction(SingleSession userAction) {
        enrichUserProfile(userAction);
        return userAction;
    }

    @Override
    public void enrichUserProfile(SingleSession userAction) {
        if(userAction.getMarketplaceId()!=null && userAction.getAaiUid()==null) {
            UserEntity user = userRepository.findDistinctByMarketplaceId(userAction.getMarketplaceId());
            if(user!=null) {
                userAction.setAaiUid(user.getAaiId());
            }
        }
        if(userAction.getMarketplaceId()==null && userAction.getAaiUid()!=null) {
            Optional<UserEntity> userOptional = userRepository.findById(userAction.getAaiUid());
            if(userOptional.isPresent()){
                UserEntity user = userOptional.get();
                userAction.setMarketplaceId(user.getMarketplaceId());
            }

        }
    }
}
