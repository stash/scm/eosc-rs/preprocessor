package pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular.service.statistics;

import com.google.common.collect.Lists;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Primary;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import pl.psnc.eosc.rs.preprocessor.app.cloud.kafka.Producer;
import pl.psnc.eosc.rs.preprocessor.app.context.user.action.artificial.model.entity.ArtificialDayStatsEntity;
import pl.psnc.eosc.rs.preprocessor.app.context.user.action.artificial.model.entity.ArtificialTotalStatsEntity;
import pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular.model.internal.Resource;
import pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular.model.statistics.database.*;
import pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular.repository.AbstractStatsPerDayRepository;
import pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular.repository.AbstractStatsTodayRepository;
import pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular.repository.AbstractStatsTotalRepository;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;


@Slf4j
@Primary
@Component
public class StatisticsPublisherImpl implements StatisticsPublisher {

    private final
    String topic;

    private
    AbstractStatsPerDayRepository dayRepository;

    private
    AbstractStatsTotalRepository totalRepository;

    private
    AbstractStatsTodayRepository todayRepository;

    private final
    DayProvider dayProvider;

    private final
    Producer producer;

    private final
    Class<? extends DayStats> dayStatsClazz;

    private final
    Class<? extends TotalStats> totalStatsClazz;


    public <TDay extends DayStats, TTotal extends TotalStats, TToday extends TodayStats> StatisticsPublisherImpl(
            @Value(value = "${user-action.statistics.kafka-topic}") String topic,
            AbstractStatsPerDayRepository<TDay> dayRepository, AbstractStatsTotalRepository<TTotal> totalRepository,
            AbstractStatsTodayRepository<TToday> todayRepository, DayProvider dayProvider,
            Producer producer, Class <TDay> dayStatsClazz, Class <TTotal> totalStatsClazz) {
        this.topic = topic;
        this.dayRepository = dayRepository;
        this.totalRepository = totalRepository;
        this.todayRepository = todayRepository;
        this.dayProvider = dayProvider;
        this.producer = producer;
        this.dayStatsClazz = dayStatsClazz;
        this.totalStatsClazz = totalStatsClazz;
    }

    @Autowired
    public StatisticsPublisherImpl(@Value(value = "${user-action.statistics.kafka-topic}") String topic,
                                          @Qualifier("statsPerDayRepository") AbstractStatsPerDayRepository<DayStatsEntity> dayRepository,
                                          @Qualifier("statsTotalRepository") AbstractStatsTotalRepository<TotalStatsEntity> totalRepository,
                                          @Qualifier("statsTodayRepository") AbstractStatsTodayRepository<TodayStatsEntity> todayRepository,
                                          DayProvider dayProvider,
                                          Producer producer) {
        this.topic = topic;
        this.dayRepository = dayRepository;
        this.totalRepository = totalRepository;
        this.todayRepository = todayRepository;
        this.dayProvider = dayProvider;
        this.producer = producer;
        this.dayStatsClazz = DayStatsEntity.class;
        this.totalStatsClazz = TotalStatsEntity.class;
    }


    @Scheduled(cron = "${user-action.statistics.cron.daily}", zone = "UTC")
    @Transactional
    @Override
    public void calculateStats(){
        calculateStats(dayStatsClazz,totalStatsClazz);
    }

    public <TDay extends DayStats, TTotal extends TotalStats> void calculateStats(Class<TDay> dayStatsType, Class<TTotal> totalStatsType){
        log.debug("Calculating statistics for "+dayStatsType.getSimpleName()+" and "+totalStatsType.getSimpleName());
        Integer dayBefore = dayProvider.getCurrentDay()-1;
        publishKafkaMessage(dayBefore.toString(),"The calculation for day "+dayBefore+" started.");

        List<ResourceCounter> resourcesVisitsToUpdate = getCountedVisitsPerResource(dayBefore);
        Map<Resource,Integer> countedOrders = getCountedOrdersPerResource(dayBefore);
        Map<Resource, TTotal> totalEntities = getTotalEntities(resourcesVisitsToUpdate);
        resourcesVisitsToUpdate.forEach((entry)-> {
            Resource resource = entry.toResource();
            Integer visitsCounter = entry.getCounter();
            Integer ordersCounter = countedOrders.get(resource)!=null?
                    countedOrders.get(resource):0;
            ResourceDayKey resourceDayKey = new ResourceDayKey(resource.getResourceType(),resource.getResourceId(),dayBefore);
            if(dayStatsType==DayStatsEntity.class){
                DayStatsEntity dayStats = new DayStatsEntity(
                        resourceDayKey, visitsCounter, ordersCounter);
                dayRepository.save(dayStats);
            } else if(dayStatsType==ArtificialDayStatsEntity.class){
                ArtificialDayStatsEntity dayStats = new ArtificialDayStatsEntity(
                        resourceDayKey, visitsCounter, ordersCounter);
                dayRepository.save(dayStats);
            }

            //Update total
            if(totalEntities.containsKey(resource)) {
                Integer tempTotalVisitsCounter = totalEntities.get(resource).getVisits();
                Integer tempTotalOrdersCounter = totalEntities.get(resource).getOrders();
                totalEntities.get(resource).setVisits(tempTotalVisitsCounter + visitsCounter);
                totalEntities.get(resource).setOrders(tempTotalOrdersCounter + ordersCounter);
            } else {
                if(totalStatsType==TotalStatsEntity.class){
                    totalEntities.put(
                            resource,
                            (TTotal) new TotalStatsEntity(resource.getResourceType(),resource.getResourceId(),visitsCounter,ordersCounter));
                } else if(totalStatsType==ArtificialTotalStatsEntity.class){
                    totalEntities.put(
                            resource,
                            (TTotal) new ArtificialTotalStatsEntity(resource.getResourceType(),resource.getResourceId(),visitsCounter,ordersCounter));
                }
            }
            totalRepository.saveAll(totalEntities.values());
        });
        log.info("Statistics are updated.");


        Integer status = refreshMaterializedView();
        if(status==1){
            publishKafkaMessage(dayBefore.toString(),"The computation for day "+dayBefore+" is completed.");
            todayRepository.deleteDayAndBefore(dayBefore);
            log.debug("Kafka message about statistics readiness was sent.");
        } else {
            publishKafkaMessage(dayBefore.toString(),"An unexpected error occurred during the computation of day "+dayBefore+".");
        }
    }

    private Integer refreshMaterializedView(){
        Integer status = 0;
        if(dayStatsClazz==DayStatsEntity.class){
            status = dayRepository.refreshMaterializedViewsInSchema("base");
        } else if(dayStatsClazz==ArtificialDayStatsEntity.class){
            status = dayRepository.refreshMaterializedViewsInSchema("artificial");
        }
        return status;
    }

    private  List<ResourceCounter> getCountedVisitsPerResource(Integer dayBefore) {
        return todayRepository.countVisitsPerResource(dayBefore);
    }

    private  Map<Resource, Integer> getCountedOrdersPerResource(Integer dayBefore) {
        return (Map<Resource, Integer>) todayRepository.countOrdersPerResource(dayBefore)
                .stream().collect(Collectors.toMap(ResourceCounter::toResource, ResourceCounter::getCounter));
    }

    private <TTotal extends TotalStats> Map<Resource, TTotal> getTotalEntities(List<ResourceCounter> resourcesToUpdate) {
        List<ResourceKey> resourcesToUpdateKeys = resourcesToUpdate.stream().map(ResourceCounter::getResource).collect(Collectors.toList());
        Map<Resource, TTotal> result = new HashMap<>();
        for (List<ResourceKey> partition : Lists.partition(resourcesToUpdateKeys,1000)) {
            var partialResult = (Map<Resource, TTotal>) totalRepository.findAllById(partition)
                    .stream().collect(Collectors.toMap(TTotal::toResource, Function.identity()));
            result.putAll(partialResult);
        }
        return result;
    }

    private void publishKafkaMessage(String keyDay, String message) {
        log.info("Publishing: \""+message+"\" to kafka topic: "+topic);
        producer.publish(topic,keyDay,message);
    }
}