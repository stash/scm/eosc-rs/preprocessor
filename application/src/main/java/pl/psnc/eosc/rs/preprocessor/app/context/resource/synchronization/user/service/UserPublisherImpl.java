package pl.psnc.eosc.rs.preprocessor.app.context.resource.synchronization.user.service;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import pl.psnc.eosc.rs.preprocessor.app.cloud.kafka.Producer;
import pl.psnc.eosc.rs.preprocessor.app.context.resource.synchronization.user.model.UserEvent;

@Component
public class UserPublisherImpl implements UserChangesPublisher {

    private String topic;

    Producer producer;

    Gson gson = new GsonBuilder()
            .setDateFormat("yyyy-MM-dd hh:mm:ss.S")
            .create();

    @Autowired
    public UserPublisherImpl(Producer producer,
                             @Value(value = "${sync.user.kafka-topic}") String topic){
        this.producer = producer;
        this.topic = topic;
    }

    @Override
    public void publish(UserEvent user) {
        String json = gson.toJson(user);
        producer.publish(topic,user.getAaiId(),json);
    }
}
