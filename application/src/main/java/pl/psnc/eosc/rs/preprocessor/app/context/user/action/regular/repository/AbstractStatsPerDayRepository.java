package pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.query.Procedure;
import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.data.repository.query.Param;
import pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular.model.statistics.database.DayStats;
import pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular.model.statistics.database.ResourceDayKey;

import java.util.List;


@NoRepositoryBean
public interface AbstractStatsPerDayRepository<T extends DayStats> extends JpaRepository<T, ResourceDayKey> {

    @Procedure("base.refreshallmaterializedviews")
    int refreshMaterializedViewsInSchema(@Param("schema_arg") String schema);

    List<T> findFirst100ById_DayOrderByVisitsDesc(Integer day);

    List<T> findFirst100ById_DayOrderByOrdersDesc(Integer day);

}
