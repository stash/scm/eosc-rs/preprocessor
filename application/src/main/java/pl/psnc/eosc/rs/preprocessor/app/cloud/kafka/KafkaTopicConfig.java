package pl.psnc.eosc.rs.preprocessor.app.cloud.kafka;

import org.apache.kafka.clients.admin.NewTopic;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.config.TopicBuilder;

@Configuration
public class KafkaTopicConfig {

    @Bean
    public NewTopic userActionSessionTopic(
            @Value(value = "${user-action.session.kafka-topic}") String topic,
            @Value(value = "${kafka.config.partitions}") Integer partitions,
            @Value(value = "${kafka.config.replicas}") Integer replicas){
        return TopicBuilder.name(topic)
                .partitions(partitions)
                .replicas(replicas)
                .build();
    }
    @Bean
    public NewTopic userActionStatTopic(
            @Value(value = "${user-action.statistics.kafka-topic}") String topic,
            @Value(value = "${kafka.config.partitions}") Integer partitions,
            @Value(value = "${kafka.config.replicas}") Integer replicas){
        return TopicBuilder.name(topic)
                .partitions(partitions)
                .replicas(replicas)
                .build();
    }

    @Bean
    public NewTopic artificialUserActionSessionTopic(
            @Value(value = "${user-action.artificial.session.kafka-topic}") String topic,
            @Value(value = "${kafka.config.partitions}") Integer partitions,
            @Value(value = "${kafka.config.replicas}") Integer replicas){
        return TopicBuilder.name(topic)
                .partitions(partitions)
                .replicas(replicas)
                .build();
    }

    @Bean
    public NewTopic artificialUserActionStatTopic(
            @Value(value = "${user-action.artificial.statistics.kafka-topic}") String topic,
            @Value(value = "${kafka.config.partitions}") Integer partitions,
            @Value(value = "${kafka.config.replicas}") Integer replicas){
        return TopicBuilder.name(topic)
                .partitions(partitions)
                .replicas(replicas)
                .build();
    }
    @Bean
    public NewTopic userSyncTopic(
            @Value(value = "${sync.user.kafka-topic}") String topic,
            @Value(value = "${kafka.config.partitions}") Integer partitions,
            @Value(value = "${kafka.config.replicas}") Integer replicas){
        return TopicBuilder.name(topic)
                .partitions(partitions)
                .replicas(replicas)
                .build();
    }
    @Bean
    public NewTopic serviceSyncTopic(
            @Value(value = "${sync.service.kafka-topic}") String topic,
            @Value(value = "${kafka.config.partitions}") Integer partitions,
            @Value(value = "${kafka.config.replicas}") Integer replicas){
        return TopicBuilder.name(topic)
                .partitions(partitions)
                .replicas(replicas)
                .build();
    }

    @Bean
    public NewTopic trainingSyncTopic(
            @Value(value = "${sync.training.kafka-topic}") String topic,
            @Value(value = "${kafka.config.partitions}") Integer partitions,
            @Value(value = "${kafka.config.replicas}") Integer replicas){
        return TopicBuilder.name(topic)
                .partitions(partitions)
                .replicas(replicas)
                .build();
    }

    @Bean
    public NewTopic recommendationEvaluationTopic(
            @Value(value = "${recommendation.evaluation.kafka-topic}") String topic,
            @Value(value = "${kafka.config.partitions}") Integer partitions,
            @Value(value = "${kafka.config.replicas}") Integer replicas){
        return TopicBuilder.name(topic)
                .partitions(partitions)
                .replicas(replicas)
                .build();
    }
    @Bean
    public NewTopic sparkJobStatusTopic(
            @Value(value = "${spark.job.status.kafka-topic}") String topic,
            @Value(value = "${kafka.config.partitions}") Integer partitions,
            @Value(value = "${kafka.config.replicas}") Integer replicas){
        return TopicBuilder.name(topic)
                .partitions(partitions)
                .replicas(replicas)
                .build();
    }

}
