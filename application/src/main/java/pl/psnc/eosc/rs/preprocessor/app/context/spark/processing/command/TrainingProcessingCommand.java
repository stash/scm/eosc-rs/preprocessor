package pl.psnc.eosc.rs.preprocessor.app.context.spark.processing.command;

import lombok.Data;
import lombok.experimental.SuperBuilder;
import lombok.extern.slf4j.Slf4j;
import org.apache.spark.launcher.SparkAppHandle;
import pl.psnc.eosc.rs.preprocessor.app.commons.spark.SparkJobLauncherAbstract;
import pl.psnc.eosc.rs.preprocessor.app.context.trainings.job.TrainingsPreprocJobLauncher;

@Slf4j
@SuperBuilder
@Data
public class TrainingProcessingCommand extends Command {

    String inputFiles;

    String outputDir;

    String previousDumpDir;

    @Override
    public void execute(SparkJobLauncherAbstract sparkJobLauncherAbstract,
                        SparkAppHandle.Listener... listeners) {
        if (sparkJobLauncherAbstract instanceof TrainingsPreprocJobLauncher) {
            ((TrainingsPreprocJobLauncher)sparkJobLauncherAbstract)
                    .runAsync(previousDumpDir,inputFiles,outputDir,listeners);
        }
        else{
            //TODO throw exception
            log.error("Improper Spark job launcher.");
        }
    }
}