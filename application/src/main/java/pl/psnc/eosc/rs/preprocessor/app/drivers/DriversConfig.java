package pl.psnc.eosc.rs.preprocessor.app.drivers;

import org.apache.hadoop.security.UserGroupInformation;
import org.apache.hadoop.yarn.conf.YarnConfiguration;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import pl.psnc.eosc.rs.preprocessor.app.drivers.api.hdfs.HDFSDriver;
import pl.psnc.eosc.rs.preprocessor.app.drivers.api.resourcemanager.ResourceManagerDriver;
import pl.psnc.eosc.rs.preprocessor.app.drivers.api.searchservice.SearchServiceDriver;
import pl.psnc.eosc.rs.preprocessor.app.drivers.impl.hdfs.HDFSDriverImpl;
import pl.psnc.eosc.rs.preprocessor.app.drivers.impl.resourcemanager.YarnDriverImpl;
import pl.psnc.eosc.rs.preprocessor.app.drivers.impl.searchservice.SearchServiceDriverImpl;

import java.net.URI;

@Configuration
public class DriversConfig {

    static {
        UserGroupInformation.setLoginUser(UserGroupInformation.createRemoteUser("hadoop"));
    }

    @Bean
    SearchServiceDriver searchServiceDriver(@Value("${search-service.base-path}") String searchServiceUrl){
        return SearchServiceDriverImpl.create(searchServiceUrl);
    }

    @Bean
    HDFSDriver hdfsDriver(@Value("${hadoop.webhdfs.base-path}") String webhdfsUrl){
        return HDFSDriverImpl.create(URI.create(webhdfsUrl));
    }

    @Bean
    ResourceManagerDriver resourceManagerDriver(@Value("${hadoop.yarn.base-path}") String resourceManagerAddress){
        YarnConfiguration configuration = new YarnConfiguration();
        configuration.set("yarn.resourcemanager.address", resourceManagerAddress);
        configuration.set("yarn.client.failover-max-attempts","0");

        return YarnDriverImpl.create(configuration);
    }

}
