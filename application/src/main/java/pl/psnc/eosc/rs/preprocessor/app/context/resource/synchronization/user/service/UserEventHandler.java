package pl.psnc.eosc.rs.preprocessor.app.context.resource.synchronization.user.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import pl.psnc.eosc.rs.preprocessor.app.cloud.jms.MessageBroadcaster;
import pl.psnc.eosc.rs.preprocessor.app.context.resource.converter.JsonObjectConverter;
import pl.psnc.eosc.rs.preprocessor.app.context.resource.converter.ResourceEvent;
import pl.psnc.eosc.rs.preprocessor.app.context.resource.synchronization.EventService;
import pl.psnc.eosc.rs.preprocessor.app.context.resource.synchronization.EventType;
import pl.psnc.eosc.rs.preprocessor.app.context.resource.synchronization.user.model.UserEntity;
import pl.psnc.eosc.rs.preprocessor.app.context.resource.synchronization.user.model.UserEvent;
import pl.psnc.eosc.rs.preprocessor.app.context.resource.synchronization.user.repository.UserRepository;

import java.util.Optional;

/**
 * Update and remove users according to appropriate type of {@link ResourceEvent}.
 */
@Slf4j
@Component
public class UserEventHandler extends EventService {

    UserRepository userRepository;

    JsonObjectConverter<ResourceEvent> resourceEventConverter;

    JsonObjectConverter<UserEvent> userEventConverter;

    UserChangesPublisher userChangesPropagator;

    @Autowired
    public UserEventHandler(UserRepository userRepository,
                            @Qualifier("resourceEventBroadcaster") MessageBroadcaster broadcaster,
                            JsonObjectConverter<ResourceEvent> resourceEventConverter,
                            JsonObjectConverter<UserEvent> userEventConverter,
                            UserChangesPublisher userChangesPropagator) {
        super(broadcaster);
        this.userRepository=userRepository;
        this.resourceEventConverter=resourceEventConverter;
        this.userEventConverter=userEventConverter;
        this.userChangesPropagator=userChangesPropagator;
    }

    @Override
    public void processMessage(String rawMessage) {
        ResourceEvent resourceEvent = resourceEventConverter.convert(rawMessage);
        if(isUserEvent(resourceEvent)){
            UserEvent userEvent = userEventConverter.convert(resourceEvent.getRecord().toString());
            if(isRemoveEvent(resourceEvent)){
                deleteUser(userEvent);
            } else if (isModifyEvent(resourceEvent)) {
                Optional<UserEntity> fetchedUser = userRepository.findById(userEvent.getAaiId());
                if(fetchedUser.isEmpty()){
                    createUser(userEvent);
                } else if(!toUserEntity(userEvent).equals(fetchedUser.get())){
                    updateUser(userEvent);
                } else {
                    log.debug("Nothing to change! User id: "+userEvent.getAaiId());
                }
            }
        }
    }

    private void createUser(UserEvent user){
        log.debug("Creating user: "+user.getAaiId());
        user.setEventType(EventType.CREATE.getName());
        userRepository.saveAndFlush(toUserEntity(user));
        userChangesPropagator.publish(user);
    }

    private void deleteUser(UserEvent user){
        log.debug("Removing user: "+user.getAaiId());
        user.setEventType(EventType.DELETE.getName());
        userRepository.deleteById(user.getAaiId());
        userChangesPropagator.publish(user);
    }

    private void updateUser(UserEvent user){
        log.debug("Updating user: "+user.getAaiId());
        user.setEventType(EventType.UPDATE.getName());
        userRepository.saveAndFlush(toUserEntity(user));
        userChangesPropagator.publish(user);
    }

    private boolean isUserEvent(ResourceEvent resourceEvent){
        return resourceEvent.getModel().equals("User");
    }

    private UserEntity toUserEntity(UserEvent userEvent){
        return UserEntity.builder()
                .marketplaceId(userEvent.getMarketplaceId())
                .aaiId(userEvent.getAaiId())
                .scientificDomainValues(userEvent.getScientificDomainValues())
                .categoryValues(userEvent.getCategoryValues())
                .accessedServiceValues(userEvent.getAccessedServiceValues())
                .build();
    }
}
