package pl.psnc.eosc.rs.preprocessor.app.context.recommendation;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.psnc.eosc.rs.preprocessor.app.context.evaluation.entity.EvaluationEntity;
import pl.psnc.eosc.rs.preprocessor.app.context.recommendation.entity.RecommendationEntity;

@Repository
public interface RecommendationRepository extends JpaRepository<RecommendationEntity, Long> { }
