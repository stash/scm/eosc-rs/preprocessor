package pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.data.repository.query.Param;
import pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular.model.statistics.database.ResourceCounter;
import pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular.model.statistics.database.TodayStats;
import pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular.model.statistics.database.UserResourceDayKey;

import java.util.List;


@NoRepositoryBean
public interface AbstractStatsTodayRepository<T extends TodayStats> extends JpaRepository<T, UserResourceDayKey> {

    @Query(value = "SELECT new pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular.model.statistics.database.ResourceCounter" +
            "(t.id.resourceType, t.id.resourceId, COUNT(t.id.resourceId)) " +
            "FROM #{#entityName} t " +
            "WHERE t.id.day = :day AND t.visited = 't' " +
            "GROUP BY t.id.resourceType, t.id.resourceId")
    List<ResourceCounter> countVisitsPerResource(@Param("day") Integer day);

    @Query(value = "SELECT new pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular.model.statistics.database.ResourceCounter" +
            "(t.id.resourceType, t.id.resourceId, COUNT(t.id.resourceId)) " +
            "FROM #{#entityName} t " +
            "WHERE t.id.day = :day AND t.ordered = 't' " +
            "GROUP BY t.id.resourceType, t.id.resourceId")
    List<ResourceCounter> countOrdersPerResource(@Param("day") Integer day);

    @Modifying
    @Query(value = "DELETE FROM #{#entityName} t WHERE t.id.day <= :day")
    Integer deleteDayAndBefore(@Param("day") Integer day);
}
