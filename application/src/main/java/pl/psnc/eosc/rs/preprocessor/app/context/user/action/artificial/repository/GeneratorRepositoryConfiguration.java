package pl.psnc.eosc.rs.preprocessor.app.context.user.action.artificial.repository;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular.repository.AbstractStatsPerDayRepository;
import pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular.repository.AbstractStatsTodayRepository;
import pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular.repository.AbstractStatsTotalRepository;

@Configuration
public class GeneratorRepositoryConfiguration {

    @Bean
    AbstractStatsPerDayRepository artificialDayRepository(ArtificialStatsPerDayRepository statsPerDayRepository){
        return statsPerDayRepository;
    }

    @Bean
    AbstractStatsTodayRepository artificialTodayRepository(ArtificialStatsTodayRepository statsTodayRepository){
        return statsTodayRepository;
    }

    @Bean
    AbstractStatsTotalRepository artificialTotalRepository(ArtificialStatsTotalRepository statsTotalRepository){
        return statsTotalRepository;
    }
}
