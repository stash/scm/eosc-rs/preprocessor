package pl.psnc.eosc.rs.preprocessor.app.cloud.jms;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Profile;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.messaging.Message;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

import static com.google.common.base.Throwables.getStackTraceAsString;

/**
 * The JMS consumer is used to receive messages regarding recommendation evaluation.
 */
@Profile("!jms-disabled")
@Primary
@Slf4j
@Component("recommendationEventBroadcaster")
@Getter
public class RecommendationConsumer implements MessageBroadcaster {

    List<Subscriber> recommendationEventSubscribers = new ArrayList<>();

    public void subscribe(Subscriber subscriber) {
        recommendationEventSubscribers.add(subscriber);
    }

    @JmsListener(destination = "${jms.topic.recommendations.name}", containerFactory = "defaultFactory")
    public void resourcePublisher(@Payload final Message message)  {
        try{
            String messagePayload = JmsMessageExtractor.extractMessage(message);
            log.debug("Received recommendation:\n"+messagePayload);
            recommendationEventSubscribers.forEach(subscriber -> subscriber.processMessage(messagePayload));
        } catch (Exception e) {
            log.warn(e.getMessage());
        }
    }
}
