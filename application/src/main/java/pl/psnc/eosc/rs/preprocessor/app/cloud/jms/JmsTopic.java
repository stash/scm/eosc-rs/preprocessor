package pl.psnc.eosc.rs.preprocessor.app.cloud.jms;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@Getter
@Setter
public class JmsTopic {
    private String name;
}
