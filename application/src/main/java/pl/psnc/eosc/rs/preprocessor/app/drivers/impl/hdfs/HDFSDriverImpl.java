package pl.psnc.eosc.rs.preprocessor.app.drivers.impl.hdfs;

import lombok.extern.slf4j.Slf4j;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import pl.psnc.eosc.rs.preprocessor.app.drivers.api.hdfs.HDFSDriver;
import pl.psnc.eosc.rs.preprocessor.app.drivers.api.hdfs.HdfsFileStatus;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.net.URI;
import java.util.Arrays;
import java.util.List;

import static java.util.stream.Collectors.toList;

@Slf4j
public class HDFSDriverImpl implements HDFSDriver {

    FileSystem fileSystem;

    protected HDFSDriverImpl(FileSystem fileSystem){
        this.fileSystem = fileSystem;
    }

    @Override
    public List<HdfsFileStatus> listFiles(Path path) {
        try {
            FileStatus[] statuses = fileSystem.listStatus(path);
            return Arrays.stream(statuses).map(this::mapToInternalHdfsFileStatus).collect(toList());
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }

    @Override
    public boolean delete(Path path, boolean recursive) {
        try {
            return fileSystem.delete(path,recursive);
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }

    private HdfsFileStatus mapToInternalHdfsFileStatus(FileStatus fileStatus){
        HdfsFileStatus result = new HdfsFileStatus();
        result.setFilename(fileStatus.getPath().getName());
        result.setIsDirectory(fileStatus.isDirectory());
        return result;
    }

    /**
     * Factory method
     */
    public static HDFSDriverImpl create(URI webhdfsBasePath){
        try {
            FileSystem fileSystem = FileSystem.get(webhdfsBasePath, new Configuration());
            return new HDFSDriverImpl(fileSystem);
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }
}
