package pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular.service.statistics;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

import java.time.*;

@Primary
@Component
public class DayProviderImpl implements DayProvider{

    private Clock clock = Clock.systemUTC();

    @Value("${user-action.session.zero-day.day:1}")
    private int day;

    @Value("${user-action.session.zero-day.month:6}")
    private int month;

    @Value("${user-action.session.zero-day.year:2022}")
    private int year;

    public Integer getCurrentDay(){
        ZonedDateTime zeroDay = LocalDate.of(year, Month.of(month), day).atStartOfDay(ZoneOffset.UTC);
             long zero = zeroDay.toInstant().toEpochMilli();
        return Math.toIntExact( (clock.millis()-zero) / (24*60*60*1000) );
    }

}
