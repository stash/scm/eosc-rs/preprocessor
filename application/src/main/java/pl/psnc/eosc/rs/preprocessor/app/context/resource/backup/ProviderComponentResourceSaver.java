package pl.psnc.eosc.rs.preprocessor.app.context.resource.backup;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;
import pl.psnc.eosc.rs.preprocessor.app.cloud.jms.ProviderComponentMessageBroadcaster;
import pl.psnc.eosc.rs.preprocessor.app.cloud.jms.ProviderComponentResourceSubscriber;
import pl.psnc.eosc.rs.preprocessor.app.context.user.action.generator.ResourceEnum;

import javax.annotation.PostConstruct;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.Validator;
import java.time.Instant;
import java.util.Set;

/**
 * Save and validate (raw - non-deserialized string only) all messages from Marketplace database.
 */
@Slf4j
@Profile("backup-raw-jms")
@Component
public class ProviderComponentResourceSaver implements ProviderComponentResourceSubscriber {

    @Autowired
    ProviderComponentEventRepository rawResourceRepository;

    @Autowired
    @Qualifier("trainingEventBroadcaster")
    ProviderComponentMessageBroadcaster broadcaster;

    @Autowired
    Validator validator;

    @PostConstruct
    public void subscribeJms() {
        broadcaster.subscribe(this);
    }

    @Override
    public void processMessage(String rawMessage, OperationType operationType, ResourceEnum resourceType) {
        log.debug("Backing up raw Provider Component resource");
        ProviderComponentResourceEntity resourceEventEntity = new ProviderComponentResourceEntity(
                Instant.now().getEpochSecond(),rawMessage,resourceType.getName(), operationType.name());
        validate(resourceEventEntity);
        rawResourceRepository.save(resourceEventEntity);
    }

    private void validate(ProviderComponentResourceEntity resourceEventEntity){
        Set<ConstraintViolation<ProviderComponentResourceEntity>> violations = validator.validate(resourceEventEntity);
        if (!violations.isEmpty()) {
            StringBuilder sb = new StringBuilder();
            for (var constraintViolation : violations) {
                sb.append(constraintViolation.getMessage());
            }
            throw new ConstraintViolationException("Error occurred: " + sb, violations);
        }
    }
}
