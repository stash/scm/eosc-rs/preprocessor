package pl.psnc.eosc.rs.preprocessor.app.context.spark.processing.listener;

import lombok.extern.slf4j.Slf4j;
import org.apache.spark.launcher.SparkAppHandle;
import pl.psnc.eosc.rs.preprocessor.app.context.spark.processing.ProcessingBroker;

/**
 * The listener updates database according to Spark Jobs state changes.
 */
@Slf4j
public class SparkJobStatusUpdateListener implements SparkAppHandle.Listener {

    ProcessingBroker broker;
    Long commandId;

    public SparkJobStatusUpdateListener(ProcessingBroker broker, Long commandId) {
        this.broker=broker;
        this.commandId=commandId;
    }

    @Override
    public void stateChanged(SparkAppHandle handle) {
        broker.updateCommand(commandId,handle.getAppId(),handle.getState());
    }

    @Override
    public void infoChanged(SparkAppHandle handle) { }
}