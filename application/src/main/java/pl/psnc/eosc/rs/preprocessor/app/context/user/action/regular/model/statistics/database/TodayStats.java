package pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular.model.statistics.database;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.MappedSuperclass;

@MappedSuperclass
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class TodayStats {

    @EmbeddedId
    protected UserResourceDayKey id;

    @Column(columnDefinition = "BOOLEAN")
    protected Boolean visited;

    @Column(columnDefinition = "BOOLEAN")
    protected Boolean ordered;

}
