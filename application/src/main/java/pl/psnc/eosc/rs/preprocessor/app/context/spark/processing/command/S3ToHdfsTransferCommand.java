package pl.psnc.eosc.rs.preprocessor.app.context.spark.processing.command;

import lombok.Data;
import lombok.experimental.SuperBuilder;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.spark.launcher.SparkAppHandle;
import pl.psnc.eosc.rs.preprocessor.app.commons.spark.SparkJobLauncherAbstract;
import pl.psnc.eosc.rs.preprocessor.app.context.oag.job.S3toHdfsTransferJobLauncher;

import java.util.Arrays;
import java.util.Map;

@Slf4j
@SuperBuilder
@Data
public class S3ToHdfsTransferCommand extends Command {

    String sourceDir;

    String sourceFiles;

    String destDir;


    @Override
    public void execute(SparkJobLauncherAbstract sparkJobLauncherAbstract,
                        SparkAppHandle.Listener... listeners) {
        if (sparkJobLauncherAbstract instanceof S3toHdfsTransferJobLauncher) {
            if(StringUtils.isNotBlank(sourceDir)) {
                ((S3toHdfsTransferJobLauncher)sparkJobLauncherAbstract)
                        .runAsync(Map.of(sourceDir,destDir),listeners);
            } else if(StringUtils.isNotBlank(sourceFiles)) {
                ((S3toHdfsTransferJobLauncher)sparkJobLauncherAbstract)
                        .runAsync(Arrays.asList(sourceFiles.split(";")),destDir,listeners);
            }
        }
        else{
            //TODO throw exception
            log.error("Improper Spark job launcher.");
        }
    }
}