package pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular.service.statistics;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;
import pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular.model.internal.Resource;
import pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular.model.internal.SingleSession;
import pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular.service.AbstractUserActionHandler;

import static pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular.model.ResourceMapper.resourceTypeToShort;

@Slf4j
@Primary
@Component
public class StatisticsHandlerImpl extends AbstractUserActionHandler implements StatisticsHandler {

    StatisticsStorage statisticsStorage;

    @Autowired
    public StatisticsHandlerImpl(StatisticsStorage statisticsStorage){
        this.statisticsStorage=statisticsStorage;
    }

    @Override
    protected SingleSession processUserAction(SingleSession userAction) {
        log.debug("Statistics module received UA regarding resource - "+
                userAction.getUserAction().getResourceType()+" : "+
                userAction.getUserAction().getResourceId());

        if(userAction.getUserAction().getResourceId()!=null) {
            Resource resource = new Resource(resourceTypeToShort(userAction.getUserAction().getResourceType()),
                    userAction.getUserAction().getResourceId());

            receiveUserAction(userAction.getSessionId().toString(),
                    userAction.getAaiUid(),
                    resource,
                    userAction.getUserAction().getIsOrderAction());

        }
        return userAction;
    }

    @Override
    public void receiveUserAction(String sessionId, String userId, Resource resource, Boolean isOrder) {
        String user = sessionId;
        if(userId!=null) {
            statisticsStorage.makeUserNonAnonymous(userId, sessionId);
            user = userId;
        }
        statisticsStorage.addVisit(user,resource);
        if(isOrder){
            statisticsStorage.addOrder(user,resource);
        }
    }
}
