package pl.psnc.eosc.rs.preprocessor.app.drivers.api.searchservice;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class DumpElement {

    String name;
    String referenceType;
    String reference;

}
