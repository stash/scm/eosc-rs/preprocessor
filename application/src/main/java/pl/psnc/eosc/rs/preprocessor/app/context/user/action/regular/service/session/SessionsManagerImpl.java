package pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular.service.session;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Primary;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular.model.internal.Session;
import pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular.model.internal.SingleSession;
import pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular.model.internal.User;
import pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular.model.internal.UserAction;
import pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular.model.output.SessionDto;
import pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular.service.AbstractUserActionHandler;

import java.util.*;
import java.util.concurrent.TimeUnit;

import static org.apache.kafka.common.utils.Utils.isBlank;
import static pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular.model.Mapper.toUserActionDtoList;

@Slf4j
@Primary
@Component
public class SessionsManagerImpl extends AbstractUserActionHandler implements SessionManagerHandler {

    SessionPublisher sessionPublisher;

    private Long sessionExpirationTime;

    private Long sessionExpirationDelay;

    private final HashMap<UUID, Session> sessions = new HashMap<>();

    Gson gson = new GsonBuilder()
            .setDateFormat("yyyy-MM-dd hh:mm:ss.S")
            .setPrettyPrinting()
            .create();

    @Autowired
    public SessionsManagerImpl(SessionPublisher sessionPublisher,
                               @Value("${user-action.session.expiration-time}") Long sessionExpirationTime,
                               @Value("${user-action.session.expiration-time.delay}") Long sessionExpirationDelay){
        this.sessionPublisher = sessionPublisher;
        this.sessionExpirationTime = sessionExpirationTime;
        this.sessionExpirationDelay = sessionExpirationDelay;
    }



    @Override
    protected SingleSession processUserAction(SingleSession singleSession) {
        User user = User.builder()
                .aaiUid(singleSession.getAaiUid())
                .marketplaceId(singleSession.getMarketplaceId())
                .sessionId(singleSession.getSessionId())
                .build();
        addUserAction(user, singleSession.getUserAction());
        return singleSession;
    }

    @Override
    public void addUserAction(User user, UserAction userAction){
        Session currentSession = getProperOrInitSession(user.getSessionId());
        // Update User data
        if(currentSession.getIsAnonymous() && !isBlank(user.getAaiUid())) {
            currentSession.setAaiUid(user.getAaiUid());
            currentSession.setIsAnonymous(false);
            if(!isBlank(user.getMarketplaceId()))
                currentSession.setMarketplaceId(user.getMarketplaceId()); // optional
        }
        List<UserAction> userActions = currentSession.getUserActions();
        userActions.add(userAction);
        userActions.sort(Comparator.comparing(UserAction::getReceiptTimestamp));
        SessionDto resultedUser = new SessionDto(
                user.getSessionId().toString(),
                currentSession.getIsAnonymous(),
                currentSession.getAaiUid(),
                currentSession.getMarketplaceId(),
                currentSession.getUserActions().size(),
                toUserActionDtoList(userActions));
        String sessionJson = gson.toJson(resultedUser);
        sessionPublisher.publish(String.valueOf(user.getSessionId()),sessionJson);
    }

    public Session getProperOrInitSession(UUID sessionId) {
        if(sessions.get(sessionId) == null) {
            Session session = new Session();
            List<UserAction> userActions = new ArrayList<>();
            session.setUserActions(userActions);
            session.setIsAnonymous(true);
            sessions.put(sessionId,session);
            return session;
        }
        else
            return sessions.get(sessionId);
    }

    @Scheduled(cron = "${user-action.session.cron.remover}", zone = "UTC")
    public void findClosedSessions() {
        Long userActionsExpirationTime = sessionExpirationTime+sessionExpirationDelay;

        for (Iterator<Map.Entry<UUID, Session>> it = sessions.entrySet().iterator();
             it.hasNext(); ) {
            Map.Entry<UUID, Session> entry = it.next();
            UUID sessionId = entry.getKey();
            Session session = entry.getValue();
            List<UserAction> userActions = session.getUserActions();
            Long duration = System.currentTimeMillis() - userActions.get(userActions.size()-1).getReceiptTimestamp();
            if(duration>userActionsExpirationTime) {
                it.remove();
                String readableDuration = String.format("%d min, %d sec",
                        TimeUnit.MILLISECONDS.toMinutes(duration),
                        TimeUnit.MILLISECONDS.toSeconds(duration) -
                                TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(duration))
                );
                log.debug("Session " + sessionId + " was removed from Session Manager. " + readableDuration + " have passed since the last UA (in the session).");
            }
        }
        log.info("Sessions were removed from Session Manager.");
    }

}
