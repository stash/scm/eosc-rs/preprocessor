package pl.psnc.eosc.rs.preprocessor.app.commons.metrics;

import com.codahale.metrics.MetricRegistry;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class MetricRegistryConfig {

    @Bean
    MetricRegistry metricRegistry(){
        MetricRegistry registry = new MetricRegistry();

        CsvMetricWriter.add(registry);
        return registry;
    }
}
