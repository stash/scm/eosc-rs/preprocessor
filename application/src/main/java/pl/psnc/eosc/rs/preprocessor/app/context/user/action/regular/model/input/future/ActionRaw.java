package pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular.model.input.future;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ActionRaw {

    private String type;

    private String text;

    private Boolean order;
}
