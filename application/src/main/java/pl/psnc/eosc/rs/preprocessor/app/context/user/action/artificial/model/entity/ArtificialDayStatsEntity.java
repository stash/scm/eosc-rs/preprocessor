package pl.psnc.eosc.rs.preprocessor.app.context.user.action.artificial.model.entity;

import lombok.NoArgsConstructor;
import pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular.model.statistics.database.DayStats;
import pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular.model.statistics.database.ResourceDayKey;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name="artificial.artificial_stats_day_table")
@NoArgsConstructor
public class ArtificialDayStatsEntity extends DayStats {

    public ArtificialDayStatsEntity(ResourceDayKey id, Integer visits, Integer orders) {
        super(id, visits, orders);
    }
}