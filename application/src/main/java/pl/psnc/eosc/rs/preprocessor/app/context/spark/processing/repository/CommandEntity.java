package pl.psnc.eosc.rs.preprocessor.app.context.spark.processing.repository;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import javax.validation.constraints.Size;

@Entity
@Table(name="spark.command_table")
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class CommandEntity {

    @Id
    @Column(name="command_id")
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    Long id;

    @Column(
            nullable = false,
            updatable = false
    )
    Long timestamp;

    @Size(max=32)
    @Column(name="processing_id",
            length = 32)
    String processingId;

    @Size(max=16)
    @Column(length = 16)
    String status;

    @Size(max=32)
    @Column(
            length = 32,
            nullable = false,
            updatable = false
    )
    String type;

    @Column(
            name = "is_started_manually",
            nullable = false,
            updatable = false
    )
    Boolean isStartedManually;

    @Lob
    @Type(type="org.hibernate.type.BinaryType")
    @Column(
            name = "java_object",
            nullable = false,
            updatable = false
    )
    byte[] command;

    @ManyToOne
    @JoinColumn(columnDefinition="bigint",
            name = "command_predecessor_id")
    CommandEntity predecessor;


    public CommandEntity(Long timestamp, String status, String type, Boolean isStartedManually, byte[] command, CommandEntity predecessor) {
        this.timestamp = timestamp;
        this.status = status;
        this.type = type;
        this.isStartedManually = isStartedManually;
        this.command = command;
        this.predecessor = predecessor;
    }
}