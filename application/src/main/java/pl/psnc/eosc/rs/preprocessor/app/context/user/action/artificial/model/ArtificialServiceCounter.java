package pl.psnc.eosc.rs.preprocessor.app.context.user.action.artificial.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class ArtificialServiceCounter {

    private Integer serviceId;

    private Integer counter;

    public ArtificialServiceCounter(int serviceId, long counter){
        this.serviceId=serviceId;
        this.counter = Math.toIntExact(counter);
    }
}
