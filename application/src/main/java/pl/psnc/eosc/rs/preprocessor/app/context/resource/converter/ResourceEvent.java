package pl.psnc.eosc.rs.preprocessor.app.context.resource.converter;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.JsonNode;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class ResourceEvent {

    @JsonProperty("cud")
    private String cud;

    @JsonProperty("model")
    private String model;

    @JsonProperty("record")
    private JsonNode record;

}
