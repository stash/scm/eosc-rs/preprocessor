package pl.psnc.eosc.rs.preprocessor.app.context.resource.synchronization.service.model;

import com.vladmihalcea.hibernate.type.array.ListArrayType;
import lombok.*;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.List;
import java.util.Objects;

@Entity(name="Service")
@Table(name="sync.service_table")
@TypeDef(
        name = "list-array",
        typeClass = ListArrayType.class
)
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
public class ServiceEntity {

    @Id
    @Column(name="id")
    String id;

    @Column
    String name;

    @Column
    String description;

    @Type(type = "list-array")
    @Column(
            name="scientific_domains",
            columnDefinition = "integer[]")
    List<Integer> scientificDomainValues;

    @Type(type = "list-array")
    @Column(
            name="categories",
            columnDefinition = "integer[]")
    List<Integer> categoryValues;

    @Type(type = "list-array")
    @Column(
            name="related_services",
            columnDefinition = "integer[]")
    List<Integer> relatedServices;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ServiceEntity that = (ServiceEntity) o;
        return id.equals(that.id) && name.equals(that.name) && Objects.equals(description, that.description) && Objects.equals(scientificDomainValues, that.scientificDomainValues) && Objects.equals(categoryValues, that.categoryValues) && Objects.equals(relatedServices, that.relatedServices);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, description, scientificDomainValues, categoryValues, relatedServices);
    }
}
