package pl.psnc.eosc.rs.preprocessor.app.cloud.jms;

public interface Subscriber {

    void processMessage(String text);

}
