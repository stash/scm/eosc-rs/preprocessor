package pl.psnc.eosc.rs.preprocessor.app.context.spark.processing;

import org.apache.spark.launcher.SparkAppHandle;
import pl.psnc.eosc.rs.preprocessor.app.context.spark.processing.command.Command;

public interface ProcessingBroker {

    void execute(Command command, Boolean isStartedManually, Long predecessorCommandId);

    void updateCommand(Long commandId, String processingId, SparkAppHandle.State status);
}
