package pl.psnc.eosc.rs.preprocessor.app.context.resource.backup;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RawResourceRepository extends JpaRepository<ResourceEventBackupEntity, Long> { }
