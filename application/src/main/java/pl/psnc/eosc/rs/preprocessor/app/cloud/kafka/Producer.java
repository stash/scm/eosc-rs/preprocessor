package pl.psnc.eosc.rs.preprocessor.app.cloud.kafka;

public interface Producer {

    void publish(String topic, String key, String message);

}
