package pl.psnc.eosc.rs.preprocessor.app.context.user.action.generator;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;
import pl.psnc.eosc.rs.preprocessor.app.cloud.jms.UserActionConsumer;
import pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular.model.input.future.UserActionRaw;


@Slf4j
@Profile("artificial-flow")
@Component
public class UserActionDirectPublisher implements UserActionsPublisher{

    @Autowired
    @Qualifier("userActionBroadcaster")
    UserActionConsumer userActionConsumer;

    @Override
    public void publish(UserActionRaw userAction) {
        String producedMessage = userAction.toJson();
        userActionConsumer.userActionPublisher(producedMessage);
    }
}
