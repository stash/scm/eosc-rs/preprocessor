package pl.psnc.eosc.rs.preprocessor.app.context.spark.processing.listener;

import org.apache.spark.launcher.SparkAppHandle;
import pl.psnc.eosc.rs.preprocessor.app.commons.spark.ResourceType;

/**
 * The listener broadcast information about final Spark Jobs states
 * via {@link SparkFinalStatusListener}.
 */
public class SparkJobFinalStatusForwardingListener implements SparkAppHandle.Listener {

    SparkFinalStatusListener finalStatusListener;

    String commandName;

    ResourceType resourceType;

    public SparkJobFinalStatusForwardingListener(SparkFinalStatusListener finalStatusListener,
                                                 String commandName, ResourceType resourceType){
        this.finalStatusListener = finalStatusListener;
        this.commandName = commandName;
        this.resourceType = resourceType;
    }

    @Override
    public void stateChanged(SparkAppHandle appHandle) {
        if(appHandle.getState().isFinal())
            finalStatusListener.handle(appHandle, commandName, resourceType);
    }

    @Override
    public void infoChanged(SparkAppHandle handle) {

    }

    // Handler interface
    public interface SparkFinalStatusListener {
        void handle(SparkAppHandle finalHandle, String commandName, ResourceType resourceType);
    }

}
