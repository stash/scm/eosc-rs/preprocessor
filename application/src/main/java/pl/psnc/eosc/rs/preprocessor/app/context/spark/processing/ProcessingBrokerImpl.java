package pl.psnc.eosc.rs.preprocessor.app.context.spark.processing;

import lombok.extern.slf4j.Slf4j;
import org.apache.spark.launcher.SparkAppHandle;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.psnc.eosc.rs.preprocessor.app.context.marketplace.job.MarketplaceResourcesPreprocJobLauncher;
import pl.psnc.eosc.rs.preprocessor.app.context.oag.job.DbUpdateJobLauncher;
import pl.psnc.eosc.rs.preprocessor.app.context.oag.job.OagResourcesPreprocJobLauncher;
import pl.psnc.eosc.rs.preprocessor.app.context.oag.job.S3toHdfsTransferJobLauncher;
import pl.psnc.eosc.rs.preprocessor.app.context.spark.processing.command.*;
import pl.psnc.eosc.rs.preprocessor.app.context.spark.processing.listener.*;
import pl.psnc.eosc.rs.preprocessor.app.context.spark.processing.repository.CommandEntity;
import pl.psnc.eosc.rs.preprocessor.app.context.spark.processing.repository.CommandRepository;
import pl.psnc.eosc.rs.preprocessor.app.context.trainings.job.TrainingsPreprocJobLauncher;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Slf4j
@Service
public class ProcessingBrokerImpl implements ProcessingBroker {

    @Autowired
    CommandRepository commandRepository;

    @Autowired
    S3toHdfsTransferJobLauncher s3toHdfsTransferJobLauncher;

    @Autowired
    OagResourcesPreprocJobLauncher oagResourcesPreprocJobLauncher;

    @Autowired
    TrainingsPreprocJobLauncher trainingsPreprocJobLauncher;

    @Autowired
    MarketplaceResourcesPreprocJobLauncher marketplaceResourcesPreprocJobLauncher;

    @Autowired
    SparkJobFinalStatusForwardingListener.SparkFinalStatusListener sparkFinalStatusPublisher;

    @Autowired
    DbUpdateJobLauncher dbUpdateJobLauncher;

    public void execute(Command command, Boolean isStartedManually, Long predecessorCommandId) {
        CommandEntity commandEntity = new CommandEntity(Instant.now().getEpochSecond(),SparkAppHandle.State.UNKNOWN.name(),
                command.getClass().getSimpleName(),isStartedManually,transformCommandToByteArray(command),
                getPredecessorCommand(predecessorCommandId));
        CommandEntity savedCommandEntity = commandRepository.saveAndFlush(commandEntity);
        List<SparkAppHandle.Listener> listeners = new ArrayList<>();
        // add trigger for processing
        if(command instanceof TransferAndRunProcessingCommand){
            listeners.add(
                    new SparkJobProcessingTrigger(
                            this,
                            savedCommandEntity.getId(),
                            ((TransferAndRunProcessingCommand) command).getDestDir(),
                            ((TransferAndRunProcessingCommand) command).getPreviousDumpDir()));
        }
        // add trigger to reflect changes in database
        if(!isStartedManually && (
                command instanceof OagProcessingCommand ||
                command instanceof TrainingProcessingCommand ||
                command instanceof MarketplaceProcessingCommand)) {
            listeners.add(
                    new SparkJobUpdaterTrigger(
                            this,
                            savedCommandEntity.getId(),
                            extractDumpDir(command),
                            command.getResourceType()));
        }
        listeners.add(
                new SparkJobStatusUpdateListener(
                        this,
                        savedCommandEntity.getId()));
        listeners.add(
                new SparkJobFinalStatusForwardingListener(
                        sparkFinalStatusPublisher,
                        commandEntity.getType(),
                        command.getResourceType()));
        listeners.add(
                new SparkJobLoggingListener());
        execute(command,listeners.toArray(new SparkAppHandle.Listener[0]));
    }

    private void execute(Command command, SparkAppHandle.Listener... listeners) {
        if(command instanceof S3ToHdfsTransferCommand ||
                command instanceof TransferAndRunProcessingCommand)
            command.execute(s3toHdfsTransferJobLauncher,listeners);
        else if(command instanceof OagProcessingCommand)
            command.execute(oagResourcesPreprocJobLauncher,listeners);
        else if(command instanceof TrainingProcessingCommand)
            command.execute(trainingsPreprocJobLauncher,listeners);
        else if(command instanceof MarketplaceProcessingCommand)
            command.execute(marketplaceResourcesPreprocJobLauncher,listeners);
        else if (command instanceof DbUpdateCommand) {
            command.execute(dbUpdateJobLauncher,listeners);
        } else{
            log.error("Unknown launcher.");
        }
    }

    private String extractDumpDir(Command command){
        if(command instanceof OagProcessingCommand)
            return ((OagProcessingCommand) command).getOutputDir();
        else if(command instanceof TrainingProcessingCommand)
            return ((TrainingProcessingCommand) command).getOutputDir();
        else if(command instanceof MarketplaceProcessingCommand)
            return ((MarketplaceProcessingCommand) command).getOutputDir();
        log.error("Unknown command.");
        return null;
    }

    private CommandEntity getPredecessorCommand(Long predecessorCommandId){
        CommandEntity predecessorCommand = null;
        if(predecessorCommandId!=null){
            predecessorCommand = commandRepository.getReferenceById(predecessorCommandId);
        }
        return predecessorCommand;
    }

    public void updateCommand(Long commandId, String processingId, SparkAppHandle.State status){
        Optional<CommandEntity> optionalCommand = commandRepository.findById(commandId);
        if(optionalCommand.isPresent()){
            CommandEntity command = optionalCommand.get();
            if(processingId!=null){
                command.setProcessingId(processingId);
            }
            command.setStatus(status.name());
            commandRepository.save(command);
        }else{
            log.warn("Update of command status failed! CommandId: "+commandId);
        }
    }

    private byte[] transformCommandToByteArray(Command command){
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try {
            ObjectOutputStream oos = new ObjectOutputStream(baos);
            oos.writeObject(command);
            oos.close();
        } catch (IOException e){
            throw new RuntimeException(e);
        }
        return baos.toByteArray();
    }

}
