package pl.psnc.eosc.rs.preprocessor.app.context.oag.job;

import org.apache.spark.launcher.SparkAppHandle;
import org.apache.spark.launcher.SparkLauncher;
import org.springframework.stereotype.Service;
import pl.psnc.eosc.rs.preprocessor.app.commons.spark.SparkJobLauncherAbstract;
import pl.psnc.eosc.rs.preprocessor.app.commons.spark.SparkLauncherConfigProperties;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service
public class S3toHdfsTransferJobLauncher extends SparkJobLauncherAbstract {

    static final String MODE_1 = "1";
    static final String MODE_2 = "2";

    public S3toHdfsTransferJobLauncher(SparkLauncherConfigProperties launcherConfig, S3toHdfsJobLaunchConfigProperties jobConfig) {
        super(launcherConfig, jobConfig,
                Map.of("spark.hadoop.fs.s3a.access.key",jobConfig.getS3AccessKey(),
                        "spark.hadoop.fs.s3a.secret.key",jobConfig.getS3SecretKey(),
                        "spark.hadoop.fs.s3a.endpoint",jobConfig.getS3Endpoint(),
                        "spark.hadoop.fs.s3a.list.version",jobConfig.getS3ListVersion()));
    }

    @Override
    protected void postInit(SparkLauncher sparkLauncher) {

    }


    public void runAsync(Map<String,String> fileCopyMap, SparkAppHandle.Listener... listeners){
        List<String> args = new ArrayList<>();

        List<String> srcFiles = new ArrayList<>();
        List<String> destPaths = new ArrayList<>();
        fileCopyMap.forEach((s,d) -> {srcFiles.add(s); destPaths.add(d);});

        args.addAll(srcFiles);
        args.addAll(destPaths);
        args.add(MODE_2);

        this.launchAsync(args.toArray(String[]::new), listeners);
    }

    public void runAsync(List<String> srcFiles, String destDir, SparkAppHandle.Listener... listeners){
        List<String> args = new ArrayList<>();
        args.addAll(srcFiles);
        args.add(destDir);
        args.add(MODE_1);

        this.launchAsync(args.toArray(String[]::new), listeners);
    }
}
