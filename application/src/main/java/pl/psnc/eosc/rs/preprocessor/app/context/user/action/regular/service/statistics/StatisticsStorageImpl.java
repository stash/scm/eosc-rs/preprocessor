package pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular.service.statistics;

import lombok.Getter;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;
import pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular.model.internal.Resource;
import pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular.model.statistics.internal.ServicesSet;

import java.util.HashMap;

@Getter
@Primary
@Component
public class StatisticsStorageImpl implements StatisticsStorage{

    // In case of anonymous users is equal to sessionId
    private HashMap<String, ServicesSet<Resource>> usersVisits = new HashMap<>();

    private HashMap<String, ServicesSet<Resource>> usersOrders = new HashMap<>();


    private void addService(String userId, Resource resource, HashMap<String, ServicesSet<Resource>> statisticsMap) {
        if(!statisticsMap.containsKey(userId))
            statisticsMap.put(userId,new ServicesSet<Resource>());
        statisticsMap.get(userId).add(resource);
    }

    @Override
    public void addVisit(String userId, Resource resource) {
        addService(userId,resource,usersVisits);
    }

    @Override
    public void addOrder(String userId, Resource resource) {
        addService(userId,resource,usersOrders);
    }

    private void makeUserNonAnonymous(String userId, String sessionId,HashMap<String, ServicesSet<Resource>> statisticsMap) {
        ServicesSet<Resource> services = statisticsMap.remove(sessionId);
        if(services!=null){
            if(!statisticsMap.containsKey(userId))
                statisticsMap.put(userId,new ServicesSet<Resource>());
            statisticsMap.get(userId).addAll(services);
        }
    }

    @Override
    public void makeUserNonAnonymous(String userId, String sessionId) {
        makeUserNonAnonymous(userId,sessionId,usersVisits);
        makeUserNonAnonymous(userId,sessionId,usersOrders);
    }

    @Override
    public void clearMaps() {
        usersVisits = new HashMap<>();
        usersOrders = new HashMap<>();
    }
}
