package pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular.service.statistics;

public interface StatisticsPublisher {

    void calculateStats();

}
