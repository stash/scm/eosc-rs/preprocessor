package pl.psnc.eosc.rs.preprocessor.app.context.user.action.artificial.model.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular.model.statistics.database.ResourceKey;
import pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular.model.statistics.database.TotalStats;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name="artificial.artificial_stats_total_table")
@AllArgsConstructor
@Getter
@Setter
public class ArtificialTotalStatsEntity extends TotalStats {

    public ArtificialTotalStatsEntity(Short resourceType, String resourceId, Integer visits, Integer orders) {
        super(new ResourceKey(resourceType,resourceId), visits, orders);
    }
}