package pl.psnc.eosc.rs.preprocessor.app.context.user.action.generator;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
public enum ResourceEnum {
    SERVICE((short) 0,"service","services.dat"),
    SOFTWARE((short) 1,"software","software.dat"),
    PUBLICATION((short) 2,"publication","publications.dat"),
    DATASET((short) 3,"dataset","datasets.dat"),
    TRAINING((short) 4,"training","trainings.dat"),
    OTHER_RP((short) 5,"other","others.dat"),
    DATA_SOURCE((short) 7,"data source","datasource.dat");

    @Getter
    private final Short id;

    @Getter
    private final String name;

    @Getter
    private final String file;
}
