package pl.psnc.eosc.rs.preprocessor.app.cloud.jms;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Profile;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.messaging.Message;
import org.springframework.messaging.handler.annotation.Headers;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static com.google.common.base.Throwables.getStackTraceAsString;

/**
 * The JMS consumer is used to receive messages regarding changes in Marketplace database.
 */
@Profile("!jms-disabled")
@Primary
@Slf4j
@Component("resourceEventBroadcaster")
@Getter
public class ResourceConsumer implements MessageBroadcaster {

    List<Subscriber> resourceEventSubscribers = new ArrayList<>();

    public void subscribe(Subscriber subscriber) {
        resourceEventSubscribers.add(subscriber);
    }

    @JmsListener(destination = "${jms.topic.resourceEvents.name}", containerFactory = "defaultFactory")
    public void resourcePublisher(@Payload final Message message, @Headers final Map<String, Object> headers) {
        try{
            String messagePayload = JmsMessageExtractor.extractMessage(message);
            log.debug("Received resource update:\n"+messagePayload);
            resourceEventSubscribers.forEach(subscriber -> subscriber.processMessage(messagePayload));
        } catch (Exception e) {
            log.warn(e.getMessage());
        }
    }
}
