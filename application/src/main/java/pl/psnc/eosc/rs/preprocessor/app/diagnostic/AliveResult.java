package pl.psnc.eosc.rs.preprocessor.app.diagnostic;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class AliveResult {
    boolean isAlive;
    String errorMessage;

}
