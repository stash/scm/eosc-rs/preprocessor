package pl.psnc.eosc.rs.preprocessor.app.cloud.jms;

import lombok.extern.slf4j.Slf4j;
import org.fusesource.stomp.jms.StompJmsConnectionFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.jms.DefaultJmsListenerContainerFactoryConfigurer;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.jms.annotation.EnableJms;
import org.springframework.jms.config.DefaultJmsListenerContainerFactory;
import org.springframework.jms.config.JmsListenerContainerFactory;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.util.ErrorHandler;

import javax.jms.ConnectionFactory;

/**
 * Configuration for beta JMS Listener.
 */
@Profile("!integration & !test & !jms-disabled")
@Configuration
@EnableJms
@EnableConfigurationProperties(value = JmsTopicConfig.class)
public class JmsBetaConfig {

    @Value("${beta.stomp.address}")
    private String betaBrokerUrl;

    @Value(value = "${beta.stomp.username}")
    private String betaUsername;

    @Value(value = "${beta.stomp.password}")
    private String betaPassword;

    @Bean
    public ConnectionFactory betaConnectionFactory() {
        StompJmsConnectionFactory connFactory = new StompJmsConnectionFactory();
        connFactory.setBrokerURI(betaBrokerUrl);
        connFactory.setUsername(betaUsername);
        connFactory.setPassword(betaPassword);
        return connFactory;
    }

    @Bean
    public JmsTemplate betaJmsTemplate() {
        JmsTemplate jmsTemplate = new JmsTemplate();
        jmsTemplate.setConnectionFactory(betaConnectionFactory());
        return jmsTemplate;
    }

    @Bean(name = "betaFactory")
    public JmsListenerContainerFactory<?> betaFactory(
            @Qualifier("betaConnectionFactory") ConnectionFactory connectionFactory,
            DefaultJmsListenerContainerFactoryConfigurer configurer,
            ErrorHandler errorHandler) {
        DefaultJmsListenerContainerFactory factory = new DefaultJmsListenerContainerFactory();
        configurer.configure(factory, connectionFactory);
        factory.setPubSubDomain(true);
        factory.setErrorHandler(errorHandler);
        return factory;
    }
}
