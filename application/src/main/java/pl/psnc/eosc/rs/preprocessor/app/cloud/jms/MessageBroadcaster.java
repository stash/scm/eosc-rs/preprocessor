package pl.psnc.eosc.rs.preprocessor.app.cloud.jms;

public interface MessageBroadcaster {

    void subscribe(Subscriber subscriber);

}
