package pl.psnc.eosc.rs.preprocessor.app.buildinfo;

public class RecentCommit {
    private final String id;
    private final String message;

    public RecentCommit(String id, String message) {
        this.id = id;
        this.message = message;
    }

    public String getId() {
        return id;
    }

    public String getMessage() {
        return message;
    }
}

