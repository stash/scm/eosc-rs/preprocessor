package pl.psnc.eosc.rs.preprocessor.app.context.resource.synchronization.training;

import pl.psnc.eosc.rs.preprocessor.app.context.resource.synchronization.service.model.ServiceEvent;

public interface TrainingChangesPublisher {
    void publish(TrainingEvent trainingEvent);
}
