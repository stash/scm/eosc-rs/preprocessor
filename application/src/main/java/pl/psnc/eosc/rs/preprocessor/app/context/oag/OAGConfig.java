package pl.psnc.eosc.rs.preprocessor.app.context.oag;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import pl.psnc.eosc.rs.preprocessor.app.commons.spark.SparkJobLaunchConfigProperties;

@Configuration
@EnableConfigurationProperties
public class OAGConfig {

    @Bean(name = "oagResourcesPreprocJobLaunchConfig")
    @ConfigurationProperties(prefix = "spark.job.oag-resources")
    public SparkJobLaunchConfigProperties oagResourcesPreprocJobLaunchConfig() {
        return new SparkJobLaunchConfigProperties();
    }

}
