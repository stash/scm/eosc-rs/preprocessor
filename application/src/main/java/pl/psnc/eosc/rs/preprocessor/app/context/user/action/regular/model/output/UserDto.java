package pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular.model.output;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserDto {

    private String Id;

    private Boolean isAnonymous;

    private Integer numberOfSessions;

    private List<SessionDto> sessions = new ArrayList<>();
}
