package pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular.model.statistics.database;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular.model.internal.Resource;

import javax.persistence.EmbeddedId;
import javax.persistence.MappedSuperclass;

@MappedSuperclass
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class TotalStats implements Comparable<TotalStats>{

    @EmbeddedId
    protected ResourceKey resource;

    protected Integer visits;

    protected Integer orders;

    public Resource toResource(){
        return new Resource(resource.getResourceType(),resource.getResourceId());
    }

    @Override
    public int compareTo(TotalStats other) {
        int compareType = resource.getResourceType().compareTo(
                other.getResource().getResourceType());
        if (compareType != 0) {
            return compareType;
        }
        return resource.getResourceId().compareTo(
                other.getResource().getResourceId());
    }

}
