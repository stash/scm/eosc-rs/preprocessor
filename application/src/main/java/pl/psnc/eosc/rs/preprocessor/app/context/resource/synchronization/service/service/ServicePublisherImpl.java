package pl.psnc.eosc.rs.preprocessor.app.context.resource.synchronization.service.service;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import pl.psnc.eosc.rs.preprocessor.app.cloud.kafka.Producer;
import pl.psnc.eosc.rs.preprocessor.app.context.resource.synchronization.service.model.ServiceEvent;

@Component
public class ServicePublisherImpl implements ServiceChangesPublisher {

    private String topic;

    Producer producer;

    Gson gson = new GsonBuilder()
            .setDateFormat("yyyy-MM-dd hh:mm:ss.S")
            .create();

    @Autowired
    public ServicePublisherImpl(Producer producer,
                                @Value(value = "${sync.service.kafka-topic}") String topic){
        this.producer = producer;
        this.topic = topic;
    }


    @Override
    public void publish(ServiceEvent service) {
        String json = gson.toJson(service);
        producer.publish(topic,service.getId(),json);
    }
}
