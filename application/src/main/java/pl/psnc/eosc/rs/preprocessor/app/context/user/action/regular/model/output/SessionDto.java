package pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular.model.output;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class SessionDto {

    private String sessionId;

    private Boolean isAnonymous;

    private String aaiUid;

    private String marketplaceId;

    private Integer numberOfUA;

    private List<UserActionDto> userActions = new ArrayList<>();

}
