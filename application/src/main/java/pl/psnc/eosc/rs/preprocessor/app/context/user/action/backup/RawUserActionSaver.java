package pl.psnc.eosc.rs.preprocessor.app.context.user.action.backup;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;
import pl.psnc.eosc.rs.preprocessor.app.cloud.jms.MessageBroadcaster;

import javax.annotation.PostConstruct;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.Validator;
import java.time.Instant;
import java.util.Set;

/**
 * Save and validate (raw - non-deserialized string only) all User Actions.
 */
@Slf4j
@Profile("backup-raw-jms")
@Component
public class RawUserActionSaver implements BaseBackup{

    @Autowired
    RawUserActionRepository rawUaRepository;

    @Autowired
    @Qualifier("userActionBroadcaster")
    MessageBroadcaster broadcaster;

    @Autowired
    Validator validator;

    @PostConstruct
    public void subscribeJms() {
        broadcaster.subscribe(this);
    }

    @Override
    public void processMessage(String rawMessage) {
        log.debug("Backing up raw user action");
        save(rawMessage);
    }

    @Override
    public void save(String userAction) {
        UserActionBackupEntity userActionEntity = new UserActionBackupEntity(Instant.now().getEpochSecond(),userAction);
        validate(userActionEntity);
        rawUaRepository.save(userActionEntity);
    }

    private void validate(UserActionBackupEntity userActionEntity){
        Set<ConstraintViolation<UserActionBackupEntity>> violations = validator.validate(userActionEntity);
        if (!violations.isEmpty()) {
            StringBuilder sb = new StringBuilder();
            for (var constraintViolation : violations) {
                sb.append(constraintViolation.getMessage());
            }
            throw new ConstraintViolationException("Error occurred: " + sb, violations);
        }
    }


}
