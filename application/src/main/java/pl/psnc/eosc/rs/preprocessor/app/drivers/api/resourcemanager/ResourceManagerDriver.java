package pl.psnc.eosc.rs.preprocessor.app.drivers.api.resourcemanager;

import java.util.List;
import java.util.Optional;

public interface ResourceManagerDriver {

    Optional<ApplicationStatus> getApp(String jobId);
    List<ApplicationStatus> listApps();

}
