package pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular.model.internal;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Root {

    @NotBlank
    private String type;

    private String panelId;

    private Integer position;

}
