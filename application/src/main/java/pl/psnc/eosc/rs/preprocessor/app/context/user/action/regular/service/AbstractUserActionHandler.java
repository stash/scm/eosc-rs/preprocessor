package pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular.service;

import lombok.extern.slf4j.Slf4j;
import pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular.model.internal.SingleSession;

@Slf4j
public abstract class AbstractUserActionHandler {

    protected AbstractUserActionHandler nextHandler;

    public AbstractUserActionHandler getNextHandler() { return nextHandler; }

    public void setNextHandler(AbstractUserActionHandler nextHandler){
        this.nextHandler=nextHandler;
    }

    public void handleUserAction(SingleSession userAction){
        SingleSession result = processUserAction(userAction);

        if(nextHandler!=null && result!=null){
            nextHandler.handleUserAction(result);
        }
    }

    abstract protected SingleSession processUserAction(SingleSession userAction);

}
