package pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular.model.statistics.internal;

import lombok.NoArgsConstructor;

import java.util.Collection;
import java.util.HashSet;

@NoArgsConstructor
public class ServicesSet<E> extends HashSet<E>{

    public ServicesSet(Collection<? extends E> c) {
        super(c);
    }
}
