package pl.psnc.eosc.rs.preprocessor.app.context.user.action.generator;

import org.springframework.stereotype.Component;

import java.util.Random;

@Component
public class RandomUserProvider implements UserProvider {

    Random random = new Random();

    @Override
    public String pickUser() {
        return String.valueOf(random.nextInt(4000));
    }
}
