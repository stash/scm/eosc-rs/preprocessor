package pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular.model;

import pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular.model.internal.UserAction;
import pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular.model.output.UserActionDto;

import java.sql.Timestamp;
import java.util.List;
import java.util.stream.Collectors;

public class Mapper {

    public static List<UserActionDto> toUserActionDtoList(List<UserAction> userActions) {
        return userActions.stream().map(userAction -> {
                    String rootType = userAction.getRoot()==null?null:userAction.getRoot().getType();
                    String rootPanel= userAction.getRoot()==null?null:userAction.getRoot().getPanelId();
                    String resourceType = userAction.getResourceType()==null?"service":userAction.getResourceType();
                    return new UserActionDto(
                            Timestamp.valueOf(userAction.getTimestamp()),
                            userAction.getTargetPageId(),
                            resourceType,
                            userAction.getResourceId(),
                            rootType,
                            rootPanel,
                            userAction.getActionType(),
                            userAction.getIsOrderAction());
                })
                .collect(Collectors.toList());
    }
}
