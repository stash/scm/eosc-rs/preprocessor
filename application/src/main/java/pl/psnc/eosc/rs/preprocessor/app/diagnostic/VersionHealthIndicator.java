package pl.psnc.eosc.rs.preprocessor.app.diagnostic;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.actuate.health.HealthIndicator;
import org.springframework.stereotype.Component;
import pl.psnc.eosc.rs.preprocessor.app.buildinfo.Buildinfo;

@Component
public class VersionHealthIndicator implements HealthIndicator {

    @Autowired
    Buildinfo buildinfo;

    @Override
    public Health health() {
        Health.Builder status = Health.up();
        status.withDetail("version",buildinfo.getCommitInfo());
        return status.build();
    }
}
