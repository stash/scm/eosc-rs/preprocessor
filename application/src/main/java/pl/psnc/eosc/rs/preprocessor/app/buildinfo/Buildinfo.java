package pl.psnc.eosc.rs.preprocessor.app.buildinfo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.stream.Collectors;

@Component
public class Buildinfo {

    static public final String PROPERTIES_FILE_NAME = "git-pozidapi.properties";
    static public final String RECENTCOMMITS_FILE_NAME = "recentcommits";
    static private final Logger log = LoggerFactory.getLogger(Buildinfo.class);

    private final Properties properties;
    private final List<RecentCommit> recentCommits;

    Buildinfo() {
        this(RECENTCOMMITS_FILE_NAME);
    }

    Buildinfo(String recentcommitsFilE) {
        properties = loadProperties();
        recentCommits = loadRecentCommits(recentcommitsFilE);
    }

    public String getCommitInfo() {
        return properties.getProperty("git.commit.id.describe");
    }

    public List<RecentCommit> getRecentCommits() {
        return recentCommits;
    }

    private Properties loadProperties() {
        try {
            InputStream inputStream = this.getClass().getResourceAsStream("/" + PROPERTIES_FILE_NAME);
            if (inputStream == null) {
                log.error("Buildinfo file could not be found: " + PROPERTIES_FILE_NAME);
                return new Properties();
            }
            Properties p = new Properties();
            p.load(inputStream);
            return p;
        } catch (IOException e) {
            log.error("Buildinfo could not be loaded from " + PROPERTIES_FILE_NAME, e);
            return new Properties();
        }
    }

    private List<RecentCommit> loadRecentCommits(String recentcommitsFilE) {
        try {
            URL url = this.getClass().getResource("/" + recentcommitsFilE);
            if (url == null) {
                return new ArrayList<>();
            }
            return Files
                    .readAllLines(Paths.get(url.toURI()))
                    .stream().map(this::lineToRecentCommit)
                    .collect(Collectors.toList());
        } catch (IOException | URISyntaxException e) {
            return new ArrayList<>();
        }
    }

    private RecentCommit lineToRecentCommit(String line) {
        String id = line.substring(0, 7);
        String message = line.substring(7);
        return new RecentCommit(id, message);
    }
}