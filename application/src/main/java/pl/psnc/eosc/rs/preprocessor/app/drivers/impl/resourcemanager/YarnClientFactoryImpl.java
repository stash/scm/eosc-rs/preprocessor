package pl.psnc.eosc.rs.preprocessor.app.drivers.impl.resourcemanager;

import org.apache.hadoop.yarn.client.api.YarnClient;
import org.apache.hadoop.yarn.conf.YarnConfiguration;

public class YarnClientFactoryImpl implements YarnClientFactory {

    YarnConfiguration configuration;

    public YarnClientFactoryImpl(YarnConfiguration configuration) {
        this.configuration = configuration;
    }

    public YarnClient create() {
        YarnClient yarnClient = YarnClient.createYarnClient();
        yarnClient.init(configuration);
        return yarnClient;
    }

}
