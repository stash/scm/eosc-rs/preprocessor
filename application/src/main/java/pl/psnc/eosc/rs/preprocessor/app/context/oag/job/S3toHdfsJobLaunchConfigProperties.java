package pl.psnc.eosc.rs.preprocessor.app.context.oag.job;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import pl.psnc.eosc.rs.preprocessor.app.commons.spark.SparkJobLaunchConfigProperties;

@Data
@Configuration
@ConfigurationProperties(prefix = "spark.job.s3tohdfs")
public class S3toHdfsJobLaunchConfigProperties extends SparkJobLaunchConfigProperties {

    String s3AccessKey;
    String s3SecretKey;
    String s3Endpoint;
    String s3ListVersion;

}
