package pl.psnc.eosc.rs.preprocessor.app.context.user.action.generator;

import java.util.Random;

public class Commons {

    public static <T extends Enum<?>> T randomEnum(Class<T> clazz){
        Random random = new Random();
        int x = random.nextInt(clazz.getEnumConstants().length);
        return clazz.getEnumConstants()[x];
    }
}
