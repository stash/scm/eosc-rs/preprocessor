package pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular.service.validator;

import pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular.model.internal.SingleSession;

public interface ValidatorHandler {

    boolean isValid(SingleSession singleSession);
}
