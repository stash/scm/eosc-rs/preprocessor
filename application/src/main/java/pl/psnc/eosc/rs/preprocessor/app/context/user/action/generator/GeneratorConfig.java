package pl.psnc.eosc.rs.preprocessor.app.context.user.action.generator;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;

@Configuration
public class GeneratorConfig {


    @Bean
    @DependsOn({"randomSessionRandomUserProvider"})
    UserActionGeneratorImpl randomUaArtificialResourceGenerator(@Qualifier("artificialResourceProvider") ResourceProvider resourceProvider,
                                                                @Qualifier("randomSessionRandomUserProvider")SessionProvider sessionProvider){
        return new UserActionGeneratorImpl(resourceProvider,sessionProvider);
    }

    @Bean
    SessionProvider randomSessionRandomUserProvider(@Qualifier("randomUserProvider") UserProvider userProvider){
        return new RandomSessionProvider(userProvider);
    }

    @Bean
    @DependsOn({"randomSessionExistingUserProvider"})
    UserActionGeneratorImpl randomUaExistingResourceGenerator(@Qualifier("randomResourceProvider") ResourceProvider resourceProvider,
                                                              @Qualifier("randomSessionExistingUserProvider")SessionProvider sessionProvider){
        return new UserActionGeneratorImpl(resourceProvider,sessionProvider);
    }

    @Bean
    SessionProvider randomSessionExistingUserProvider(@Qualifier("randomExistingUserProvider") UserProvider userProvider){
        return new RandomSessionProvider(userProvider);
    }
}
