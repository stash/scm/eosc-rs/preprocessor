package pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular.service.statistics;

import pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular.model.internal.Resource;
import pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular.model.statistics.internal.ServicesSet;

import java.util.HashMap;

public interface StatisticsStorage {

    HashMap<String, ServicesSet<Resource>> getUsersVisits();

    HashMap<String, ServicesSet<Resource>> getUsersOrders();

    void addVisit(String userId, Resource resource);

    void addOrder(String userId, Resource resource);

    void makeUserNonAnonymous(String userId, String sessionId);

    void clearMaps();
}
