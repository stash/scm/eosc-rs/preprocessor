package pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;
import pl.psnc.eosc.rs.preprocessor.app.cloud.jms.MessageBroadcaster;
import pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular.model.internal.SingleSession;
import pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular.service.session.SessionManagerHandler;
import pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular.service.statistics.StatisticsHandler;
import pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular.service.user.UserDataSupplier;
import pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular.service.user.UserRecognitionHandler;
import pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular.service.validator.ValidatorHandler;

import javax.annotation.PostConstruct;

@Slf4j
@Primary
@Service
public class UserActionServiceImpl implements UserActionService {

    MessageConverter messageConverter;

    UserDataSupplier userIdMappingHandler;

    SessionManagerHandler sessionManagerHandler;

    StatisticsHandler statisticsHandler;

    ValidatorHandler validatorHandler;

    UserRecognitionHandler userRecognitionHandler;

    MessageBroadcaster broadcaster;

    AbstractUserActionHandler starterHandler;

    @Autowired
    public UserActionServiceImpl(MessageConverter messageConverter, UserDataSupplier userIdMappingHandler,
                                 SessionManagerHandler sessionManagerHandler, StatisticsHandler statisticsHandler,
                                 ValidatorHandler validatorHandler, UserRecognitionHandler userRecognitionHandler,
                                 @Qualifier("userActionBroadcaster") MessageBroadcaster broadcaster) {
        this.messageConverter = messageConverter;
        this.userIdMappingHandler = userIdMappingHandler;
        this.sessionManagerHandler = sessionManagerHandler;
        this.statisticsHandler = statisticsHandler;
        this.validatorHandler = validatorHandler;
        this.userRecognitionHandler = userRecognitionHandler;
        this.broadcaster = broadcaster;
    }

    @PostConstruct
    public void constructChain() {
        AbstractUserActionHandler validator = (AbstractUserActionHandler) validatorHandler;
        AbstractUserActionHandler userIdMapper = (AbstractUserActionHandler) userIdMappingHandler;
        AbstractUserActionHandler userRecognizer = (AbstractUserActionHandler) userRecognitionHandler;
        AbstractUserActionHandler session = (AbstractUserActionHandler) sessionManagerHandler;
        AbstractUserActionHandler stats = (AbstractUserActionHandler) statisticsHandler;
        // Chain order
        starterHandler = validator;
        validator.setNextHandler(userIdMapper);
        userIdMapper.setNextHandler(userRecognizer);
        userRecognizer.setNextHandler(session);
        session.setNextHandler(stats);
    }

    @PostConstruct
    public void subscribeJms() {
        broadcaster.subscribe(this);
    }

    @Override
    public void processMessage(String message) {
        try {
            SingleSession singleSession = messageConverter.messageToSingleSession(message);
            if(singleSession!=null) starterHandler.handleUserAction(singleSession);
        } catch (Exception e) {
            log.warn("Exception occurs during processing UA "+e.getMessage());
        }
    }
}
