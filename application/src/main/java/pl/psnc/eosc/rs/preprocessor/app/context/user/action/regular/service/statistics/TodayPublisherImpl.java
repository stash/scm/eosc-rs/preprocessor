package pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular.service.statistics;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Primary;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import pl.psnc.eosc.rs.preprocessor.app.context.user.action.artificial.model.entity.ArtificialTodayStatsEntity;
import pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular.model.internal.Resource;
import pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular.model.statistics.database.TodayStats;
import pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular.model.statistics.database.TodayStatsEntity;
import pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular.model.statistics.database.UserResourceDayKey;
import pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular.model.statistics.internal.ServicesSet;
import pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular.repository.AbstractStatsTodayRepository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Slf4j
@Primary
@Component
public class TodayPublisherImpl implements TodayPublisher{

    AbstractStatsTodayRepository todayRepository;

    StatisticsStorage statisticsStorage;

    DayProvider dayProvider;

    Class<? extends TodayStats> clazz;

    @Autowired
    public <T extends TodayStats> TodayPublisherImpl(@Qualifier("statsTodayRepository") AbstractStatsTodayRepository<TodayStatsEntity> todayRepository,
                                                     StatisticsStorage statisticsStorage,
                                                     DayProvider dayProvider){
        this.todayRepository = todayRepository;
        this.statisticsStorage = statisticsStorage;
        this.dayProvider = dayProvider;
        this.clazz = TodayStatsEntity.class;
    }

    public <T extends TodayStats> TodayPublisherImpl(AbstractStatsTodayRepository<T> todayRepository,
                              StatisticsStorage statisticsStorage,
                              DayProvider dayProvider,
                              Class <T> clazz){
        this.todayRepository = todayRepository;
        this.statisticsStorage = statisticsStorage;
        this.dayProvider = dayProvider;
        this.clazz = clazz;
    }

    @Override
    @Scheduled(cron = "${user-action.statistics.cron.today-update}", zone = "UTC")
    public void updateToday(){
        log.debug("Updating today visits and orders for "+clazz.getSimpleName());
        updateToday(clazz);
    }

    public <T extends TodayStats> void updateToday(Class<T> todayStatsType){
        HashMap<String, ServicesSet<Resource>> usersVisits = statisticsStorage.getUsersVisits();
        HashMap<String, ServicesSet<Resource>> usersOrders = statisticsStorage.getUsersOrders();
        statisticsStorage.clearMaps();

        Integer day = dayProvider.getCurrentDay();
        List<UserResourceDayKey> keys = new ArrayList<>();
        List<T> aggregatedServices = new ArrayList<>();
        usersVisits.forEach((user,resources)->{
            for(Resource resource : resources) {
                UserResourceDayKey key = new UserResourceDayKey(user,resource.getResourceType(),resource.getResourceId(),day);
                keys.add(key);
            }
        });

        Map<UserResourceDayKey,TodayStats> oldTodayStats = new HashMap<>();
        todayRepository.findAllById(keys).forEach(
                todayStatsEntity -> oldTodayStats.put(((TodayStats)todayStatsEntity).getId(),(TodayStats)todayStatsEntity));

        usersVisits.forEach((user,resources)->{
            for(Resource resource : resources) {
                boolean isOrdered =
                        (usersOrders.get(user)!=null && usersOrders.get(user).contains(resource));
                UserResourceDayKey key = new UserResourceDayKey(user,resource.getResourceType(),resource.getResourceId(),day);
                if(oldTodayStats.containsKey(key)){
                    // check only order
                    // the service was included in the list so it had to be visited
                    isOrdered = oldTodayStats.get(key).getOrdered() || isOrdered;
                }
                if(todayStatsType==ArtificialTodayStatsEntity.class){
                    ArtificialTodayStatsEntity todayStats = new ArtificialTodayStatsEntity(
                            key,
                            true,
                            isOrdered);
                    aggregatedServices.add((T)todayStats);
                }
                if(todayStatsType==TodayStatsEntity.class){
                    TodayStatsEntity todayStats = new TodayStatsEntity(
                            key,
                            true,
                            isOrdered);
                    aggregatedServices.add((T)todayStats);
                }
            }
        });
        todayRepository.saveAll(aggregatedServices);
        log.debug("Today table is updated. Number of aggregated today resources: "+aggregatedServices.size());
    }
}

