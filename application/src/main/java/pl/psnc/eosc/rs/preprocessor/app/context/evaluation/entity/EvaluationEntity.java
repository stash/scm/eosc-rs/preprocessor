package pl.psnc.eosc.rs.preprocessor.app.context.evaluation.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.Size;

@Entity
@Table(name="recommendation.evaluation_table")
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class EvaluationEntity {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    Long id;

    Long timestamp;

    @Column(name="raw_evaluation_event")
    String rawEvaluationEvent;

    public EvaluationEntity(long timestamp, String rawRecommendationEvent) {
        this.timestamp = timestamp;
        this.rawEvaluationEvent = rawRecommendationEvent;
    }
}
