package pl.psnc.eosc.rs.preprocessor.app.context.resource.synchronization.training;

import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class TrainingEvent {

    @JsonProperty(value="eventType",access=JsonProperty.Access.READ_ONLY)
    private String eventType;

    @JsonProperty("id")
    private String id;

    @JsonProperty("title")
    private String title;

    @JsonProperty("description")
    private String description;

    @JsonAlias("accessRights")
    @JsonProperty("access_rights")
    private String accessRights;

    @JsonProperty("languages")
    private List<String> languages;

    @JsonAlias("scientificDomains")
    @JsonProperty("scientific_domains")
    private List<Object> scientificDomains;

    public TrainingEvent(String id, String title, String description, String accessRights, List<String> languages, List<Object> scientificDomains) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.accessRights = accessRights;
        this.languages = languages;
//        this.scientificDomains = scientificDomains;
    }
}
