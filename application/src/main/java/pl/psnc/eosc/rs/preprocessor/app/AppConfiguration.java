package pl.psnc.eosc.rs.preprocessor.app;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.ConfigurationPropertiesScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import pl.psnc.eosc.rs.preprocessor.app.cloud.jms.JmsConfig;
import pl.psnc.eosc.rs.preprocessor.app.commons.metrics.MetricRegistryConfig;
import pl.psnc.eosc.rs.preprocessor.app.commons.spark.SparkLauncherConfigProperties;
import pl.psnc.eosc.rs.preprocessor.app.context.oag.OAGConfig;
import pl.psnc.eosc.rs.preprocessor.app.drivers.DriversConfig;

@Configuration
@EnableJpaRepositories
@Import({OAGConfig.class,
        DriversConfig.class,
        JmsConfig.class,
        MetricRegistryConfig.class})
@ConfigurationPropertiesScan
public class AppConfiguration {

    // Spark launcher application.properties bindings
    @Bean
    @ConfigurationProperties(prefix = "spark.launcher")
    public SparkLauncherConfigProperties sparkLauncherConfigProperties() {
        return new SparkLauncherConfigProperties();
    }


}
