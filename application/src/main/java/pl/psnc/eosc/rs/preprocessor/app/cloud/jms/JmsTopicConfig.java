package pl.psnc.eosc.rs.preprocessor.app.cloud.jms;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

@Primary
@Component
@ConfigurationProperties(prefix="jms.topic")
@Getter
@Setter
public class JmsTopicConfig {
    JmsTopic statisticsVisits;
    JmsTopic statisticsOrders;
    JmsTopic userActions;
    JmsTopic artificialUserActions;
    JmsTopic evaluations;
    JmsTopic recommendations;

}
