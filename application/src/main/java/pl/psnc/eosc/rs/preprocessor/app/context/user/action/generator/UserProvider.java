package pl.psnc.eosc.rs.preprocessor.app.context.user.action.generator;

public interface UserProvider {
    String pickUser();
}
