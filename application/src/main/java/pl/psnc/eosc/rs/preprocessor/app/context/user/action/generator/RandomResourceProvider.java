package pl.psnc.eosc.rs.preprocessor.app.context.user.action.generator;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import pl.psnc.eosc.rs.preprocessor.app.commons.error.NullOrEmptyException;
import pl.psnc.eosc.rs.preprocessor.app.context.user.action.generator.model.GeneratedResource;
import pl.psnc.eosc.rs.preprocessor.app.context.user.action.generator.model.ResourceIds;

import java.util.Random;

import static pl.psnc.eosc.rs.preprocessor.app.context.user.action.generator.Commons.randomEnum;

@Component
public class RandomResourceProvider implements ResourceProvider {

    @Autowired
    @Qualifier("resourceDataset")
    ResourceIds datasets;

    @Autowired
    @Qualifier("resourcePublication")
    ResourceIds publications;

    @Autowired
    @Qualifier("resourceService")
    ResourceIds services;

    @Autowired
    @Qualifier("resourceSoftware")
    ResourceIds software;

    @Autowired
    @Qualifier("resourceTraining")
    ResourceIds trainings;

    @Autowired
    @Qualifier("resourceOther")
    ResourceIds others;

    @Autowired
    @Qualifier("resourceDatasource")
    ResourceIds datasource;

    Random rand = new Random();

    @Override
    public GeneratedResource getResource() {
        ResourceEnum resource = randomEnum(ResourceEnum.class);
        String resourceId = null;
        String resourceType = null;
        if(ResourceEnum.DATASET.equals(resource)){
            var resourceList = datasets.getResourceIds();
            resourceType = ResourceEnum.DATASET.getName();
            resourceId = resourceList.get(rand.nextInt(resourceList.size()));
        }
        if(ResourceEnum.PUBLICATION.equals(resource)){
            var resourceList = publications.getResourceIds();
            resourceType = ResourceEnum.PUBLICATION.getName();
            resourceId = resourceList.get(rand.nextInt(resourceList.size()));
        }
        if(ResourceEnum.SERVICE.equals(resource)){
            var resourceList = services.getResourceIds();
            resourceType = ResourceEnum.SERVICE.getName();
            resourceId = resourceList.get(rand.nextInt(resourceList.size()));
        }
        if(ResourceEnum.SOFTWARE.equals(resource)){
            var resourceList = software.getResourceIds();
            resourceType = ResourceEnum.SOFTWARE.getName();
            resourceId = resourceList.get(rand.nextInt(resourceList.size()));
        }
        if(ResourceEnum.TRAINING.equals(resource)){
            var resourceList = trainings.getResourceIds();
            resourceType = ResourceEnum.TRAINING.getName();
            resourceId = resourceList.get(rand.nextInt(resourceList.size()));
        }
        if(ResourceEnum.OTHER_RP.equals(resource)){
            var resourceList = others.getResourceIds();
            resourceType = ResourceEnum.OTHER_RP.getName();
            resourceId = resourceList.get(rand.nextInt(resourceList.size()));
        }
        if(ResourceEnum.DATA_SOURCE.equals(resource)){
            var resourceList = datasource.getResourceIds();
            resourceType = ResourceEnum.DATA_SOURCE.getName();
            resourceId = resourceList.get(rand.nextInt(resourceList.size()));
        }
        if(StringUtils.isBlank(resourceId) || StringUtils.isBlank(resourceType))
            throw new NullOrEmptyException("Generated resource is null or empty.");
        return new GeneratedResource(resourceId,resourceType);
    }
}
