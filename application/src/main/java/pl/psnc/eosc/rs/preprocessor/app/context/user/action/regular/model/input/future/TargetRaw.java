package pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular.model.input.future;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class TargetRaw {

    private UUID visit_id;

    private String page_id;

}
