package pl.psnc.eosc.rs.preprocessor.app.diagnostic;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import pl.psnc.eosc.rs.preprocessor.app.commons.spark.ResourceType;
import pl.psnc.eosc.rs.preprocessor.app.context.marketplace.job.MarketplaceResourcesPreprocJobLauncher;
import pl.psnc.eosc.rs.preprocessor.app.context.oag.job.OagResourcesPreprocJobLauncher;
import pl.psnc.eosc.rs.preprocessor.app.context.oag.job.S3toHdfsTransferJobLauncher;
import pl.psnc.eosc.rs.preprocessor.app.context.spark.processing.*;
import pl.psnc.eosc.rs.preprocessor.app.context.spark.processing.command.*;
import pl.psnc.eosc.rs.preprocessor.app.context.trainings.job.TrainingsPreprocJobLauncher;

@RestController
@Controller
public class ApiController {

    @Autowired
    OagResourcesPreprocJobLauncher oagResourcesPreprocJobLauncher;

    @Autowired
    S3toHdfsTransferJobLauncher s3toHdfsTransferJobLauncher;

    @Autowired
    TrainingsPreprocJobLauncher trainingsPreprocJobLauncher;

    @Autowired
    ProcessingBroker broker;

    @Value("${spark.job.s3tohdfs.s3-uri-scheme}")
    String s3UriScheme;

    @Value("${spark.job.s3tohdfs.s3-bucket}")
    String s3BucketName;


    @PostMapping("run-marketplace-preproc-job")
    public ResponseEntity<?> runMarketplacePreproc(
            @RequestParam String inputFiles,
            @RequestParam String outputDir,
            @RequestParam(required = false,defaultValue = "") String previousDumpDir,
            @RequestParam MarketplaceResourcesPreprocJobLauncher.MarketplaceResourceType resourceType) {
        MarketplaceProcessingCommand command = MarketplaceProcessingCommand.builder()
                .inputFiles(inputFiles)
                .outputDir(outputDir)
                .previousDumpDir(previousDumpDir)
                .resourceType(resourceType)
                .build();
        broker.execute(command,true,null);
        return new ResponseEntity<>(null, HttpStatus.OK);
    }

    @PostMapping("run-trainings-preproc-job")
    public ResponseEntity<?> runTrainingsPreproc(
            @RequestParam String inputFiles,
            @RequestParam String outputDir,
            @RequestParam(required = false,defaultValue = "") String previousDumpDir) {
        TrainingProcessingCommand command = TrainingProcessingCommand.builder()
                .inputFiles(inputFiles)
                .outputDir(outputDir)
                .previousDumpDir(previousDumpDir)
                .resourceType(TrainingsPreprocJobLauncher.TrainingResourceType.TRAINING)
                .build();
        broker.execute(command,true,null);
        return new ResponseEntity<>(null, HttpStatus.OK);
    }

    @PostMapping("run-oag-preproc-job")
    public ResponseEntity<?> runOagPreproc(
            @RequestParam String inputFiles,
            @RequestParam String outputDir,
            @RequestParam(required = false,defaultValue = "") String previousDumpDir,
            @RequestParam OagResourcesPreprocJobLauncher.BasicResourceType resourceType) {
        OagProcessingCommand command = OagProcessingCommand.builder()
                .inputFiles(inputFiles)
                .outputDir(outputDir)
                .previousDumpDir(previousDumpDir)
                .resourceType(resourceType)
                .build();
        broker.execute(command,true,null);
        return new ResponseEntity<>(null, HttpStatus.OK);
    }

    @PostMapping("run-s3-to-hdfs-job")
    public ResponseEntity<?> run(
            @RequestParam(required = false) String sourceDir,
            @RequestParam(required = false) String sourceFiles,
            @RequestParam String destDir) {
        S3ToHdfsTransferCommand command = S3ToHdfsTransferCommand.builder()
                .sourceDir(sourceDir)
                .sourceFiles(sourceFiles)
                .destDir(destDir)
                .build();
        broker.execute(command,true,null);
        return new ResponseEntity<>(null, HttpStatus.OK);
    }

    public enum Type {BasicResourceType,Forest,Hills,Desert};

    @PostMapping("run-db-updater-job")
    public ResponseEntity<?> run(@RequestParam String dumpDir,
                                 @RequestParam String resourceType) throws Exception {
        DbUpdateCommand command = DbUpdateCommand.builder()
                .dumpDir(dumpDir)
                .resourceType(stringToResource(resourceType))
                .build();
        broker.execute(command,true,null);
        return new ResponseEntity<>(null, HttpStatus.OK);
    }

    @PostMapping("transfer-and-process-all-resources")
    public ResponseEntity<?> transferAndProcess(
            @RequestParam String sourceDir,
            @RequestParam(required = false) String destDir,
            @RequestParam(required = false,defaultValue = "") String previousDumpDir) {
        TransferAndRunProcessingCommand command = TransferAndRunProcessingCommand.builder()
                .sourceDir(s3UriScheme+"://"+s3BucketName+"/"+sourceDir)
                .destDir(destDir)
                .previousDumpDir(previousDumpDir)
                .build();
        broker.execute(command,true,null);
        return new ResponseEntity<>(null, HttpStatus.OK);
    }

    private ResourceType stringToResource(String resourceType) throws Exception {
        switch (resourceType){
            case "TRAINING":
                    return TrainingsPreprocJobLauncher.TrainingResourceType.valueOf(resourceType);
            case "PUBLICATION":
            case "DATASET":
            case "SOFTWARE":
            case "OTHER_RESEARCH_PRODUCT":
                return OagResourcesPreprocJobLauncher.BasicResourceType.valueOf(resourceType);
            case "DATASOURCE":
            case "SERVICE":
                MarketplaceResourcesPreprocJobLauncher.MarketplaceResourceType.valueOf(resourceType);
            default:
                throw new Exception("Invalid resource type");
        }
    }
}
