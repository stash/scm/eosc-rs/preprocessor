package pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular.service;

import pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular.model.internal.SingleSession;

public interface MessageConverter {

    SingleSession messageToSingleSession(String userActionJson);

}
