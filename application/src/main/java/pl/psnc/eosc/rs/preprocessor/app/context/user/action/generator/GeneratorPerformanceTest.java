package pl.psnc.eosc.rs.preprocessor.app.context.user.action.generator;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Profile;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular.model.input.future.UserActionRaw;

@Slf4j
@Profile("ua-performance-test")
@Component
public class GeneratorPerformanceTest implements GeneratorManager{

    @Qualifier("randomUaArtificialResourceGenerator")
    @Autowired
    UserActionGenerator userActionGenerator;

    @Qualifier("userActionDirectPublisher")
    @Autowired
    UserActionsPublisher userActionsPublisher;

    Integer generatorLoopCounter = 0;

    @Scheduled(fixedDelay=1000)
    @Override
    public void generateAndPublishUA() {
        if(generatorLoopCounter<3600)
            for(int i=0;i<101;i++){
                UserActionRaw generatedUserAction = userActionGenerator.generate();
                userActionsPublisher.publish(generatedUserAction);
            }
        generatorLoopCounter++;

    }
}
