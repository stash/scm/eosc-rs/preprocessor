package pl.psnc.eosc.rs.preprocessor.app.drivers.impl.resourcemanager;

import lombok.extern.slf4j.Slf4j;
import org.apache.hadoop.yarn.api.records.ApplicationId;
import org.apache.hadoop.yarn.api.records.ApplicationReport;
import org.apache.hadoop.yarn.api.records.YarnApplicationState;
import org.apache.hadoop.yarn.client.api.YarnClient;
import org.apache.hadoop.yarn.conf.YarnConfiguration;
import org.apache.hadoop.yarn.exceptions.YarnException;
import org.eclipse.jetty.io.RuntimeIOException;
import pl.psnc.eosc.rs.preprocessor.app.drivers.api.resourcemanager.ApplicationStatus;
import pl.psnc.eosc.rs.preprocessor.app.drivers.api.resourcemanager.ResourceManagerDriver;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

@Slf4j
public class YarnDriverImpl implements ResourceManagerDriver {

    YarnClientFactory yarnClientFactory;

    protected YarnDriverImpl(YarnClientFactory yarnClientFactory){
        this.yarnClientFactory = yarnClientFactory;
    }

    @Override
    public List<ApplicationStatus> listApps() {
        YarnClient yarnClient = yarnClientFactory.create();
        yarnClient.start();

        try {
            List<ApplicationReport> applicationReports = yarnClient.getApplications();
            return applicationReports.stream().map(this::mapToInternalApplicationStatus).collect(Collectors.toList());
        } catch (IOException e) {
            throw new RuntimeIOException(e);
        } catch (YarnException e) {
            throw new RuntimeYarnException(e);
        } finally {
            yarnClient.stop();
        }
    }

    @Override
    public Optional<ApplicationStatus> getApp(String jobId) {
        YarnClient yarnClient = yarnClientFactory.create();
        yarnClient.start();

        try {
            ApplicationReport applicationReports = yarnClient.getApplicationReport(ApplicationId.fromString(jobId));
            if(applicationReports == null) return Optional.empty();
            return Optional.of(mapToInternalApplicationStatus(applicationReports));
        } catch (IOException e) {
            throw new RuntimeIOException(e);
        } catch (YarnException e) {
            throw new RuntimeYarnException(e);
        } finally {
            yarnClient.stop();
        }
    }

    private ApplicationStatus mapToInternalApplicationStatus(ApplicationReport applicationReport){
        ApplicationStatus result = new ApplicationStatus();
        result.setId(applicationReport.getApplicationId().toString());
        result.setState(ApplicationStatus.State.valueOf(applicationReport.getYarnApplicationState().name()));
        return result;
    }

    private static Map<YarnApplicationState, ApplicationStatus.State> stateMappings = Map.of(
            YarnApplicationState.NEW, ApplicationStatus.State.NEW,
            YarnApplicationState.NEW_SAVING, ApplicationStatus.State.NEW,
            YarnApplicationState.FINISHED, ApplicationStatus.State.FINISHED,
            YarnApplicationState.RUNNING, ApplicationStatus.State.RUNNING,
            YarnApplicationState.ACCEPTED, ApplicationStatus.State.ACCEPTED,
            YarnApplicationState.KILLED, ApplicationStatus.State.KILLED,
            YarnApplicationState.SUBMITTED, ApplicationStatus.State.SUBMITTED,
            YarnApplicationState.FAILED, ApplicationStatus.State.FAILED
    );

    private ApplicationStatus.State mapToInternalState(YarnApplicationState state) {
        return stateMappings.get(state);
    }

    /**
     * Factory method
     */
    public static YarnDriverImpl create(YarnConfiguration yarnConfiguration){
        return new YarnDriverImpl(new YarnClientFactoryImpl(yarnConfiguration));
    }
}
