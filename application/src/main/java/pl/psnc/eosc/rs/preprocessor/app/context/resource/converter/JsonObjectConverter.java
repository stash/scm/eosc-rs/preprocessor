package pl.psnc.eosc.rs.preprocessor.app.context.resource.converter;

public interface JsonObjectConverter<T> {

    T convert(String json);
}
