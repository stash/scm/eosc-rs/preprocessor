package pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular.model.internal;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Session {

    private String aaiUid;

    private String marketplaceId;

    private Boolean isAnonymous;

    private List<UserAction> userActions = new ArrayList<>();

}
