package pl.psnc.eosc.rs.preprocessor.app.context.resource.synchronization.user.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;


@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@JsonIgnoreProperties(ignoreUnknown = true)
public class UserEvent {

    @JsonProperty(value="event_type",access=JsonProperty.Access.READ_ONLY)
    private String eventType;

    @JsonProperty("aai_uid")
    private String aaiId;

    @JsonProperty("id")
    private String marketplaceId;

    @JsonProperty("scientific_domains")
    private List<Integer> scientificDomainValues;

    @JsonProperty("categories")
    private List<Integer> categoryValues;

    @JsonProperty("accessed_services")
    private List<Integer> accessedServiceValues;

    public UserEvent(String aaiId, String marketplaceId, List<Integer> scientificDomainValues, List<Integer> categoryValues, List<Integer> accessedServiceValues) {
        this.aaiId = aaiId;
        this.marketplaceId = marketplaceId;
        this.scientificDomainValues = scientificDomainValues;
        this.categoryValues = categoryValues;
        this.accessedServiceValues = accessedServiceValues;
    }
}