package pl.psnc.eosc.rs.preprocessor.app.context.resource.converter;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.json.JsonMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class GenericConverterImpl<T> implements JsonObjectConverter<T> {

    final Class<T> clazz;

    ObjectMapper mapper = JsonMapper.builder()
            .addModule(new JavaTimeModule())
            .build();

    public GenericConverterImpl(Class<T> clazz) {
        this.clazz = clazz;
    }

    @Override
    public T convert(String resourceEventJson) {
        try {
            return mapper.readValue(resourceEventJson, clazz);
        } catch (JsonProcessingException e) {
            log.error("Error occurred while converting JSON message "+e.getMessage());
            return null;
        }
    }
}
