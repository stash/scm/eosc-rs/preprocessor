package pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular.model.statistics.database;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular.model.internal.Resource;

@Getter
@Setter
@NoArgsConstructor
public class ResourceCounter implements Comparable<ResourceCounter> {

    private ResourceKey resource;

    private Integer counter;

    public ResourceCounter(Short resourceType, String resourceId, long counter){
        this.resource = new ResourceKey(resourceType,resourceId);
        this.counter = Math.toIntExact(counter);
    }

    public Resource toResource(){
        return new Resource(resource.getResourceType(),resource.getResourceId());
    }

    @Override
    public int compareTo(ResourceCounter other) {
        int compareType = resource.getResourceType().compareTo(
                other.getResource().getResourceType());
        if (compareType != 0) {
            return compareType;
        }
        return resource.getResourceId().compareTo(
                other.getResource().getResourceId());
    }
}
