package pl.psnc.eosc.rs.preprocessor.app.context.recommendation;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.json.JsonMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import pl.psnc.eosc.rs.preprocessor.app.cloud.jms.MessageBroadcaster;
import pl.psnc.eosc.rs.preprocessor.app.context.recommendation.dto.RecommendationDto;
import pl.psnc.eosc.rs.preprocessor.app.context.recommendation.entity.RecommendationEntity;
import pl.psnc.eosc.rs.preprocessor.app.context.user.action.backup.BaseBackup;

import javax.annotation.PostConstruct;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.Validator;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Set;

@Slf4j
//@Profile("backup-raw-jms")
@Component
public class RecommendationSaver implements BaseBackup {
    @Autowired
    RecommendationRepository recommendationRepository;

    @Autowired
    @Qualifier("recommendationEventBroadcaster")
    MessageBroadcaster broadcaster;

    @Autowired
    Validator validator;

    ObjectMapper mapper = JsonMapper.builder()
            .addModule(new JavaTimeModule())
            .build();

    @PostConstruct
    public void subscribeJms() {
        broadcaster.subscribe(this);
    }

    @Override
    public void processMessage(String rawMessage) {
        save(rawMessage);
    }

    @Override
    public void save(String message) {
        RecommendationDto recommendationDto = null;
        try {
            recommendationDto = mapper.readValue(message, RecommendationDto.class);
        } catch (JsonProcessingException e) {
            log.error("Error occurred while converting JSON message "+e.getMessage());
        }
        assert recommendationDto != null;
        Long timestamp = null;
        if(recommendationDto.getTimestamp()!=null){
            ZonedDateTime zdt = ZonedDateTime.of(recommendationDto.getTimestamp(), ZoneId.systemDefault());
            timestamp = zdt.toInstant().toEpochMilli();
        }

        RecommendationEntity recommendationEntity = RecommendationEntity.builder()
                .aaiUid(recommendationDto.getAaiUid())
                .userId(recommendationDto.getUserId())
                .uniqueId(recommendationDto.getUniqueId())
                .clientId(recommendationDto.getClientId())
                .timestamp(timestamp)
                .panelId(recommendationDto.getPanelId())
                .engineVersion(recommendationDto.getEngineVersion())
                .recommendations(recommendationDto.getRecommendations())
                .rawRecommendationEvent(message)
                .build();
        validate(recommendationEntity);
        log.debug("Saving recommendation");
        recommendationRepository.save(recommendationEntity);
    }

    private void validate(RecommendationEntity recommendationEntity) {
        Set<ConstraintViolation<RecommendationEntity>> violations = validator.validate(recommendationEntity);
        if (!violations.isEmpty()) {
            StringBuilder sb = new StringBuilder();
            for (var constraintViolation : violations) {
                sb.append(constraintViolation.getMessage());
            }
            throw new ConstraintViolationException("Error occurred: " + sb, violations);
        }
    }
}
