package pl.psnc.eosc.rs.preprocessor.app.commons.metrics;

import com.codahale.metrics.ConsoleReporter;
import com.codahale.metrics.MetricRegistry;

import java.io.PrintStream;
import java.util.concurrent.TimeUnit;

public class ConsoleMetricWriter {

    public static void add(MetricRegistry metricRegistry) {
        ConsoleReporter reporter = ConsoleReporter.forRegistry(metricRegistry)
                .convertRatesTo(TimeUnit.SECONDS)
                .convertDurationsTo(TimeUnit.MILLISECONDS)
                .outputTo(new PrintStream(System.out))
                .build();
        reporter.start(100, TimeUnit.SECONDS);
    }
}
