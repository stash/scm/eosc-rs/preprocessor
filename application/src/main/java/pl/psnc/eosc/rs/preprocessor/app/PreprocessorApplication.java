package pl.psnc.eosc.rs.preprocessor.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;

@SpringBootApplication
@Import(AppConfiguration.class)
public class PreprocessorApplication {
	public static void main(String[] args) {
		SpringApplication.run(PreprocessorApplication.class, args);
	}
}