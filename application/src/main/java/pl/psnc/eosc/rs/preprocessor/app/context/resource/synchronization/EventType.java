package pl.psnc.eosc.rs.preprocessor.app.context.resource.synchronization;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum EventType {

        CREATE("create"),

        UPDATE("update"),

        DELETE("delete");

        String name;
}
