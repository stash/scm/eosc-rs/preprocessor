package pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular.model.input.future;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.time.LocalDateTime;
import java.util.UUID;

@Slf4j
@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserActionRaw {

    @JsonIgnore
    private Long receiptTimestamp = System.currentTimeMillis();

    @JsonProperty("user_id")
    private String marketplaceId;

    @JsonProperty("aai_uid")
    private String aaiUid;

    @JsonProperty("unique_id")
    private UUID uniqueId;

    @JsonProperty("client_id")
    private String clientId;

//    @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSZ", shape = JsonFormat.Shape.STRING)
    private LocalDateTime timestamp;

    private SourceRaw source;

    private TargetRaw target;

    private ActionRaw action;

    public String toJson(){
        ObjectMapper mapper = new ObjectMapper();
        mapper.registerModule(new JavaTimeModule());
        mapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
        String json = null;
        try {
            json = mapper.writeValueAsString(this);
        } catch (JsonProcessingException e) {
            log.error("Error occurred while converting JSON message "+e.getMessage());
        }
        return json;
    }

}
