package pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular.model;

import pl.psnc.eosc.rs.preprocessor.app.context.user.action.generator.ResourceEnum;

import java.util.Arrays;
import java.util.Objects;

public class ResourceMapper {

    public static Short resourceTypeToShort(String name){
        var resource = Arrays.stream(ResourceEnum.values())
                .filter(r -> r.getName().equals(name))
                .findFirst()
                .orElseThrow(() -> new IllegalStateException(String.format("Unsupported type of resource %s.", name)));
        return resource.getId();
    }

    public static String resourceTypeName(Short resourceTypeId){
        var resource = Arrays.stream(ResourceEnum.values())
                .filter(r -> Objects.equals(r.getId(), resourceTypeId))
                .findFirst()
                .orElseThrow(() -> new IllegalStateException(String.format("Unsupported type of resource %d.", resourceTypeId)));
        return resource.getName();
    }

}
