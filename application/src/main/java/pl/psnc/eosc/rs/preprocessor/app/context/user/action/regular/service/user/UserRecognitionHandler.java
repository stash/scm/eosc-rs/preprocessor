package pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular.service.user;

import pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular.model.internal.SingleSession;

public interface UserRecognitionHandler {

    void recognizeUser(SingleSession userAction);
}
