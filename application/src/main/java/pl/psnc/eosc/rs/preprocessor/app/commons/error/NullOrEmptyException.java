package pl.psnc.eosc.rs.preprocessor.app.commons.error;

public class NullOrEmptyException extends RuntimeException {
    public NullOrEmptyException(String errorMessage) {
        super(errorMessage);
    }
}