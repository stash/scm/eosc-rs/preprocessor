package pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular.model.statistics.database;

import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.Table;

@Entity
@Table(name="base.stats_day_table",
        indexes={
        @Index(name="day_index",columnList="day")
})
@NoArgsConstructor
public class DayStatsEntity extends DayStats {

    public DayStatsEntity(ResourceDayKey id, Integer visits, Integer orders) {
        super(id, visits, orders);
    }
}
