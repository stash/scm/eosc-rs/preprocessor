package pl.psnc.eosc.rs.preprocessor.app.cloud.jms;

public interface JmsProducer {
    void sendMessage(JmsTopic topic, String message);
}
