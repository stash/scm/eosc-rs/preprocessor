package pl.psnc.eosc.rs.preprocessor.app.context.user.action.generator;

import pl.psnc.eosc.rs.preprocessor.app.context.user.action.generator.model.GeneratedUserSession;

public interface SessionProvider {
    GeneratedUserSession getUserSession();
}
