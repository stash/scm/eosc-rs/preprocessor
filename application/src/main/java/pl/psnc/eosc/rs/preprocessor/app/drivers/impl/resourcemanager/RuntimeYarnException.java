package pl.psnc.eosc.rs.preprocessor.app.drivers.impl.resourcemanager;

public class RuntimeYarnException extends RuntimeException {

    public RuntimeYarnException() {
        super();
    }

    public RuntimeYarnException(String message) {
        super(message);
    }

    public RuntimeYarnException(Throwable cause) {
        super(cause);
    }

    public RuntimeYarnException(String message, Throwable cause) {
        super(message, cause);
    }

}
