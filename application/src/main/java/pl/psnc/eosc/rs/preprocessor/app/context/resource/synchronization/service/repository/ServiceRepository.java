package pl.psnc.eosc.rs.preprocessor.app.context.resource.synchronization.service.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.psnc.eosc.rs.preprocessor.app.context.resource.synchronization.service.model.ServiceEntity;

/**
 * Repository for synchronization Services with Marketplace DB - {@link ServiceEntity}
 */
@Repository
public interface ServiceRepository extends JpaRepository<ServiceEntity, String> { }
