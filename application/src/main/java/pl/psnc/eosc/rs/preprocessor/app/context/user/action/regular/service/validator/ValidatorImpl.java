package pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular.service.validator;

import lombok.extern.slf4j.Slf4j;
import org.hibernate.validator.messageinterpolation.ParameterMessageInterpolator;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;
import pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular.model.internal.SingleSession;
import pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular.service.AbstractUserActionHandler;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import java.util.Set;

@Slf4j
@Primary
@Component
public class ValidatorImpl extends AbstractUserActionHandler implements ValidatorHandler {

    Validator validator = Validation.byDefaultProvider().configure()
            .messageInterpolator(new ParameterMessageInterpolator())
            .buildValidatorFactory()
            .getValidator();

    @Override
    protected SingleSession processUserAction(SingleSession session) {
        if(isValid(session))
            return session;
        return null;
    }

    @Override
    public boolean isValid(SingleSession session) {
        if(session.getUserAction()==null || session.getUserAction().getRoot()==null){
            log.warn("UserAction and Root should not be null");
            return false;
        }
        Set<ConstraintViolation<SingleSession>> violations = validator.validate(session);
        violations.forEach(v -> log.warn(v.getMessageTemplate()+" "+v.getPropertyPath().toString()+" - "+v.getMessage()));
        return violations.isEmpty();
    }


}
