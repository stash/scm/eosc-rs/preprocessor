package pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular.repository;

import org.springframework.stereotype.Repository;
import pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular.model.statistics.database.TodayStatsEntity;

@Repository
public interface StatsTodayRepository extends AbstractStatsTodayRepository<TodayStatsEntity> { }
