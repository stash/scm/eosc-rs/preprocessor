package pl.psnc.eosc.rs.preprocessor.app.context.resource.synchronization.training;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class TrainingParentEvent {

    @JsonProperty("trainingResource")
    private TrainingEvent trainingResource;
}
