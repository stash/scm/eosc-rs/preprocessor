package pl.psnc.eosc.rs.preprocessor.app.context.resource.backup;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.Size;

@Entity
@Table(name="raw_backup.pc_resource_table")
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class ProviderComponentResourceEntity {

    @Id @GeneratedValue(strategy= GenerationType.IDENTITY)
    Long id;

    Long timestamp;

    String resourceType;

    String operationType;

    @Size(max=8192)
    @Column(name="raw_resource_event", length = 8192)
    String rawEvent;

    public ProviderComponentResourceEntity(long timestamp, String rawEvent, String resourceType, String operationType) {
        this.timestamp = timestamp;
        this.rawEvent = rawEvent;
        this.resourceType = resourceType;
        this.operationType = operationType;
    }
}
