package pl.psnc.eosc.rs.preprocessor.app.drivers.api.resourcemanager;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ApplicationStatus {

    String id;
    State state;

    public enum State{
        NEW(false),

        /** Application which is being saved. */
        NEW_SAVING(false),

        /** Application which has been submitted. */
        SUBMITTED(false),

        /** Application has been accepted by the scheduler */
        ACCEPTED(false),

        /** Application which is currently running. */
        RUNNING(false),

        /** Application which finished successfully. */
        FINISHED(true),

        /** Application which failed. */
        FAILED(true),

        /** Application which was terminated by a user or admin. */
        KILLED(true);

        boolean isFinal;

        State(boolean isFinal){
            this.isFinal = isFinal;
        }

        public boolean isFinal() {
            return isFinal;
        }
    }

}
