package pl.psnc.eosc.rs.preprocessor.app.commons.spark;

import org.apache.hadoop.security.UserGroupInformation;
import org.apache.spark.launcher.SparkAppHandle;
import org.apache.spark.launcher.SparkLauncher;
import pl.psnc.eosc.rs.preprocessor.app.context.spark.processing.listener.SparkJobLoggingListener;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.util.*;

public abstract class SparkJobLauncherAbstract {

    private static final String SPARK_YARN_ARCHIVE_CONF = "spark.yarn.archive";
    private static final String SPARK_JARS_IVY_CONF = "spark.jars.ivy";
    private static final String SPARK_EXECUTOR_INSTANCES_CONF = "spark.executor.instances";
    private static final String SPARK_EXECUTOR_CORES_CONF = "spark.executor.cores";
    private static final String SPARK_NETWORK_TIMEOUT_CONF = "spark.network.timeout";

    //CSV sink metrics
    private static final String SPARK_METRICS_PREFIX = "spark.metrics.conf.";
    private static final String SPARK_CSV_CLASS_CONF = SPARK_METRICS_PREFIX+"*.sink.csv.class";
    private static final String SPARK_CSV_PERIOD_CONF = SPARK_METRICS_PREFIX+"*.sink.csv.period";
    private static final String SPARK_CSV_UNIT_CONF = SPARK_METRICS_PREFIX+"*.sink.csv.unit";
    private static final String SPARK_CSV_DIR_CONF = SPARK_METRICS_PREFIX+"*.sink.csv.directory";
    private static final String SPARK_WORKER_CSV_PERIOD_CONF = SPARK_METRICS_PREFIX+"worker.sink.csv.period";
    private static final String SPARK_WORKER_CSV_UNIT_CONF = SPARK_METRICS_PREFIX+"worker.sink.csv.unit";

    SparkLauncherConfigProperties launcherConfig;
    SparkJobLaunchConfigProperties jobConfig;
    Map<String,String> additionalConf;

    public SparkJobLauncherAbstract(SparkLauncherConfigProperties launcherConfig, SparkJobLaunchConfigProperties jobConfig) {
        this.launcherConfig = launcherConfig;
        this.jobConfig = jobConfig;
    }

    public SparkJobLauncherAbstract(SparkLauncherConfigProperties launcherConfig, SparkJobLaunchConfigProperties jobConfig, Map<String,String> additionalConf) {
        this.launcherConfig = launcherConfig;
        this.jobConfig = jobConfig;
        this.additionalConf = additionalConf;
    }

    protected abstract void postInit(SparkLauncher sparkLauncher);

    protected void launchAsync(String[] args, SparkAppHandle.Listener... listeners){
        SparkLauncher sparkLauncher = new SparkLauncher();
        Integer executorInstances = Optional.ofNullable(jobConfig.getExecutorInstances())
                        .or(() -> Optional.ofNullable(launcherConfig.getDefaultExecutorInstances()))
                        .orElse(8);

        sparkLauncher.setSparkHome(launcherConfig.getSparkHome())
                .setAppResource(jobConfig.getJarFile())
                .addAppArgs(args)
                .setMainClass(jobConfig.getMainClass())
                .setMaster(launcherConfig.getDefaultMaster())
                .setDeployMode(launcherConfig.getDefaultDeployMode())
                .setConf(SPARK_YARN_ARCHIVE_CONF, launcherConfig.getSparkYarnArchive())
                .setConf(SPARK_JARS_IVY_CONF,"/tmp/.ivy")
                .setConf(SPARK_EXECUTOR_INSTANCES_CONF,executorInstances.toString())
                .setConf("spark.sql.shuffle.partitions","200")
                .setConf("spark.default.parallelism", "300");


//                .setConf("spark.metrics.conf.master.sink.console.class","org.apache.spark.metrics.sink.ConsoleSink")
//                .setConf("spark.metrics.conf.master.sink.console.period","1")
//                .setConf("spark.metrics.conf.master.sink.console.unit","minutes");

//                .setConf(SPARK_CSV_CLASS_CONF,"org.apache.spark.metrics.sink.CsvSink")
//                .setConf(SPARK_CSV_PERIOD_CONF,"10")
//                .setConf(SPARK_CSV_UNIT_CONF,"seconds")
//                .setConf(SPARK_CSV_DIR_CONF,"hdfs://hadoop01.dev.eosc.pcss.pl:9000/user/hadoop/preproc/test/")
//                .setConf(SPARK_WORKER_CSV_PERIOD_CONF,"10")
//                .setConf(SPARK_WORKER_CSV_UNIT_CONF,"seconds")
//                .setConf("spark.metrics.conf.*.source.jvm.class","org.apache.spark.metrics.source.JvmSource");

        if(jobConfig.getJars() != null) sparkLauncher.addJar(jobConfig.getJars());

        if(additionalConf != null) additionalConf.forEach(sparkLauncher::setConf);

        postInit(sparkLauncher);

        UserGroupInformation.setLoginUser(UserGroupInformation.createRemoteUser("hadoop"));

        List<SparkAppHandle.Listener> listenersList = new ArrayList<>();
        if(listeners!=null) listenersList.addAll(Arrays.asList(listeners));
        try {
            sparkLauncher.startApplication(listenersList.toArray(SparkAppHandle.Listener[]::new));
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }
}
