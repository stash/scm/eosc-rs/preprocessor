package pl.psnc.eosc.rs.preprocessor.app.drivers.api.hdfs;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class HdfsFileStatus {

    String filename;
    Boolean isDirectory;

}
