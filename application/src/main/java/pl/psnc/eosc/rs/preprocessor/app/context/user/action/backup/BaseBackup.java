package pl.psnc.eosc.rs.preprocessor.app.context.user.action.backup;

import pl.psnc.eosc.rs.preprocessor.app.cloud.jms.Subscriber;

public interface BaseBackup extends Subscriber {

    void save(String message);
}

