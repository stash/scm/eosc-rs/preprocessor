package pl.psnc.eosc.rs.preprocessor.app.context.resource.synchronization.user.model;

import com.vladmihalcea.hibernate.type.array.ListArrayType;
import lombok.*;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.List;
import java.util.Objects;


@Entity(name="User")
@Table(name="sync.user_table")
@TypeDef(
        name = "list-array",
        typeClass = ListArrayType.class
)
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
public class UserEntity {

    @Id
    @Column(name="aai_id")
    String aaiId;

    @Column(name="marketplace_id")
    String marketplaceId;

    @Type(type = "list-array")
    @Column(
            name="scientific_domains",
            columnDefinition = "integer[]")
    List<Integer> scientificDomainValues;

    @Type(type = "list-array")
    @Column(
            name="categories",
            columnDefinition = "integer[]")
    List<Integer> categoryValues;

    @Type(type = "list-array")
    @Column(
            name="accessed_services",
            columnDefinition = "integer[]")
    List<Integer> accessedServiceValues;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserEntity that = (UserEntity) o;
        return Objects.equals(aaiId, that.aaiId) && Objects.equals(marketplaceId, that.marketplaceId) && Objects.equals(scientificDomainValues, that.scientificDomainValues) && Objects.equals(categoryValues, that.categoryValues) && Objects.equals(accessedServiceValues, that.accessedServiceValues);
    }

    @Override
    public int hashCode() {
        return Objects.hash(aaiId, marketplaceId, scientificDomainValues, categoryValues, accessedServiceValues);
    }
}
