package pl.psnc.eosc.rs.preprocessor.app.context.user.action.artificial;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;
import org.springframework.context.annotation.Profile;
import pl.psnc.eosc.rs.preprocessor.app.cloud.jms.MessageBroadcaster;
import pl.psnc.eosc.rs.preprocessor.app.cloud.kafka.Producer;
import pl.psnc.eosc.rs.preprocessor.app.context.user.action.artificial.model.entity.ArtificialDayStatsEntity;
import pl.psnc.eosc.rs.preprocessor.app.context.user.action.artificial.model.entity.ArtificialTodayStatsEntity;
import pl.psnc.eosc.rs.preprocessor.app.context.user.action.artificial.model.entity.ArtificialTotalStatsEntity;
import pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular.repository.AbstractStatsPerDayRepository;
import pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular.repository.AbstractStatsTodayRepository;
import pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular.repository.AbstractStatsTotalRepository;
import pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular.service.MessageConverter;
import pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular.service.UserActionService;
import pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular.service.UserActionServiceImpl;
import pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular.service.session.SessionManagerHandler;
import pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular.service.session.SessionPublisher;
import pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular.service.session.SessionPublisherImpl;
import pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular.service.session.SessionsManagerImpl;
import pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular.service.statistics.*;
import pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular.service.user.UserDataSupplier;
import pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular.service.user.UserDataSupplierImpl;
import pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular.service.user.UserRecognitionHandler;
import pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular.service.user.UserRecognitionHandlerImpl;
import pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular.service.validator.ValidatorHandler;

/**
 * Configuration for abstract User Action processing flow. It is separated from regular flow.
 */
@Profile("!artificialUAConfig-disabled & artificial-flow")
@Configuration
public class UserActionProcessingConfig {

    @Bean
    ValidatorHandler fakeValidator() { return new FakeValidator(); }

    @Bean
    SessionPublisher futureSessionPublisher(Producer producer,
                                            @Value(value = "${user-action.artificial.session.kafka-topic}") String topic){
        return new SessionPublisherImpl(producer,topic);
    }

    @Bean
    @DependsOn({"futureSessionPublisher"})
    SessionManagerHandler futureSessionManagerHandler(@Qualifier("futureSessionPublisher") SessionPublisher futureSessionPublisher,
                                                      @Value("${user-action.session.expiration-time}") Long sessionExpirationTime,
                                                      @Value("${user-action.session.expiration-time.delay}") Long sessionExpirationDelay){
        return new SessionsManagerImpl(futureSessionPublisher,sessionExpirationTime,sessionExpirationDelay);
    }

    @Bean
    StatisticsStorage futureStatisticsStorage(){
        return new StatisticsStorageImpl();
    }

    @Bean
    StatisticsPublisher futureStatisticsPublisher(@Value(value = "${user-action.artificial.statistics.kafka-topic}") String topic,
                                                  DayProvider dayProvider, Producer producer,
                                                  @Qualifier("artificialDayRepository") AbstractStatsPerDayRepository dayRepository,
                                                  @Qualifier("artificialTodayRepository") AbstractStatsTodayRepository todayRepository,
                                                  @Qualifier("artificialTotalRepository") AbstractStatsTotalRepository totalRepository) {
        return new StatisticsPublisherImpl(topic,dayRepository,totalRepository,todayRepository,
                dayProvider,producer,ArtificialDayStatsEntity.class,ArtificialTotalStatsEntity.class);
    }

    @Bean
    StatisticsHandler futureStatisticsHandler(){
        return new StatisticsHandlerImpl(futureStatisticsStorage());
    }

    @Bean
    TodayPublisher futureTodayPublisher(DayProvider dayProvider,
                                        @Qualifier("artificialTodayRepository") AbstractStatsTodayRepository<ArtificialTodayStatsEntity> todayRepository) {
        return new TodayPublisherImpl(
                todayRepository,
                futureStatisticsStorage(),
                dayProvider,
                ArtificialTodayStatsEntity.class);
    }

    @Bean
    UserDataSupplier artificialUserIdMappingHandler() {return new UserDataSupplierImpl();}


    @Bean
    UserRecognitionHandler futureUserRecognitionHandler(){
        return new UserRecognitionHandlerImpl();
    }

    @Bean
    UserActionService futureUserActionService(MessageConverter messageConverter,
                                              @Qualifier("artificialUserIdMappingHandler") UserDataSupplier artificialUserIdMappingHandler,
                                              @Qualifier("futureSessionManagerHandler") SessionManagerHandler futureSessionManagerHandler,
                                              @Qualifier("futureStatisticsHandler") StatisticsHandler futureStatisticsHandler,
                                              @Qualifier("fakeValidator") ValidatorHandler futureValidatorHandler,
                                              @Qualifier("futureUserRecognitionHandler") UserRecognitionHandler futureUserRecognitionHandler,
                                              @Qualifier("artificialConsumer") MessageBroadcaster broadcaster){
        return new UserActionServiceImpl(messageConverter,
                artificialUserIdMappingHandler,
                futureSessionManagerHandler,
                futureStatisticsHandler,
                futureValidatorHandler,
                futureUserRecognitionHandler,
                broadcaster);
    }
}
