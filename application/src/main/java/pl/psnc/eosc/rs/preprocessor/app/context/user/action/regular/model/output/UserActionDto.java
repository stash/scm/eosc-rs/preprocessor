package pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular.model.output;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserActionDto {

    private Timestamp timestamp;

    private String pageId;

    private String resourceType;

    private String resourceId;

    private String rootType;

    private String rootPanelId;

    private String actionType;

    private Boolean isOrderAction;

}
