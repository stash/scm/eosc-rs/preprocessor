package pl.psnc.eosc.rs.preprocessor.app.context.marketplace.job;

import lombok.Getter;
import org.apache.spark.launcher.SparkAppHandle;
import org.apache.spark.launcher.SparkLauncher;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import pl.psnc.eosc.rs.preprocessor.app.commons.spark.ResourceType;
import pl.psnc.eosc.rs.preprocessor.app.commons.spark.SparkJobLaunchConfigProperties;
import pl.psnc.eosc.rs.preprocessor.app.commons.spark.SparkJobLauncherAbstract;
import pl.psnc.eosc.rs.preprocessor.app.commons.spark.SparkLauncherConfigProperties;

import java.util.Map;

@Service
public class MarketplaceResourcesPreprocJobLauncher extends SparkJobLauncherAbstract {

    public enum MarketplaceResourceType implements ResourceType {
        DATASOURCE("datasource"),
        SERVICE("service");

        @Getter
        String name;

        MarketplaceResourceType(String name){
            this.name = name;
        }
    }

    public MarketplaceResourcesPreprocJobLauncher(SparkLauncherConfigProperties launcherConfig, @Qualifier("marketplaceResourcesPreprocJobLaunchConfig") SparkJobLaunchConfigProperties jobConfig) {
        super(launcherConfig, jobConfig,
                Map.of("spark.model.path",jobConfig.getLanguagePredictionPipelinePath()));
    }

    @Override
    protected void postInit(SparkLauncher sparkLauncher) {

    }

    public void runAsync(String previousDumpDir,String inputFiles, String outputDirectory, SparkAppHandle.Listener... listeners){
        this.launchAsync(new String[]{previousDumpDir,inputFiles,outputDirectory}, listeners);
    }

}
