package pl.psnc.eosc.rs.preprocessor.app.context.resource.synchronization.service.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import pl.psnc.eosc.rs.preprocessor.app.cloud.jms.MessageBroadcaster;
import pl.psnc.eosc.rs.preprocessor.app.context.resource.converter.JsonObjectConverter;
import pl.psnc.eosc.rs.preprocessor.app.context.resource.converter.ResourceEvent;
import pl.psnc.eosc.rs.preprocessor.app.context.resource.synchronization.EventService;
import pl.psnc.eosc.rs.preprocessor.app.context.resource.synchronization.EventType;
import pl.psnc.eosc.rs.preprocessor.app.context.resource.synchronization.service.model.ServiceEntity;
import pl.psnc.eosc.rs.preprocessor.app.context.resource.synchronization.service.model.ServiceEvent;
import pl.psnc.eosc.rs.preprocessor.app.context.resource.synchronization.service.repository.ServiceRepository;

import java.util.Optional;

/**
 * Update and remove services according to appropriate type of {@link ResourceEvent}.
 */
@Slf4j
@Component
public class ServiceEventHandler extends EventService {

    ServiceRepository serviceRepository;

    JsonObjectConverter<ResourceEvent> resourceEventConverter;

    JsonObjectConverter<ServiceEvent> serviceEventConverter;

    ServiceChangesPublisher serviceChangesPropagator;

    @Autowired
    public ServiceEventHandler(ServiceRepository serviceRepository,
                               @Qualifier("resourceEventBroadcaster") MessageBroadcaster broadcaster,
                               JsonObjectConverter<ResourceEvent> resourceEventConverter,
                               JsonObjectConverter<ServiceEvent> serviceEventConverter,
                               ServiceChangesPublisher serviceChangesPropagator) {
        super(broadcaster);
        this.serviceRepository=serviceRepository;
        this.resourceEventConverter=resourceEventConverter;
        this.serviceEventConverter=serviceEventConverter;
        this.serviceChangesPropagator=serviceChangesPropagator;
    }

    @Override
    public void processMessage(String rawMessage) {
        ResourceEvent resourceEvent = resourceEventConverter.convert(rawMessage);
        if(isServiceEvent(resourceEvent)){
            ServiceEvent serviceEvent = serviceEventConverter.convert(resourceEvent.getRecord().toString());
            if(isRemoveEvent(resourceEvent)){
                deleteService(serviceEvent);
            } else if (isModifyEvent(resourceEvent)) {
                Optional<ServiceEntity> fetchedService = serviceRepository.findById(serviceEvent.getId());
                if(fetchedService.isEmpty()){
                    createService(serviceEvent);
                } else if(!toServiceEntity(serviceEvent).equals(fetchedService.get())){
                    updateService(serviceEvent);
                } else {
                    log.debug("Nothing to change! Service id: "+serviceEvent.getId());
                }
            }
        }
    }

    private void createService(ServiceEvent service){
        log.debug("Creating service: "+service.getId());
        service.setEventType(EventType.CREATE.getName());
        serviceRepository.saveAndFlush(toServiceEntity(service));
        serviceChangesPropagator.publish(service);
    }

    private void deleteService(ServiceEvent service){
        log.debug("Removing service: "+service.getId());
        service.setEventType(EventType.DELETE.getName());
        serviceRepository.deleteById(service.getId());
        serviceChangesPropagator.publish(service);
    }

    private void updateService(ServiceEvent service){
        log.debug("Updating service: "+service.getId());
        service.setEventType(EventType.UPDATE.getName());
        serviceRepository.saveAndFlush(toServiceEntity(service));
        serviceChangesPropagator.publish(service);
    }

    private boolean isServiceEvent(ResourceEvent resourceEvent){
        return resourceEvent.getModel().equals("Service");
    }

    private ServiceEntity toServiceEntity(ServiceEvent serviceEvent){
        return ServiceEntity.builder()
                .id(serviceEvent.getId())
                .name(serviceEvent.getName())
                .description(serviceEvent.getDescription())
                .scientificDomainValues(serviceEvent.getScientificDomainValues())
                .categoryValues(serviceEvent.getCategoryValues())
                .relatedServices(serviceEvent.getRelatedServices())
                .build();
    }
}
