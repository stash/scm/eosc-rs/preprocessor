package pl.psnc.eosc.rs.preprocessor.app.context.resource.synchronization.user.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import pl.psnc.eosc.rs.preprocessor.app.context.resource.synchronization.user.model.UserEntity;

import java.util.List;

/**
 * Repository for synchronization Users with Marketplace DB - {@link UserEntity}
 */
@Repository
public interface UserRepository extends JpaRepository<UserEntity, String> {

    UserEntity findDistinctByMarketplaceId(String marketplaceId);

    @Query("select u.aaiId from #{#entityName} u")
    List<String> getAllIds();
}