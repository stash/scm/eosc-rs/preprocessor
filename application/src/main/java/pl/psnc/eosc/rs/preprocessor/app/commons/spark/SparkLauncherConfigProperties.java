package pl.psnc.eosc.rs.preprocessor.app.commons.spark;

import lombok.Data;

@Data
public class SparkLauncherConfigProperties {
    String sparkHome;
    String sparkYarnArchive;
    String defaultDeployMode;
    String defaultMaster;
    Integer defaultExecutorInstances;
}
