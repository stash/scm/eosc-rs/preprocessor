package pl.psnc.eosc.rs.preprocessor.app.context.spark.processing.listener;

import lombok.extern.slf4j.Slf4j;
import org.apache.spark.launcher.SparkAppHandle;
import pl.psnc.eosc.rs.preprocessor.app.context.marketplace.job.MarketplaceResourcesPreprocJobLauncher;
import pl.psnc.eosc.rs.preprocessor.app.context.oag.job.OagResourcesPreprocJobLauncher;
import pl.psnc.eosc.rs.preprocessor.app.context.spark.processing.ProcessingBroker;
import pl.psnc.eosc.rs.preprocessor.app.context.spark.processing.command.MarketplaceProcessingCommand;
import pl.psnc.eosc.rs.preprocessor.app.context.spark.processing.command.OagProcessingCommand;
import pl.psnc.eosc.rs.preprocessor.app.context.spark.processing.command.TrainingProcessingCommand;
import pl.psnc.eosc.rs.preprocessor.app.context.trainings.job.TrainingsPreprocJobLauncher;

import static pl.psnc.eosc.rs.preprocessor.app.context.marketplace.job.MarketplaceResourcesPreprocJobLauncher.MarketplaceResourceType.DATASOURCE;
import static pl.psnc.eosc.rs.preprocessor.app.context.marketplace.job.MarketplaceResourcesPreprocJobLauncher.MarketplaceResourceType.SERVICE;
import static pl.psnc.eosc.rs.preprocessor.app.context.oag.job.OagResourcesPreprocJobLauncher.BasicResourceType.*;

/**
 * The listener to catch the positive completion of Spark Job responsible for file transfer.
 * Used to trigger the launch of specific resources processing.
 */
@Slf4j
public class SparkJobProcessingTrigger implements SparkAppHandle.Listener {

    Long commandId;

    String sourceDir;

    String previousDumpDir;

    ProcessingBroker broker;

    public SparkJobProcessingTrigger(ProcessingBroker broker, Long commandId, String sourceDir, String previousDumpDir) {
        this.broker=broker;
        this.commandId=commandId;
        this.sourceDir=sourceDir;
        this.previousDumpDir=previousDumpDir;
    }

    @Override
    public void stateChanged(SparkAppHandle handle) {
        if(handle.getState()==SparkAppHandle.State.FINISHED && handle.getError().isEmpty()){
            broker.execute(prepareOagProcessingCommand(DATASET),
                    false,commandId);
            broker.execute(prepareOagProcessingCommand(OTHER_RESEARCH_PRODUCT),
                    false,commandId);
            broker.execute(prepareOagProcessingCommand(PUBLICATION),
                    false,commandId);
            broker.execute(prepareOagProcessingCommand(SOFTWARE),
                    false,commandId);
            broker.execute(prepareTrainingProcessingCommand(),
                    false,commandId);
            broker.execute(prepareMarketplaceProcessingCommand(SERVICE),
                    false,commandId);
            broker.execute(prepareMarketplaceProcessingCommand(DATASOURCE),
                    false,commandId);
        }
    }

    @Override
    public void infoChanged(SparkAppHandle handle) { }

    private MarketplaceProcessingCommand prepareMarketplaceProcessingCommand(
            MarketplaceResourcesPreprocJobLauncher.MarketplaceResourceType resourceType){
        String directory = "/"+resourceType.getName()+"/";
        String previousDump = "";
        if(previousDumpDir!=null && !previousDumpDir.isEmpty())
            previousDump="preproc/output/"+previousDumpDir+directory;
        return MarketplaceProcessingCommand.builder()
                .inputFiles("preproc/input/"+sourceDir+directory+"*")
                .previousDumpDir(previousDump)
                .outputDir("preproc/output/"+sourceDir+directory)
                .resourceType(resourceType)
                .build();
    }

    private OagProcessingCommand prepareOagProcessingCommand(OagResourcesPreprocJobLauncher.BasicResourceType resourceType){
        String directory = "/"+resourceType.getName()+"/";
        String previousDump = "";
        if(previousDumpDir!=null && !previousDumpDir.isEmpty())
            previousDump="preproc/output/"+previousDumpDir+directory;
        return OagProcessingCommand.builder()
                .inputFiles("preproc/input/"+sourceDir+directory+"*")
                .previousDumpDir(previousDump)
                .outputDir("preproc/output/"+sourceDir+directory)
                .resourceType(resourceType)
                .build();
    }

    private TrainingProcessingCommand prepareTrainingProcessingCommand(){
        String directory = "/"+ TrainingsPreprocJobLauncher.TrainingResourceType.TRAINING.getName()+"/";
        String previousDump = "";
        if(previousDumpDir!=null && !previousDumpDir.isEmpty())
            previousDump="preproc/output/"+previousDumpDir+directory;
        return TrainingProcessingCommand.builder()
                .inputFiles("preproc/input/"+sourceDir+directory+"*")
                .previousDumpDir(previousDump)
                .outputDir("preproc/output/"+sourceDir+directory)
                .resourceType(TrainingsPreprocJobLauncher.TrainingResourceType.TRAINING)
                .build();
    }
}
