package pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular.repository;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

@Configuration
public class StatisticsRepositoryConfiguration {

    @Bean
    @Primary
    AbstractStatsPerDayRepository dayRepository(StatsPerDayRepository statsPerDayRepository){
        return statsPerDayRepository;
    }

    @Bean
    @Primary
    AbstractStatsTodayRepository todayRepository(StatsTodayRepository statsTodayRepository){
        return statsTodayRepository;
    }

    @Bean
    @Primary
    AbstractStatsTotalRepository totalRepository(StatsTotalRepository statsTotalRepository){
        return statsTotalRepository;
    }
}
