package pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular.service.statistics;

import pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular.model.internal.Resource;

public interface StatisticsHandler {

    void receiveUserAction(String sessionId, String userId, Resource resource, Boolean isOrder);
}
