package pl.psnc.eosc.rs.preprocessor.app.drivers.api.searchservice;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.OffsetDateTime;
import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Dump {

    String name;
    OffsetDateTime createdAt;
    OffsetDateTime updatedAt;
    List<DumpElement> elements;

}
