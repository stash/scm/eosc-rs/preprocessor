package pl.psnc.eosc.rs.preprocessor.app.context.oag.job;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import pl.psnc.eosc.rs.preprocessor.app.commons.spark.SparkJobLaunchConfigProperties;

@Data
@Configuration
@ConfigurationProperties(prefix = "spark.job.dbupdate")
public class DbUpdateJobConfig extends SparkJobLaunchConfigProperties {
    String serverUrl;
    String tableName;
    String user;
    String password;
    String batchSize;
}
