package pl.psnc.eosc.rs.preprocessor.app.context.spark.processing;

import lombok.extern.slf4j.Slf4j;
import org.apache.spark.launcher.SparkAppHandle;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import pl.psnc.eosc.rs.preprocessor.app.context.spark.processing.command.Command;
import pl.psnc.eosc.rs.preprocessor.app.context.spark.processing.repository.CommandEntity;
import pl.psnc.eosc.rs.preprocessor.app.context.spark.processing.repository.CommandRepository;

import java.io.*;
import java.util.List;

@Slf4j
@Component
public class CommandReRunnerImpl implements FailedCommandReRunner{

    @Autowired
    CommandRepository commandRepository;

    @Autowired
    ProcessingBroker processingBroker;

    //TODO
//    @Scheduled
    @Override
    public void reExecuteFailedCommands() {
        List<CommandEntity> commands = commandRepository.findByStatusIn(List.of(
                SparkAppHandle.State.FAILED.name()));

        for(CommandEntity commandEntity : commands) {
            Command command = transformByteArrayToCommand(
                    commandEntity.getCommand());
            processingBroker.execute(command,false,commandEntity.getId());
        }
    }

    private Command transformByteArrayToCommand(byte[] bytes){
        Command command = null;
        try {
            // bytearray to object
            ByteArrayInputStream bais = new ByteArrayInputStream(bytes);
            ObjectInputStream oi = new ObjectInputStream(bais);
            command = (Command)oi.readObject();
            bais.close();
            oi.close();
        } catch (Exception e) {
            log.error("Error occurred while transforming byte array "+e.getMessage());
        }
        return command;
    }

}
