package pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular.service.user;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Primary;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular.model.internal.SingleSession;
import pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular.model.internal.UserIdTimestamp;
import pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular.service.AbstractUserActionHandler;

import java.time.Clock;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

@Slf4j
@Primary
@Component
public class UserRecognitionHandlerImpl extends AbstractUserActionHandler implements UserRecognitionHandler {

    HashMap<UUID, UserIdTimestamp> userSessions = new HashMap<>();

    private Clock clock = Clock.systemUTC();

    @Value("${user-action.session.expiration-time}")
    private Long sessionExpirationTime;

    @Value("${user-action.session.expiration-time.delay}")
    private Long sessionExpirationDelay;

    @Override
    protected SingleSession processUserAction(SingleSession userAction) {
        recognizeUser(userAction);
        return userAction;
    }

    @Override
    public void recognizeUser(SingleSession userAction) {
        UUID sessionId = userAction.getSessionId();
        String userId = userAction.getAaiUid();

        // Update timestamp
        if(userSessions.containsKey(sessionId)){
            userSessions.get(sessionId).setTimestamp(clock.millis());
        }

        //Recognize
        if(userId!=null){
            if(!userSessions.containsKey(sessionId)){
                UserIdTimestamp value = new UserIdTimestamp(clock.millis(),userId);
                userSessions.put(sessionId,value);
            }
        } else {
            if(userSessions.containsKey(sessionId)) {
                log.debug("User was recognized based on sessionId");
                userAction.setAaiUid(userSessions.get(sessionId).getUser());
            }
        }
    }

    @Scheduled(cron = "${user-action.session.cron.remover}", zone = "UTC")
    public void findExpiredSessionIds() {
        Long userActionsExpirationTime = sessionExpirationTime+sessionExpirationDelay;
        for (Iterator<Map.Entry<UUID, UserIdTimestamp>> it = userSessions.entrySet().iterator();
             it.hasNext(); ) {
            Map.Entry<UUID, UserIdTimestamp> entry = it.next();
            UUID sessionId = entry.getKey();
            UserIdTimestamp session = entry.getValue();
            Long duration = System.currentTimeMillis() - session.getTimestamp();
            if(duration>userActionsExpirationTime) {
                it.remove();
                String readableDuration = String.format("%d min",
                        TimeUnit.MILLISECONDS.toMinutes(duration)
                );
                log.debug("SessionId " + sessionId + " was removed from User Recognizer. " + readableDuration + " have passed since the last UA (in the session).");
            }
        }
        log.info("SessionIds were removed from User Recognizer.");
    }
}
