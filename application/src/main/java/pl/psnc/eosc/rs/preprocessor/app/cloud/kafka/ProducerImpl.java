package pl.psnc.eosc.rs.preprocessor.app.cloud.kafka;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class ProducerImpl implements Producer{

    @Autowired
    KafkaTemplate<String, String> kafkaTemplate;

    @Override
    public void publish(String topic, String key, String message) {
        log.debug("Publishing: \""+message+"\" with key: "+key+" to kafka topic: "+topic);
        kafkaTemplate.send(topic,key,message);
    }
}
