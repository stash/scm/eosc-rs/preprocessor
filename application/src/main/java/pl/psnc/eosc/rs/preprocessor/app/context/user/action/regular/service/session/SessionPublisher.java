package pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular.service.session;

public interface SessionPublisher {

    void publish(String key, String message);
}
