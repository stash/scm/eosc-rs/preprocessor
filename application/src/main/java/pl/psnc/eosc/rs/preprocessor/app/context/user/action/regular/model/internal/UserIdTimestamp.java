package pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular.model.internal;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
@AllArgsConstructor
public class UserIdTimestamp {

    Long timestamp;

    String user;

}
