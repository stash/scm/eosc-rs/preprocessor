package pl.psnc.eosc.rs.preprocessor.app.cloud.jms;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.util.ErrorHandler;

import java.util.Arrays;

/**
 * Default error handler for JMS listeners.
 */
@Slf4j
@Component
public class JmsErrorHandler implements ErrorHandler {
    @Override
    public void handleError(Throwable t) {
        log.error("Error in listener: " + t.getMessage());
    }
}
