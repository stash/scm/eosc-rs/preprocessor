package pl.psnc.eosc.rs.preprocessor.app.context.resource.synchronization.training;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import pl.psnc.eosc.rs.preprocessor.app.cloud.jms.ProviderComponentMessageBroadcaster;
import pl.psnc.eosc.rs.preprocessor.app.cloud.jms.ProviderComponentResourceSubscriber;
import pl.psnc.eosc.rs.preprocessor.app.context.resource.converter.JsonObjectConverter;
import pl.psnc.eosc.rs.preprocessor.app.context.resource.synchronization.EventType;
import pl.psnc.eosc.rs.preprocessor.app.context.user.action.generator.ResourceEnum;
import pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular.model.statistics.database.ResourceKey;
import pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular.model.statistics.database.TotalStatsEntity;
import pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular.repository.StatsTotalRepository;

import javax.annotation.PostConstruct;

/**
 * CUD trainings according to JMS messages.
 */
@Slf4j
@Component
public class TrainingEventHandler implements ProviderComponentResourceSubscriber {
    StatsTotalRepository statsTotalRepository;

    JsonObjectConverter<TrainingParentEvent> trainingEventConverter;

    TrainingChangesPublisher trainingChangesPropagator;

    ProviderComponentMessageBroadcaster broadcaster;

    @PostConstruct
    protected void subscribeJms() {
        broadcaster.subscribe(this);
    }

    @Autowired
    public TrainingEventHandler(StatsTotalRepository statsTotalRepository,
                               @Qualifier("trainingEventBroadcaster") ProviderComponentMessageBroadcaster broadcaster,
                               JsonObjectConverter<TrainingParentEvent> trainingParentEventConverter,
                               TrainingChangesPublisher trainingChangesPropagator) {
        this.broadcaster = broadcaster;
        this.statsTotalRepository=statsTotalRepository;
        this.trainingEventConverter=trainingParentEventConverter;
        this.trainingChangesPropagator=trainingChangesPropagator;
    }

    @Override
    public void processMessage(String json, OperationType operationType, ResourceEnum resourceType) {
        if (!resourceType.equals(ResourceEnum.TRAINING)){
            return;
        }
        TrainingParentEvent parentEvent = trainingEventConverter.convert(json);
        TrainingEvent trainingEvent = parentEvent.getTrainingResource();
        switch (operationType) {
            case CREATE:
                createTraining(trainingEvent);
                break;
            case UPDATE:
                updateTraining(trainingEvent);
                break;
            case DELETE:
                deleteTraining(trainingEvent);
                break;
        }
    }

    private void createTraining(TrainingEvent training){
        log.debug("Creating training: "+training.getId());
        saveTraining(training);
        training.setEventType(EventType.CREATE.getName());
        trainingChangesPropagator.publish(training);
    }

    private void deleteTraining(TrainingEvent training){
        log.debug("Removing training: "+training.getId());
        ResourceKey key = new ResourceKey(ResourceEnum.TRAINING.getId(),training.getId());
        statsTotalRepository.deleteById(key);
        training.setEventType(EventType.DELETE.getName());
        trainingChangesPropagator.publish(training);
    }

    private void updateTraining(TrainingEvent training){
        log.debug("Updating training: "+training.getId());
        ResourceKey key = new ResourceKey(ResourceEnum.TRAINING.getId(),training.getId());
        if(!statsTotalRepository.findById(key).isPresent()){
            saveTraining(training);
        }
        training.setEventType(EventType.UPDATE.getName());
        trainingChangesPropagator.publish(training);
    }

    private void saveTraining(TrainingEvent training){
        TotalStatsEntity entity = new TotalStatsEntity(ResourceEnum.TRAINING.getId(),training.getId(),0,0);
        statsTotalRepository.saveAndFlush(entity);
    }
}
