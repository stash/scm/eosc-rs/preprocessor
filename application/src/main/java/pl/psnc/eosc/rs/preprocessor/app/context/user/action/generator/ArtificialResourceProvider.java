package pl.psnc.eosc.rs.preprocessor.app.context.user.action.generator;

import org.springframework.stereotype.Component;
import pl.psnc.eosc.rs.preprocessor.app.context.user.action.generator.model.GeneratedResource;

import java.util.Random;

import static pl.psnc.eosc.rs.preprocessor.app.context.user.action.generator.Commons.randomEnum;

@Component
public class ArtificialResourceProvider implements ResourceProvider {

    Random rand = new Random();

    @Override
    public GeneratedResource getResource() {
        ResourceEnum resource = randomEnum(ResourceEnum.class);
        String resourceId = String.valueOf(rand.nextInt(10000));
        String resourceType = resource.getName();
        return new GeneratedResource(resourceId,resourceType);
    }
}
