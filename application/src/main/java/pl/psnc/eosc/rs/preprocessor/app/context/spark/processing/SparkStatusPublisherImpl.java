package pl.psnc.eosc.rs.preprocessor.app.context.spark.processing;

import lombok.extern.slf4j.Slf4j;
import org.apache.spark.launcher.SparkAppHandle;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import pl.psnc.eosc.rs.preprocessor.app.cloud.kafka.Producer;
import pl.psnc.eosc.rs.preprocessor.app.commons.spark.ResourceType;
import pl.psnc.eosc.rs.preprocessor.app.context.spark.processing.listener.SparkJobFinalStatusForwardingListener;

@Slf4j
@Component
public class SparkStatusPublisherImpl implements SparkJobFinalStatusForwardingListener.SparkFinalStatusListener {

    Producer producer;

    String topic;

    @Autowired
    public SparkStatusPublisherImpl(@Value(value = "${spark.job.status.kafka-topic}") String topic,
                                    Producer producer){
        this.producer = producer;
        this.topic = topic;
    }


    @Override
    public void handle(SparkAppHandle finalHandle, String commandName, ResourceType resourceType) {
        String resourceName = (resourceType!=null) ? resourceType.getName():null;
        SparkMessage sparkMessage =
                toSparkMessage(finalHandle, commandName, resourceName);
        String jsonMessage = sparkMessage.toJson();
        producer.publish(topic,finalHandle.getAppId(),jsonMessage);
        log.info("Publishing to "+topic+" msg: " + jsonMessage);
    }

    private SparkMessage toSparkMessage(SparkAppHandle finalHandle, String commandName, String resourceType){
        String errorMessage = null;
        if(finalHandle.getError().isPresent()) {
            errorMessage = finalHandle.getError().get().getMessage();
        }
        return new SparkMessage(System.currentTimeMillis(),
                commandName,
                finalHandle.getAppId(),
                resourceType,
                finalHandle.getState().name(),
                errorMessage);
    }
}
