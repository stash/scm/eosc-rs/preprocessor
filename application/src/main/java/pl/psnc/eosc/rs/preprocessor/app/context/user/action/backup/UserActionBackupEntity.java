package pl.psnc.eosc.rs.preprocessor.app.context.user.action.backup;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.Size;

@Entity
@Table(name="raw_backup.user_action_table")
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class UserActionBackupEntity {

    @Id @GeneratedValue(strategy= GenerationType.IDENTITY)
    Long id;
    
    Long timestamp;

    @Size(max=8192)
    @Column(name="raw_user_action", length = 8192)
    String rawUserAction;

    public UserActionBackupEntity(long timestamp, String rawUserAction) {
        this.timestamp = timestamp;
        this.rawUserAction = rawUserAction;
    }
}
