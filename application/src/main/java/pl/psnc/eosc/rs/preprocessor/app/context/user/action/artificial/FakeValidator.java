package pl.psnc.eosc.rs.preprocessor.app.context.user.action.artificial;

import pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular.model.internal.SingleSession;
import pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular.service.AbstractUserActionHandler;
import pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular.service.validator.ValidatorHandler;

public class FakeValidator extends AbstractUserActionHandler implements ValidatorHandler {

    @Override
    protected SingleSession processUserAction(SingleSession session) {
        if(isValid(session))
            return session;
        return null;
    }

    @Override
    public boolean isValid(SingleSession session) {
        return true;
    }


}
