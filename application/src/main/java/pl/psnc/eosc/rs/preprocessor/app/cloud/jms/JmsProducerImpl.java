package pl.psnc.eosc.rs.preprocessor.app.cloud.jms;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Profile;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;


@Profile({"!jms-disabled & ua-generator"})
@Slf4j
@Component
@Primary
public class JmsProducerImpl implements JmsProducer {

    @Autowired
    @Qualifier("artificialJmsTemplate")
    JmsTemplate jmsTemplate;

    @Autowired
    JmsTopicConfig jmsTopicConfig;


    @Override
    public void sendMessage(JmsTopic topic, String message) {
        log.debug("Message send to JMS "+topic.getName());
        jmsTemplate.convertAndSend(topic.getName(),message);
    }
}
