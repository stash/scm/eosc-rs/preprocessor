package pl.psnc.eosc.rs.preprocessor.app.commons.spark;

import lombok.Data;

@Data
public class SparkJobLaunchConfigProperties {
    String jarFile;
    String mainClass;
    String jars;
    Integer executorInstances;
    String languagePredictionPipelinePath;
}
