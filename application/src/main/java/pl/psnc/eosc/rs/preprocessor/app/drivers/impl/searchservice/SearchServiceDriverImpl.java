package pl.psnc.eosc.rs.preprocessor.app.drivers.impl.searchservice;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import org.openapitools.jackson.nullable.JsonNullableModule;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.client.BufferingClientHttpRequestFactory;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.AbstractJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;
import pl.psnc.eosc.rs.preprocessor.app.drivers.api.searchservice.Dump;
import pl.psnc.eosc.rs.preprocessor.app.drivers.api.searchservice.DumpElement;
import pl.psnc.eosc.rs.preprocessor.app.drivers.api.searchservice.SearchServiceDriver;
import pl.psnc.eosc.rs.preprocessor.common.pitocr.api.V1Api;
import pl.psnc.eosc.rs.preprocessor.common.pitocr.invoker.ApiClient;
import pl.psnc.eosc.rs.preprocessor.common.pitocr.model.DumpResults;

import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class SearchServiceDriverImpl implements SearchServiceDriver {

    private static final Integer PAGE_SIZE = 20;

    V1Api dumpsApi;

    @Autowired
    protected SearchServiceDriverImpl(V1Api dumpsApi){
        this.dumpsApi = dumpsApi;
    }


    /**
     * Lists all dumps available on SearchService API
     */
    @Override
    public List<Dump> listDumps() {
        List<Dump> result = new ArrayList<>();

        String currentCursor = "0";
        while (currentCursor != null){
            DumpResults dumpResultsPage = dumpsApi.dumpsGetV1DumpsGet(currentCursor,PAGE_SIZE);

            if(dumpResultsPage.getDumps() != null)
                result.addAll(dumpResultsPage.getDumps().stream().map(this::mapToInternalDump).collect(Collectors.toList()));

            currentCursor = dumpResultsPage.getNextCursor();
        }

        return result;
    }

    /**
     * Maps SearchService Dump Model to internal Dump Model
     */
    private Dump mapToInternalDump(pl.psnc.eosc.rs.preprocessor.common.pitocr.model.Dump dump){
        Dump result = new Dump();
        result.setName(dump.getName());
        result.setCreatedAt(OffsetDateTime.parse(dump.getCreatedAt()));
        result.setUpdatedAt(OffsetDateTime.parse(dump.getUpdatedAt()));
        result.setElements(dump.getElements().stream().map(this::mapToInternalDumpElement).collect(Collectors.toList()));
        return result;
    }

    private DumpElement mapToInternalDumpElement(pl.psnc.eosc.rs.preprocessor.common.pitocr.model.DumpElements element){
        DumpElement result = new DumpElement();
        result.setReference(element.getReference());
        result.setName(element.getName());
        result.setReferenceType(element.getReferenceType());
        return result;
    }

    /**
     * Factory method
     */
    public static SearchServiceDriverImpl create(String searchServiceUrl){
        return create(defaultRestTemplate(),searchServiceUrl);
    }

    /**
     * Factory method
     */
    public static SearchServiceDriverImpl create(RestTemplate restTemplate, String searchServiceUrl){
        ApiClient apiClient = new ApiClient(restTemplate);
        apiClient.setBasePath(searchServiceUrl);
        apiClient.addDefaultHeader("Authorization","Bearer abc");

        return new SearchServiceDriverImpl(new V1Api(apiClient));
    }

    private static RestTemplate defaultRestTemplate(){
        RestTemplate restTemplate = new RestTemplate();
        for(HttpMessageConverter converter:restTemplate.getMessageConverters()){
            if(converter instanceof AbstractJackson2HttpMessageConverter){
                ObjectMapper mapper = ((AbstractJackson2HttpMessageConverter)converter).getObjectMapper();
                mapper.registerModule(new JsonNullableModule());
                mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);
            }
        }
        restTemplate.setRequestFactory(new BufferingClientHttpRequestFactory(restTemplate.getRequestFactory()));
        return restTemplate;
    }
}
