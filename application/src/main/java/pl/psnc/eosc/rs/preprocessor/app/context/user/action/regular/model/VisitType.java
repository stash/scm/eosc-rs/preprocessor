package pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular.model;

public enum VisitType {
    REVIEW("review"),
    COMPARISON("comparison"),
    REGULAR("regular");

    public final String name;

    VisitType(String name) {
        this.name = name;
    }
}
