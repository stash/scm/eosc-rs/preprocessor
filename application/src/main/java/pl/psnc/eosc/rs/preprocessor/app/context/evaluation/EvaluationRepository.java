package pl.psnc.eosc.rs.preprocessor.app.context.evaluation;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.psnc.eosc.rs.preprocessor.app.context.evaluation.entity.EvaluationEntity;

@Repository
public interface EvaluationRepository extends JpaRepository<EvaluationEntity, Long> { }
