package pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular.model.statistics.database;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name="base.stats_total_table")
@NoArgsConstructor
@Getter
@Setter
public class TotalStatsEntity extends TotalStats {

    public TotalStatsEntity(Short resourceType, String resourceId, Integer visits, Integer orders) {
        super(new ResourceKey(resourceType,resourceId), visits, orders);
    }
}
