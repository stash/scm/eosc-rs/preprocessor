package pl.psnc.eosc.rs.preprocessor.app.context.user.action.generator;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import pl.psnc.eosc.rs.preprocessor.app.context.user.action.generator.model.ResourceIds;

@Configuration
public class ResourceConfig {

    @Bean
    ResourceIds resourceDataset(){
        return new ResourceIds(ResourceEnum.DATASET);
    }

    @Bean
    ResourceIds resourcePublication(){
        return new ResourceIds(ResourceEnum.PUBLICATION);
    }

    @Bean
    ResourceIds resourceService(){
        return new ResourceIds(ResourceEnum.SERVICE);
    }

    @Bean
    ResourceIds resourceSoftware(){
        return new ResourceIds(ResourceEnum.SOFTWARE);
    }

    @Bean
    ResourceIds resourceTraining(){
        return new ResourceIds(ResourceEnum.TRAINING);
    }

    @Bean
    ResourceIds resourceOther() { return new ResourceIds(ResourceEnum.OTHER_RP); }

    @Bean
    ResourceIds resourceDatasource() { return new ResourceIds(ResourceEnum.DATA_SOURCE); }
}
