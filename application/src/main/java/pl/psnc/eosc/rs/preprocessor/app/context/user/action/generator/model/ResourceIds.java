package pl.psnc.eosc.rs.preprocessor.app.context.user.action.generator.model;

import lombok.Getter;
import pl.psnc.eosc.rs.preprocessor.app.context.user.action.generator.LineReader;
import pl.psnc.eosc.rs.preprocessor.app.context.user.action.generator.ResourceEnum;

import java.util.List;

@Getter
public class ResourceIds {

    List<String> resourceIds;

    public ResourceIds(ResourceEnum file){
        resourceIds = (new LineReader(file.getFile()))
                .getRecords();
    }

}
