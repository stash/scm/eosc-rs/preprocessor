package pl.psnc.eosc.rs.preprocessor.app.context.evaluation;

import pl.psnc.eosc.rs.preprocessor.app.cloud.jms.Subscriber;

public interface EvaluationService extends Subscriber {
    void handleMessage(String message);

}
