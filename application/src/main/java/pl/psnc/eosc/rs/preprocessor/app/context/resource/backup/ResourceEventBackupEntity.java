package pl.psnc.eosc.rs.preprocessor.app.context.resource.backup;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.Size;

@Entity
@Table(name="raw_backup.resource_table")
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class ResourceEventBackupEntity {

    @Id @GeneratedValue(strategy= GenerationType.IDENTITY)
    Long id;

    Long timestamp;

    @Size(max=8192)
    @Column(name="raw_resource_event", length = 8192)
    String rawResourceEvent;

    public ResourceEventBackupEntity(long timestamp, String rawResourceEvent) {
        this.timestamp = timestamp;
        this.rawResourceEvent = rawResourceEvent;
    }
}
