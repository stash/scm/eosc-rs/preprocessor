package pl.psnc.eosc.rs.preprocessor.app.context.resource.converter;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import pl.psnc.eosc.rs.preprocessor.app.context.resource.synchronization.service.model.ServiceEvent;
import pl.psnc.eosc.rs.preprocessor.app.context.resource.synchronization.training.TrainingEvent;
import pl.psnc.eosc.rs.preprocessor.app.context.resource.synchronization.training.TrainingParentEvent;
import pl.psnc.eosc.rs.preprocessor.app.context.resource.synchronization.user.model.UserEvent;

@Configuration
public class ConverterConfig {

    /*
    The most general, initial converter for the rest.
     */
    @Bean
    public GenericConverterImpl<ResourceEvent> resourceEventConverter(){
        return new GenericConverterImpl<>(ResourceEvent.class);
    }

    /*
    The converter for messages regarding Users.
     */
    @Bean
    public GenericConverterImpl<UserEvent> userEventConverter(){
        return new GenericConverterImpl<>(UserEvent.class);
    }

    /*
    The converter for messages regarding Services.
     */
    @Bean
    public GenericConverterImpl<ServiceEvent> serviceEventConverter(){
        return new GenericConverterImpl<>(ServiceEvent.class);
    }

    /*
    The converter for messages regarding Trainings.
     */
    @Bean
    public GenericConverterImpl<TrainingParentEvent> trainingParentEventConverter(){
        return new GenericConverterImpl<>(TrainingParentEvent.class);
    }
}
