package pl.psnc.eosc.rs.preprocessor.app.cloud.jms;

public interface ProviderComponentMessageBroadcaster {

    void subscribe(ProviderComponentResourceSubscriber subscriber);
}
