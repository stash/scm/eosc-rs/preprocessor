package pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular.model.statistics.database;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
@AllArgsConstructor
@NoArgsConstructor
@Getter
public class ResourceDayKey implements Serializable {

    @Column(name="resource_type",updatable = false, nullable = false)
    private Short resourceType;

    @Column(name="resource_id",updatable = false, nullable = false)
    private String resourceId;

    @Column(updatable = false, nullable = false)
    private Integer day;
}
