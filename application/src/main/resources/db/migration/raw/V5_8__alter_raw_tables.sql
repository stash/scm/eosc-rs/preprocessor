ALTER TABLE raw_backup.resource_table ALTER COLUMN raw_resource_event TYPE VARCHAR(8192);
ALTER TABLE raw_backup.user_action_table ALTER COLUMN raw_user_action TYPE VARCHAR(8192);