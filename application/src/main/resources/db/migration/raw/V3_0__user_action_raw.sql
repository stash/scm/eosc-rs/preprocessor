CREATE SCHEMA IF NOT EXISTS user_action;
CREATE TABLE IF NOT EXISTS user_action.user_action_table (id BIGSERIAL PRIMARY KEY, timestamp BIGINT NOT NULL, raw_user_action VARCHAR(2040) NOT NULL);