CREATE SCHEMA IF NOT EXISTS sample;
CREATE TABLE IF NOT EXISTS sample.publication_table (resource_id VARCHAR(64) PRIMARY KEY , resource_category VARCHAR(32) NOT NULL);