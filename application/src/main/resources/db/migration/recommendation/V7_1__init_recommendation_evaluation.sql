CREATE TABLE IF NOT EXISTS recommendation.evaluation_table (id BIGSERIAL PRIMARY KEY, timestamp BIGINT NOT NULL,
    raw_evaluation_event VARCHAR NOT NULL);