CREATE SCHEMA IF NOT EXISTS recommendation;
CREATE TABLE IF NOT EXISTS recommendation.recommendation_table (id BIGSERIAL PRIMARY KEY, timestamp BIGINT NOT NULL,
    aai_uid VARCHAR, user_id VARCHAR, unique_id VARCHAR, client_id VARCHAR,
    panel_id VARCHAR, engine_version VARCHAR, recommendations VARCHAR[], raw_recommendation_event VARCHAR NOT NULL);