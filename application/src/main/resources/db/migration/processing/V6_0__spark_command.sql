CREATE SCHEMA IF NOT EXISTS spark;

CREATE TABLE IF NOT EXISTS spark.command_table (
    command_id BIGSERIAL PRIMARY KEY,
    timestamp BIGINT NOT NULL,
    processing_id VARCHAR(32),
    status VARCHAR(16),
    type VARCHAR(32) NOT NULL,
    is_started_manually BOOLEAN NOT NULL,
    java_object BYTEA NOT NULL,
    command_predecessor_id BIGINT
    );