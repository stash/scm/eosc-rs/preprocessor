ALTER TABLE base.visits_week_weight_view RENAME COLUMN week_visits to visits;
ALTER TABLE base.visits_month_weight_view RENAME COLUMN month_visits to visits;
ALTER TABLE base.orders_week_weight_view RENAME COLUMN week_orders to orders;
ALTER TABLE base.orders_month_weight_view RENAME COLUMN month_orders to orders;