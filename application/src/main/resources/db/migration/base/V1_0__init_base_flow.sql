CREATE SCHEMA IF NOT EXISTS base;

CREATE TABLE IF NOT EXISTS base.resource_type_table (id SMALLINT PRIMARY KEY, resource_name VARCHAR(32) NOT NULL);

INSERT INTO base.resource_type_table (id, resource_name) VALUES (0, 'service') ON CONFLICT DO NOTHING RETURNING *;
INSERT INTO base.resource_type_table (id, resource_name) VALUES (1, 'software') ON CONFLICT DO NOTHING RETURNING *;
INSERT INTO base.resource_type_table (id, resource_name) VALUES (2, 'publication') ON CONFLICT DO NOTHING RETURNING *;
INSERT INTO base.resource_type_table (id, resource_name) VALUES (3, 'dataset') ON CONFLICT DO NOTHING RETURNING *;
INSERT INTO base.resource_type_table (id, resource_name) VALUES (4, 'training') ON CONFLICT DO NOTHING RETURNING *;


CREATE TABLE IF NOT EXISTS base.stats_day_table (day INTEGER NOT NULL, resource_type SMALLINT NOT NULL REFERENCES base.resource_type_table, resource_id VARCHAR(64) NOT NULL, orders INTEGER NOT NULL DEFAULT '0', visits INTEGER NOT NULL DEFAULT '1', PRIMARY KEY (day, resource_type, resource_id)) PARTITION BY LIST(resource_type);
CREATE TABLE IF NOT EXISTS base.stats_total_table (resource_type SMALLINT NOT NULL REFERENCES base.resource_type_table, resource_id VARCHAR(64) NOT NULL, orders INTEGER NOT NULL DEFAULT '0', visits INTEGER NOT NULL DEFAULT '1', PRIMARY KEY (resource_type, resource_id)) PARTITION BY LIST(resource_type);
CREATE TABLE IF NOT EXISTS base.today_table (user_id VARCHAR(36) NOT NULL, day INTEGER NOT NULL, resource_type SMALLINT NOT NULL REFERENCES base.resource_type_table, resource_id VARCHAR(64) NOT NULL, ordered BOOLEAN NOT NULL DEFAULT false, visited BOOLEAN NOT NULL DEFAULT true, PRIMARY KEY (user_id, day, resource_type, resource_id));

CREATE INDEX IF NOT EXISTS day_index ON base.stats_day_table (day);

CREATE TABLE IF NOT EXISTS base.stats_day_table_service PARTITION OF base.stats_day_table FOR VALUES IN (0);
CREATE TABLE IF NOT EXISTS base.stats_day_table_software PARTITION OF base.stats_day_table FOR VALUES IN (1);
CREATE TABLE IF NOT EXISTS base.stats_day_table_publication PARTITION OF base.stats_day_table FOR VALUES IN (2);
CREATE TABLE IF NOT EXISTS base.stats_day_table_dataset PARTITION OF base.stats_day_table FOR VALUES IN (3);
CREATE TABLE IF NOT EXISTS base.stats_day_table_training PARTITION OF base.stats_day_table FOR VALUES IN (4);
CREATE TABLE IF NOT EXISTS base.stats_day_table_other PARTITION OF base.stats_day_table DEFAULT;

CREATE TABLE IF NOT EXISTS base.stats_total_table_service PARTITION OF base.stats_total_table FOR VALUES IN (0);
CREATE TABLE IF NOT EXISTS base.stats_total_table_software PARTITION OF base.stats_total_table FOR VALUES IN (1);
CREATE TABLE IF NOT EXISTS base.stats_total_table_publication PARTITION OF base.stats_total_table FOR VALUES IN (2);
CREATE TABLE IF NOT EXISTS base.stats_total_table_dataset PARTITION OF base.stats_total_table FOR VALUES IN (3);
CREATE TABLE IF NOT EXISTS base.stats_total_table_training PARTITION OF base.stats_total_table FOR VALUES IN (4);
CREATE TABLE IF NOT EXISTS base.stats_total_table_other PARTITION OF base.stats_total_table DEFAULT;