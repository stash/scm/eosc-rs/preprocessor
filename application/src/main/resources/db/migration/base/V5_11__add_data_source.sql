INSERT INTO base.resource_type_table (id, resource_name) VALUES (7, 'datasource') ON CONFLICT DO NOTHING RETURNING *;

CREATE TABLE IF NOT EXISTS base.stats_day_table_datasource PARTITION OF base.stats_day_table FOR VALUES IN (7);
CREATE TABLE IF NOT EXISTS base.stats_total_table_datasource PARTITION OF base.stats_total_table FOR VALUES IN (7);