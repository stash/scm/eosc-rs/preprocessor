INSERT INTO base.resource_type_table (id, resource_name) VALUES (5, 'other') ON CONFLICT DO NOTHING RETURNING *;
INSERT INTO base.resource_type_table (id, resource_name) VALUES (6, 'mismatched') ON CONFLICT DO NOTHING RETURNING *;

ALTER TABLE base.stats_day_table_other RENAME TO stats_day_table_mismatched;
ALTER TABLE base.stats_total_table_other RENAME TO stats_total_table_mismatched;

CREATE TABLE IF NOT EXISTS base.stats_day_table_other PARTITION OF base.stats_day_table FOR VALUES IN (5);
CREATE TABLE IF NOT EXISTS base.stats_total_table_other PARTITION OF base.stats_total_table FOR VALUES IN (5);