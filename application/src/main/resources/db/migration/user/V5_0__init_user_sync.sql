CREATE SCHEMA IF NOT EXISTS sync;
CREATE TABLE IF NOT EXISTS sync.user_table (aai_id VARCHAR(128) PRIMARY KEY, marketplace_id VARCHAR(8) NOT NULL,
                                            scientific_domains integer[], categories integer[], accessed_services integer[]);