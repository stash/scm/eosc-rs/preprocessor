-- SERVICE
CREATE MATERIALIZED VIEW IF NOT EXISTS artificial.artificial_service_visits_month_weight_view AS
SELECT resource_type, resource_id, SUM(v_day) as month_visits FROM
    (
        SELECT resource_type, resource_id, SUM(visits)::float as v_day FROM artificial.artificial_stats_day_table_service stat
                                                                         INNER JOIN
                                                                     (SELECT DISTINCT day FROM artificial.artificial_stats_day_table ORDER BY day desc LIMIT 7) as stat1
                                                                     ON stat.day = stat1.day
        GROUP BY stat.day, resource_type, resource_id
        UNION ALL
        SELECT resource_type, resource_id, SUM(visits) * 1/2::float as v_day FROM artificial.artificial_stats_day_table_service stat
                                                                               INNER JOIN
                                                                           (SELECT DISTINCT day FROM artificial.artificial_stats_day_table ORDER BY day desc LIMIT 7 OFFSET 7) as stat1
                                                                           ON stat.day = stat1.day
        GROUP BY stat.day, resource_type, resource_id
        UNION ALL
        SELECT resource_type, resource_id, SUM(visits) * 1/3::float as v_day FROM artificial.artificial_stats_day_table_service stat
                                                                               INNER JOIN
                                                                           (SELECT DISTINCT day FROM artificial.artificial_stats_day_table ORDER BY day desc LIMIT 7 OFFSET 14) as stat1
                                                                           ON stat.day = stat1.day
        GROUP BY stat.day, resource_type, resource_id
        UNION ALL
        SELECT resource_type, resource_id, SUM(visits) * 1/4::float as v_day FROM artificial.artificial_stats_day_table_service stat
                                                                               INNER JOIN
                                                                           (SELECT DISTINCT day FROM artificial.artificial_stats_day_table ORDER BY day desc LIMIT 7 OFFSET 21) as stat1
                                                                           ON stat.day = stat1.day
        GROUP BY stat.day, resource_type, resource_id
    ) AS subquery
GROUP BY resource_type, resource_id
ORDER BY month_visits DESC
LIMIT 100;

-- SOFTWARE
CREATE MATERIALIZED VIEW IF NOT EXISTS artificial.artificial_software_visits_month_weight_view AS
SELECT resource_type, resource_id, SUM(v_day) as month_visits FROM
    (
        SELECT resource_type, resource_id, SUM(visits)::float as v_day FROM artificial.artificial_stats_day_table_software stat
                                                                         INNER JOIN
                                                                     (SELECT DISTINCT day FROM artificial.artificial_stats_day_table ORDER BY day desc LIMIT 7) as stat1
                                                                     ON stat.day = stat1.day
        GROUP BY stat.day, resource_type, resource_id
        UNION ALL
        SELECT resource_type, resource_id, SUM(visits) * 1/2::float as v_day FROM artificial.artificial_stats_day_table_software stat
                                                                               INNER JOIN
                                                                           (SELECT DISTINCT day FROM artificial.artificial_stats_day_table ORDER BY day desc LIMIT 7 OFFSET 7) as stat1
                                                                           ON stat.day = stat1.day
        GROUP BY stat.day, resource_type, resource_id
        UNION ALL
        SELECT resource_type, resource_id, SUM(visits) * 1/3::float as v_day FROM artificial.artificial_stats_day_table_software stat
                                                                               INNER JOIN
                                                                           (SELECT DISTINCT day FROM artificial.artificial_stats_day_table ORDER BY day desc LIMIT 7 OFFSET 14) as stat1
                                                                           ON stat.day = stat1.day
        GROUP BY stat.day, resource_type, resource_id
        UNION ALL
        SELECT resource_type, resource_id, SUM(visits) * 1/4::float as v_day FROM artificial.artificial_stats_day_table_software stat
                                                                               INNER JOIN
                                                                           (SELECT DISTINCT day FROM artificial.artificial_stats_day_table ORDER BY day desc LIMIT 7 OFFSET 21) as stat1
                                                                           ON stat.day = stat1.day
        GROUP BY stat.day, resource_type, resource_id
    ) AS subquery
GROUP BY resource_type, resource_id
ORDER BY month_visits DESC
LIMIT 100;

-- PUBLICATION
CREATE MATERIALIZED VIEW IF NOT EXISTS artificial.artificial_publication_visits_month_weight_view AS
SELECT resource_type, resource_id, SUM(v_day) as month_visits FROM
    (
        SELECT resource_type, resource_id, SUM(visits)::float as v_day FROM artificial.artificial_stats_day_table_publication stat
                                                                         INNER JOIN
                                                                     (SELECT DISTINCT day FROM artificial.artificial_stats_day_table ORDER BY day desc LIMIT 7) as stat1
                                                                     ON stat.day = stat1.day
        GROUP BY stat.day, resource_type, resource_id
        UNION ALL
        SELECT resource_type, resource_id, SUM(visits) * 1/2::float as v_day FROM artificial.artificial_stats_day_table_publication stat
                                                                               INNER JOIN
                                                                           (SELECT DISTINCT day FROM artificial.artificial_stats_day_table ORDER BY day desc LIMIT 7 OFFSET 7) as stat1
                                                                           ON stat.day = stat1.day
        GROUP BY stat.day, resource_type, resource_id
        UNION ALL
        SELECT resource_type, resource_id, SUM(visits) * 1/3::float as v_day FROM artificial.artificial_stats_day_table_publication stat
                                                                               INNER JOIN
                                                                           (SELECT DISTINCT day FROM artificial.artificial_stats_day_table ORDER BY day desc LIMIT 7 OFFSET 14) as stat1
                                                                           ON stat.day = stat1.day
        GROUP BY stat.day, resource_type, resource_id
        UNION ALL
        SELECT resource_type, resource_id, SUM(visits) * 1/4::float as v_day FROM artificial.artificial_stats_day_table_publication stat
                                                                               INNER JOIN
                                                                           (SELECT DISTINCT day FROM artificial.artificial_stats_day_table ORDER BY day desc LIMIT 7 OFFSET 21) as stat1
                                                                           ON stat.day = stat1.day
        GROUP BY stat.day, resource_type, resource_id
    ) AS subquery
GROUP BY resource_type, resource_id
ORDER BY month_visits DESC
LIMIT 100;

-- DATASET
CREATE MATERIALIZED VIEW IF NOT EXISTS artificial.artificial_dataset_visits_month_weight_view AS
SELECT resource_type, resource_id, SUM(v_day) as month_visits FROM
    (
        SELECT resource_type, resource_id, SUM(visits)::float as v_day FROM artificial.artificial_stats_day_table_dataset stat
                                                                         INNER JOIN
                                                                     (SELECT DISTINCT day FROM artificial.artificial_stats_day_table ORDER BY day desc LIMIT 7) as stat1
                                                                     ON stat.day = stat1.day
        GROUP BY stat.day, resource_type, resource_id
        UNION ALL
        SELECT resource_type, resource_id, SUM(visits) * 1/2::float as v_day FROM artificial.artificial_stats_day_table_dataset stat
                                                                               INNER JOIN
                                                                           (SELECT DISTINCT day FROM artificial.artificial_stats_day_table ORDER BY day desc LIMIT 7 OFFSET 7) as stat1
                                                                           ON stat.day = stat1.day
        GROUP BY stat.day, resource_type, resource_id
        UNION ALL
        SELECT resource_type, resource_id, SUM(visits) * 1/3::float as v_day FROM artificial.artificial_stats_day_table_dataset stat
                                                                               INNER JOIN
                                                                           (SELECT DISTINCT day FROM artificial.artificial_stats_day_table ORDER BY day desc LIMIT 7 OFFSET 14) as stat1
                                                                           ON stat.day = stat1.day
        GROUP BY stat.day, resource_type, resource_id
        UNION ALL
        SELECT resource_type, resource_id, SUM(visits) * 1/4::float as v_day FROM artificial.artificial_stats_day_table_dataset stat
                                                                               INNER JOIN
                                                                           (SELECT DISTINCT day FROM artificial.artificial_stats_day_table ORDER BY day desc LIMIT 7 OFFSET 21) as stat1
                                                                           ON stat.day = stat1.day
        GROUP BY stat.day, resource_type, resource_id
    ) AS subquery
GROUP BY resource_type, resource_id
ORDER BY month_visits DESC
LIMIT 100;

-- TRAINING
CREATE MATERIALIZED VIEW IF NOT EXISTS artificial.artificial_training_visits_month_weight_view AS
SELECT resource_type, resource_id, SUM(v_day) as month_visits FROM
    (
        SELECT resource_type, resource_id, SUM(visits)::float as v_day FROM artificial.artificial_stats_day_table_training stat
                                                                         INNER JOIN
                                                                     (SELECT DISTINCT day FROM artificial.artificial_stats_day_table ORDER BY day desc LIMIT 7) as stat1
                                                                     ON stat.day = stat1.day
        GROUP BY stat.day, resource_type, resource_id
        UNION ALL
        SELECT resource_type, resource_id, SUM(visits) * 1/2::float as v_day FROM artificial.artificial_stats_day_table_training stat
                                                                               INNER JOIN
                                                                           (SELECT DISTINCT day FROM artificial.artificial_stats_day_table ORDER BY day desc LIMIT 7 OFFSET 7) as stat1
                                                                           ON stat.day = stat1.day
        GROUP BY stat.day, resource_type, resource_id
        UNION ALL
        SELECT resource_type, resource_id, SUM(visits) * 1/3::float as v_day FROM artificial.artificial_stats_day_table_training stat
                                                                               INNER JOIN
                                                                           (SELECT DISTINCT day FROM artificial.artificial_stats_day_table ORDER BY day desc LIMIT 7 OFFSET 14) as stat1
                                                                           ON stat.day = stat1.day
        GROUP BY stat.day, resource_type, resource_id
        UNION ALL
        SELECT resource_type, resource_id, SUM(visits) * 1/4::float as v_day FROM artificial.artificial_stats_day_table_training stat
                                                                               INNER JOIN
                                                                           (SELECT DISTINCT day FROM artificial.artificial_stats_day_table ORDER BY day desc LIMIT 7 OFFSET 21) as stat1
                                                                           ON stat.day = stat1.day
        GROUP BY stat.day, resource_type, resource_id
    ) AS subquery
GROUP BY resource_type, resource_id
ORDER BY month_visits DESC
LIMIT 100;

-- OTHER
CREATE MATERIALIZED VIEW IF NOT EXISTS artificial.artificial_other_visits_month_weight_view AS
SELECT resource_type, resource_id, SUM(v_day) as month_visits FROM
    (
        SELECT resource_type, resource_id, SUM(visits)::float as v_day FROM artificial.artificial_stats_day_table_other stat
                                                                         INNER JOIN
                                                                     (SELECT DISTINCT day FROM artificial.artificial_stats_day_table ORDER BY day desc LIMIT 7) as stat1
                                                                     ON stat.day = stat1.day
        GROUP BY stat.day, resource_type, resource_id
        UNION ALL
        SELECT resource_type, resource_id, SUM(visits) * 1/2::float as v_day FROM artificial.artificial_stats_day_table_other stat
                                                                               INNER JOIN
                                                                           (SELECT DISTINCT day FROM artificial.artificial_stats_day_table ORDER BY day desc LIMIT 7 OFFSET 7) as stat1
                                                                           ON stat.day = stat1.day
        GROUP BY stat.day, resource_type, resource_id
        UNION ALL
        SELECT resource_type, resource_id, SUM(visits) * 1/3::float as v_day FROM artificial.artificial_stats_day_table_other stat
                                                                               INNER JOIN
                                                                           (SELECT DISTINCT day FROM artificial.artificial_stats_day_table ORDER BY day desc LIMIT 7 OFFSET 14) as stat1
                                                                           ON stat.day = stat1.day
        GROUP BY stat.day, resource_type, resource_id
        UNION ALL
        SELECT resource_type, resource_id, SUM(visits) * 1/4::float as v_day FROM artificial.artificial_stats_day_table_other stat
                                                                               INNER JOIN
                                                                           (SELECT DISTINCT day FROM artificial.artificial_stats_day_table ORDER BY day desc LIMIT 7 OFFSET 21) as stat1
                                                                           ON stat.day = stat1.day
        GROUP BY stat.day, resource_type, resource_id
    ) AS subquery
GROUP BY resource_type, resource_id
ORDER BY month_visits DESC
LIMIT 100;

CREATE OR REPLACE VIEW artificial.artificial_visits_month_weight_view AS
SELECT resource_type, resource_id, month_visits
FROM
    (
        SELECT resource_type, resource_id, month_visits
        FROM artificial.artificial_service_visits_month_weight_view
        UNION
        SELECT resource_type, resource_id, month_visits
        FROM artificial.artificial_software_visits_month_weight_view
        UNION
        SELECT resource_type, resource_id, month_visits
        FROM artificial.artificial_publication_visits_month_weight_view
        UNION
        SELECT resource_type, resource_id, month_visits
        FROM artificial.artificial_training_visits_month_weight_view
        UNION
        SELECT resource_type, resource_id, month_visits
        FROM artificial.artificial_dataset_visits_month_weight_view
        UNION
        SELECT resource_type, resource_id, month_visits
        FROM artificial.artificial_other_visits_month_weight_view
    ) AS subquery;