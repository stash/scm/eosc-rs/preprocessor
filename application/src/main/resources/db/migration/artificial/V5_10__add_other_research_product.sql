ALTER TABLE artificial.artificial_stats_day_table_other RENAME TO artificial_stats_day_table_mismatched;
ALTER TABLE artificial.artificial_stats_total_table_other RENAME TO artificial_stats_total_table_mismatched;

CREATE TABLE IF NOT EXISTS artificial.artificial_stats_day_table_other PARTITION OF artificial.artificial_stats_day_table FOR VALUES IN (5);
CREATE TABLE IF NOT EXISTS artificial.artificial_stats_total_table_other PARTITION OF artificial.artificial_stats_total_table FOR VALUES IN (5);