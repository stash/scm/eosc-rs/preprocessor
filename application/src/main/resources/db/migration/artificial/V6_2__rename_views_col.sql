ALTER TABLE artificial.artificial_visits_week_weight_view RENAME COLUMN week_visits to visits;
ALTER TABLE artificial.artificial_visits_month_weight_view RENAME COLUMN month_visits to visits;
ALTER TABLE artificial.artificial_orders_week_weight_view RENAME COLUMN week_orders to orders;
ALTER TABLE artificial.artificial_orders_month_weight_view RENAME COLUMN month_orders to orders;