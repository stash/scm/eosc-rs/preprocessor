-- SERVICE
CREATE MATERIALIZED VIEW IF NOT EXISTS artificial.artificial_service_orders_week_weight_view AS
SELECT resource_type, resource_id, SUM(o_day) as week_orders FROM
    (
        SELECT resource_type, resource_id, SUM(orders)::float as o_day FROM artificial.artificial_stats_day_table_service as stat
                                                                         INNER JOIN
                                                                     (SELECT DISTINCT day FROM artificial.artificial_stats_day_table ORDER BY day desc LIMIT 1) as stat1
                                                                     ON stat.day = stat1.day
        GROUP BY stat.day, resource_type, resource_id
        UNION ALL
        SELECT resource_type, resource_id, SUM(orders) * 1/2::float as o_day FROM artificial.artificial_stats_day_table_service as stat
                                                                               INNER JOIN
                                                                           (SELECT DISTINCT day FROM artificial.artificial_stats_day_table ORDER BY day desc LIMIT 1 OFFSET 1) as stat1
                                                                           ON stat.day = stat1.day
        GROUP BY stat.day, resource_type, resource_id
        UNION ALL
        SELECT resource_type, resource_id, SUM(orders) * 1/3::float as o_day FROM artificial.artificial_stats_day_table_service as stat
                                                                               INNER JOIN
                                                                           (SELECT DISTINCT day FROM artificial.artificial_stats_day_table ORDER BY day desc LIMIT 1 OFFSET 2) as stat1
                                                                           ON stat.day = stat1.day
        GROUP BY stat.day, resource_type, resource_id
        UNION ALL
        SELECT resource_type, resource_id, SUM(orders) * 1/4::float as o_day FROM artificial.artificial_stats_day_table_service as stat
                                                                               INNER JOIN
                                                                           (SELECT DISTINCT day FROM artificial.artificial_stats_day_table ORDER BY day desc LIMIT 1 OFFSET 3) as stat1
                                                                           ON stat.day = stat1.day
        GROUP BY stat.day, resource_type, resource_id
        UNION ALL
        SELECT resource_type, resource_id, SUM(orders) * 1/5::float as o_day FROM artificial.artificial_stats_day_table_service as stat
                                                                               INNER JOIN
                                                                           (SELECT DISTINCT day FROM artificial.artificial_stats_day_table ORDER BY day desc LIMIT 1 OFFSET 4) as stat1
                                                                           ON stat.day = stat1.day
        GROUP BY stat.day, resource_type, resource_id
        UNION ALL
        SELECT resource_type, resource_id, SUM(orders) * 1/6::float as o_day FROM artificial.artificial_stats_day_table_service as stat
                                                                               INNER JOIN
                                                                           (SELECT DISTINCT day FROM artificial.artificial_stats_day_table ORDER BY day desc LIMIT 1 OFFSET 5) as stat1
                                                                           ON stat.day = stat1.day
        GROUP BY stat.day, resource_type, resource_id
        UNION ALL
        SELECT resource_type, resource_id, SUM(orders) * 1/7::float as o_day FROM artificial.artificial_stats_day_table_service as stat
                                                                               INNER JOIN
                                                                           (SELECT DISTINCT day FROM artificial.artificial_stats_day_table ORDER BY day desc LIMIT 1 OFFSET 6) as stat1
                                                                           ON stat.day = stat1.day
        GROUP BY stat.day, resource_type, resource_id
    ) AS subquery
GROUP BY resource_type, resource_id
ORDER BY week_orders DESC
LIMIT 100;

-- SOFTWARE
CREATE MATERIALIZED VIEW IF NOT EXISTS artificial.artificial_software_orders_week_weight_view AS
SELECT resource_type, resource_id, SUM(o_day) as week_orders FROM
    (
        SELECT resource_type, resource_id, SUM(orders)::float as o_day FROM artificial.artificial_stats_day_table_software as stat
                                                                         INNER JOIN
                                                                     (SELECT DISTINCT day FROM artificial.artificial_stats_day_table ORDER BY day desc LIMIT 1) as stat1
                                                                     ON stat.day = stat1.day
        GROUP BY stat.day, resource_type, resource_id
        UNION ALL
        SELECT resource_type, resource_id, SUM(orders) * 1/2::float as o_day FROM artificial.artificial_stats_day_table_software as stat
                                                                               INNER JOIN
                                                                           (SELECT DISTINCT day FROM artificial.artificial_stats_day_table ORDER BY day desc LIMIT 1 OFFSET 1) as stat1
                                                                           ON stat.day = stat1.day
        GROUP BY stat.day, resource_type, resource_id
        UNION ALL
        SELECT resource_type, resource_id, SUM(orders) * 1/3::float as o_day FROM artificial.artificial_stats_day_table_software as stat
                                                                               INNER JOIN
                                                                           (SELECT DISTINCT day FROM artificial.artificial_stats_day_table ORDER BY day desc LIMIT 1 OFFSET 2) as stat1
                                                                           ON stat.day = stat1.day
        GROUP BY stat.day, resource_type, resource_id
        UNION ALL
        SELECT resource_type, resource_id, SUM(orders) * 1/4::float as o_day FROM artificial.artificial_stats_day_table_software as stat
                                                                               INNER JOIN
                                                                           (SELECT DISTINCT day FROM artificial.artificial_stats_day_table ORDER BY day desc LIMIT 1 OFFSET 3) as stat1
                                                                           ON stat.day = stat1.day
        GROUP BY stat.day, resource_type, resource_id
        UNION ALL
        SELECT resource_type, resource_id, SUM(orders) * 1/5::float as o_day FROM artificial.artificial_stats_day_table_software as stat
                                                                               INNER JOIN
                                                                           (SELECT DISTINCT day FROM artificial.artificial_stats_day_table ORDER BY day desc LIMIT 1 OFFSET 4) as stat1
                                                                           ON stat.day = stat1.day
        GROUP BY stat.day, resource_type, resource_id
        UNION ALL
        SELECT resource_type, resource_id, SUM(orders) * 1/6::float as o_day FROM artificial.artificial_stats_day_table_software as stat
                                                                               INNER JOIN
                                                                           (SELECT DISTINCT day FROM artificial.artificial_stats_day_table ORDER BY day desc LIMIT 1 OFFSET 5) as stat1
                                                                           ON stat.day = stat1.day
        GROUP BY stat.day, resource_type, resource_id
        UNION ALL
        SELECT resource_type, resource_id, SUM(orders) * 1/7::float as o_day FROM artificial.artificial_stats_day_table_software as stat
                                                                               INNER JOIN
                                                                           (SELECT DISTINCT day FROM artificial.artificial_stats_day_table ORDER BY day desc LIMIT 1 OFFSET 6) as stat1
                                                                           ON stat.day = stat1.day
        GROUP BY stat.day, resource_type, resource_id
    ) AS subquery
GROUP BY resource_type, resource_id
ORDER BY week_orders DESC
LIMIT 100;

-- PUBLICATION
CREATE MATERIALIZED VIEW IF NOT EXISTS artificial.artificial_publication_orders_week_weight_view AS
SELECT resource_type, resource_id, SUM(o_day) as week_orders FROM
    (
        SELECT resource_type, resource_id, SUM(orders)::float as o_day FROM artificial.artificial_stats_day_table_publication as stat
                                                                         INNER JOIN
                                                                     (SELECT DISTINCT day FROM artificial.artificial_stats_day_table ORDER BY day desc LIMIT 1) as stat1
                                                                     ON stat.day = stat1.day
        GROUP BY stat.day, resource_type, resource_id
        UNION ALL
        SELECT resource_type, resource_id, SUM(orders) * 1/2::float as o_day FROM artificial.artificial_stats_day_table_publication as stat
                                                                               INNER JOIN
                                                                           (SELECT DISTINCT day FROM artificial.artificial_stats_day_table ORDER BY day desc LIMIT 1 OFFSET 1) as stat1
                                                                           ON stat.day = stat1.day
        GROUP BY stat.day, resource_type, resource_id
        UNION ALL
        SELECT resource_type, resource_id, SUM(orders) * 1/3::float as o_day FROM artificial.artificial_stats_day_table_publication as stat
                                                                               INNER JOIN
                                                                           (SELECT DISTINCT day FROM artificial.artificial_stats_day_table ORDER BY day desc LIMIT 1 OFFSET 2) as stat1
                                                                           ON stat.day = stat1.day
        GROUP BY stat.day, resource_type, resource_id
        UNION ALL
        SELECT resource_type, resource_id, SUM(orders) * 1/4::float as o_day FROM artificial.artificial_stats_day_table_publication as stat
                                                                               INNER JOIN
                                                                           (SELECT DISTINCT day FROM artificial.artificial_stats_day_table ORDER BY day desc LIMIT 1 OFFSET 3) as stat1
                                                                           ON stat.day = stat1.day
        GROUP BY stat.day, resource_type, resource_id
        UNION ALL
        SELECT resource_type, resource_id, SUM(orders) * 1/5::float as o_day FROM artificial.artificial_stats_day_table_publication as stat
                                                                               INNER JOIN
                                                                           (SELECT DISTINCT day FROM artificial.artificial_stats_day_table ORDER BY day desc LIMIT 1 OFFSET 4) as stat1
                                                                           ON stat.day = stat1.day
        GROUP BY stat.day, resource_type, resource_id
        UNION ALL
        SELECT resource_type, resource_id, SUM(orders) * 1/6::float as o_day FROM artificial.artificial_stats_day_table_publication as stat
                                                                               INNER JOIN
                                                                           (SELECT DISTINCT day FROM artificial.artificial_stats_day_table ORDER BY day desc LIMIT 1 OFFSET 5) as stat1
                                                                           ON stat.day = stat1.day
        GROUP BY stat.day, resource_type, resource_id
        UNION ALL
        SELECT resource_type, resource_id, SUM(orders) * 1/7::float as o_day FROM artificial.artificial_stats_day_table_publication as stat
                                                                               INNER JOIN
                                                                           (SELECT DISTINCT day FROM artificial.artificial_stats_day_table ORDER BY day desc LIMIT 1 OFFSET 6) as stat1
                                                                           ON stat.day = stat1.day
        GROUP BY stat.day, resource_type, resource_id
    ) AS subquery
GROUP BY resource_type, resource_id
ORDER BY week_orders DESC
LIMIT 100;

-- DATASET
CREATE MATERIALIZED VIEW IF NOT EXISTS artificial.artificial_dataset_orders_week_weight_view AS
SELECT resource_type, resource_id, SUM(o_day) as week_orders FROM
    (
        SELECT resource_type, resource_id, SUM(orders)::float as o_day FROM artificial.artificial_stats_day_table_dataset as stat
                                                                         INNER JOIN
                                                                     (SELECT DISTINCT day FROM artificial.artificial_stats_day_table ORDER BY day desc LIMIT 1) as stat1
                                                                     ON stat.day = stat1.day
        GROUP BY stat.day, resource_type, resource_id
        UNION ALL
        SELECT resource_type, resource_id, SUM(orders) * 1/2::float as o_day FROM artificial.artificial_stats_day_table_dataset as stat
                                                                               INNER JOIN
                                                                           (SELECT DISTINCT day FROM artificial.artificial_stats_day_table ORDER BY day desc LIMIT 1 OFFSET 1) as stat1
                                                                           ON stat.day = stat1.day
        GROUP BY stat.day, resource_type, resource_id
        UNION ALL
        SELECT resource_type, resource_id, SUM(orders) * 1/3::float as o_day FROM artificial.artificial_stats_day_table_dataset as stat
                                                                               INNER JOIN
                                                                           (SELECT DISTINCT day FROM artificial.artificial_stats_day_table ORDER BY day desc LIMIT 1 OFFSET 2) as stat1
                                                                           ON stat.day = stat1.day
        GROUP BY stat.day, resource_type, resource_id
        UNION ALL
        SELECT resource_type, resource_id, SUM(orders) * 1/4::float as o_day FROM artificial.artificial_stats_day_table_dataset as stat
                                                                               INNER JOIN
                                                                           (SELECT DISTINCT day FROM artificial.artificial_stats_day_table ORDER BY day desc LIMIT 1 OFFSET 3) as stat1
                                                                           ON stat.day = stat1.day
        GROUP BY stat.day, resource_type, resource_id
        UNION ALL
        SELECT resource_type, resource_id, SUM(orders) * 1/5::float as o_day FROM artificial.artificial_stats_day_table_dataset as stat
                                                                               INNER JOIN
                                                                           (SELECT DISTINCT day FROM artificial.artificial_stats_day_table ORDER BY day desc LIMIT 1 OFFSET 4) as stat1
                                                                           ON stat.day = stat1.day
        GROUP BY stat.day, resource_type, resource_id
        UNION ALL
        SELECT resource_type, resource_id, SUM(orders) * 1/6::float as o_day FROM artificial.artificial_stats_day_table_dataset as stat
                                                                               INNER JOIN
                                                                           (SELECT DISTINCT day FROM artificial.artificial_stats_day_table ORDER BY day desc LIMIT 1 OFFSET 5) as stat1
                                                                           ON stat.day = stat1.day
        GROUP BY stat.day, resource_type, resource_id
        UNION ALL
        SELECT resource_type, resource_id, SUM(orders) * 1/7::float as o_day FROM artificial.artificial_stats_day_table_dataset as stat
                                                                               INNER JOIN
                                                                           (SELECT DISTINCT day FROM artificial.artificial_stats_day_table ORDER BY day desc LIMIT 1 OFFSET 6) as stat1
                                                                           ON stat.day = stat1.day
        GROUP BY stat.day, resource_type, resource_id
    ) AS subquery
GROUP BY resource_type, resource_id
ORDER BY week_orders DESC
LIMIT 100;

-- TRAINING
CREATE MATERIALIZED VIEW IF NOT EXISTS artificial.artificial_training_orders_week_weight_view AS
SELECT resource_type, resource_id, SUM(o_day) as week_orders FROM
    (
        SELECT resource_type, resource_id, SUM(orders)::float as o_day FROM artificial.artificial_stats_day_table_training as stat
                                                                         INNER JOIN
                                                                     (SELECT DISTINCT day FROM artificial.artificial_stats_day_table ORDER BY day desc LIMIT 1) as stat1
                                                                     ON stat.day = stat1.day
        GROUP BY stat.day, resource_type, resource_id
        UNION ALL
        SELECT resource_type, resource_id, SUM(orders) * 1/2::float as o_day FROM artificial.artificial_stats_day_table_training as stat
                                                                               INNER JOIN
                                                                           (SELECT DISTINCT day FROM artificial.artificial_stats_day_table ORDER BY day desc LIMIT 1 OFFSET 1) as stat1
                                                                           ON stat.day = stat1.day
        GROUP BY stat.day, resource_type, resource_id
        UNION ALL
        SELECT resource_type, resource_id, SUM(orders) * 1/3::float as o_day FROM artificial.artificial_stats_day_table_training as stat
                                                                               INNER JOIN
                                                                           (SELECT DISTINCT day FROM artificial.artificial_stats_day_table ORDER BY day desc LIMIT 1 OFFSET 2) as stat1
                                                                           ON stat.day = stat1.day
        GROUP BY stat.day, resource_type, resource_id
        UNION ALL
        SELECT resource_type, resource_id, SUM(orders) * 1/4::float as o_day FROM artificial.artificial_stats_day_table_training as stat
                                                                               INNER JOIN
                                                                           (SELECT DISTINCT day FROM artificial.artificial_stats_day_table ORDER BY day desc LIMIT 1 OFFSET 3) as stat1
                                                                           ON stat.day = stat1.day
        GROUP BY stat.day, resource_type, resource_id
        UNION ALL
        SELECT resource_type, resource_id, SUM(orders) * 1/5::float as o_day FROM artificial.artificial_stats_day_table_training as stat
                                                                               INNER JOIN
                                                                           (SELECT DISTINCT day FROM artificial.artificial_stats_day_table ORDER BY day desc LIMIT 1 OFFSET 4) as stat1
                                                                           ON stat.day = stat1.day
        GROUP BY stat.day, resource_type, resource_id
        UNION ALL
        SELECT resource_type, resource_id, SUM(orders) * 1/6::float as o_day FROM artificial.artificial_stats_day_table_training as stat
                                                                               INNER JOIN
                                                                           (SELECT DISTINCT day FROM artificial.artificial_stats_day_table ORDER BY day desc LIMIT 1 OFFSET 5) as stat1
                                                                           ON stat.day = stat1.day
        GROUP BY stat.day, resource_type, resource_id
        UNION ALL
        SELECT resource_type, resource_id, SUM(orders) * 1/7::float as o_day FROM artificial.artificial_stats_day_table_training as stat
                                                                               INNER JOIN
                                                                           (SELECT DISTINCT day FROM artificial.artificial_stats_day_table ORDER BY day desc LIMIT 1 OFFSET 6) as stat1
                                                                           ON stat.day = stat1.day
        GROUP BY stat.day, resource_type, resource_id
    ) AS subquery
GROUP BY resource_type, resource_id
ORDER BY week_orders DESC
LIMIT 100;

-- OTHER
CREATE MATERIALIZED VIEW IF NOT EXISTS artificial.artificial_other_orders_week_weight_view AS
SELECT resource_type, resource_id, SUM(o_day) as week_orders FROM
    (
        SELECT resource_type, resource_id, SUM(orders)::float as o_day FROM artificial.artificial_stats_day_table_other as stat
                                                                         INNER JOIN
                                                                     (SELECT DISTINCT day FROM artificial.artificial_stats_day_table ORDER BY day desc LIMIT 1) as stat1
                                                                     ON stat.day = stat1.day
        GROUP BY stat.day, resource_type, resource_id
        UNION ALL
        SELECT resource_type, resource_id, SUM(orders) * 1/2::float as o_day FROM artificial.artificial_stats_day_table_other as stat
                                                                               INNER JOIN
                                                                           (SELECT DISTINCT day FROM artificial.artificial_stats_day_table ORDER BY day desc LIMIT 1 OFFSET 1) as stat1
                                                                           ON stat.day = stat1.day
        GROUP BY stat.day, resource_type, resource_id
        UNION ALL
        SELECT resource_type, resource_id, SUM(orders) * 1/3::float as o_day FROM artificial.artificial_stats_day_table_other as stat
                                                                               INNER JOIN
                                                                           (SELECT DISTINCT day FROM artificial.artificial_stats_day_table ORDER BY day desc LIMIT 1 OFFSET 2) as stat1
                                                                           ON stat.day = stat1.day
        GROUP BY stat.day, resource_type, resource_id
        UNION ALL
        SELECT resource_type, resource_id, SUM(orders) * 1/4::float as o_day FROM artificial.artificial_stats_day_table_other as stat
                                                                               INNER JOIN
                                                                           (SELECT DISTINCT day FROM artificial.artificial_stats_day_table ORDER BY day desc LIMIT 1 OFFSET 3) as stat1
                                                                           ON stat.day = stat1.day
        GROUP BY stat.day, resource_type, resource_id
        UNION ALL
        SELECT resource_type, resource_id, SUM(orders) * 1/5::float as o_day FROM artificial.artificial_stats_day_table_other as stat
                                                                               INNER JOIN
                                                                           (SELECT DISTINCT day FROM artificial.artificial_stats_day_table ORDER BY day desc LIMIT 1 OFFSET 4) as stat1
                                                                           ON stat.day = stat1.day
        GROUP BY stat.day, resource_type, resource_id
        UNION ALL
        SELECT resource_type, resource_id, SUM(orders) * 1/6::float as o_day FROM artificial.artificial_stats_day_table_other as stat
                                                                               INNER JOIN
                                                                           (SELECT DISTINCT day FROM artificial.artificial_stats_day_table ORDER BY day desc LIMIT 1 OFFSET 5) as stat1
                                                                           ON stat.day = stat1.day
        GROUP BY stat.day, resource_type, resource_id
        UNION ALL
        SELECT resource_type, resource_id, SUM(orders) * 1/7::float as o_day FROM artificial.artificial_stats_day_table_other as stat
                                                                               INNER JOIN
                                                                           (SELECT DISTINCT day FROM artificial.artificial_stats_day_table ORDER BY day desc LIMIT 1 OFFSET 6) as stat1
                                                                           ON stat.day = stat1.day
        GROUP BY stat.day, resource_type, resource_id
    ) AS subquery
GROUP BY resource_type, resource_id
ORDER BY week_orders DESC
LIMIT 100;

CREATE OR REPLACE VIEW artificial.artificial_orders_week_weight_view AS
SELECT resource_type, resource_id, week_orders
FROM
(
    SELECT resource_type, resource_id, week_orders
    FROM artificial.artificial_service_orders_week_weight_view
    UNION
    SELECT resource_type, resource_id, week_orders
    FROM artificial.artificial_software_orders_week_weight_view
    UNION
    SELECT resource_type, resource_id, week_orders
    FROM artificial.artificial_publication_orders_week_weight_view
    UNION
    SELECT resource_type, resource_id, week_orders
    FROM artificial.artificial_training_orders_week_weight_view
    UNION
    SELECT resource_type, resource_id, week_orders
    FROM artificial.artificial_dataset_orders_week_weight_view
    UNION
    SELECT resource_type, resource_id, week_orders
    FROM artificial.artificial_other_orders_week_weight_view
) AS subquery;