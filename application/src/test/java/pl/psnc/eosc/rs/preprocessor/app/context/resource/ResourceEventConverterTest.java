package pl.psnc.eosc.rs.preprocessor.app.context.resource;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.apache.spark.launcher.SparkAppHandle;
import org.junit.jupiter.api.Test;
import pl.psnc.eosc.rs.preprocessor.app.context.resource.converter.GenericConverterImpl;
import pl.psnc.eosc.rs.preprocessor.app.context.resource.converter.ResourceEvent;
import pl.psnc.eosc.rs.preprocessor.app.context.resource.synchronization.service.model.ServiceEvent;
import pl.psnc.eosc.rs.preprocessor.app.context.resource.synchronization.training.TrainingEvent;
import pl.psnc.eosc.rs.preprocessor.app.context.resource.synchronization.training.TrainingParentEvent;

import java.util.LinkedHashMap;
import java.util.List;

import static com.google.common.truth.Truth.assertThat;

public class ResourceEventConverterTest {

    @Test
    void convertUserEventTest(){
        GenericConverterImpl<ResourceEvent> converter = new GenericConverterImpl<>(ResourceEvent.class);
        String nestedRecord ="{\"id\":2653,\"aai_uid\":\"f46306e0325dbfdf34d0f1dc07c849240e2f6ad1b800f31a3872ea31ef08e2f4@aai.eosc-portal.eu\"," +
                "\"scientific_domains\":[32],\"categories\":[168,203],\"accessed_services\":[]}";
        ResourceEvent result = converter.convert("{\"cud\":\"update\",\"model\":\"User\",\"record\":" +
                nestedRecord+",\"timestamp\":\"2022-10-04T17:41:12Z\"}");
        assertThat(result.getCud()).isEqualTo("update");
        assertThat(result.getModel()).isEqualTo("User");
        assertThat(result.getRecord().toString()).isEqualTo(nestedRecord);
    }

    @Test
    void convertServiceEventTest() {
        GenericConverterImpl<ResourceEvent> resourceConverter = new GenericConverterImpl<>(ResourceEvent.class);
        GenericConverterImpl<ServiceEvent> serviceConverter = new GenericConverterImpl<>(ServiceEvent.class);
        String nestedRecord ="{\"id\":485,\"name\":\"3DBionotes-WS\",\"description\":\"The web platform 3DBionotes-WS integrates multiple Web Services" +
                " and an interactive Web Viewer to provide a unified environment in which biological annotations can be analyzed in their structural" +
                " context.\\n\\nCurrent sources of information include post-translational modifications, genomic variations associated to diseases," +
                " short linear motifs, immune epitopes sites, disordered regions and domain families.\\n\\nSince the COVID-19 outbreak, new structural" +
                " data from many viral proteins have been incorporated in a new 3DBionotes-COVID-19 section.\"," +
                "\"tagline\":\"3DBIONOTES-WS a web application designed to automatically annotate biochemical and biomedical information onto structural models.\"," +
                "\"countries\":[\"WW\"],\"order_type\":\"fully_open_access\",\"rating\":\"0.0\",\"status\":\"published\",\"categories\":[36,62,99,100,142,187,94]," +
                "\"providers\":[235,305,315],\"resource_organisation\":235,\"scientific_domains\":[21],\"platforms\":[],\"target_users\":[1],\"access_modes\":[1612]," +
                "\"access_types\":[1607],\"trls\":[1590],\"life_cycle_statuses\":[1599],\"required_services\":[],\"related_services\":[]}";
        ResourceEvent result = resourceConverter.convert("{\"cud\":\"update\",\"model\":\"Service\",\"record\":" +
                nestedRecord+",\"timestamp\":\"2022-10-04T17:41:12Z\"}");
        assertThat(result.getCud()).isEqualTo("update");
        assertThat(result.getModel()).isEqualTo("Service");
        assertThat(result.getRecord().toString()).isEqualTo(nestedRecord);

        ServiceEvent serviceEvent = serviceConverter.convert(result.getRecord().toString());
        assertThat(serviceEvent.getId()).isEqualTo("485");
        assertThat(serviceEvent.getName()).isEqualTo("3DBionotes-WS");
        assertThat(serviceEvent.getCategoryValues()).containsExactlyElementsIn(List.of(36, 62, 99, 100, 142, 187, 94));
    }

    @Test
    void convertTrainingEvent_create() {
        GenericConverterImpl<TrainingParentEvent> resourceConverter = new GenericConverterImpl<>(TrainingParentEvent.class);
        String json ="{\"metadata\":{\"registeredBy\":\"Marcin Rochowiak\",\"registeredAt\":\"1701254026010\",\"modifiedBy\":\"Marcin Rochowiak\",\"modifiedAt\":\"1701254026010\",\"terms\":null,\"published\":true},\"active\":true,\"suspended\":false,\"identifiers\":{\"originalId\":\"psnc.4302bb7f279048c4c9c62fbfae0e35e1\"},\"migrationStatus\":null,\"loggingInfo\":[{\"date\":\"1701254026010\",\"userEmail\":\"mrochowiak@man.poznan.pl\",\"userFullName\":\"Marcin Rochowiak\",\"userRole\":\"provider\",\"type\":\"onboard\",\"comment\":null,\"actionType\":\"registered\"},{\"date\":\"1701254026012\",\"userEmail\":\"mrochowiak@man.poznan.pl\",\"userFullName\":\"Marcin Rochowiak\",\"userRole\":\"provider\",\"type\":\"onboard\",\"comment\":null,\"actionType\":\"approved\"}],\"latestAuditInfo\":null,\"latestOnboardingInfo\":{\"date\":\"1701254026012\",\"userEmail\":\"mrochowiak@man.poznan.pl\",\"userFullName\":\"Marcin Rochowiak\",\"userRole\":\"provider\",\"type\":\"onboard\",\"comment\":null,\"actionType\":\"approved\"},\"latestUpdateInfo\":null,\"status\":\"approved resource\",\"id\":\"eosc.psnc.4302bb7f279048c4c9c62fbfae0e35e1\",\"trainingResource\":{\"id\":\"eosc.psnc.4302bb7f279048c4c9c62fbfae0e35e1\",\"title\":\"Test Preproc\",\"resourceOrganisation\":\"eosc.psnc\",\"resourceProviders\":[],\"authors\":[\"MArcin\"],\"url\":\"https://test\",\"urlType\":null,\"eoscRelatedServices\":[],\"alternativeIdentifiers\":[{\"type\":\"EOSC PID\",\"value\":\"e39cce9f\"}],\"description\":\"Sample description\",\"keywords\":[\"test resource\"],\"license\":\"test\",\"accessRights\":\"tr_access_right-open_access\",\"versionDate\":1644278400000,\"targetGroups\":[\"target_user-funders\"],\"learningResourceTypes\":[],\"learningOutcomes\":[\"test\"],\"expertiseLevel\":\"tr_expertise_level-advanced\",\"contentResourceTypes\":[],\"qualifications\":[],\"duration\":null,\"languages\":[\"en\"],\"geographicalAvailabilities\":[\"EO\"],\"scientificDomains\":[{\"scientificDomain\":\"scientific_domain-agricultural_sciences\",\"scientificSubdomain\":\"scientific_subdomain-agricultural_sciences-agricultural_biotechnology\"},{\"scientificDomain\":\"scientific_domain-sample_sciences\",\"scientificSubdomain\":\"scientific_subdomain-sample_sciences-agricultural_biotechnology\"}],\"contact\":{\"firstName\":\"Marcin\",\"lastName\":\"Plociennik\",\"email\":\"marcinp@man.poznan.pl\",\"phone\":\"\",\"position\":null,\"organisation\":null},\"catalogueId\":\"eosc\"}}";
        TrainingParentEvent parentEvent = resourceConverter.convert(json);
        TrainingEvent result = parentEvent.getTrainingResource();
        assertThat(result.getId()).isEqualTo("eosc.psnc.4302bb7f279048c4c9c62fbfae0e35e1");
        assertThat(result.getTitle()).isEqualTo("Test Preproc");
        assertThat(result.getDescription()).isEqualTo("Sample description");
        assertThat(result.getAccessRights()).isEqualTo("tr_access_right-open_access");
        assertThat(result.getLanguages()).containsExactly("en");
        assertThat(result.getScientificDomains()).isNotEmpty();
        assertThat(result.getScientificDomains().toString()).isEqualTo("[{scientificDomain=scientific_domain-agricultural_sciences, scientificSubdomain=scientific_subdomain-agricultural_sciences-agricultural_biotechnology}, {scientificDomain=scientific_domain-sample_sciences, scientificSubdomain=scientific_subdomain-sample_sciences-agricultural_biotechnology}]");
    }

    @Test
    void convertTrainingEvent_update() {
        GenericConverterImpl<TrainingParentEvent> resourceConverter = new GenericConverterImpl<>(TrainingParentEvent.class);
        String json ="{\"metadata\":{\"registeredBy\":\"Marcin Rochowiak\",\"registeredAt\":\"1701308570385\",\"modifiedBy\":\"Marcin Rochowiak\",\"modifiedAt\":\"1701308570385\",\"terms\":null,\"published\":true},\"active\":true,\"suspended\":false,\"identifiers\":{\"originalId\":\"psnc.4302bb7f279048c4c9c62fbfae0e35e1\"},\"migrationStatus\":null,\"loggingInfo\":[{\"date\":\"1701308570386\",\"userEmail\":\"mrochowiak@man.poznan.pl\",\"userFullName\":\"Marcin Rochowiak\",\"userRole\":\"user\",\"type\":\"update\",\"comment\":\"Test purposes\",\"actionType\":\"updated\"},{\"date\":\"1701254026012\",\"userEmail\":\"mrochowiak@man.poznan.pl\",\"userFullName\":\"Marcin Rochowiak\",\"userRole\":\"provider\",\"type\":\"onboard\",\"comment\":null,\"actionType\":\"approved\"},{\"date\":\"1701254026010\",\"userEmail\":\"mrochowiak@man.poznan.pl\",\"userFullName\":\"Marcin Rochowiak\",\"userRole\":\"provider\",\"type\":\"onboard\",\"comment\":null,\"actionType\":\"registered\"}],\"latestAuditInfo\":null,\"latestOnboardingInfo\":{\"date\":\"1701254026012\",\"userEmail\":\"mrochowiak@man.poznan.pl\",\"userFullName\":\"Marcin Rochowiak\",\"userRole\":\"provider\",\"type\":\"onboard\",\"comment\":null,\"actionType\":\"approved\"},\"latestUpdateInfo\":{\"date\":\"1701308570386\",\"userEmail\":\"mrochowiak@man.poznan.pl\",\"userFullName\":\"Marcin Rochowiak\",\"userRole\":\"user\",\"type\":\"update\",\"comment\":\"Test purposes\",\"actionType\":\"updated\"},\"status\":\"approved resource\",\"id\":\"eosc.psnc.4302bb7f279048c4c9c62fbfae0e35e1\",\"trainingResource\":{\"id\":\"eosc.psnc.4302bb7f279048c4c9c62fbfae0e35e1\",\"title\":\"Test Preproc Updated\",\"resourceOrganisation\":\"eosc.psnc\",\"resourceProviders\":[],\"authors\":[\"Marcin\"],\"url\":\"https://test\",\"urlType\":null,\"eoscRelatedServices\":[],\"alternativeIdentifiers\":[{\"type\":\"EOSC PID\",\"value\":\"e39cce9f\"}],\"description\":\"Sample description updated\",\"keywords\":[\"test resource\"],\"license\":\"test\",\"accessRights\":\"tr_access_right-restricted_access\",\"versionDate\":1644278400000,\"targetGroups\":[\"target_user-funders\"],\"learningResourceTypes\":[],\"learningOutcomes\":[\"test\"],\"expertiseLevel\":\"tr_expertise_level-advanced\",\"contentResourceTypes\":[],\"qualifications\":[],\"duration\":null,\"languages\":[\"en\",\"pl\"],\"geographicalAvailabilities\":[\"EO\"],\"scientificDomains\":[{\"scientificDomain\":\"scientific_domain-agricultural_sciences\",\"scientificSubdomain\":\"scientific_subdomain-agricultural_sciences-agricultural_biotechnology\"}],\"contact\":{\"firstName\":\"Marcin\",\"lastName\":\"Plociennik\",\"email\":\"marcinp@man.poznan.pl\",\"phone\":\"\",\"position\":null,\"organisation\":null},\"catalogueId\":\"eosc\"}}\n";
        TrainingParentEvent parentEvent = resourceConverter.convert(json);
        TrainingEvent result = parentEvent.getTrainingResource();
        assertThat(result.getId()).isEqualTo("eosc.psnc.4302bb7f279048c4c9c62fbfae0e35e1");
        assertThat(result.getTitle()).isEqualTo("Test Preproc Updated");
        assertThat(result.getDescription()).isEqualTo("Sample description updated");
        assertThat(result.getAccessRights()).isEqualTo("tr_access_right-restricted_access");
        assertThat(result.getLanguages()).containsExactly("en","pl");
        assertThat(result.getScientificDomains()).isNotEmpty();
        assertThat(result.getScientificDomains().toString()).isEqualTo("[{scientificDomain=scientific_domain-agricultural_sciences, scientificSubdomain=scientific_subdomain-agricultural_sciences-agricultural_biotechnology}]");
    }
}
