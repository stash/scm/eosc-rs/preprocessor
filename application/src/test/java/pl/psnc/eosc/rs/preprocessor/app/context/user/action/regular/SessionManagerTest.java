package pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular;

import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.annotation.DirtiesContext;
import pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular.model.internal.SingleSession;
import pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular.model.internal.User;
import pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular.service.session.SessionManagerHandler;
import pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular.service.session.SessionPublisher;
import pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular.service.session.SessionsManagerImpl;

import java.util.UUID;

import static com.google.common.truth.Truth.assertThat;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular.SampleSessionProvider.getSampleSession;

@SpringBootTest(classes = {SessionsManagerImpl.class,SessionPublisher.class})
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class SessionManagerTest {

    @Autowired
    SessionManagerHandler sessionManagerHandler;

    @MockBean
    SessionPublisher sessionPublisher;

    @Test
    void checkSessionCreation_forSameSession_anonymousUser() {
        SingleSession s = getSampleSession();
        ArgumentCaptor<String> keyCaptor = ArgumentCaptor.forClass(String.class);
        ArgumentCaptor<String> valueCaptor = ArgumentCaptor.forClass(String.class);
        User sampleUser = new User(s.getSessionId(),null,null);

        // add first UA
        sessionManagerHandler.addUserAction(sampleUser, s.getUserAction());
        verify(sessionPublisher, times(1)).publish(keyCaptor.capture(),valueCaptor.capture());
        assertThat(keyCaptor.getValue().contains("0daf38fb-30aa-48c3-994a-bab70078a370")).isTrue();
        assertThat(valueCaptor.getValue().contains(
                "  \"sessionId\": \"0daf38fb-30aa-48c3-994a-bab70078a370\",\n" +
                        "  \"isAnonymous\": true,\n" +
                        "  \"numberOfUA\": 1,")).isTrue();

        // add second UA
        sessionManagerHandler.addUserAction(sampleUser, s.getUserAction());
        verify(sessionPublisher, times(2)).publish(keyCaptor.capture(),valueCaptor.capture());
        assertThat(keyCaptor.getValue().contains("0daf38fb-30aa-48c3-994a-bab70078a370")).isTrue();
        assertThat(valueCaptor.getValue().contains(
                "  \"sessionId\": \"0daf38fb-30aa-48c3-994a-bab70078a370\",\n" +
                        "  \"isAnonymous\": true,\n" +
                        "  \"numberOfUA\": 2,")).isTrue();
    }

    @Test
    void checkSessionCreation_forOtherSessions_anonymousUser() {
        SingleSession s = getSampleSession();
        ArgumentCaptor<String> keyCaptor = ArgumentCaptor.forClass(String.class);
        ArgumentCaptor<String> valueCaptor = ArgumentCaptor.forClass(String.class);
        User sampleUser = new User(s.getSessionId(),null,null);
        User sampleOtherUser = new User(UUID.fromString("715cdbc9-799e-4edb-befb-584ad254a120"),
                null,null);

        // add first UA
        sessionManagerHandler.addUserAction(sampleUser, s.getUserAction());
        verify(sessionPublisher, times(1)).publish(keyCaptor.capture(),valueCaptor.capture());
        assertThat(keyCaptor.getValue().contains("0daf38fb-30aa-48c3-994a-bab70078a370")).isTrue();
        assertThat(valueCaptor.getValue().contains(
                "  \"sessionId\": \"0daf38fb-30aa-48c3-994a-bab70078a370\",\n" +
                        "  \"isAnonymous\": true,\n" +
                        "  \"numberOfUA\": 1,")).isTrue();

        // add second UA for other session
        sessionManagerHandler.addUserAction(sampleOtherUser, s.getUserAction());
        verify(sessionPublisher, times(2)).publish(keyCaptor.capture(),valueCaptor.capture());
        assertThat(keyCaptor.getValue().contains("715cdbc9-799e-4edb-befb-584ad254a120")).isTrue();
        assertThat(valueCaptor.getValue().contains(
                "  \"sessionId\": \"715cdbc9-799e-4edb-befb-584ad254a120\",\n" +
                        "  \"isAnonymous\": true,\n" +
                        "  \"numberOfUA\": 1,")).isTrue();
    }

    @Test
    void checkSessionCreation_forSameSession_NonAnonymousUser() {
        SingleSession s = getSampleSession();
        ArgumentCaptor<String> keyCaptor = ArgumentCaptor.forClass(String.class);
        ArgumentCaptor<String> valueCaptor = ArgumentCaptor.forClass(String.class);
        User sampleUser = new User(s.getSessionId(),
                "marketplace_id","aai_uid");

        String expectedSharedOutput = "  \"sessionId\": \"0daf38fb-30aa-48c3-994a-bab70078a370\",\n" +
                "  \"isAnonymous\": false,\n" +
                "  \"aaiUid\": \"aai_uid\",\n" +
                "  \"marketplaceId\": \"marketplace_id\",\n";

        // add first UA
        sessionManagerHandler.addUserAction(sampleUser, s.getUserAction());
        verify(sessionPublisher, times(1)).publish(keyCaptor.capture(),valueCaptor.capture());
        assertThat(keyCaptor.getValue().contains("0daf38fb-30aa-48c3-994a-bab70078a370")).isTrue();
        assertThat(valueCaptor.getValue().contains(
                expectedSharedOutput+"  \"numberOfUA\": 1")).isTrue();

        // add second UA
        sessionManagerHandler.addUserAction(sampleUser, s.getUserAction());
        verify(sessionPublisher, times(2)).publish(keyCaptor.capture(),valueCaptor.capture());
        assertThat(keyCaptor.getValue().contains("0daf38fb-30aa-48c3-994a-bab70078a370")).isTrue();
        assertThat(valueCaptor.getValue().contains(
                expectedSharedOutput+"  \"numberOfUA\": 2")).isTrue();
    }

    @Test
    void checkSessionCreation_forOtherSessions_NonAnonymousUser() {
        SingleSession s = getSampleSession();
        ArgumentCaptor<String> keyCaptor = ArgumentCaptor.forClass(String.class);
        ArgumentCaptor<String> valueCaptor = ArgumentCaptor.forClass(String.class);
        User sampleUser = new User(s.getSessionId(),
                "marketplace_id","aai_uid");
        User sampleOtherUser = new User(UUID.fromString("715cdbc9-799e-4edb-befb-584ad254a120"),
                "marketplace_id","aai_uid");

        String expectedSharedOutput = "  \"isAnonymous\": false,\n" +
                "  \"aaiUid\": \"aai_uid\",\n" +
                "  \"marketplaceId\": \"marketplace_id\",\n" +
                "  \"numberOfUA\": 1,";

        // add first UA
        sessionManagerHandler.addUserAction(sampleUser, s.getUserAction());
        verify(sessionPublisher, times(1)).publish(keyCaptor.capture(),valueCaptor.capture());
        assertThat(keyCaptor.getValue().contains("0daf38fb-30aa-48c3-994a-bab70078a370")).isTrue();
        assertThat(valueCaptor.getValue().contains(
                "  \"sessionId\": \"0daf38fb-30aa-48c3-994a-bab70078a370\",\n" +
                        expectedSharedOutput)).isTrue();

        // add second UA for other session
        sessionManagerHandler.addUserAction(sampleOtherUser, s.getUserAction());
        verify(sessionPublisher, times(2)).publish(keyCaptor.capture(),valueCaptor.capture());
        assertThat(keyCaptor.getValue().contains("715cdbc9-799e-4edb-befb-584ad254a120")).isTrue();
        assertThat(valueCaptor.getValue().contains(
                "  \"sessionId\": \"715cdbc9-799e-4edb-befb-584ad254a120\",\n" +
                        expectedSharedOutput)).isTrue();
    }

}
