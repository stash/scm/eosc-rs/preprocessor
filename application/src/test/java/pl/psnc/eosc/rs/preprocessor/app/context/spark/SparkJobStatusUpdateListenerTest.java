package pl.psnc.eosc.rs.preprocessor.app.context.spark;

import org.apache.spark.launcher.SparkAppHandle;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import pl.psnc.eosc.rs.preprocessor.app.context.spark.processing.ProcessingBroker;
import pl.psnc.eosc.rs.preprocessor.app.context.spark.processing.command.Command;
import pl.psnc.eosc.rs.preprocessor.app.context.spark.processing.listener.SparkJobStatusUpdateListener;

import java.util.Optional;

import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;
import static org.mockito.Mockito.times;

@ExtendWith(MockitoExtension.class)
public class SparkJobStatusUpdateListenerTest {

    @Mock
    SparkAppHandle sparkAppHandle;

    @Mock
    ProcessingBroker processingBroker;

    @Captor
    ArgumentCaptor<Command> commandCaptor;


    @Test
    public void sparkJobStatusUpdateListenerTest() {
        Long commandId = 123L;
        String processingId = "1";
        SparkAppHandle.State state = SparkAppHandle.State.FINISHED;
        when(sparkAppHandle.getState()).thenReturn(state);
        when(sparkAppHandle.getAppId()).thenReturn(processingId);
        SparkJobStatusUpdateListener listener = new SparkJobStatusUpdateListener(processingBroker,commandId);

        //when:
        listener.stateChanged(sparkAppHandle);
        //then:
        verify(processingBroker,times(1)).updateCommand(eq(commandId),eq(processingId),eq(state));
    }
}