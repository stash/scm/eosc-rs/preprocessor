package pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular;

import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular.model.internal.SingleSession;
import pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular.service.user.UserRecognitionHandlerImpl;

import static com.google.common.truth.Truth.assertThat;
import static pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular.SampleSessionProvider.getSampleSession;


public class UserRecognitionTest {

    @Test
    void check_userSignInAndOut() {
        UserRecognitionHandlerImpl userRecognitionHandler = new UserRecognitionHandlerImpl();

        SingleSession s0 = getSampleSession();
        SingleSession s1 = getSampleSession();
        SingleSession s2 = getSampleSession();
        ArgumentCaptor<String> captor = ArgumentCaptor.forClass(String.class);

        // add first UA
        s0.setAaiUid(null);
        userRecognitionHandler.recognizeUser(s0);
        assertThat(s0.getAaiUid()).isNull();

        // add UA with User
        s1.setAaiUid("testUserId");
        userRecognitionHandler.recognizeUser(s1);
        assertThat(s1.getAaiUid()).isNotNull();
        assertThat(s1.getAaiUid()).isEqualTo("testUserId");

        // add UA without User, check if the user was recognised
        s2.setAaiUid(null);
        userRecognitionHandler.recognizeUser(s2);
        assertThat(s2.getAaiUid()).isNotNull();
        assertThat(s2.getAaiUid()).isEqualTo("testUserId");
    }

}