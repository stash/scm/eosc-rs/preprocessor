package pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.NullSource;
import org.junit.jupiter.params.provider.ValueSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import pl.psnc.eosc.rs.preprocessor.app.cloud.jms.MessageBroadcaster;
import pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular.model.internal.SingleSession;
import pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular.service.AbstractUserActionHandler;
import pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular.service.MessageConverter;
import pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular.service.UserActionService;
import pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular.service.UserActionServiceImpl;
import pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular.service.session.SessionManagerHandler;
import pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular.service.statistics.StatisticsHandler;
import pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular.service.statistics.StatisticsStorageImpl;
import pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular.service.user.UserDataSupplier;
import pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular.service.user.UserRecognitionHandler;
import pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular.service.validator.ValidatorHandler;

import static com.google.common.truth.Truth.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular.SampleSessionProvider.getSampleSession;

@SpringBootTest(properties = { "spring.main.allow-bean-definition-overriding = true" },
        classes = {UserActionServiceImpl.class, StatisticsStorageImpl.class})
@Import(UserActionTestConfiguration.class)
public class UserActionServiceTest {

    @Autowired
    UserActionService userActionService;

    @Autowired
    MessageConverter messageConverter;

    @Autowired
    UserDataSupplier userIdMappingHandler;

    @Autowired
    SessionManagerHandler sessionManagerHandler;

    @Autowired
    StatisticsHandler statisticsHandler;

    @Autowired
    ValidatorHandler validatorHandler;

    @Autowired
    UserRecognitionHandler userRecognitionHandler;

    @MockBean(name="userActionBroadcaster")
    MessageBroadcaster consumer;

    @AfterEach
    void setUp() {
        clearInvocations(messageConverter);
        clearInvocations(sessionManagerHandler);
        clearInvocations(statisticsHandler);
        clearInvocations(validatorHandler);
        clearInvocations(userRecognitionHandler);
        clearInvocations(consumer);
    }

    @Test
    void check_subscription() {
        verify(consumer,times(1)).subscribe(any());
    }

    @Test
    void check_chain() {
        assertThat(((AbstractUserActionHandler) validatorHandler).getNextHandler()).isInstanceOf(UserDataSupplier.class);
        assertThat(((AbstractUserActionHandler) userIdMappingHandler).getNextHandler()).isInstanceOf(UserRecognitionHandler.class);
        assertThat(((AbstractUserActionHandler) userRecognitionHandler).getNextHandler()).isInstanceOf(SessionManagerHandler.class);
        assertThat(((AbstractUserActionHandler) sessionManagerHandler).getNextHandler()).isInstanceOf(StatisticsHandler.class);
        assertThat(((AbstractUserActionHandler) statisticsHandler).getNextHandler()).isNull();
    }

    @Test
    void check_processUserAction() {
        SingleSession singleSession = getSampleSession();
        when(messageConverter.messageToSingleSession(any())).thenReturn(singleSession);
        when(validatorHandler.isValid(eq(singleSession))).thenReturn(true);
//        sessionManagerHandler always returns 1

        String validJson = "{\"timestamp\":\"2022-05-19T18:11:18.754Z\",\"source\":{\"visit_id\":\"daccf290-d79b-11ec-ac11-77343ac251f2\",\"page_id\":\"/services/amber-based-portal-server-for-nmr-structures-amps-nmr\",\"root\":{\"type\":\"other\"}},\"target\":{\"visit_id\":\"147a6420-d79f-11ec-ac11-77343ac251f2\",\"page_id\":\"/services/amber-based-portal-server-for-nmr-structures-amps-nmr/details\"},\"action\":{\"type\":\"browser action\",\"text\":\"\",\"order\":false},\"unique_id\":\"0daf38fb-30aa-48c3-994a-bab70078a370\"}";

        userActionService.processMessage(validJson);

        verify(messageConverter,times(1)).messageToSingleSession(any());
        verify((AbstractUserActionHandler) validatorHandler,times(1)).handleUserAction(eq(singleSession));
        verify((AbstractUserActionHandler) userRecognitionHandler,times(1)).handleUserAction(eq(singleSession));
        verify((AbstractUserActionHandler) sessionManagerHandler,times(1)).handleUserAction(eq(singleSession));
        verify((AbstractUserActionHandler) statisticsHandler,times(1)).handleUserAction(eq(singleSession));
    }

    @ParameterizedTest
    @NullSource
    @ValueSource(strings = {"","  ","test","{\"timestamp\":\"2022-05-19T18:11:18.754Z\"}"})
    void check_processInvalidUserAction(String userAction) {
        when(messageConverter.messageToSingleSession(any())).thenReturn(null);

        userActionService.processMessage(userAction);

        verify(messageConverter,times(1)).messageToSingleSession(any());
        verify((AbstractUserActionHandler) validatorHandler,times(0)).handleUserAction(any());
        verify((AbstractUserActionHandler) userRecognitionHandler,times(0)).handleUserAction(any());
        verify((AbstractUserActionHandler) sessionManagerHandler,times(0)).handleUserAction(any());
        verify((AbstractUserActionHandler) statisticsHandler,times(0)).handleUserAction(any());
    }

}
