package pl.psnc.eosc.rs.preprocessor.app.context.evaluation;

import org.json.JSONException;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;
import pl.psnc.eosc.rs.preprocessor.app.cloud.jms.*;
import pl.psnc.eosc.rs.preprocessor.app.cloud.kafka.Producer;
import pl.psnc.eosc.rs.preprocessor.app.context.user.action.backup.BaseBackup;
import pl.psnc.eosc.rs.preprocessor.app.context.user.action.generator.RandomUserProvider;
import pl.psnc.eosc.rs.preprocessor.app.test.JsonFileAssertions;
import pl.psnc.eosc.rs.preprocessor.app.test.ResourcesTestHelper;

import static com.google.common.truth.Truth.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@SpringBootTest
@ActiveProfiles({"test","jms-disabled","artificialUAConfig-disabled","backup-raw-jms"})
public class EvaluationForwarderTest {
    @Qualifier("evaluationSaver")
    @Autowired
    BaseBackup baseBackup;

    @MockBean
    EvaluationRepository evaluationRepository;

    @Autowired
    EvaluationService recEvaluationService;

    @MockBean
    Producer producer;

    @MockBean
    JmsTopicConfig jmsTopic;

    @MockBean(name = "userActionBroadcaster")
    MessageBroadcaster userActionBroadcaster;

    @MockBean(name = "resourceEventBroadcaster")
    MessageBroadcaster resourceEventBroadcaster;

    @MockBean(name = "evaluationEventBroadcaster")
    MessageBroadcaster evaluationEventBroadcaster;

    @MockBean(name = "recommendationEventBroadcaster")
    MessageBroadcaster recommendationEventBroadcaster;

    @MockBean(name = "trainingEventBroadcaster")
    ProviderComponentMessageBroadcaster trainingMessageBroadcaster;

    @MockBean
    RandomUserProvider randomUserPicker;

    @Test
    public void forwardEvaluationTest() throws JSONException {
        recEvaluationService.processMessage(
                ResourcesTestHelper.asString(JsonFileAssertions.EVALUATION_SAMPLE_INPUT));

        ArgumentCaptor<String> forwardedEvaluationCaptor = ArgumentCaptor.forClass(String.class);
        verify(producer, times(1)).publish(any(),any(),forwardedEvaluationCaptor.capture());

        JSONAssert.assertEquals(
                ResourcesTestHelper.asString(JsonFileAssertions.EVALUATION_SAMPLE_OUTPUT),
                forwardedEvaluationCaptor.getValue(),
                false);
    }
}