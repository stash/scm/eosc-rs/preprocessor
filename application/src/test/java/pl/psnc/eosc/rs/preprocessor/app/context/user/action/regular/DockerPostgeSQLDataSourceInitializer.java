package pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular;

import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.test.context.support.TestPropertySourceUtils;
import org.testcontainers.containers.PostgreSQLContainer;

public class DockerPostgeSQLDataSourceInitializer implements ApplicationContextInitializer<ConfigurableApplicationContext> {

    public static PostgreSQLContainer<?> postgresDBContainer = new PostgreSQLContainer<>("postgres:14.5");

    static {
        postgresDBContainer.start();
    }

    @Override
    public void initialize(ConfigurableApplicationContext applicationContext) {

        TestPropertySourceUtils.addInlinedPropertiesToEnvironment(
                applicationContext,
                "spring.datasource.url=" + postgresDBContainer.getJdbcUrl(),
                "spring.datasource.username=" + postgresDBContainer.getUsername(),
                "spring.datasource.password=" + postgresDBContainer.getPassword()
        );
    }
}