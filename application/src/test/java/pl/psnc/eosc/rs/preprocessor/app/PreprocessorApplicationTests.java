package pl.psnc.eosc.rs.preprocessor.app;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;
import pl.psnc.eosc.rs.preprocessor.app.cloud.jms.JmsProducer;
import pl.psnc.eosc.rs.preprocessor.app.cloud.jms.MessageBroadcaster;
import pl.psnc.eosc.rs.preprocessor.app.cloud.jms.ProviderComponentMessageBroadcaster;
import pl.psnc.eosc.rs.preprocessor.app.context.user.action.generator.RandomExistingUserProvider;

@SpringBootTest
@ActiveProfiles({"test","jms-disabled","artificialUAConfig-disabled"})
class PreprocessorApplicationTests {

	@MockBean
	JmsProducer jmsProducer;

	@MockBean(name="userActionBroadcaster")
	MessageBroadcaster userActionBroadcaster;

	@MockBean(name="resourceEventBroadcaster")
	MessageBroadcaster resourceEventBroadcaster;

	@MockBean(name = "evaluationEventBroadcaster")
	MessageBroadcaster evaluationEventBroadcaster;

	@MockBean(name = "recommendationEventBroadcaster")
	MessageBroadcaster recommendationEventBroadcaster;

	@MockBean(name = "trainingEventBroadcaster")
	ProviderComponentMessageBroadcaster trainingMessageBroadcaster;

	@MockBean
    RandomExistingUserProvider randomUserPicker;

	@Test
	void contextLoads() {
	}
}
