package pl.psnc.eosc.rs.preprocessor.app.context.recommendation;

import org.json.JSONException;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;
import pl.psnc.eosc.rs.preprocessor.app.cloud.jms.*;
import pl.psnc.eosc.rs.preprocessor.app.context.recommendation.entity.RecommendationEntity;
import pl.psnc.eosc.rs.preprocessor.app.context.user.action.backup.BaseBackup;
import pl.psnc.eosc.rs.preprocessor.app.context.user.action.generator.RandomUserProvider;
import pl.psnc.eosc.rs.preprocessor.app.test.JsonFileAssertions;
import pl.psnc.eosc.rs.preprocessor.app.test.ResourcesTestHelper;

import javax.validation.Validator;

import static com.google.common.truth.Truth.assertThat;
import static org.mockito.Mockito.*;

@SpringBootTest
@ActiveProfiles({"test","jms-disabled","artificialUAConfig-disabled","backup-raw-jms"})
public class RecommendationSaverTest {

    @Qualifier("recommendationSaver")
    @Autowired
    BaseBackup baseBackup;

    @MockBean
    RecommendationRepository recommendationRepository;

    @Autowired
    Validator validator;

    @MockBean
    JmsTopicConfig jmsTopicConfig;

    @MockBean
    JmsProducer jmsProducer;

    @MockBean(name = "userActionBroadcaster")
    MessageBroadcaster userActionBroadcaster;

    @MockBean(name = "resourceEventBroadcaster")
    MessageBroadcaster resourceEventBroadcaster;

    @MockBean(name = "evaluationEventBroadcaster")
    MessageBroadcaster evaluationEventBroadcaster;

    @MockBean(name = "recommendationEventBroadcaster")
    MessageBroadcaster recommendationEventBroadcaster;

    @MockBean(name = "trainingEventBroadcaster")
    ProviderComponentMessageBroadcaster trainingMessageBroadcaster;

    @MockBean
    RandomUserProvider randomUserPicker;

    @Test
    public void recommendationEventBackupTest_Marketplace() throws JSONException {
        String receivedEvent = ResourcesTestHelper.asString(JsonFileAssertions.MARKETPLACE_RECOMMENDATION);
        baseBackup.processMessage(receivedEvent);

        ArgumentCaptor<RecommendationEntity> backupEntityCaptor = ArgumentCaptor.forClass(RecommendationEntity.class);
        verify(recommendationRepository, times(1)).save(backupEntityCaptor.capture());

        RecommendationEntity result = backupEntityCaptor.getValue();
        assertThat(result.getAaiUid()).isNull();
        assertThat(result.getUniqueId()).isEqualTo("5642c351-80fe-44cf-b606-304f2f338122");
        assertThat(result.getUserId()).isEqualTo("1");
        assertThat(result.getClientId()).isEqualTo("marketplace");
        assertThat(result.getEngineVersion()).isEqualTo("NCF");
        assertThat(result.getPanelId()).isEqualTo("v1");
        assertThat(result.getTimestamp()).isEqualTo(1669035591488L);
        assertThat(result.getId()).isNull();
        assertThat(result.getRecommendations().size()).isEqualTo(3);
        JSONAssert.assertEquals(receivedEvent, result.getRawRecommendationEvent(), true);
    }

    @Test
    public void recommendationEventBackupTest_OneResource() throws JSONException {
        String receivedEvent = ResourcesTestHelper.asString(JsonFileAssertions.ONE_RESOURCE_TYPE_RECOMMENDATION);
        baseBackup.processMessage(receivedEvent);

        ArgumentCaptor<RecommendationEntity> backupEntityCaptor = ArgumentCaptor.forClass(RecommendationEntity.class);
        verify(recommendationRepository, times(1)).save(backupEntityCaptor.capture());

        RecommendationEntity result = backupEntityCaptor.getValue();
        assertThat(result.getAaiUid()).isNull();
        assertThat(result.getUniqueId()).isEqualTo("8");
        assertThat(result.getUserId()).isEqualTo("8");
        assertThat(result.getClientId()).isNull();
        assertThat(result.getEngineVersion()).isNull();
        assertThat(result.getPanelId()).isEqualTo("datasets");
        assertThat(result.getTimestamp()).isEqualTo(1669035591488L);
        assertThat(result.getId()).isNull();
        assertThat(result.getRecommendations().size()).isEqualTo(3);
        JSONAssert.assertEquals(receivedEvent, result.getRawRecommendationEvent(), true);
    }

    @Test
    public void recommendationEventBackupTest_AllResources() throws JSONException {
        String receivedEvent = ResourcesTestHelper.asString(JsonFileAssertions.ALL_RESOURCE_TYPE_RECOMMENDATION);
        baseBackup.processMessage(receivedEvent);

        ArgumentCaptor<RecommendationEntity> backupEntityCaptor = ArgumentCaptor.forClass(RecommendationEntity.class);
        verify(recommendationRepository, times(1)).save(backupEntityCaptor.capture());

        RecommendationEntity result = backupEntityCaptor.getValue();
        assertThat(result.getAaiUid()).isNull();
        assertThat(result.getUniqueId()).isEqualTo("8");
        assertThat(result.getUserId()).isEqualTo("8");
        assertThat(result.getClientId()).isNull();
        assertThat(result.getEngineVersion()).isNull();
        assertThat(result.getPanelId()).isEqualTo("other_research_product");
        assertThat(result.getTimestamp()).isEqualTo(1669035591488L);
        assertThat(result.getId()).isNull();
        assertThat(result.getRecommendations().size()).isEqualTo(10);
        JSONAssert.assertEquals(receivedEvent, result.getRawRecommendationEvent(), true);
    }
}