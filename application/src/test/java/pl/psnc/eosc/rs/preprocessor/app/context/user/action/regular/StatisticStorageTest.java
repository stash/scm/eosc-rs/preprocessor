package pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import pl.psnc.eosc.rs.preprocessor.app.context.user.action.generator.ResourceEnum;
import pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular.model.internal.Resource;
import pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular.service.statistics.StatisticsStorage;
import pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular.service.statistics.StatisticsStorageImpl;

import java.util.List;
import java.util.stream.Collectors;

import static com.google.common.truth.Truth.assertThat;

@SpringBootTest(classes = {StatisticsStorageImpl.class})
public class StatisticStorageTest {

    @Autowired
    StatisticsStorage statisticsStorage;

    @Test
    public void addServicesSingleUser(){
        statisticsStorage.clearMaps();
        statisticsStorage.addVisit("user0",new Resource(ResourceEnum.SERVICE.getId(), "r1"));
        statisticsStorage.addVisit("user0",new Resource(ResourceEnum.SERVICE.getId(),"r1"));
        statisticsStorage.addVisit("user0",new Resource(ResourceEnum.SERVICE.getId(),"r2"));
        statisticsStorage.addVisit("user0",new Resource(ResourceEnum.SERVICE.getId(),"r3"));
        statisticsStorage.addOrder("user0",new Resource(ResourceEnum.SERVICE.getId(),"r1"));
        statisticsStorage.addOrder("user0",new Resource(ResourceEnum.SERVICE.getId(),"r1"));
        statisticsStorage.addOrder("user0",new Resource(ResourceEnum.SERVICE.getId(),"r2"));

        var usersVisits = statisticsStorage.getUsersVisits();
        assertThat(usersVisits.size()).isEqualTo(1);
        var user0Visits = usersVisits.get("user0");
        assertThat(user0Visits.size()).isEqualTo(3);
        var listOfVisits = user0Visits.stream()
                .map(Resource::getResourceId)
                .collect(Collectors.toList());
        assertThat(listOfVisits).contains("r1");
        assertThat(listOfVisits).contains("r2");
        assertThat(listOfVisits).contains("r3");

        var usersOrders = statisticsStorage.getUsersOrders();
        assertThat(usersOrders.size()).isEqualTo(1);
        var user0Orders = usersOrders.get("user0");
        assertThat(user0Orders.size()).isEqualTo(2);
        var listOfOrders = user0Orders.stream()
                .map(Resource::getResourceId)
                .collect(Collectors.toList());
        assertThat(listOfOrders).contains("r1");
        assertThat(listOfOrders).contains("r2");
    }

    @Test
    public void addResourcesSingleUser(){
        statisticsStorage.clearMaps();
        statisticsStorage.addVisit("user0",new Resource(ResourceEnum.SERVICE.getId(),"r1"));
        statisticsStorage.addVisit("user0",new Resource(ResourceEnum.SERVICE.getId(),"r1"));
        statisticsStorage.addVisit("user0",new Resource(ResourceEnum.PUBLICATION.getId(),"r1"));
        statisticsStorage.addVisit("user0",new Resource(ResourceEnum.DATASET.getId(),"r1"));

        statisticsStorage.addOrder("user0",new Resource(ResourceEnum.SERVICE.getId(),"r1"));
        statisticsStorage.addOrder("user0",new Resource(ResourceEnum.SERVICE.getId(),"r1"));
        statisticsStorage.addOrder("user0",new Resource(ResourceEnum.PUBLICATION.getId(),"r1"));
        statisticsStorage.addOrder("user0",new Resource(ResourceEnum.DATASET.getId(),"r1"));

        var usersVisits = statisticsStorage.getUsersVisits();
        assertThat(usersVisits.size()).isEqualTo(1);
        var user0Visits = usersVisits.get("user0");
        assertThat(user0Visits.size()).isEqualTo(3);

        var usersOrders = statisticsStorage.getUsersOrders();
        assertThat(usersOrders.size()).isEqualTo(1);
        var user0Orders = usersOrders.get("user0");
        assertThat(user0Orders.size()).isEqualTo(3);
    }

    @Test
    public void addResourcesMultipleUsers(){
        statisticsStorage.clearMaps();
        statisticsStorage.addVisit("user1",new Resource(ResourceEnum.SERVICE.getId(),"r1"));
        statisticsStorage.addVisit("user0",new Resource(ResourceEnum.SERVICE.getId(),"r1"));

        statisticsStorage.addOrder("user1",new Resource(ResourceEnum.SERVICE.getId(),"r1"));
        statisticsStorage.addOrder("user0",new Resource(ResourceEnum.SERVICE.getId(),"r1"));

        var usersVisits = statisticsStorage.getUsersVisits();
        assertThat(usersVisits.size()).isEqualTo(2);
        var user0Visits = usersVisits.get("user0");
        assertThat(user0Visits.size()).isEqualTo(1);
        var user1Visits = usersVisits.get("user1");
        assertThat(user1Visits.size()).isEqualTo(1);

        var usersOrders = statisticsStorage.getUsersOrders();
        assertThat(usersOrders.size()).isEqualTo(2);
        var user0Orders = usersOrders.get("user0");
        assertThat(user0Orders.size()).isEqualTo(1);
        var user1Orders = usersOrders.get("user1");
        assertThat(user1Orders.size()).isEqualTo(1);
    }

    @Test
    public void makeUserNonAnonymous(){
        statisticsStorage.clearMaps();
        statisticsStorage.addVisit("session0",new Resource(ResourceEnum.SERVICE.getId(),"r1"));
        statisticsStorage.addVisit("user0",new Resource(ResourceEnum.SERVICE.getId(),"r2"));
        statisticsStorage.addOrder("session0",new Resource(ResourceEnum.SERVICE.getId(),"r1"));
        statisticsStorage.addOrder("user0",new Resource(ResourceEnum.SERVICE.getId(),"r2"));

        statisticsStorage.makeUserNonAnonymous("user0","session0");

        var usersVisits = statisticsStorage.getUsersVisits();
        assertThat(usersVisits.size()).isEqualTo(1);
        assertThat(usersVisits.containsKey("session0")).isFalse();
        assertThat(usersVisits.containsKey("user0")).isTrue();
        assertThat(usersVisits.get("user0").stream()
                .map(Resource::getResourceId)
                .collect(Collectors.toList())).containsExactlyElementsIn(List.of("r1","r2"));

        var usersOrders = statisticsStorage.getUsersOrders();
        assertThat(usersOrders.size()).isEqualTo(1);
        assertThat(usersOrders.containsKey("session0")).isFalse();
        assertThat(usersOrders.containsKey("user0")).isTrue();
        assertThat(usersOrders.get("user0").stream()
                .map(Resource::getResourceId)
                .collect(Collectors.toList())).containsExactlyElementsIn(List.of("r1","r2"));
    }

    @Test
    public void clearMap(){
        statisticsStorage.clearMaps();
        statisticsStorage.addVisit("user0",new Resource(ResourceEnum.SERVICE.getId(),"r1"));
        statisticsStorage.addOrder("user0",new Resource(ResourceEnum.SERVICE.getId(),"r1"));
        assertThat(statisticsStorage.getUsersVisits().size()).isEqualTo(1);
        assertThat(statisticsStorage.getUsersOrders().size()).isEqualTo(1);

        statisticsStorage.clearMaps();
        assertThat(statisticsStorage.getUsersVisits().size()).isEqualTo(0);
        assertThat(statisticsStorage.getUsersOrders().size()).isEqualTo(0);
    }
}
