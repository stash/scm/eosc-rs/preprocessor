package pl.psnc.eosc.rs.preprocessor.app.drivers.impl.searchservice;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.web.client.MockRestServiceServer;
import org.springframework.web.client.RestTemplate;
import pl.psnc.eosc.rs.preprocessor.app.drivers.api.searchservice.Dump;
import pl.psnc.eosc.rs.preprocessor.app.test.ResourcesTestHelper;

import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Paths;
import java.time.OffsetDateTime;
import java.util.Comparator;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.client.ExpectedCount.once;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.method;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.requestTo;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withStatus;

class SearchServiceDriverImplTest {

    RestTemplate restTemplate;
    MockRestServiceServer searchServiceServiceMock;

    @BeforeEach
    public void initSearchServiceServiceMock(){
        restTemplate = new RestTemplate();
        searchServiceServiceMock = MockRestServiceServer.createServer(restTemplate);
    }

    @Test
    void listDumps() throws URISyntaxException {
        //Mock 2 pages of dumps
        searchServiceServiceMock.expect(once(),
                        requestTo(new URI("http://host:9090/v1/dumps?cursor=0&rows=20")))
                .andExpect(method(HttpMethod.GET))
                .andRespond(withStatus(HttpStatus.OK)
                        .contentType(MediaType.APPLICATION_JSON)
                        .body(ResourcesTestHelper.asString(Paths.get("/assertions/searchservice/dumps_1page_mock.json")))
                );

        searchServiceServiceMock.expect(once(),
                        requestTo(new URI("http://host:9090/v1/dumps?cursor=20&rows=20")))
                .andExpect(method(HttpMethod.GET))
                .andRespond(withStatus(HttpStatus.OK)
                        .contentType(MediaType.APPLICATION_JSON)
                        .body(ResourcesTestHelper.asString(Paths.get("/assertions/searchservice/dumps_2page_mock.json")))
                );

        //WHEN
        SearchServiceDriverImpl searchServiceDriver = SearchServiceDriverImpl.create(restTemplate,"http://host:9090");

        //THEN
        List<Dump> result = searchServiceDriver.listDumps();
        result.sort(Comparator.comparing(Dump::getName));
        Dump firstDump = result.get(0);

        assertEquals(4,result.size());
        assertEquals("dump1",firstDump.getName());
        assertEquals(OffsetDateTime.parse("2022-02-16T15:37:42.020461Z"),firstDump.getCreatedAt());
        assertEquals(4,firstDump.getElements().size());
    }

}