package pl.psnc.eosc.rs.preprocessor.app.context.user.action.generator;

import org.junit.jupiter.api.Test;

import static com.google.common.truth.Truth.assertThat;

public class FileReaderTest {

    @Test
    public void basicTest(){
        LineReader dataset = new LineReader(ResourceEnum.DATASET.getFile());
        assertThat(dataset.getRecords()).isNotEmpty();
        assertThat(dataset.getRecords().size()).isAtLeast(67563);
        assertThat(dataset.getRecords().get(0)).isEqualTo("50|doi_________::014e12fad82df5d98476bf67851d5bcc");

    }
}
