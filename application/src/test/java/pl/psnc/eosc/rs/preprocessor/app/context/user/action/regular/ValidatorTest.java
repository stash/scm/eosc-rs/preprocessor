package pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.Logger;
import org.junit.jupiter.api.Test;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;
import pl.psnc.eosc.rs.preprocessor.app.cloud.jms.MessageBroadcaster;
import pl.psnc.eosc.rs.preprocessor.app.cloud.jms.ProviderComponentMessageBroadcaster;
import pl.psnc.eosc.rs.preprocessor.app.commons.LogStorage;
import pl.psnc.eosc.rs.preprocessor.app.context.evaluation.EvaluationService;
import pl.psnc.eosc.rs.preprocessor.app.context.user.action.backup.BaseBackup;
import pl.psnc.eosc.rs.preprocessor.app.context.user.action.generator.RandomExistingUserProvider;
import pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular.model.internal.SingleSession;
import pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular.service.validator.ValidatorHandler;
import pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular.service.validator.ValidatorImpl;

import static com.google.common.truth.Truth.assertThat;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular.SampleSessionProvider.getSampleNullSession;
import static pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular.SampleSessionProvider.getSampleSession;

@SpringBootTest
@ActiveProfiles({"test","jms-disabled","artificialUAConfig-disabled"})
public class ValidatorTest {

    @MockBean(name="rawResourceSaver")
    BaseBackup rawResourceSaver;

    @MockBean(name="userActionBroadcaster")
    MessageBroadcaster userActionBroadcaster;

    @MockBean(name="resourceEventBroadcaster")
    MessageBroadcaster resourceEventBroadcaster;

    @MockBean(name = "evaluationEventBroadcaster")
    MessageBroadcaster evaluationEventBroadcaster;

    @MockBean(name = "recommendationEventBroadcaster")
    MessageBroadcaster recommendationEventBroadcaster;

    @MockBean(name = "trainingEventBroadcaster")
    ProviderComponentMessageBroadcaster trainingMessageBroadcaster;

    @MockBean
    EvaluationService evaluationService;

    @MockBean
    RandomExistingUserProvider randomUserPicker;

    @Autowired
    ValidatorHandler validatorHandler;

    private static final String LOGGER_NAME = "pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular.service.validator.ValidatorImpl";

    @Test
    void validatePositive() {
        Logger logger = (Logger) LoggerFactory.getLogger(ValidatorImpl.class);
        LogStorage logStorage = new LogStorage();
        logger.addAppender(logStorage);
        logStorage.start();

        SingleSession singleSession = getSampleSession();
        validatorHandler.isValid(singleSession);

        assertThat(logStorage.countEventsForLogger(LOGGER_NAME)).isEqualTo(0);
        assertTrue(validatorHandler.isValid(singleSession));
    }

    @Test
    void validateNegative() {
        Logger logger = (Logger) LoggerFactory.getLogger(ValidatorImpl.class);
        LogStorage logStorage = new LogStorage();
        logger.addAppender(logStorage);
        logStorage.start();

        SingleSession singleSession = getSampleNullSession();
        validatorHandler.isValid(singleSession);

        assertThat(logStorage.countEventsForLogger(LOGGER_NAME)).isEqualTo(10);
        assertThat(logStorage.contains("NotNull.message} userAction.receiptTimestamp",Level.WARN)).isTrue();
        assertThat(logStorage.contains("NotBlank.message} userAction.sourcePageId",Level.WARN)).isTrue();
        assertThat(logStorage.contains("NotBlank.message} userAction.root.type",Level.WARN)).isTrue();
        assertThat(logStorage.contains("NotNull.message} userAction.isOrderAction",Level.WARN)).isTrue();
        assertThat(logStorage.contains("NotBlank.message} userAction.actionType",Level.WARN)).isTrue();
        assertThat(logStorage.contains("NotNull.message} sessionId",Level.WARN)).isTrue();
        assertThat(logStorage.contains("NotNull.message} userAction.timestamp",Level.WARN)).isTrue();
        assertThat(logStorage.contains("NotBlank.message} userAction.targetPageId",Level.WARN)).isTrue();
        assertThat(logStorage.contains("NotBlank.message} userAction.resourceId",Level.WARN)).isTrue();
        assertThat(logStorage.contains("NotBlank.message} userAction.resourceType",Level.WARN)).isTrue();
        assertFalse(validatorHandler.isValid(singleSession));
    }

    @Test
    void validateNullUA() {
        Logger logger = (Logger) LoggerFactory.getLogger(ValidatorImpl.class);
        LogStorage logStorage = new LogStorage();
        logger.addAppender(logStorage);
        logStorage.start();

        SingleSession singleSession = getSampleNullSession();
        singleSession.setUserAction(null);
        validatorHandler.isValid(singleSession);

        assertThat(logStorage.countEventsForLogger(LOGGER_NAME)).isEqualTo(1);
        assertThat(logStorage.contains("UserAction and Root should not be null",Level.WARN)).isTrue();
        assertFalse(validatorHandler.isValid(singleSession));
    }

    @Test
    void validateNullRoot() {
        Logger logger = (Logger) LoggerFactory.getLogger(ValidatorImpl.class);
        LogStorage logStorage = new LogStorage();
        logger.addAppender(logStorage);
        logStorage.start();

        SingleSession singleSession = getSampleNullSession();
        singleSession.getUserAction().setRoot(null);
        validatorHandler.isValid(singleSession);

        assertThat(logStorage.countEventsForLogger(LOGGER_NAME)).isEqualTo(1);
        assertThat(logStorage.contains("UserAction and Root should not be null",Level.WARN)).isTrue();
        assertFalse(validatorHandler.isValid(singleSession));
    }

}
