package pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular;

import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.test.context.support.TestPropertySourceUtils;
import org.testcontainers.containers.MySQLContainer;

public class DockerMySQLDataSourceInitializer implements ApplicationContextInitializer<ConfigurableApplicationContext> {

    public static MySQLContainer<?> mysqlDBContainer = new MySQLContainer<>("mysql:8.0");

    static {
        mysqlDBContainer.start();
    }

    @Override
    public void initialize(ConfigurableApplicationContext applicationContext) {

        TestPropertySourceUtils.addInlinedPropertiesToEnvironment(
                applicationContext,
                "spring.datasource.url=" + mysqlDBContainer.getJdbcUrl(),
                "spring.datasource.username=" + mysqlDBContainer.getUsername(),
                "spring.datasource.password=" + mysqlDBContainer.getPassword()
        );
    }
}