package pl.psnc.eosc.rs.preprocessor.app.context.resource.synchronization.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import pl.psnc.eosc.rs.preprocessor.app.context.resource.converter.JsonObjectConverter;
import pl.psnc.eosc.rs.preprocessor.app.context.resource.converter.ResourceEvent;
import pl.psnc.eosc.rs.preprocessor.app.context.resource.synchronization.EventType;
import pl.psnc.eosc.rs.preprocessor.app.context.resource.synchronization.service.model.ServiceEntity;
import pl.psnc.eosc.rs.preprocessor.app.context.resource.synchronization.service.model.ServiceEvent;
import pl.psnc.eosc.rs.preprocessor.app.context.resource.synchronization.service.repository.ServiceRepository;
import pl.psnc.eosc.rs.preprocessor.app.context.resource.synchronization.service.service.ServiceChangesPublisher;
import pl.psnc.eosc.rs.preprocessor.app.context.resource.synchronization.service.service.ServiceEventHandler;

import java.util.List;
import java.util.Optional;

import static com.google.common.truth.Truth.assertThat;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class ServiceEventHandlerTest {

    @InjectMocks
    ServiceEventHandler serviceEventHandler;

    @Mock
    JsonObjectConverter eventConverter;

    @Mock
    ServiceRepository serviceRepository;

    @Mock
    ServiceChangesPublisher serviceChangesPropagator;

    private static ObjectMapper mapper = new ObjectMapper();

    @Test
    public void serviceEvent_create() throws JsonProcessingException {
        EventType type = EventType.CREATE;
        String newString = "{\"test\": \"any\"}";
        JsonNode node = mapper.readTree(newString);
        when(eventConverter.convert(anyString()))
                .thenReturn(new ResourceEvent(
                        "create",
                        "Service",
                        node))
                .thenReturn(new ServiceEvent(
                        "id",
                        "test_service",
                        "Sample description.",
                        List.of(1,9),
                        List.of(2,8),
                        List.of(3,7)));
        serviceEventHandler.processMessage("any");
        ArgumentCaptor<ServiceEntity> serviceEntityArgumentCaptor = ArgumentCaptor.forClass(ServiceEntity.class);
        verify(serviceRepository,times(1)).saveAndFlush(serviceEntityArgumentCaptor.capture());
        ServiceEntity serviceEntity = serviceEntityArgumentCaptor.getValue();

        Assertions.assertThat(serviceEntity).usingRecursiveComparison().isEqualTo(getExpectedServiceEntity());
        assertEqualityOfSavedAndPublishedService(serviceEntity,type);
    }

    @Test
    public void serviceEvent_update() throws JsonProcessingException {
        EventType type = EventType.UPDATE;
        String newString = "{\"test\": \"any\"}";
        JsonNode node = mapper.readTree(newString);
        ServiceEntity searchedService = new ServiceEntity();
        when(eventConverter.convert(anyString()))
                .thenReturn(new ResourceEvent(
                        "update",
                        "Service",
                        node))
                .thenReturn(new ServiceEvent(
                "id",
                "test_service",
                "Sample description.",
                List.of(1,9),
                List.of(2,8),
                List.of(3,7)));
        when(serviceRepository.findById("id")).thenReturn(Optional.of(searchedService));
        //then:
        serviceEventHandler.processMessage("any");
        ArgumentCaptor<ServiceEntity> serviceEntityArgumentCaptor = ArgumentCaptor.forClass(ServiceEntity.class);
        verify(serviceRepository,times(1)).saveAndFlush(serviceEntityArgumentCaptor.capture());
        ServiceEntity serviceEntity = serviceEntityArgumentCaptor.getValue();

        Assertions.assertThat(serviceEntity).usingRecursiveComparison().isEqualTo(getExpectedServiceEntity());
        assertEqualityOfSavedAndPublishedService(serviceEntity,type);
    }

    @Test
    public void serviceEvent_delete() throws JsonProcessingException {
        String newString = "{\"test\": \"any\"}";
        JsonNode node = mapper.readTree(newString);
        when(eventConverter.convert(anyString()))
                .thenReturn(new ResourceEvent(
                        "delete",
                        "Service",
                        node))
                .thenReturn(new ServiceEvent(
                        "id",
                        "test_service",
                        "Sample description.",
                        List.of(1,9),
                        List.of(2,8),
                        List.of(3,7)));
        serviceEventHandler.processMessage("any");
        ArgumentCaptor<String> serviceIdArgumentCaptor = ArgumentCaptor.forClass(String.class);
        verify(serviceRepository,times(1)).deleteById(serviceIdArgumentCaptor.capture());
        assertThat(serviceIdArgumentCaptor.getValue()).isEqualTo("id");
    }

    @Test
    public void serviceEvent_other() throws JsonProcessingException {
        String newString = "{\"test\": \"any\"}";
        JsonNode node = mapper.readTree(newString);
        when(eventConverter.convert(anyString()))
                .thenReturn(new ResourceEvent(
                        "other",
                        "Service",
                        node))
                .thenReturn(new ServiceEvent(
                        "id",
                        "test_service",
                        "Sample description.",
                        List.of(1,9),
                        List.of(2,8),
                        List.of(3,7)));
        serviceEventHandler.processMessage("any");
        verify(serviceRepository,times(0)).saveAndFlush(any());
        verify(serviceRepository,times(0)).deleteById(anyString());
    }

    @Test
    public void otherEvent() throws JsonProcessingException {
        String newString = "{\"test\": \"any\"}";
        JsonNode node = mapper.readTree(newString);
        when(eventConverter.convert(anyString()))
                .thenReturn(new ResourceEvent(
                        "delete",
                        "Other",
                        node))
                .thenReturn(new ServiceEvent(
                        "id",
                        "test_service",
                        "Sample description.",
                        List.of(1,9),
                        List.of(2,8),
                        List.of(3,7)));
        serviceEventHandler.processMessage("any");
        verify(serviceRepository,times(0)).saveAndFlush(any());
        verify(serviceRepository,times(0)).deleteById(anyString());
    }

    private ServiceEntity getExpectedServiceEntity(){
        return new ServiceEntity(
                "id",
                "test_service",
                "Sample description.",
                List.of(1,9),
                List.of(2,8),
                List.of(3,7));
    }

    private void assertEqualityOfSavedAndPublishedService(ServiceEntity expectedServiceEntity, EventType expectedEventType){
        ArgumentCaptor<ServiceEvent> propagatedServiceCaptor = ArgumentCaptor.forClass(ServiceEvent.class);
        verify(serviceChangesPropagator,times(1)).publish(propagatedServiceCaptor.capture());
        ServiceEvent publishedServiceEntity = propagatedServiceCaptor.getValue();
        assertThat(publishedServiceEntity.getEventType()).isEqualTo(expectedEventType.getName());
        assertThat(publishedServiceEntity.getId()).isEqualTo(expectedServiceEntity.getId());
        assertThat(publishedServiceEntity.getName()).isEqualTo(expectedServiceEntity.getName());
        assertThat(publishedServiceEntity.getCategoryValues()).isEqualTo(expectedServiceEntity.getCategoryValues());
        assertThat(publishedServiceEntity.getScientificDomainValues()).isEqualTo(expectedServiceEntity.getScientificDomainValues());
        assertThat(publishedServiceEntity.getRelatedServices()).isEqualTo(expectedServiceEntity.getRelatedServices());
    }
}
