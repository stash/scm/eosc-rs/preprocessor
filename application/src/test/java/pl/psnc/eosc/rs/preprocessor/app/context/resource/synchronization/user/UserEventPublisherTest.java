package pl.psnc.eosc.rs.preprocessor.app.context.resource.synchronization.user;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import pl.psnc.eosc.rs.preprocessor.app.cloud.kafka.Producer;
import pl.psnc.eosc.rs.preprocessor.app.context.resource.synchronization.EventType;
import pl.psnc.eosc.rs.preprocessor.app.context.resource.synchronization.user.model.UserEvent;
import pl.psnc.eosc.rs.preprocessor.app.context.resource.synchronization.user.service.UserPublisherImpl;

import java.util.List;

import static com.google.common.truth.Truth.assertThat;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
public class UserEventPublisherTest {

    @Mock
    Producer producer;

    @Test
    public void userEvent_update() {
        UserPublisherImpl userChangesPropagator = new UserPublisherImpl(producer,"test_topic");
        EventType type = EventType.UPDATE;
        UserEvent user = new UserEvent(type.getName(),"aai_id","markeplace_id",List.of(1,2),List.of(3,4),List.of(5,6));

        UserEvent expectedEvent = new UserEvent(
                "aai_id",
                "marketplace_id",
                List.of(1,2),
                List.of(3,4),
                List.of(5,6));

        //when:
        userChangesPropagator.publish(user);

        //then:
        ArgumentCaptor<String> topicArgumentCaptor = ArgumentCaptor.forClass(String.class);
        ArgumentCaptor<String> keyArgumentCaptor = ArgumentCaptor.forClass(String.class);
        ArgumentCaptor<String> messageArgumentCaptor = ArgumentCaptor.forClass(String.class);
        verify(producer,times(1)).publish(
                topicArgumentCaptor.capture(),
                keyArgumentCaptor.capture(),
                messageArgumentCaptor.capture());

        assertThat(topicArgumentCaptor.getValue()).isEqualTo("test_topic");
        assertThat(keyArgumentCaptor.getValue()).isEqualTo("aai_id");
        assertThat(messageArgumentCaptor.getValue()).contains(getSampleMessage());
    }

    private String getSampleMessage(){
        return "{\"eventType\":\"update\"," +
                "\"aaiId\":\"aai_id\"," +
                "\"marketplaceId\":\"markeplace_id\"," +
                "\"scientificDomainValues\":[1,2]," +
                "\"categoryValues\":[3,4]," +
                "\"accessedServiceValues\":[5,6]}";
    }
}
