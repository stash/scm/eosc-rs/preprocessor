package pl.psnc.eosc.rs.preprocessor.app.drivers.impl.resourcemanager;

import lombok.SneakyThrows;
import org.apache.hadoop.yarn.api.records.ApplicationAttemptId;
import org.apache.hadoop.yarn.api.records.ApplicationId;
import org.apache.hadoop.yarn.api.records.ApplicationReport;
import org.apache.hadoop.yarn.api.records.YarnApplicationState;
import org.apache.hadoop.yarn.client.api.YarnClient;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import pl.psnc.eosc.rs.preprocessor.app.drivers.api.resourcemanager.ApplicationStatus;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

class YarnDriverImplTest {

    @SneakyThrows
    @Test
    public void listApps(){
        YarnClientFactory yarnClientFactoryMock = Mockito.mock(YarnClientFactory.class);
        YarnClient yarnClientMock = Mockito.mock(YarnClient.class);
        when(yarnClientFactoryMock.create()).thenReturn(yarnClientMock);
        when(yarnClientMock.getApplications()).thenReturn(exampleApplicationReports_eachStatus());

        //GIVEN
        YarnDriverImpl yarnDriver = new YarnDriverImpl(yarnClientFactoryMock);

        //WHEN
        List<ApplicationStatus> result = yarnDriver.listApps();

        //THEN - verify
        verify(yarnClientMock,times(1)).start();
        verify(yarnClientMock,times(1)).stop();

        //THEN - assert
        assertEquals(YarnApplicationState.values().length,result.size());
        List<ApplicationStatus.State> resultStatuses = result.stream().map(ApplicationStatus::getState).collect(Collectors.toList());
        Assertions.assertThat(resultStatuses).containsAll(Arrays.stream(ApplicationStatus.State.values()).collect(Collectors.toList()));
    }

    @SneakyThrows
    @Test
    public void getApp(){
        YarnClientFactory yarnClientFactoryMock = Mockito.mock(YarnClientFactory.class);
        YarnClient yarnClientMock = Mockito.mock(YarnClient.class);
        when(yarnClientFactoryMock.create()).thenReturn(yarnClientMock);
        when(yarnClientMock.getApplicationReport(eq(ApplicationId.newInstance(123456789,1)))).thenReturn(exampleApplicationReport(ApplicationId.newInstance(123456789,1),YarnApplicationState.FINISHED));

        //GIVEN
        YarnDriverImpl yarnDriver = new YarnDriverImpl(yarnClientFactoryMock);

        //WHEN
        ApplicationStatus result = yarnDriver.getApp("application_123456789_0001").get();

        //THEN - verify
        verify(yarnClientMock,times(1)).start();
        verify(yarnClientMock,times(1)).stop();

        //THEN - assert
        assertEquals(ApplicationStatus.State.FINISHED,result.getState());
        assertEquals("application_123456789_0001",result.getId());
    }

    private List<ApplicationReport> exampleApplicationReports_eachStatus(){
        YarnApplicationState[] statuses = YarnApplicationState.values();
        return IntStream.range(0,statuses.length).mapToObj(index -> exampleApplicationReport(ApplicationId.newInstance(123456789,index),statuses[index])).collect(Collectors.toList());
    }

    private ApplicationReport exampleApplicationReport(ApplicationId applicationId,YarnApplicationState state){
        ApplicationAttemptId attemptId = ApplicationAttemptId.newInstance(applicationId,1);

        return ApplicationReport.newInstance(applicationId, attemptId,"","","","",9000,null, state,"","",123456789,123456789,123456789,null,null,"",100f,"",null);
    }

}