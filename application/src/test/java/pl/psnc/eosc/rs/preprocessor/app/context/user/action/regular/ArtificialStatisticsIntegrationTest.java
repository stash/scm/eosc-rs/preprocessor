package pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular;

import org.apache.activemq.junit.EmbeddedActiveMQBroker;
import org.junit.Rule;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.testcontainers.junit.jupiter.Testcontainers;
import pl.psnc.eosc.rs.preprocessor.app.cloud.jms.JmsProducer;
import pl.psnc.eosc.rs.preprocessor.app.cloud.kafka.Producer;
import pl.psnc.eosc.rs.preprocessor.app.context.user.action.artificial.model.entity.ArtificialDayStatsEntity;
import pl.psnc.eosc.rs.preprocessor.app.context.user.action.artificial.model.entity.ArtificialTotalStatsEntity;
import pl.psnc.eosc.rs.preprocessor.app.context.user.action.artificial.repository.ArtificialStatsPerDayRepository;
import pl.psnc.eosc.rs.preprocessor.app.context.user.action.artificial.repository.ArtificialStatsTodayRepository;
import pl.psnc.eosc.rs.preprocessor.app.context.user.action.artificial.repository.ArtificialStatsTotalRepository;
import pl.psnc.eosc.rs.preprocessor.app.context.user.action.generator.ResourceEnum;
import pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular.model.internal.SingleSession;
import pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular.model.statistics.database.ResourceCounter;
import pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular.model.statistics.database.ResourceDayKey;
import pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular.service.AbstractUserActionHandler;
import pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular.service.statistics.DayProvider;
import pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular.service.statistics.StatisticsHandler;
import pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular.service.statistics.StatisticsPublisher;
import pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular.service.statistics.TodayPublisher;

import java.util.Collections;
import java.util.List;
import java.util.UUID;

import static com.google.common.truth.Truth.assertThat;
import static org.mockito.Mockito.when;
import static pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular.SampleSessionProvider.getSampleSession;

@ActiveProfiles({"integration","artificial-flow"})
@Import(IntegrationTestConf.class)
@SpringBootTest(webEnvironment=SpringBootTest.WebEnvironment.RANDOM_PORT)
@ContextConfiguration(initializers = DockerPostgeSQLDataSourceInitializer.class)
@Testcontainers
public class ArtificialStatisticsIntegrationTest {

    @Rule
    public EmbeddedActiveMQBroker broker = new EmbeddedActiveMQBroker();

    @Autowired
    @Qualifier("futureStatisticsHandler")
    StatisticsHandler statisticsHandler;

    @Autowired
    @Qualifier("futureStatisticsPublisher")
    StatisticsPublisher statisticsPublisher;

    @Autowired
    ArtificialStatsTodayRepository statsTodayRepository;

    @Autowired
    ArtificialStatsPerDayRepository statsPerDayRepository;

    @Autowired
    ArtificialStatsTotalRepository statsTotalRepository;

    @Autowired
    @Qualifier("futureTodayPublisher")
    TodayPublisher todayPublisher;

    @Autowired
    JmsTemplate jmsTemplate;

    @MockBean
    DayProvider dayProviderMock;

    @MockBean
    JmsProducer jmsProducer;

    @MockBean
    Producer producer;


    @Test
    public void testChain() {
        // day is needed by "update today" and by stats computation unit to calculate stats for day before
        when(dayProviderMock.getCurrentDay()).thenReturn(1,2,2,2,3);
        receiveBasicSessions();
        updateAndCheckToday(1);//day1 (needs day)
        calculateStats(5);//day1 (needs day)
        checkPerDayStats();//day1
        checkTotalStats();//day1
        receiveBasicSessions();//day2
        receiveBasicSessions();//day2 (check if 2 entries are calculated as 1 (per user)
        updateAndCheckToday(2);//day2 (needs day)
        receiveExtraSessions();//day2 (needs day)
        updateAndCheckTodayWithExtraSessions(2);//day2 (needs day) (check if update today works properly)
        calculateStats(7);//day2
        checkPerDayStatsFinal();//day2
        checkTotalStatsFinal();//day2

    }

    public void receiveBasicSessions(){
        SingleSession userAction0 = getSampleSession();
        userAction0.getUserAction().setResourceId("10");

        SingleSession userAction1 = getSampleSession();
        userAction1.getUserAction().setResourceId("10");

        SingleSession userAction2_otherSession = getSampleSession();
        userAction2_otherSession.getUserAction().setResourceId("12");
        userAction2_otherSession.setSessionId(UUID.fromString("1507dabf-acf6-477b-89de-033f8f9d570e"));

        SingleSession userAction3_withUser = getSampleSession();
        userAction3_withUser.getUserAction().setResourceId("12");
        userAction3_withUser.setAaiUid("test-id0");

        SingleSession userAction4_order = getSampleSession();
        userAction4_order.getUserAction().setResourceId("10");
        userAction4_order.getUserAction().setIsOrderAction(true);

        SingleSession userAction5_otherUser_order = getSampleSession();
        userAction5_otherUser_order.getUserAction().setResourceId("10");
        userAction5_otherUser_order.getUserAction().setIsOrderAction(true);
        userAction5_otherUser_order.setAaiUid("test-id1");
        userAction5_otherUser_order.setSessionId(UUID.fromString("0dd4676d-0561-4405-b031-9e05e6cb5c07"));


        ((AbstractUserActionHandler) statisticsHandler).handleUserAction(userAction0);
        ((AbstractUserActionHandler) statisticsHandler).handleUserAction(userAction1);
        ((AbstractUserActionHandler) statisticsHandler).handleUserAction(userAction2_otherSession);
        ((AbstractUserActionHandler) statisticsHandler).handleUserAction(userAction3_withUser);
        ((AbstractUserActionHandler) statisticsHandler).handleUserAction(userAction4_order);
        ((AbstractUserActionHandler) statisticsHandler).handleUserAction(userAction5_otherUser_order);
    }

    public void receiveExtraSessions() {

        SingleSession userAction0 = getSampleSession();
        userAction0.getUserAction().setResourceId("20");

        SingleSession userAction1 = getSampleSession();
        userAction1.getUserAction().setResourceId("21");
        userAction1.setAaiUid("test-id2");
        userAction1.getUserAction().setIsOrderAction(true);
        userAction1.setSessionId(UUID.fromString("7c77102f-9a4a-4a69-a494-bc762c697e1c"));


        ((AbstractUserActionHandler) statisticsHandler).handleUserAction(userAction0);
        ((AbstractUserActionHandler) statisticsHandler).handleUserAction(userAction1);
    }

    public void updateAndCheckToday(Integer day) {

        // assumptions
        assertThat(statsTodayRepository.count()).isEqualTo(0);

        // when
        todayPublisher.updateToday();

        // then
        assertThat(statsTodayRepository.count()).isEqualTo(5);
        var todayStatistics= statsTodayRepository.findAll();
        assertThat(todayStatistics.get(0).getId().getDay()).isEqualTo(day);
        assertThat(todayStatistics.get(0).getId().getUserId()).isEqualTo("0daf38fb-30aa-48c3-994a-bab70078a370");
        assertThat(todayStatistics.get(0).getId().getResourceType()).isEqualTo(0);
        assertThat(todayStatistics.get(0).getId().getResourceId()).isEqualTo("10");
        assertThat(todayStatistics.get(0).getVisited()).isTrue();
        assertThat(todayStatistics.get(0).getOrdered()).isTrue();
        assertThat(todayStatistics.get(1).getId().getUserId()).isEqualTo("1507dabf-acf6-477b-89de-033f8f9d570e");
        assertThat(todayStatistics.get(1).getVisited()).isTrue();
        assertThat(todayStatistics.get(1).getOrdered()).isFalse();

        var visitsPerServiceCounter = statsTodayRepository.countVisitsPerResource(day);
        var ordersPerServiceCounter = statsTodayRepository.countOrdersPerResource(day);
        Collections.sort(visitsPerServiceCounter);
        Collections.sort(ordersPerServiceCounter);
        assertThat(visitsPerServiceCounter.size()).isEqualTo(2);
        assertThat(ordersPerServiceCounter.size()).isEqualTo(1);
        assertThat(visitsPerServiceCounter.get(0).getResource().getResourceId()).isEqualTo("10");
        assertThat(visitsPerServiceCounter.get(0).getCounter()).isEqualTo(3);
        assertThat(ordersPerServiceCounter.get(0).getResource().getResourceId()).isEqualTo("10");
        assertThat(ordersPerServiceCounter.get(0).getCounter()).isIn(List.of(2,3));
        assertThat(visitsPerServiceCounter.get(1).getResource().getResourceId()).isEqualTo("12");
    }

    public void updateAndCheckTodayWithExtraSessions(Integer day) {

        // assumptions
        assertThat(statsTodayRepository.count()).isEqualTo(5);

        // when
        todayPublisher.updateToday();

        // then
        assertThat(statsTodayRepository.count()).isEqualTo(7);
        var todayStatistics= statsTodayRepository.findAll();

        assertThat(todayStatistics.get(0).getId().getDay()).isEqualTo(day);
        assertThat(todayStatistics.get(0).getId().getUserId()).isEqualTo("0daf38fb-30aa-48c3-994a-bab70078a370");
        assertThat(todayStatistics.get(0).getId().getResourceId()).isEqualTo("10");
        assertThat(todayStatistics.get(0).getVisited()).isTrue();
        assertThat(todayStatistics.get(0).getOrdered()).isTrue();
        assertThat(todayStatistics.get(1).getId().getUserId()).isEqualTo("1507dabf-acf6-477b-89de-033f8f9d570e");
        assertThat(todayStatistics.get(1).getVisited()).isTrue();
        assertThat(todayStatistics.get(1).getOrdered()).isFalse();

        var visitsPerServiceCounter = statsTodayRepository.countVisitsPerResource(day);
        var ordersPerServiceCounter = statsTodayRepository.countOrdersPerResource(day);
        assertThat(visitsPerServiceCounter.size()).isEqualTo(4);
        assertThat(ordersPerServiceCounter.size()).isEqualTo(2);
        Collections.sort(visitsPerServiceCounter);
        Collections.sort(ordersPerServiceCounter);

        checkResourceCounter(visitsPerServiceCounter.get(0),expectedResourceCounter("10",3));
        checkResourceCounter(visitsPerServiceCounter.get(1),expectedResourceCounter("12",2));
        checkResourceCounter(visitsPerServiceCounter.get(2),expectedResourceCounter("20",1));
        checkResourceCounter(visitsPerServiceCounter.get(3),expectedResourceCounter("21",1));
        checkResourceCounter(ordersPerServiceCounter.get(0),expectedResourceCounter("10",3));
        checkResourceCounter(ordersPerServiceCounter.get(1),expectedResourceCounter("21",1));
    }

    public void calculateStats(Integer expectedStatsNumber) {
        assertThat(statsTodayRepository.count()).isEqualTo(expectedStatsNumber);
        statisticsPublisher.calculateStats();
        assertThat(statsTodayRepository.count()).isEqualTo(0);
    }

    public void checkPerDayStats(){
        assertThat(statsPerDayRepository.count()).isEqualTo(2);
        var dayEntities = statsPerDayRepository.findAll();
        checkDayStatEntity(dayEntities.get(0),expectedDayStatsService(1,"10",3,2));
        checkDayStatEntity(dayEntities.get(1),expectedDayStatsService(1,"12",2,0));
    }

    public void checkTotalStats(){
        assertThat(statsTotalRepository.count()).isEqualTo(2);
        var totalEntities = statsTotalRepository.findAll();
        Collections.sort(totalEntities);
        checkTotalStatsService(totalEntities.get(0),expectedTotalStatsService("10",3,2));
        checkTotalStatsService(totalEntities.get(1),expectedTotalStatsService("12",2,0));
    }

    public void checkPerDayStatsFinal(){
        assertThat(statsPerDayRepository.count()).isEqualTo(6);
        var dayEntities = statsPerDayRepository.findAll();
        checkDayStatEntity(dayEntities.get(0),expectedDayStatsService(1,"10",3,2));
        checkDayStatEntity(dayEntities.get(1),expectedDayStatsService(1,"12",2,0));
        checkDayStatEntity(dayEntities.get(2),expectedDayStatsService(2,"10",3,3));
        checkDayStatEntity(dayEntities.get(3),expectedDayStatsService(2,"12",2,0));
        checkDayStatEntity(dayEntities.get(4),expectedDayStatsService(2,"20",1,0));
        checkDayStatEntity(dayEntities.get(5),expectedDayStatsService(2,"21",1,1));
    }

    public void checkTotalStatsFinal(){
        assertThat(statsTotalRepository.count()).isEqualTo(4);
        var totalEntities = statsTotalRepository.findAll();
        Collections.sort(totalEntities);
        checkTotalStatsService(totalEntities.get(0),expectedTotalStatsService("10",6,5));
        checkTotalStatsService(totalEntities.get(1),expectedTotalStatsService("12",4,0));
        checkTotalStatsService(totalEntities.get(2),expectedTotalStatsService("20",1,0));
        checkTotalStatsService(totalEntities.get(3),expectedTotalStatsService("21",1,1));
    }

    private ArtificialDayStatsEntity expectedDayStatsService(Integer day,String resourceId,Integer visits,Integer orders){
        return new ArtificialDayStatsEntity(new ResourceDayKey(ResourceEnum.SERVICE.getId(),resourceId,day),visits,orders);
    }
    private void checkDayStatEntity(ArtificialDayStatsEntity actual, ArtificialDayStatsEntity expected) {
        assertThat(actual.getId().getDay()).isEqualTo(expected.getId().getDay());
        assertThat(actual.getId().getResourceType()).isEqualTo(expected.getId().getResourceType());
        assertThat(actual.getId().getResourceId()).isEqualTo(expected.getId().getResourceId());
        assertThat(actual.getVisits()).isEqualTo(expected.getVisits());
        assertThat(actual.getOrders()).isEqualTo(expected.getOrders());
    }

    private ArtificialTotalStatsEntity expectedTotalStatsService(String resourceId, Integer visits, Integer orders){
        return new ArtificialTotalStatsEntity(ResourceEnum.SERVICE.getId(),resourceId,visits,orders);
    }
    private void checkTotalStatsService(ArtificialTotalStatsEntity actual, ArtificialTotalStatsEntity expected) {
        assertThat(actual.getResource().getResourceType()).isEqualTo(expected.getResource().getResourceType());
        assertThat(actual.getResource().getResourceId()).isEqualTo(expected.getResource().getResourceId());
        assertThat(actual.getVisits()).isEqualTo(expected.getVisits());
        assertThat(actual.getOrders()).isEqualTo(expected.getOrders());
    }

    private ResourceCounter expectedResourceCounter(String resourceId, long counter){
        return new ResourceCounter(ResourceEnum.SERVICE.getId(),resourceId,counter);
    }
    private void checkResourceCounter(ResourceCounter actual, ResourceCounter expected) {
        assertThat(actual.getResource().getResourceType()).isEqualTo(expected.getResource().getResourceType());
        assertThat(actual.getResource().getResourceId()).isEqualTo(expected.getResource().getResourceId());
        assertThat(actual.getCounter()).isEqualTo(expected.getCounter());
    }


}
