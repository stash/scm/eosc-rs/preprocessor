package pl.psnc.eosc.rs.preprocessor.app.context.resource.synchronization.user;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import pl.psnc.eosc.rs.preprocessor.app.context.resource.converter.JsonObjectConverter;
import pl.psnc.eosc.rs.preprocessor.app.context.resource.converter.ResourceEvent;
import pl.psnc.eosc.rs.preprocessor.app.context.resource.synchronization.EventType;
import pl.psnc.eosc.rs.preprocessor.app.context.resource.synchronization.user.model.UserEntity;
import pl.psnc.eosc.rs.preprocessor.app.context.resource.synchronization.user.model.UserEvent;
import pl.psnc.eosc.rs.preprocessor.app.context.resource.synchronization.user.repository.UserRepository;
import pl.psnc.eosc.rs.preprocessor.app.context.resource.synchronization.user.service.UserChangesPublisher;
import pl.psnc.eosc.rs.preprocessor.app.context.resource.synchronization.user.service.UserEventHandler;

import java.util.List;
import java.util.Optional;

import static com.google.common.truth.Truth.assertThat;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class UserEventHandlerTest {

    @InjectMocks
    UserEventHandler userEventHandler;

    @Mock
    JsonObjectConverter eventConverter;

    @Mock
    UserRepository userRepository;

    @Mock
    UserChangesPublisher userChangesPropagator;

    private static ObjectMapper mapper = new ObjectMapper();

    @Test
    public void userEvent_update() throws JsonProcessingException {
        String newString = "{\"test\": \"any\"}";
        JsonNode node = mapper.readTree(newString);
        UserEntity searchedUser = new UserEntity();
        searchedUser.setAaiId("aaiId");
        when(eventConverter.convert(anyString()))
                .thenReturn(new ResourceEvent(
                        "update",
                        "User",
                        node))
                .thenReturn(new UserEvent(
                        "aaiId",
                        "marketplaceId",
                        List.of(1,9),
                        List.of(2,8),
                        List.of(3,7)));
        when(userRepository.findById("aaiId")).thenReturn(Optional.of(searchedUser));

        //when:
        userEventHandler.processMessage("any");

        //then:
        ArgumentCaptor<UserEntity> userEntityArgumentCaptor = ArgumentCaptor.forClass(UserEntity.class);
        verify(userRepository,times(1)).saveAndFlush(userEntityArgumentCaptor.capture());
        UserEntity userEntity = userEntityArgumentCaptor.getValue();
        Assertions.assertThat(userEntity).usingRecursiveComparison().isEqualTo(getExpectedUserEntity());
        assertEqualityOfSavedAndPublishedUser(userEntity,EventType.UPDATE);
    }

    @Test
    public void userEvent_create() throws JsonProcessingException {
        String newString = "{\"test\": \"any\"}";
        JsonNode node = mapper.readTree(newString);
        when(eventConverter.convert(anyString()))
                .thenReturn(new ResourceEvent(
                        "create",
                        "User",
                        node))
                .thenReturn(new UserEvent(
                        "aaiId",
                        "marketplaceId",
                        List.of(1,9),
                        List.of(2,8),
                        List.of(3,7)));
        userEventHandler.processMessage("any");
        ArgumentCaptor<UserEntity> userEntityArgumentCaptor = ArgumentCaptor.forClass(UserEntity.class);
        verify(userRepository,times(1)).saveAndFlush(userEntityArgumentCaptor.capture());
        UserEntity userEntity = userEntityArgumentCaptor.getValue();
        Assertions.assertThat(userEntity).usingRecursiveComparison().isEqualTo(getExpectedUserEntity());

        assertEqualityOfSavedAndPublishedUser(userEntity,EventType.CREATE);
    }

    @Test
    public void userEvent_delete() throws JsonProcessingException {
        String newString = "{\"test\": \"any\"}";
        JsonNode node = mapper.readTree(newString);
        when(eventConverter.convert(anyString()))
                .thenReturn(new ResourceEvent(
                        "delete",
                        "User",
                        node))
                .thenReturn(new UserEvent(
                        "aaiId",
                        "marketplaceId",
                        List.of(1,9),
                        List.of(2,8),
                        List.of(3,7)));
        userEventHandler.processMessage("any");
        ArgumentCaptor<String> userIdArgumentCaptor = ArgumentCaptor.forClass(String.class);
        verify(userRepository,times(1)).deleteById(userIdArgumentCaptor.capture());
        assertThat(userIdArgumentCaptor.getValue()).isEqualTo("aaiId");

        UserEntity expectedUserEntity = new UserEntity();
        expectedUserEntity.setAaiId("aaiId");

        ArgumentCaptor<UserEvent> propagatedUserCaptor = ArgumentCaptor.forClass(UserEvent.class);
        verify(userChangesPropagator,times(1)).publish(propagatedUserCaptor.capture());
        UserEvent publishedUserEntity = propagatedUserCaptor.getValue();
        assertThat(publishedUserEntity.getAaiId()).isEqualTo(expectedUserEntity.getAaiId());
        assertThat(publishedUserEntity.getEventType()).isEqualTo(EventType.DELETE.getName());
    }

    @Test
    public void userEvent_other() throws JsonProcessingException {
        String newString = "{\"test\": \"any\"}";
        JsonNode node = mapper.readTree(newString);
        when(eventConverter.convert(anyString()))
                .thenReturn(new ResourceEvent(
                        "other",
                        "User",
                        node))
                .thenReturn(new UserEvent(
                        "aaiId",
                        "marketplaceId",
                        List.of(1,9),
                        List.of(2,8),
                        List.of(3,7)));
        userEventHandler.processMessage("any");
        ArgumentCaptor<String> userIdArgumentCaptor = ArgumentCaptor.forClass(String.class);
        verify(userRepository,times(0)).saveAndFlush(any());
        verify(userRepository,times(0)).deleteById(anyString());
    }

    @Test
    public void otherEvent() throws JsonProcessingException {
        String newString = "{\"test\": \"any\"}";
        JsonNode node = mapper.readTree(newString);
        when(eventConverter.convert(anyString()))
                .thenReturn(new ResourceEvent(
                        "update",
                        "Other",
                        node))
                .thenReturn(new UserEvent(
                        "aaiId",
                        "marketplaceId",
                        List.of(1,9),
                        List.of(2,8),
                        List.of(3,7)));
        userEventHandler.processMessage("any");
        ArgumentCaptor<String> userIdArgumentCaptor = ArgumentCaptor.forClass(String.class);
        verify(userRepository,times(0)).saveAndFlush(any());
        verify(userRepository,times(0)).deleteById(anyString());
    }

    private UserEntity getExpectedUserEntity(){
        return new UserEntity(
                "aaiId",
                "marketplaceId",
                List.of(1,9),
                List.of(2,8),
                List.of(3,7));
    }

    private void assertEqualityOfSavedAndPublishedUser(UserEntity expectedUserEntity, EventType expectedEventType){
        ArgumentCaptor<UserEvent> propagatedUserCaptor = ArgumentCaptor.forClass(UserEvent.class);
        verify(userChangesPropagator,times(1)).publish(propagatedUserCaptor.capture());
        UserEvent publishedUserEntity = propagatedUserCaptor.getValue();
        assertThat(publishedUserEntity.getEventType()).isEqualTo(expectedEventType.getName());
        assertThat(publishedUserEntity.getAaiId()).isEqualTo(expectedUserEntity.getAaiId());
        assertThat(publishedUserEntity.getMarketplaceId()).isEqualTo(expectedUserEntity.getMarketplaceId());
        assertThat(publishedUserEntity.getCategoryValues()).isEqualTo(expectedUserEntity.getCategoryValues());
        assertThat(publishedUserEntity.getScientificDomainValues()).isEqualTo(expectedUserEntity.getScientificDomainValues());
        assertThat(publishedUserEntity.getAccessedServiceValues()).isEqualTo(expectedUserEntity.getAccessedServiceValues());
    }
}
