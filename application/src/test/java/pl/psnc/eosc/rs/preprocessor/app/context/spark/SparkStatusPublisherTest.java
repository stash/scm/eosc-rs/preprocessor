package pl.psnc.eosc.rs.preprocessor.app.context.spark;

import org.apache.spark.launcher.SparkAppHandle;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import pl.psnc.eosc.rs.preprocessor.app.cloud.kafka.Producer;
import pl.psnc.eosc.rs.preprocessor.app.context.oag.job.OagResourcesPreprocJobLauncher;
import pl.psnc.eosc.rs.preprocessor.app.context.spark.processing.SparkStatusPublisherImpl;
import pl.psnc.eosc.rs.preprocessor.app.context.spark.processing.listener.SparkJobFinalStatusForwardingListener;

import java.util.Optional;

import static com.google.common.truth.Truth.assertThat;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class SparkStatusPublisherTest {

    @Mock
    Producer producer;

    @Mock
    SparkAppHandle sparkAppHandle;

    @Captor
    ArgumentCaptor<String> messageCaptor;

    @Test
    public void statusPublisherTest() {
        String topic = "testTopic";
        SparkJobFinalStatusForwardingListener.SparkFinalStatusListener sparkPublisher =
                new SparkStatusPublisherImpl(topic,producer);
        String processingId = "1";
        SparkAppHandle.State state = SparkAppHandle.State.FINISHED;
        when(sparkAppHandle.getState()).thenReturn(state);
        when(sparkAppHandle.getAppId()).thenReturn(processingId);
        when(sparkAppHandle.getError()).thenReturn(Optional.empty());
        SparkJobFinalStatusForwardingListener listener =
                new SparkJobFinalStatusForwardingListener(sparkPublisher, "testName",
                        OagResourcesPreprocJobLauncher.BasicResourceType.PUBLICATION);

        //when:
        listener.stateChanged(sparkAppHandle);

        verify(producer,times(1)).publish(eq(topic),eq("1"),messageCaptor.capture());
        assertThat(messageCaptor.getValue()).isNotNull();
        assertThat(messageCaptor.getValue()).contains(
                "{\"timestamp\":");
        assertThat(messageCaptor.getValue()).contains(
                "\"state\":\"FINISHED\"," +
                        "\"error\":null," +
                        "\"command_name\":\"testName\"," +
                        "\"processing_id\":\"1\"," +
                        "\"resource_type\":\"publication\"}");
    }

    @Test
    public void statusPublisher_errorTest() {
        String topic = "testTopic";
        SparkJobFinalStatusForwardingListener.SparkFinalStatusListener sparkPublisher =
                new SparkStatusPublisherImpl(topic,producer);
        String processingId = "1";
        SparkAppHandle.State state = SparkAppHandle.State.FINISHED;
        when(sparkAppHandle.getState()).thenReturn(state);
        when(sparkAppHandle.getAppId()).thenReturn(processingId);
        when(sparkAppHandle.getError()).thenReturn(Optional.of(new Throwable("test error")));
        SparkJobFinalStatusForwardingListener listener =
                new SparkJobFinalStatusForwardingListener(sparkPublisher, "testName",
                        OagResourcesPreprocJobLauncher.BasicResourceType.PUBLICATION);

        //when:
        listener.stateChanged(sparkAppHandle);

        verify(producer,times(1)).publish(eq(topic),eq("1"),messageCaptor.capture());
        assertThat(messageCaptor.getValue()).isNotNull();
        assertThat(messageCaptor.getValue()).contains(
                "{\"timestamp\":");
        assertThat(messageCaptor.getValue()).contains(
                "\"state\":\"FINISHED\"," +
                        "\"error\":\"test error\"," +
                        "\"command_name\":\"testName\"," +
                        "\"processing_id\":\"1\"," +
                        "\"resource_type\":\"publication\"}");
    }
}
