package pl.psnc.eosc.rs.preprocessor.app.context.resource.backup;

import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;
import pl.psnc.eosc.rs.preprocessor.app.cloud.jms.JmsProducer;
import pl.psnc.eosc.rs.preprocessor.app.cloud.jms.MessageBroadcaster;
import pl.psnc.eosc.rs.preprocessor.app.cloud.jms.ProviderComponentMessageBroadcaster;
import pl.psnc.eosc.rs.preprocessor.app.context.user.action.backup.BaseBackup;
import pl.psnc.eosc.rs.preprocessor.app.context.user.action.generator.RandomUserProvider;

import javax.validation.ConstraintViolationException;
import javax.validation.Validator;

import static com.google.common.truth.Truth.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@SpringBootTest
@ActiveProfiles({"test","jms-disabled","artificialUAConfig-disabled","backup-raw-jms"})
public class ResourceEventBackupTest {

    @Qualifier("rawResourceSaver")
    @Autowired
    BaseBackup baseBackup;

    @MockBean
    RawResourceRepository rawResourceRepository;

    @Autowired
    Validator validator;

    @MockBean
    JmsProducer jmsProducer;

    @MockBean(name = "userActionBroadcaster")
    MessageBroadcaster userActionBroadcaster;

    @MockBean(name = "resourceEventBroadcaster")
    MessageBroadcaster resourceEventBroadcaster;

    @MockBean(name = "evaluationEventBroadcaster")
    MessageBroadcaster evaluationEventBroadcaster;

    @MockBean(name = "recommendationEventBroadcaster")
    MessageBroadcaster recommendationEventBroadcaster;

    @MockBean(name = "trainingEventBroadcaster")
    ProviderComponentMessageBroadcaster trainingMessageBroadcaster;

    @MockBean
    RandomUserProvider randomUserPicker;

    @Test
    public void validResourceEventBackupTest() {
        String resourceEvent = org.apache.commons.lang3.StringUtils.repeat("x",8192);
        baseBackup.processMessage(resourceEvent);

        ArgumentCaptor<ResourceEventBackupEntity> backupEntityCaptor = ArgumentCaptor.forClass(ResourceEventBackupEntity.class);
        verify(rawResourceRepository, times(1)).save(backupEntityCaptor.capture());

        assertThat(backupEntityCaptor.getValue().rawResourceEvent).isEqualTo(resourceEvent);
    }

    @Test
    public void invalidResourceEventBackupTest() {
        String invalidResourceEvent = StringUtils.repeat("x",8193);

        assertThrows(ConstraintViolationException.class, () -> {
            baseBackup.processMessage(invalidResourceEvent);
        });
    }
}