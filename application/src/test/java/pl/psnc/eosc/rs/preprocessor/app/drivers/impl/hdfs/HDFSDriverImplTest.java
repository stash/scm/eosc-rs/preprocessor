package pl.psnc.eosc.rs.preprocessor.app.drivers.impl.hdfs;

import lombok.SneakyThrows;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import pl.psnc.eosc.rs.preprocessor.app.drivers.api.hdfs.HdfsFileStatus;

import java.util.List;

import static java.util.Comparator.comparing;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

class HDFSDriverImplTest {

    @SneakyThrows
    @Test
    void listFiles() {
        //GIVEN MOCKS
        FileSystem fileSystemMock = Mockito.mock(FileSystem.class);
        when(fileSystemMock.listStatus(eq(new Path("/user/hadoop"))))
                .thenReturn(new FileStatus[]{
                        exampleFileStatus("/user/hadoop/file1.txt",false),
                        exampleFileStatus("/user/hadoop/directory",true)});

        //WHEN
        List<HdfsFileStatus> result = new HDFSDriverImpl(fileSystemMock).listFiles(new Path("/user/hadoop"));

        //THEN
        result.sort(comparing(HdfsFileStatus::getFilename));
        assertEquals(2,result.size());
        assertEquals("file1.txt",result.get(1).getFilename());
        assertEquals(false,result.get(1).getIsDirectory());
        assertEquals("directory",result.get(0).getFilename());
        assertEquals(true,result.get(0).getIsDirectory());
    }

    @SneakyThrows
    @Test
    void delete() {
        //GIVEN MOCKS
        FileSystem fileSystemMock = Mockito.mock(FileSystem.class);
        when(fileSystemMock.delete(eq(new Path("/user/hadoop")),eq(true))).thenReturn(true);

        //WHEN
        Boolean result = new HDFSDriverImpl(fileSystemMock).delete(new Path("/user/hadoop"),true);

        //THEN
        assertTrue(result);
    }

    private FileStatus exampleFileStatus(String filePath, Boolean isDirectory){
        return new FileStatus(100,isDirectory,3,256,123123123,new Path(filePath));
    }
}