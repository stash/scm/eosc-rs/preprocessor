package pl.psnc.eosc.rs.preprocessor.app.context.resource.synchronization.service;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import pl.psnc.eosc.rs.preprocessor.app.cloud.kafka.Producer;
import pl.psnc.eosc.rs.preprocessor.app.context.resource.synchronization.EventType;
import pl.psnc.eosc.rs.preprocessor.app.context.resource.synchronization.service.model.ServiceEvent;
import pl.psnc.eosc.rs.preprocessor.app.context.resource.synchronization.service.service.ServicePublisherImpl;

import java.util.List;

import static com.google.common.truth.Truth.assertThat;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
public class ServiceEventPublisherTest {

    @Mock
    Producer producer;

    @Test
    public void serviceEvent() {
        ServicePublisherImpl userChangesPropagator = new ServicePublisherImpl(producer,"test_topic");
        EventType type = EventType.CREATE;
        ServiceEvent service = new ServiceEvent(type.getName(),"id","sample_name","Sample description.",List.of(1,2),List.of(3,4),List.of(5,6));

        //when:
        userChangesPropagator.publish(service);

        //then:
        ArgumentCaptor<String> topicArgumentCaptor = ArgumentCaptor.forClass(String.class);
        ArgumentCaptor<String> keyArgumentCaptor = ArgumentCaptor.forClass(String.class);
        ArgumentCaptor<String> messageArgumentCaptor = ArgumentCaptor.forClass(String.class);
        verify(producer,times(1)).publish(
                topicArgumentCaptor.capture(),
                keyArgumentCaptor.capture(),
                messageArgumentCaptor.capture());

        assertThat(topicArgumentCaptor.getValue()).isEqualTo("test_topic");
        assertThat(keyArgumentCaptor.getValue()).isEqualTo("id");
        assertThat(messageArgumentCaptor.getValue()).contains(getSampleMessage());
    }

    private String getSampleMessage(){
        return "{\"eventType\":\"create\"," +
                "\"id\":\"id\"," +
                "\"name\":\"sample_name\"," +
                "\"description\":\"Sample description.\"," +
                "\"scientificDomainValues\":[1,2]," +
                "\"categoryValues\":[3,4]," +
                "\"relatedServices\":[5,6]}";
    }
}
