package pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import pl.psnc.eosc.rs.preprocessor.app.context.resource.synchronization.user.model.UserEntity;
import pl.psnc.eosc.rs.preprocessor.app.context.resource.synchronization.user.repository.UserRepository;
import pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular.model.internal.SingleSession;
import pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular.service.user.UserDataSupplierImpl;

import java.util.List;
import java.util.Optional;

import static com.google.common.truth.Truth.assertThat;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class UserDataSupplierTest {

    @InjectMocks
    UserDataSupplierImpl userEnhancer;

    @Mock
    UserRepository userRepository;

    @Test
    public void enrichUserProfile_withAaiId(){
        String aaiId = "aaiId";
        String marketplaceId = "marketplaceId";
        SingleSession session = new SingleSession();
        session.setAaiUid(aaiId);
        UserEntity userEntity = new UserEntity(aaiId,marketplaceId, List.of(1),List.of(2),List.of(3));
        when(userRepository.findById(eq(aaiId))).thenReturn(Optional.of(userEntity));
        //when:
        userEnhancer.enrichUserProfile(session);
        //then:
        assertThat(session.getAaiUid()).isEqualTo(aaiId);
        assertThat(session.getMarketplaceId()).isEqualTo(marketplaceId);
    }

    @Test
    public void enrichUserProfile_withMarketplaceId(){
        String aaiId = "aaiId";
        String marketplaceId = "marketplaceId";
        SingleSession session = new SingleSession();
        session.setMarketplaceId(marketplaceId);
        UserEntity userEntity = new UserEntity(aaiId,marketplaceId, List.of(1),List.of(2),List.of(3));
        when(userRepository.findDistinctByMarketplaceId(eq(marketplaceId))).thenReturn(userEntity);
        //when:
        userEnhancer.enrichUserProfile(session);
        //then:
        assertThat(session.getAaiUid()).isEqualTo(aaiId);
        assertThat(session.getMarketplaceId()).isEqualTo(marketplaceId);
    }

    @Test
    public void enrichUserProfile_withAaiId_notFound(){
        String aaiId = "aaiId";
        SingleSession session = new SingleSession();
        session.setAaiUid(aaiId);
        when(userRepository.findById(eq(aaiId))).thenReturn(Optional.empty());
        //when:
        userEnhancer.enrichUserProfile(session);
        //then:
        assertThat(session.getAaiUid()).isEqualTo(aaiId);
        assertThat(session.getMarketplaceId()).isNull();
    }

    @Test
    public void enrichUserProfile_withMarketplaceId_notFound(){
        String marketplaceId = "marketplaceId";
        SingleSession session = new SingleSession();
        session.setMarketplaceId(marketplaceId);

        when(userRepository.findDistinctByMarketplaceId(eq(marketplaceId))).thenReturn(null);
        //when:
        userEnhancer.enrichUserProfile(session);
        //then:
        assertThat(session.getAaiUid()).isNull();
        assertThat(session.getMarketplaceId()).isEqualTo(marketplaceId);
    }
}
