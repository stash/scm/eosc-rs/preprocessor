package pl.psnc.eosc.rs.preprocessor.app.test;

import org.springframework.core.io.Resource;
import org.springframework.util.FileCopyUtils;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.UncheckedIOException;
import java.net.URISyntaxException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class ResourcesTestHelper {

    public static final Charset CHARSET = StandardCharsets.UTF_8;

    public static String asString(Resource resource, Charset charset) {
        try (Reader reader = new InputStreamReader(resource.getInputStream(), charset)) {
            return FileCopyUtils.copyToString(reader);
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }

    public static String asString(Resource resource) {
        return asString(resource, CHARSET);
    }

    static public String asString(Path resourceFilePath) {
        try {
            return Files.readString(Paths.get(ResourcesTestHelper.class.getResource(
                    resourceFilePath.toString()).toURI()), CHARSET);
        } catch (IOException | URISyntaxException e) {
            throw new RuntimeException("Cannot load resource " + resourceFilePath, e);
        }
    }
}