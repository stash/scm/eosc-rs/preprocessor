package pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.springframework.test.util.ReflectionTestUtils;
import pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular.service.statistics.DayProviderImpl;

import java.time.Clock;
import java.time.LocalDateTime;
import java.time.ZoneId;

import static com.google.common.truth.Truth.assertThat;

public class DayProviderTest {

    @ParameterizedTest
    @CsvSource({"0,0,0", "0,0,1", "23,59,59"})
    public void checkDay(int hour, int minute, int second){
        DayProviderImpl dayProvider = new DayProviderImpl();
        var clock = Clock.fixed(
                LocalDateTime.of(2022, 6, 2, hour, minute,second).atZone(ZoneId.of("UTC")).toInstant(),
                Clock.systemUTC().getZone());

        ReflectionTestUtils.setField(dayProvider, "clock", clock);
        ReflectionTestUtils.setField(dayProvider, "day", 1);
        ReflectionTestUtils.setField(dayProvider, "month", 6);
        ReflectionTestUtils.setField(dayProvider, "year", 2022);

        assertThat(dayProvider.getCurrentDay()).isEqualTo(1);
    }
}
