package pl.psnc.eosc.rs.preprocessor.app.context.user.action.backup;

import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;
import pl.psnc.eosc.rs.preprocessor.app.cloud.jms.JmsProducer;
import pl.psnc.eosc.rs.preprocessor.app.cloud.jms.MessageBroadcaster;
import pl.psnc.eosc.rs.preprocessor.app.cloud.jms.ProviderComponentMessageBroadcaster;
import pl.psnc.eosc.rs.preprocessor.app.context.user.action.generator.RandomUserProvider;

import javax.validation.ConstraintViolationException;
import javax.validation.Validator;

import static com.google.common.truth.Truth.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@SpringBootTest
@ActiveProfiles({"test","jms-disabled","artificialUAConfig-disabled","backup-raw-jms"})
public class UserActionBackupTest {

    @Qualifier("rawUserActionSaver")
    @Autowired
    BaseBackup rawUserActionSaver;

    @MockBean
    RawUserActionRepository rawUserActionRepository;

    @Autowired
    Validator validator;

    @MockBean
    JmsProducer jmsProducer;

    @MockBean(name = "userActionBroadcaster")
    MessageBroadcaster userActionBroadcaster;

    @MockBean(name = "resourceEventBroadcaster")
    MessageBroadcaster resourceEventBroadcaster;

    @MockBean(name = "evaluationEventBroadcaster")
    MessageBroadcaster evaluationEventBroadcaster;

    @MockBean(name = "recommendationEventBroadcaster")
    MessageBroadcaster recommendationEventBroadcaster;

    @MockBean(name = "trainingEventBroadcaster")
    ProviderComponentMessageBroadcaster trainingMessageBroadcaster;

    @MockBean
    RandomUserProvider randomUserPicker;

    @Test
    public void validUserActionBackupTest() {
        String userAction = StringUtils.repeat("x",8192);
        rawUserActionSaver.processMessage(userAction);

        ArgumentCaptor<UserActionBackupEntity> backupEntityCaptor = ArgumentCaptor.forClass(UserActionBackupEntity.class);
        verify(rawUserActionRepository,times(1)).save(backupEntityCaptor.capture());

        assertThat(backupEntityCaptor.getValue().rawUserAction).isEqualTo(userAction);
    }

    @Test
    public void invalidUserActionBackupTest() {
        String invalidUserAction = StringUtils.repeat("x",8193);

        assertThrows(ConstraintViolationException.class, () -> {
            rawUserActionSaver.processMessage(invalidUserAction);
        });
    }
}
