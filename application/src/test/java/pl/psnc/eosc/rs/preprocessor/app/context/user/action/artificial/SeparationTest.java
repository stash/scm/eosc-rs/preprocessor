package pl.psnc.eosc.rs.preprocessor.app.context.user.action.artificial;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.context.annotation.Import;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.testcontainers.junit.jupiter.Testcontainers;
import pl.psnc.eosc.rs.preprocessor.app.cloud.jms.JmsProducer;
import pl.psnc.eosc.rs.preprocessor.app.cloud.kafka.Producer;
import pl.psnc.eosc.rs.preprocessor.app.context.resource.synchronization.user.repository.UserRepository;
import pl.psnc.eosc.rs.preprocessor.app.context.user.action.generator.GeneratorManager;
import pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular.DockerPostgeSQLDataSourceInitializer;
import pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular.IntegrationTestConf;
import pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular.repository.StatsPerDayRepository;
import pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular.repository.StatsTodayRepository;
import pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular.repository.StatsTotalRepository;
import pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular.service.statistics.StatisticsHandler;
import pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular.service.statistics.StatisticsPublisher;
import pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular.service.statistics.TodayPublisher;

import java.util.List;

import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;

@ActiveProfiles({"integration","ua-generator","artificial-flow"})
@Import(IntegrationTestConf.class)
@SpringBootTest(webEnvironment=SpringBootTest.WebEnvironment.RANDOM_PORT)
@ContextConfiguration(initializers = DockerPostgeSQLDataSourceInitializer.class)
@Testcontainers
public class SeparationTest {

    @Autowired
    GeneratorManager generatorManager;

    @SpyBean
    StatisticsHandler statisticsHandler;

    @MockBean
    StatisticsPublisher statisticsPublisher;

    @SpyBean
    StatsTodayRepository artificialStatsTodayRepository;

    @MockBean
    StatsTodayRepository statsTodayRepository;

    @MockBean
    StatsPerDayRepository statsPerDayRepository;

    @MockBean
    StatsTotalRepository statsTotalRepository;

    @MockBean
    UserRepository userRepository;

    @MockBean
    TodayPublisher todayPublisher;

    @MockBean
    JmsTemplate jmsTemplate;

    @MockBean
    JmsProducer jmsProducer;

    @MockBean
    Producer producer;


    @Test
    public void isArtificialFlowIndependent(){
        when(userRepository.getAllIds()).thenReturn(List.of("1"));

        generatorManager.generateAndPublishUA();

        verify(statisticsHandler, times(0)).receiveUserAction(anyString(),anyString(),any(),anyBoolean());
        verify(statisticsPublisher, times(0)).calculateStats();
        verify(statsTodayRepository, times(0)).countOrdersPerResource(anyInt());
        verify(statsTodayRepository, times(0)).countVisitsPerResource(anyInt());
        verify(statsTodayRepository, times(0)).deleteDayAndBefore(anyInt());
        verify(statsPerDayRepository, times(0)).findFirst100ById_DayOrderByOrdersDesc(anyInt());
        verify(statsPerDayRepository, times(0)).findFirst100ById_DayOrderByVisitsDesc(anyInt());
        verify(statsTotalRepository, times(0)).findAllById(anyCollection());
        verify(todayPublisher, times(0)).updateToday();
        verify(jmsTemplate, times(0)).convertAndSend(anyString(),anyString());
    }

}
