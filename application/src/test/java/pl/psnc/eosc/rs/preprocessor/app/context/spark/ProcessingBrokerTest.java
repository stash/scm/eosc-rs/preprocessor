package pl.psnc.eosc.rs.preprocessor.app.context.spark;

import org.apache.spark.launcher.SparkAppHandle;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import pl.psnc.eosc.rs.preprocessor.app.context.oag.job.OagResourcesPreprocJobLauncher;
import pl.psnc.eosc.rs.preprocessor.app.context.oag.job.S3toHdfsTransferJobLauncher;
import pl.psnc.eosc.rs.preprocessor.app.context.spark.processing.ProcessingBrokerImpl;
import pl.psnc.eosc.rs.preprocessor.app.context.spark.processing.command.*;
import pl.psnc.eosc.rs.preprocessor.app.context.spark.processing.repository.CommandEntity;
import pl.psnc.eosc.rs.preprocessor.app.context.spark.processing.repository.CommandRepository;
import pl.psnc.eosc.rs.preprocessor.app.context.trainings.job.TrainingsPreprocJobLauncher;

import java.io.ByteArrayInputStream;
import java.io.ObjectInputStream;
import java.util.Optional;

import static com.google.common.truth.Truth.assertThat;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class ProcessingBrokerTest {

    @InjectMocks
    ProcessingBrokerImpl processingBroker;

    @Mock
    CommandRepository commandRepository;

    @Mock
    S3toHdfsTransferJobLauncher s3toHdfsTransferJobLauncher;

    @Mock
    OagResourcesPreprocJobLauncher oagResourcesPreprocJobLauncher;

    @Mock
    TrainingsPreprocJobLauncher trainingsPreprocJobLauncher;

    @Mock
    SparkAppHandle sparkAppHandle;

    @Captor
    ArgumentCaptor<SparkAppHandle.Listener> stateListenerCaptor;

    @Captor
    ArgumentCaptor<SparkAppHandle.Listener> processingTriggerCaptor;

    @Captor
    ArgumentCaptor<CommandEntity> commandEntityCaptor;


    @ParameterizedTest
    @EnumSource(value = OagResourcesPreprocJobLauncher.BasicResourceType.class)
    public void oagLauncherTest(OagResourcesPreprocJobLauncher.BasicResourceType resourceType) {
        CommandEntity sampleCommandEntity = new CommandEntity();
        sampleCommandEntity.setId(10L);
        when(commandRepository.saveAndFlush(any())).thenReturn(sampleCommandEntity);
        OagProcessingCommand command = prepareOagProcessingCommand(resourceType);
        processingBroker.execute(command,true, null);

        verify(commandRepository,times(1)).saveAndFlush(any());
        verify(oagResourcesPreprocJobLauncher,times(1)).runAsync(any(),anyString(),anyString(),any());
    }

    @Test
    public void trainingLauncherTest() {
        CommandEntity sampleCommandEntity = new CommandEntity();
        sampleCommandEntity.setId(10L);
        when(commandRepository.saveAndFlush(any())).thenReturn(sampleCommandEntity);
        TrainingProcessingCommand command = prepareTrainingProcessingCommand();
        processingBroker.execute(command,true, null);

        verify(commandRepository,times(1)).saveAndFlush(any());
        verify(trainingsPreprocJobLauncher,times(1)).runAsync(any(),anyString(),anyString(),any());
    }

    @Test
    public void transferToHdfsLauncherTest() {
        CommandEntity sampleCommandEntity = new CommandEntity();
        sampleCommandEntity.setId(10L);
        when(commandRepository.saveAndFlush(any())).thenReturn(sampleCommandEntity);
        S3ToHdfsTransferCommand command = prepareTransferCommand();
        processingBroker.execute(command,true, null);

        verify(commandRepository,times(1)).saveAndFlush(commandEntityCaptor.capture());
        verify(s3toHdfsTransferJobLauncher,times(1)).runAsync(any(),any());

        CommandEntity savedCommand = commandEntityCaptor.getValue();
        assertThat(savedCommand.getIsStartedManually()).isTrue();
        assertThat(savedCommand.getStatus()).isEqualTo("UNKNOWN");
        assertThat(savedCommand.getPredecessor()).isNull();
        assertThat(savedCommand.getType()).isEqualTo("S3ToHdfsTransferCommand");
        assertThat(savedCommand.getId()).isNull();
        S3ToHdfsTransferCommand recoveredCommand = (S3ToHdfsTransferCommand)transformByteArrayToCommand(savedCommand.getCommand());
        assertThat(recoveredCommand).isEqualTo(command);
        assertThat(recoveredCommand.getSourceDir()).isEqualTo(command.getSourceDir());
        assertThat(recoveredCommand.getDestDir()).isEqualTo(command.getDestDir());
    }

    @Test
    public void transferAndProcessingTest() {
        CommandEntity sampleCommandEntity = new CommandEntity();
        sampleCommandEntity.setId(10L);
        when(commandRepository.saveAndFlush(any())).thenReturn(sampleCommandEntity);
        when(commandRepository.findById(eq(10L)))
                .thenReturn(Optional.of(sampleCommandEntity),Optional.of(sampleCommandEntity));
        when(sparkAppHandle.getState()).thenReturn(SparkAppHandle.State.FINISHED);
        when(sparkAppHandle.getAppId()).thenReturn("1");
        when(sparkAppHandle.getError()).thenReturn(Optional.empty());
        TransferAndRunProcessingCommand command = prepareTransferAndProcessCommand();
        processingBroker.execute(command,true, null);

        verify(commandRepository,times(1)).saveAndFlush(any());
        verify(s3toHdfsTransferJobLauncher,times(1)).runAsync(
                anyMap(), processingTriggerCaptor.capture(),stateListenerCaptor.capture(), any(),any());

        stateListenerCaptor.getValue().stateChanged(sparkAppHandle);
        verify(commandRepository,times(1)).save(any());

        processingTriggerCaptor.getValue().stateChanged(sparkAppHandle);
        verify(commandRepository,times(8)).saveAndFlush(any());
        verify(oagResourcesPreprocJobLauncher,times(4)).runAsync(any(),anyString(),anyString(),any());
        verify(trainingsPreprocJobLauncher,times(1)).runAsync(any(),anyString(),anyString(),any());
    }

    @Test
    public void transferAndProcessingTest_Running() {
        CommandEntity sampleCommandEntity = new CommandEntity();
        sampleCommandEntity.setId(10L);
        when(commandRepository.saveAndFlush(any())).thenReturn(sampleCommandEntity);
        when(commandRepository.findById(eq(10L)))
                .thenReturn(Optional.of(sampleCommandEntity),Optional.of(sampleCommandEntity));
        when(sparkAppHandle.getState()).thenReturn(SparkAppHandle.State.RUNNING);
        when(sparkAppHandle.getAppId()).thenReturn("1");
        TransferAndRunProcessingCommand command = prepareTransferAndProcessCommand();
        processingBroker.execute(command,true, null);

        verify(commandRepository,times(1)).saveAndFlush(any());
        verify(s3toHdfsTransferJobLauncher,times(1)).runAsync(
                anyMap(), processingTriggerCaptor.capture(),stateListenerCaptor.capture(),any(),any());

        processingTriggerCaptor.getValue().stateChanged(sparkAppHandle);
        stateListenerCaptor.getValue().stateChanged(sparkAppHandle);

        verify(commandRepository,times(1)).save(any());
    }

    @Test
    public void transferAndProcessingTest_Error() {
        CommandEntity sampleCommandEntity = new CommandEntity();
        sampleCommandEntity.setId(10L);
        when(commandRepository.saveAndFlush(any())).thenReturn(sampleCommandEntity);
        when(commandRepository.findById(eq(10L)))
                .thenReturn(Optional.of(sampleCommandEntity),Optional.of(sampleCommandEntity));
        when(sparkAppHandle.getState()).thenReturn(SparkAppHandle.State.FINISHED);
        when(sparkAppHandle.getAppId()).thenReturn("1");
        when(sparkAppHandle.getError()).thenReturn(Optional.of(new Throwable("test error")));

        TransferAndRunProcessingCommand command = prepareTransferAndProcessCommand();
        //when:
        processingBroker.execute(command,true, null);
        //then:
        verify(commandRepository,times(1)).saveAndFlush(any());
        verify(s3toHdfsTransferJobLauncher,times(1)).runAsync(
                anyMap(), processingTriggerCaptor.capture(),stateListenerCaptor.capture(),any(),any());
        //when:
        processingTriggerCaptor.getValue().stateChanged(sparkAppHandle);
        stateListenerCaptor.getValue().stateChanged(sparkAppHandle);
        //then:
        verify(commandRepository,times(1)).save(any());
    }



    private OagProcessingCommand prepareOagProcessingCommand(OagResourcesPreprocJobLauncher.BasicResourceType resourceType){
        String relativePath = "test/"+resourceType.getName();
        return OagProcessingCommand.builder()
                .inputFiles(relativePath+"/*")
                .outputDir(relativePath)
                .resourceType(resourceType)
                .build();
    }

    private TrainingProcessingCommand prepareTrainingProcessingCommand(){
        String relativePath = "test/"+ TrainingsPreprocJobLauncher.TrainingResourceType.TRAINING.getName();
        return TrainingProcessingCommand.builder()
                .inputFiles(relativePath+"/*")
                .outputDir(relativePath)
                .build();
    }

    private S3ToHdfsTransferCommand prepareTransferCommand(){
        String relativePath = "test";
        return S3ToHdfsTransferCommand.builder()
                .sourceDir(relativePath)
                .destDir(relativePath)
                .build();
    }

    private TransferAndRunProcessingCommand prepareTransferAndProcessCommand(){
        String relativePath = "test";
        return TransferAndRunProcessingCommand.builder()
                .sourceDir(relativePath)
                .destDir(relativePath)
                .build();
    }

    private Command transformByteArrayToCommand(byte[] bytes){
        Command command = null;
        try {
            // bytearray to object
            ByteArrayInputStream bais = new ByteArrayInputStream(bytes);
            ObjectInputStream oi = new ObjectInputStream(bais);
            command = (Command)oi.readObject();
            bais.close();
            oi.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return command;
    }
}