package pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular;

import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;
import pl.psnc.eosc.rs.preprocessor.app.cloud.jms.Subscriber;
import pl.psnc.eosc.rs.preprocessor.app.cloud.jms.UserActionConsumer;
import pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular.model.internal.Resource;
import pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular.model.internal.SingleSession;
import pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular.model.internal.User;
import pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular.model.internal.UserAction;
import pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular.service.AbstractUserActionHandler;
import pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular.service.MessageConverter;
import pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular.service.session.SessionManagerHandler;
import pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular.service.statistics.StatisticsHandler;
import pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular.service.user.UserDataSupplier;
import pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular.service.user.UserRecognitionHandler;
import pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular.service.validator.ValidatorHandler;

import static org.mockito.Mockito.spy;

@TestConfiguration
public class UserActionTestConfiguration {

    @Bean
    @Primary
    public UserActionConsumer getConsumer() { return spy(new UserActionConsumerMock()); }

    @Bean
    @Primary
    public MessageConverter getMessageConverter() { return spy(new MessageConverterMock()); }

    @Bean
    @Primary
    public ValidatorHandler getValidationHandler() { return spy(new ValidatorHandlerMock()); }

    @Bean
    @Primary
    public UserDataSupplier getUserIdMappingHandler() { return spy(new UserDataSupplierMock()); }

    @Bean
    @Primary
    public UserRecognitionHandler getUserRecognitionHandler() { return spy(new UserRecognitionHandlerMock()); }

    @Bean
    @Primary
    public SessionManagerHandler getSessionManagerHandler() { return spy(new SessionManagerHandlerMock()); }

    @Bean
    @Primary
    public StatisticsHandler getStatisticsHandler() { return spy(new StatisticsHandlerMock()); }


    public static class UserActionConsumerMock extends UserActionConsumer {
        @Override
        public void subscribe(Subscriber subscriber) { }
    }

    public class MessageConverterMock implements MessageConverter {
        @Override
        public SingleSession messageToSingleSession(String userActionJson) { return null; }
    }

    public class ValidatorHandlerMock extends AbstractUserActionHandler implements ValidatorHandler {
        @Override
        protected SingleSession processUserAction(SingleSession userAction) { return userAction; }

        @Override
        public boolean isValid(SingleSession singleSession) { return true; }
    }

    public class UserDataSupplierMock extends AbstractUserActionHandler implements UserDataSupplier {
        @Override
        protected SingleSession processUserAction(SingleSession userAction) { return userAction; }

        @Override
        public void enrichUserProfile(SingleSession userAction) {  }
    }

    public class UserRecognitionHandlerMock extends AbstractUserActionHandler implements UserRecognitionHandler {
        @Override
        protected SingleSession processUserAction(SingleSession userAction) { return userAction; }

        @Override
        public void recognizeUser(SingleSession userAction) { }
    }

    public class SessionManagerHandlerMock extends AbstractUserActionHandler implements SessionManagerHandler {
        @Override
        protected SingleSession processUserAction(SingleSession userAction) { return userAction; }

        @Override
        public void addUserAction(User user, UserAction userAction) { }
    }

    public class StatisticsHandlerMock extends AbstractUserActionHandler implements StatisticsHandler {
        @Override
        protected SingleSession processUserAction(SingleSession userAction) { return null; }

        @Override
        public void receiveUserAction(String sessionId, String userId, Resource resource, Boolean isOrder) {

        }
    }


}
