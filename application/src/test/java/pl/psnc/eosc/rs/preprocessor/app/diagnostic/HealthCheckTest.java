package pl.psnc.eosc.rs.preprocessor.app.diagnostic;

import com.google.common.truth.Truth;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import pl.psnc.eosc.rs.preprocessor.app.buildinfo.Buildinfo;
import pl.psnc.eosc.rs.preprocessor.app.cloud.jms.JmsProducer;
import pl.psnc.eosc.rs.preprocessor.app.cloud.jms.MessageBroadcaster;
import pl.psnc.eosc.rs.preprocessor.app.cloud.jms.ProviderComponentMessageBroadcaster;
import pl.psnc.eosc.rs.preprocessor.app.context.user.action.generator.RandomUserProvider;

import static org.apache.http.HttpHeaders.CONTENT_TYPE;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;

@SpringBootTest
@ActiveProfiles({"test","jms-disabled","artificialUAConfig-disabled"})
@AutoConfigureMockMvc
class HealthCheckTest {

    @Autowired
    MockMvc mockMvc;

    @MockBean
    JmsProducer jmsProducer;

    @MockBean(name="userActionBroadcaster")
    MessageBroadcaster userActionBroadcaster;

    @MockBean(name="resourceEventBroadcaster")
    MessageBroadcaster resourceEventBroadcaster;

    @MockBean(name = "evaluationEventBroadcaster")
    MessageBroadcaster evaluationEventBroadcaster;

    @MockBean(name = "recommendationEventBroadcaster")
    MessageBroadcaster recommendationEventBroadcaster;

    @MockBean(name = "trainingEventBroadcaster")
    ProviderComponentMessageBroadcaster trainingMessageBroadcaster;

    @MockBean
    RandomUserProvider randomUserPicker;

    @MockBean
    Buildinfo buildinfo;

    @Test
    void actuatorTest() throws Exception {
        String commitInfo = "sample commit info";
        when(buildinfo.getCommitInfo()).thenReturn(commitInfo);
        MockHttpServletResponse response = mockMvc.perform(get("/actuator/health")).andReturn().getResponse();

        Truth.assertThat(response.getHeader(CONTENT_TYPE)).isEqualTo("application/vnd.spring-boot.actuator.v3+json");
        String responseBodyString = response.getContentAsString();
        Truth.assertThat(responseBodyString).contains("db");
        Truth.assertThat(responseBodyString).contains("diskSpace");
        Truth.assertThat(responseBodyString).contains("jms");
        Truth.assertThat(responseBodyString).contains("ping");
        Truth.assertThat(responseBodyString).contains(commitInfo);
    }
}
