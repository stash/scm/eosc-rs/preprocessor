package pl.psnc.eosc.rs.preprocessor.app.context.spark;

import org.apache.spark.launcher.SparkAppHandle;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import pl.psnc.eosc.rs.preprocessor.app.context.spark.processing.ProcessingBroker;
import pl.psnc.eosc.rs.preprocessor.app.context.spark.processing.command.Command;
import pl.psnc.eosc.rs.preprocessor.app.context.spark.processing.command.OagProcessingCommand;
import pl.psnc.eosc.rs.preprocessor.app.context.spark.processing.command.TrainingProcessingCommand;
import pl.psnc.eosc.rs.preprocessor.app.context.spark.processing.listener.SparkJobProcessingTrigger;
import pl.psnc.eosc.rs.preprocessor.app.context.spark.processing.listener.SparkJobStatusUpdateListener;

import java.util.List;
import java.util.Optional;

import static com.google.common.truth.Truth.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class SparkJobProcessingTriggerTest {

    @Mock
    SparkAppHandle sparkAppHandle;

    @Mock
    ProcessingBroker processingBroker;

    @Captor
    ArgumentCaptor<Command> commandCaptor;

    @Test
    public void processingTriggerTest() {
        Long commandId = 123L;
        String processingId = "1";
        String relativePath = "test/path";
        SparkAppHandle.State state = SparkAppHandle.State.FINISHED;
        when(sparkAppHandle.getState()).thenReturn(state);
        when(sparkAppHandle.getError()).thenReturn(Optional.empty());
        SparkJobProcessingTrigger listener = new SparkJobProcessingTrigger(processingBroker,commandId,relativePath,null);

        //when:
        listener.stateChanged(sparkAppHandle);

        //then:
        verify(processingBroker,times(7)).execute(commandCaptor.capture(),eq(false),eq(commandId));
        verify(processingBroker,times(4)).execute(isA(OagProcessingCommand.class),eq(false),eq(commandId));
        verify(processingBroker,times(1)).execute(isA(TrainingProcessingCommand.class),eq(false),eq(commandId));
        List<Command> commands = commandCaptor.getAllValues();

        assertThat(commands.get(0)).isInstanceOf(OagProcessingCommand.class);
        assertThat(((OagProcessingCommand)commands.get(0)).getInputFiles())
                .isEqualTo("preproc/input/"+relativePath+"/dataset/*");
        assertThat(((OagProcessingCommand)commands.get(0)).getOutputDir())
                .isEqualTo("preproc/output/"+relativePath+"/dataset/");

        assertThat(commands.get(1)).isInstanceOf(OagProcessingCommand.class);
        assertThat(((OagProcessingCommand)commands.get(1)).getInputFiles())
                .isEqualTo("preproc/input/"+relativePath+"/other_rp/*");
        assertThat(((OagProcessingCommand)commands.get(1)).getOutputDir())
                .isEqualTo("preproc/output/"+relativePath+"/other_rp/");

        assertThat(commands.get(2)).isInstanceOf(OagProcessingCommand.class);

        assertThat(commands.get(3)).isInstanceOf(OagProcessingCommand.class);

        assertThat(commands.get(4)).isInstanceOf(TrainingProcessingCommand.class);
        assertThat(((TrainingProcessingCommand)commands.get(4)).getInputFiles())
                .isEqualTo("preproc/input/"+relativePath+"/training/*");
        assertThat(((TrainingProcessingCommand)commands.get(4)).getOutputDir())
                .isEqualTo("preproc/output/"+relativePath+"/training/");
    }

    @Test
    public void processingTriggerTest_Error() {
        Long commandId = 123L;
        String processingId = "1";
        SparkAppHandle.State state = SparkAppHandle.State.FINISHED;
        when(sparkAppHandle.getState()).thenReturn(state);
        when(sparkAppHandle.getError()).thenReturn(Optional.of(new Throwable("test error")));
        SparkJobProcessingTrigger listener =
                new SparkJobProcessingTrigger(processingBroker,commandId,"/test/path",null);

        //when:
        listener.stateChanged(sparkAppHandle);

        //then:
        verify(processingBroker,times(0)).execute(any(),anyBoolean(),anyLong());
    }

    @ParameterizedTest
    @EnumSource(value = SparkAppHandle.State.class,
            names = {"FINISHED"},
            mode = EnumSource.Mode.EXCLUDE)
    public void processingTriggerTest_Error(SparkAppHandle.State state) {
        Long commandId = 123L;
        String processingId = "1";
        when(sparkAppHandle.getState()).thenReturn(state);
        SparkJobProcessingTrigger listener = new SparkJobProcessingTrigger(processingBroker,commandId,"/test/path",null);

        //when:
        listener.stateChanged(sparkAppHandle);

        //then:
        verify(processingBroker,times(0)).execute(any(),anyBoolean(),anyLong());
    }

    @Test
    public void sparkJobStatusUpdateListenerTest() {
        Long commandId = 123L;
        String processingId = "1";
        SparkAppHandle.State state = SparkAppHandle.State.FINISHED;
        when(sparkAppHandle.getState()).thenReturn(state);
        when(sparkAppHandle.getAppId()).thenReturn(processingId);
        SparkJobStatusUpdateListener listener = new SparkJobStatusUpdateListener(processingBroker,commandId);

        //when:
        listener.stateChanged(sparkAppHandle);
        //then:
        verify(processingBroker,times(1)).updateCommand(eq(commandId),eq(processingId),eq(state));
    }
}
