package pl.psnc.eosc.rs.preprocessor.app.context.evaluation;

import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;
import pl.psnc.eosc.rs.preprocessor.app.cloud.jms.JmsProducer;
import pl.psnc.eosc.rs.preprocessor.app.cloud.jms.MessageBroadcaster;
import pl.psnc.eosc.rs.preprocessor.app.cloud.jms.ProviderComponentMessageBroadcaster;
import pl.psnc.eosc.rs.preprocessor.app.context.evaluation.entity.EvaluationEntity;
import pl.psnc.eosc.rs.preprocessor.app.context.user.action.backup.BaseBackup;
import pl.psnc.eosc.rs.preprocessor.app.context.user.action.generator.RandomUserProvider;

import javax.validation.Validator;

import static com.google.common.truth.Truth.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@SpringBootTest
@ActiveProfiles({"test","jms-disabled","artificialUAConfig-disabled","backup-raw-jms"})
public class EvaluationSaverTest {
    @Qualifier("evaluationSaver")
    @Autowired
    BaseBackup baseBackup;

    @MockBean
    EvaluationRepository evaluationRepository;

    @Autowired
    Validator validator;

    @MockBean
    JmsProducer jmsProducer;

    @MockBean(name = "userActionBroadcaster")
    MessageBroadcaster userActionBroadcaster;

    @MockBean(name = "resourceEventBroadcaster")
    MessageBroadcaster resourceEventBroadcaster;

    @MockBean(name = "evaluationEventBroadcaster")
    MessageBroadcaster evaluationEventBroadcaster;

    @MockBean(name = "recommendationEventBroadcaster")
    MessageBroadcaster recommendationEventBroadcaster;

    @MockBean(name = "trainingEventBroadcaster")
    ProviderComponentMessageBroadcaster trainingMessageBroadcaster;

    @MockBean
    RandomUserProvider randomUserPicker;

    @Test
    public void validEvaluationEventBackupTest() {
        String evaluationEvent = org.apache.commons.lang3.StringUtils.repeat("x",8192);
        baseBackup.processMessage(evaluationEvent);

        ArgumentCaptor<EvaluationEntity> backupEntityCaptor = ArgumentCaptor.forClass(EvaluationEntity.class);
        verify(evaluationRepository, times(1)).save(backupEntityCaptor.capture());

        assertThat(backupEntityCaptor.getValue().getRawEvaluationEvent()).isEqualTo(evaluationEvent);
    }
}
