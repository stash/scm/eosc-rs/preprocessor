package pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular;

import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular.model.internal.Resource;
import pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular.model.internal.SingleSession;
import pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular.service.AbstractUserActionHandler;
import pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular.service.statistics.StatisticsHandler;
import pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular.service.statistics.StatisticsHandlerImpl;
import pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular.service.statistics.StatisticsStorage;
import pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular.service.statistics.StatisticsStorageImpl;

import java.util.UUID;

import static com.google.common.truth.Truth.assertThat;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular.SampleSessionProvider.getSampleSession;

@SpringBootTest(classes = {StatisticsHandlerImpl.class, StatisticsStorageImpl.class})
public class StatisticsReceiverTest {

    @Autowired
    StatisticsHandler statisticsHandler;

    @MockBean
    StatisticsStorage statisticsStorage;

    @Test
    public void receiveUserActions(){
        SingleSession userAction0 = getSampleSession();
        userAction0.getUserAction().setResourceId("10");
        userAction0.getUserAction().setResourceType("service");

        SingleSession userAction1 = getSampleSession();
        userAction1.getUserAction().setResourceId("11");
        userAction1.getUserAction().setResourceType("service");

        SingleSession userAction2_otherSession = getSampleSession();
        userAction2_otherSession.getUserAction().setResourceId("12");
        userAction2_otherSession.setSessionId(UUID.fromString("1507dabf-acf6-477b-89de-033f8f9d570e"));
        userAction2_otherSession.getUserAction().setResourceType("service");

        SingleSession userAction3_withUser = getSampleSession();
        userAction3_withUser.getUserAction().setResourceId("12");
        userAction3_withUser.setAaiUid("test-id0");
        userAction3_withUser.getUserAction().setResourceType("service");

        SingleSession userAction4_order = getSampleSession();
        userAction4_order.getUserAction().setResourceId("10");
        userAction4_order.getUserAction().setIsOrderAction(true);
        userAction4_order.getUserAction().setResourceType("service");

        SingleSession userAction5_otherUser_order = getSampleSession();
        userAction5_otherUser_order.getUserAction().setResourceId("10");
        userAction5_otherUser_order.getUserAction().setResourceType("service");
        userAction5_otherUser_order.getUserAction().setIsOrderAction(true);
        userAction5_otherUser_order.setAaiUid("test-id1");
        userAction5_otherUser_order.setSessionId(UUID.fromString("0dd4676d-0561-4405-b031-9e05e6cb5c07"));

        ((AbstractUserActionHandler)statisticsHandler).handleUserAction(userAction0);
        ((AbstractUserActionHandler)statisticsHandler).handleUserAction(userAction1);
        ((AbstractUserActionHandler)statisticsHandler).handleUserAction(userAction2_otherSession);
        ((AbstractUserActionHandler)statisticsHandler).handleUserAction(userAction3_withUser);
        ((AbstractUserActionHandler)statisticsHandler).handleUserAction(userAction4_order);
        ((AbstractUserActionHandler)statisticsHandler).handleUserAction(userAction5_otherUser_order);

        // Check Visits
        ArgumentCaptor<String> userVisitCaptor = ArgumentCaptor.forClass(String.class);
        ArgumentCaptor<Resource> serviceVisitCaptor = ArgumentCaptor.forClass(Resource.class);
        verify(statisticsStorage, times(6)).addVisit(userVisitCaptor.capture(),serviceVisitCaptor.capture());
        // Visit: 0
        assertThat(userVisitCaptor.getAllValues().get(0)).isEqualTo("0daf38fb-30aa-48c3-994a-bab70078a370");
        assertThat(serviceVisitCaptor.getAllValues().get(0).getResourceId()).isEqualTo("10");
        // Visit: 1
        assertThat(userVisitCaptor.getAllValues().get(1)).isEqualTo("0daf38fb-30aa-48c3-994a-bab70078a370");
        assertThat(serviceVisitCaptor.getAllValues().get(1).getResourceId()).isEqualTo("11");
        // Visit: 2
        assertThat(userVisitCaptor.getAllValues().get(2)).isEqualTo("1507dabf-acf6-477b-89de-033f8f9d570e");
        assertThat(serviceVisitCaptor.getAllValues().get(2).getResourceId()).isEqualTo("12");
        // Visit: 3
        assertThat(userVisitCaptor.getAllValues().get(3)).isEqualTo("test-id0");
        assertThat(serviceVisitCaptor.getAllValues().get(3).getResourceId()).isEqualTo("12");
        // Visit: 4
        assertThat(userVisitCaptor.getAllValues().get(4)).isEqualTo("0daf38fb-30aa-48c3-994a-bab70078a370");
        assertThat(serviceVisitCaptor.getAllValues().get(4).getResourceId()).isEqualTo("10");
        // Visit: 5
        assertThat(userVisitCaptor.getAllValues().get(5)).isEqualTo("test-id1");
        assertThat(serviceVisitCaptor.getAllValues().get(5).getResourceId()).isEqualTo("10");

        // Check Orders
        ArgumentCaptor<String> userOrderCaptor = ArgumentCaptor.forClass(String.class);
        ArgumentCaptor<Resource> serviceOrderCaptor = ArgumentCaptor.forClass(Resource.class);
        verify(statisticsStorage, times(2)).addOrder(userOrderCaptor.capture(),serviceOrderCaptor.capture());
        // Order: 0
        assertThat(userOrderCaptor.getAllValues().get(0)).isEqualTo("0daf38fb-30aa-48c3-994a-bab70078a370");
        assertThat(serviceOrderCaptor.getAllValues().get(0).getResourceId()).isEqualTo("10");
        // Visit: 1
        assertThat(userOrderCaptor.getAllValues().get(1)).isEqualTo("test-id1");
        assertThat(serviceOrderCaptor.getAllValues().get(1).getResourceId()).isEqualTo("10");

    }
}
