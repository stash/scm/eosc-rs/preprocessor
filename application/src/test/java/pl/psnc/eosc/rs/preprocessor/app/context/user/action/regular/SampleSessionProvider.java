package pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular;

import pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular.model.internal.Root;
import pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular.model.internal.SingleSession;
import pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular.model.internal.UserAction;

import java.time.LocalDateTime;
import java.util.UUID;

public class SampleSessionProvider {

    public static SingleSession getSampleSession() {
        Root root = new Root(
                "other",
                null,
                null
        );
        UserAction userAction = new UserAction(
                System.currentTimeMillis(),
                LocalDateTime.parse("2022-05-19T18:11:18.754"),
                "/services/amber-based-portal-server-for-nmr-structures-amps-nmr/details",
                "424",
                "service",
                "/services/amber-based-portal-server-for-nmr-structures-amps-nmr",
                null,
                "browser action",
                "Details",
                false,
                root
        );
        return new SingleSession(
                UUID.fromString("0daf38fb-30aa-48c3-994a-bab70078a370"),
                null,
                null,
                userAction
        );
    }

    public static SingleSession getSampleNullSession() {
        Root root = new Root(
                null,
                null,
                null
        );
        UserAction userAction = new UserAction(
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                root
        );
        return new SingleSession(
                null,
                null,
                null,
                userAction
        );
    }
}
