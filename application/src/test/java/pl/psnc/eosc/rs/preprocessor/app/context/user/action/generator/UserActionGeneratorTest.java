package pl.psnc.eosc.rs.preprocessor.app.context.user.action.generator;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import pl.psnc.eosc.rs.preprocessor.app.context.user.action.generator.model.GeneratedResource;
import pl.psnc.eosc.rs.preprocessor.app.context.user.action.generator.model.GeneratedUserSession;
import pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular.model.input.future.UserActionRaw;

import java.util.UUID;

import static com.google.common.truth.Truth.assertThat;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class UserActionGeneratorTest {

    @InjectMocks
    UserActionGeneratorImpl userActionGenerator;

    @Mock
    SessionProvider sessionProvider;

    @Mock
    ResourceProvider resourceProvider;

    @Test
    public void generateTest() {
        String userId = "testUser";
        String sessionId = "2c97f0a4-1604-4c7a-8783-6648c5a52f34";
        String resourceType = "service";
        String resourceId = "testResource";
        when(sessionProvider.getUserSession()).thenReturn(new GeneratedUserSession(userId,UUID.fromString(sessionId)));
        when(resourceProvider.getResource()).thenReturn(new GeneratedResource(resourceId,resourceType));

        UserActionRaw generatedUa = userActionGenerator.generate();

        assertThat(generatedUa).isNotNull();
        assertThat(generatedUa.getUniqueId().toString()).isEqualTo(sessionId);
        assertThat(generatedUa.getMarketplaceId()).isNull();
        assertThat(generatedUa.getAaiUid()).isEqualTo(userId);
        assertThat(generatedUa.getSource().getRoot().getResourceType()).isEqualTo(resourceType);
        assertThat(generatedUa.getSource().getRoot().getResourceId()).isEqualTo(resourceId);
    }

}
