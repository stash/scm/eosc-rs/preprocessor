package pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular;

import org.junit.jupiter.api.Test;
import pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular.model.internal.SingleSession;
import pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular.model.internal.UserAction;
import pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular.service.MessageConverter;
import pl.psnc.eosc.rs.preprocessor.app.context.user.action.regular.service.MessageConverterImpl;

import java.util.UUID;

import static com.google.common.truth.Truth.assertThat;

public class MessageConverterTest {

    MessageConverter messageConverter = new MessageConverterImpl();

    @Test
    public void convertTypeOther_userId() {
        String userId="1234";
        SingleSession singleSession = messageConverter.messageToSingleSession(sampleUserAction_TypeOther("user_id",userId));
        baseAssertions(singleSession);
        assertThat(singleSession.getMarketplaceId()).isEqualTo(userId);
        UserAction ua = singleSession.getUserAction();
        assertThat(ua.getRoot().getType()).isEqualTo("other");
        assertThat(ua.getRoot().getPanelId()).isNull();
        assertThat(ua.getRoot().getPosition()).isNull();
    }

    @Test
    public void convertTypeOther_aaiUid() {
        String userAaiUid="1234";
        SingleSession singleSession = messageConverter.messageToSingleSession(sampleUserAction_TypeOther("aai_uid","\""+userAaiUid+"\""));
        baseAssertions(singleSession);
        assertThat(singleSession.getAaiUid()).isEqualTo(userAaiUid);
        UserAction ua = singleSession.getUserAction();
        assertThat(ua.getRoot().getType()).isEqualTo("other");
        assertThat(ua.getRoot().getPanelId()).isNull();
        assertThat(ua.getRoot().getPosition()).isNull();
    }

    @Test
    public void convertTypeOther_recommendationPanel() {
        String userId="1234";
        SingleSession singleSession = messageConverter.messageToSingleSession(sampleUserAction_TypeRecommendationPanel());
        baseAssertions(singleSession);
        assertThat(singleSession.getMarketplaceId()).isEqualTo(userId);
        UserAction ua = singleSession.getUserAction();
        assertThat(ua.getRoot().getType()).isEqualTo("recommendation_panel");
        assertThat(ua.getRoot().getPanelId()).isEqualTo("v1");
        assertThat(ua.getRoot().getPosition()).isNull();
    }

    @Test
    public void convertTypeOther_sorted() {
        String userId="1234";
        SingleSession singleSession = messageConverter.messageToSingleSession(sampleUserAction_TypeSorted());
        baseAssertions(singleSession);
        assertThat(singleSession.getMarketplaceId()).isEqualTo(userId);
        UserAction ua = singleSession.getUserAction();
        assertThat(ua.getRoot().getType()).isEqualTo("sorted");
        assertThat(ua.getRoot().getPanelId()).isNull();
        assertThat(ua.getRoot().getPosition()).isEqualTo(3);
    }

    @Test
    public void convertCurrentUA() {
        SingleSession singleSession = messageConverter.messageToSingleSession(sampleCurrentUserAction());
        baseAssertions(singleSession);
        assertThat(singleSession.getMarketplaceId()).isNull();
        UserAction ua = singleSession.getUserAction();
        assertThat(ua.getRoot().getType()).isEqualTo("other");
        assertThat(ua.getRoot().getPanelId()).isNull();
        assertThat(ua.getRoot().getPosition()).isNull();
    }

    private void baseAssertions(SingleSession singleSession) {
        assertThat(singleSession.getSessionId()).isEqualTo(UUID.fromString("87187804-d134-4e3c-b052-5ef1e2e9666c"));
        UserAction ua = singleSession.getUserAction();
        assertThat(ua.getReceiptTimestamp()).isNotNull();
        assertThat(ua.getTimestamp()).isNotNull();
        assertThat(ua.getClientId()).isEqualTo("marketplace");
        assertThat(ua.getSourcePageId()).isEqualTo("/services/materials-cloud-archive");
        assertThat(ua.getTargetPageId()).isEqualTo("/services/materials-cloud-archive/information");
        assertThat(ua.getResourceType()).isEqualTo("service");
        assertThat(ua.getResourceId()).isEqualTo("483");
        assertThat(ua.getActionType()).isEqualTo("browser action");
        assertThat(ua.getActionText()).isEqualTo("search");
        assertThat(ua.getIsOrderAction()).isFalse();
    }

    private String sampleUserAction_TypeOther(String fieldName,String fieldValue){
        return "{\"timestamp\":\"2022-10-08T09:32:47.788Z\"," +
                "\"source\":{" +
                "\"visit_id\":\"171016f0-46ec-11ed-a060-5bad0ddf1b0a\"," +
                "\"page_id\":\"/services/materials-cloud-archive\"," +
                "\"root\":{" +
                "\"type\":\"other\"," +
                "\"resource_id\":\"483\"," +
                "\"resource_type\":\"service\"}}," +
                "\"target\":{" +
                "\"visit_id\":\"2b777a70-46ec-11ed-b2ad-43a079bd3717\"," +
                "\"page_id\":\"/services/materials-cloud-archive/information\"}," +
                "\"action\":{" +
                "\"type\":\"browser action\"," +
                "\"text\":\"search\"," +
                "\"order\":false}," +
                "\"client_id\":\"marketplace\"," +
                "\""+fieldName+"\":"+fieldValue+"," +
                "\"unique_id\":\"87187804-d134-4e3c-b052-5ef1e2e9666c\"}";
    }

    private String sampleUserAction_TypeRecommendationPanel(){
        return "{\"timestamp\":\"2022-10-08T09:32:47.788Z\"," +
                "\"source\":{" +
                "\"visit_id\":\"171016f0-46ec-11ed-a060-5bad0ddf1b0a\"," +
                "\"page_id\":\"/services/materials-cloud-archive\"," +
                "\"root\":{" +
                "\"type\": \"recommendation_panel\",\n" +
                "\"panel_id\": \"v1\",\n" +
                "\"resource_id\":\"483\"," +
                "\"resource_type\":\"service\"}}," +
                "\"target\":{" +
                "\"visit_id\":\"2b777a70-46ec-11ed-b2ad-43a079bd3717\"," +
                "\"page_id\":\"/services/materials-cloud-archive/information\"}," +
                "\"action\":{" +
                "\"type\":\"browser action\"," +
                "\"text\":\"search\"," +
                "\"order\":false}," +
                "\"client_id\":\"marketplace\"," +
                "\"user_id\":1234," +
                "\"unique_id\":\"87187804-d134-4e3c-b052-5ef1e2e9666c\"}";
    }

    private String sampleUserAction_TypeSorted(){
        return "{\"timestamp\":\"2022-10-08T09:32:47.788Z\"," +
                "\"source\":{" +
                "\"visit_id\":\"171016f0-46ec-11ed-a060-5bad0ddf1b0a\"," +
                "\"page_id\":\"/services/materials-cloud-archive\"," +
                "\"root\":{" +
                "\"type\": \"sorted\",\n" +
                "\"position\": 3," +
                "\"resource_id\":\"483\"," +
                "\"resource_type\":\"service\"}}," +
                "\"target\":{" +
                "\"visit_id\":\"2b777a70-46ec-11ed-b2ad-43a079bd3717\"," +
                "\"page_id\":\"/services/materials-cloud-archive/information\"}," +
                "\"action\":{" +
                "\"type\":\"browser action\"," +
                "\"text\":\"search\"," +
                "\"order\":false}," +
                "\"client_id\":\"marketplace\"," +
                "\"user_id\":1234," +
                "\"unique_id\":\"87187804-d134-4e3c-b052-5ef1e2e9666c\"}";
    }

    private String sampleCurrentUserAction(){
        return "{\"timestamp\":\"2022-10-09T22:07:52.327Z\"," +
                "\"source\":{" +
                "\"visit_id\":\"171016f0-46ec-11ed-a060-5bad0ddf1b0a\"," +
                "\"page_id\":\"/services/materials-cloud-archive\"," +
                "\"root\":{\"type\":\"other\"}}," +
                "\"target\":{" +
                "\"visit_id\":\"2b777a70-46ec-11ed-b2ad-43a079bd3717\"," +
                "\"page_id\":\"/services/materials-cloud-archive/information\"}," +
                "\"action\":{" +
                "\"type\":\"browser action\"," +
                "\"text\":\"search\"," +
                "\"order\":false}," +
                "\"client_id\":\"marketplace\"," +
                "\"unique_id\":\"87187804-d134-4e3c-b052-5ef1e2e9666c\"}";
    }
}
