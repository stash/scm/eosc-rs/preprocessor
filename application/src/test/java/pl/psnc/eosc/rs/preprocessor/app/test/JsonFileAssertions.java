package pl.psnc.eosc.rs.preprocessor.app.test;

import java.nio.file.Path;
import java.nio.file.Paths;

public class JsonFileAssertions {

    static private Path RECOMMENDATION = Paths.get("/assertions/recommendation");
    static public Path ONE_RESOURCE_TYPE_RECOMMENDATION = RECOMMENDATION.resolve("one_resource_type.json");
    static public Path ALL_RESOURCE_TYPE_RECOMMENDATION = RECOMMENDATION.resolve("all_resource_type.json");
    static public Path MARKETPLACE_RECOMMENDATION = RECOMMENDATION.resolve("marketplace.json");
    static private Path EVALUATION = Paths.get("/assertions/evaluation");
    static public Path EVALUATION_SAMPLE_INPUT = EVALUATION.resolve("sample_input.json");
    static public Path EVALUATION_SAMPLE_OUTPUT = EVALUATION.resolve("sample_output.json");

}
