package pl.psnc.eosc.rs.preprocessor.job.training;

import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.types.DataTypes;
import org.apache.spark.sql.types.StructType;

import static org.apache.spark.sql.functions.*;
import static org.apache.spark.sql.functions.col;

public class TrainingsDataFormat {
    static protected final StructType INPUT_SCHEMA = new StructType()
            .add("id", DataTypes.StringType)
            .add("title",DataTypes.StringType)
            .add("description", DataTypes.createArrayType(DataTypes.StringType))
            .add("author_names", DataTypes.createArrayType(DataTypes.StringType))
            .add("language",DataTypes.createArrayType(DataTypes.StringType))
            .add("keywords",DataTypes.createArrayType(DataTypes.StringType))
            .add("license",DataTypes.StringType)
            .add("best_access_right",DataTypes.StringType)
            .add("resource_type",DataTypes.createArrayType(DataTypes.StringType))
            .add("content_type",DataTypes.createArrayType(DataTypes.StringType))
            .add("eosc_provider",DataTypes.createArrayType(DataTypes.StringType))
            .add("url",DataTypes.createArrayType(DataTypes.StringType))
            .add("format",DataTypes.StringType)
            .add("level_of_expertise",DataTypes.StringType)
            .add("target_group",DataTypes.createArrayType(DataTypes.StringType))
            .add("qualification",DataTypes.createArrayType(DataTypes.StringType))
            .add("duration",DataTypes.StringType)
            .add("usage_counts_downloads",DataTypes.StringType)
            .add("usage_counts_views",DataTypes.StringType);

    static protected Dataset<Row> preSelector(Dataset<Row> ds) {
        return ds.select(col("id"),
                array(col("title")).as("mainTitles"),
                col("description").as("descriptions"),
                col("keywords").as("keywords"),
                col("best_access_right").as("bestAccessRight"),
                col("language").as("languages"),
                col("author_names").as("authors"),
                col("resource_type").as("resourceTypes"),
                col("content_type").as("contentTypes"),
                col("level_of_expertise").as("levelOfExpertise"),
                col("target_group").as("targetGroups"),
                col("usage_counts_downloads").as("downloadsCount"),
                col("usage_counts_views").as("viewsCount"));
    }

    static protected Dataset<Row> postSelector(Dataset<Row> ds) {
        return ds.select(
                col("id").as("id"),
                array_join(transform(col("mainTitles"), col -> array_join(col," ")),";").as("mainTitles"),
                array_join(transform(col("keywords"), col -> array_join(col," ")),";").as("keywords"),
                array_join(array_distinct(flatten(col("keywords")))," ").as("keywordsMerged"),
                array_join(transform(col("descriptions"), col -> array_join(col," ")),";").as("descriptions"),
                array_join(col("authors"),";").as("authors"),
                array_join(col("languages"),";").as("languages"),
                col("bestAccessRight").as("bestAccessRight"),
                array_join(col("resourceTypes"),";").as("resourceTypes"),
                array_join(col("contentTypes"),";").as("contentTypes"),
                col("levelOfExpertise").as("levelOfExpertise"),
                array_join(col("targetGroups"),";").as("targetGroups"),
                col("downloadsCount").as("orderCounter"),
                col("viewsCount").as("visitCounter"));
//                array_join(col("languagePrediction"), ";").as("languagePrediction"));
    }
}
