package pl.psnc.eosc.rs.preprocessor.job.training;


import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.storage.StorageLevel;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import pl.psnc.eosc.rs.preprocessor.spark.SparkStandaloneSessionSingleton;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.stream.Stream;

import static com.google.common.truth.Truth.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.params.provider.Arguments.arguments;

@Disabled
class TrainingsMappingTest {

    private static SparkSession sparkSession;

    private static Stream<Arguments> fileNameSource() throws IOException {
        Path dirPath = Path.of("target/test-classes/testdata/input/training");
        return Files.list(dirPath).map(filePath -> arguments(dirPath.toString(), filePath.getFileName().toString()));
    }

    @BeforeAll
    public static void initializeSparkSession() {
        sparkSession = SparkStandaloneSessionSingleton.getSession();
    }

    @AfterAll
    public static void closeSparkSession() {
        sparkSession.close();
    }

    @Disabled
    @DisplayName("Parameterized test for files in testdata/input")
    @ParameterizedTest(name="file = {1}")
    @MethodSource("fileNameSource")
    void processTrainings(String path, String file) {
            //Read input files
            Dataset<Row> inputData = TrainingsPreprocSparkJob.readJsonData(path+"/"+file);

            //Process data
            Dataset<Row> processedData = TrainingsPreprocSparkJob.processData(inputData);

            //Assertions
            processedData.persist(StorageLevel.MEMORY_ONLY());
            assertThat(processedData.count()).isGreaterThan(0);

            for(String row : processedData.toJSON().collectAsList()) {
                JsonObject jsonData = JsonParser.parseString(row).getAsJsonObject();

                assertThat(jsonData.get("id").getAsString()).isNotEmpty();
                assertThat(jsonData.get("mainTitles").getAsJsonArray().get(0).getAsJsonArray().isEmpty()).isFalse();
                assertThat(jsonData.get("mainTitles").getAsJsonArray().get(0).getAsJsonArray().get(0).getAsString()).isNotEmpty();
                assertThat(jsonData.get("descriptions").getAsJsonArray().get(0).getAsJsonArray().isEmpty()).isFalse();
                assertThat(jsonData.get("descriptions").getAsJsonArray().get(0).getAsJsonArray().get(0).getAsString()).isNotEmpty();
                assertThat(jsonData.get("keywords").getAsJsonArray().get(0).getAsJsonArray().isEmpty()).isFalse();
                assertThat(jsonData.get("keywords").getAsJsonArray().get(0).getAsJsonArray().get(0).getAsString()).isNotEmpty();
            }
    }

    @Disabled
    @DisplayName("Parameterized test for files in testdata/input - all fields are not empty")
    @ParameterizedTest(name="file = {1}")
    @MethodSource("fileNameSource")
    void processTrainings_NotEmptyFields(String path, String file) {
            //Read input files
            Dataset<Row> inputData = TrainingsPreprocSparkJob.readJsonData(path+"/"+file);

            //Process data
            Dataset<Row> processedData = TrainingsPreprocSparkJob.processData(inputData);

            //Assertions
            processedData.persist(StorageLevel.MEMORY_ONLY());
            assertEquals(5, processedData.count());

            for(String row : processedData.toJSON().collectAsList()) {
                JsonObject jsonData = JsonParser.parseString(row).getAsJsonObject();

                assertThat(jsonData.keySet()).containsAtLeastElementsIn(ExpectedCsvHeaders.HEADERS);
                assertThat(jsonData.get("id").getAsString()).isNotEmpty();
//                assertThat(jsonData.get("mainTitles").getAsJsonArray().get(0).getAsJsonArray().isEmpty()).isFalse();
                assertThat(jsonData.get("mainTitles").getAsString()).isNotEmpty();
//                assertThat(jsonData.get("descriptions").getAsJsonArray().get(0).getAsJsonArray().isEmpty()).isFalse();
                assertThat(jsonData.get("descriptions").getAsString()).isNotEmpty();
//                assertThat(jsonData.get("keywords").getAsJsonArray().get(0).getAsJsonArray().isEmpty()).isFalse();
                assertThat(jsonData.get("keywords").getAsString()).isNotEmpty();
                assertThat(jsonData.get("bestAccessRight").getAsString()).isNotEmpty();
                assertThat(jsonData.get("languages").getAsString()).isNotEmpty();
                assertThat(jsonData.get("authors").getAsString()).isNotEmpty();
                assertThat(jsonData.get("resourceTypes").getAsString()).isNotEmpty();
                assertThat(jsonData.get("contentTypes").getAsString()).isNotEmpty();
                assertThat(jsonData.get("levelOfExpertise").getAsString()).isNotEmpty();
                assertThat(jsonData.get("usageCountsDownloads").getAsString()).isNotEmpty();
                assertThat(jsonData.get("usageCountsViews").getAsString()).isNotEmpty();
            }


    }
}