package pl.psnc.eosc.rs.preprocessor.job.training;

import com.google.common.truth.Truth;
import org.junit.jupiter.api.Disabled;
import pl.psnc.eosc.rs.preprocessor.testing.csv.CsvParserFactory;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.apache.spark.sql.SparkSession;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.io.TempDir;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import pl.psnc.eosc.rs.preprocessor.spark.SparkStandaloneSessionSingleton;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.stream.Stream;

import static org.junit.jupiter.params.provider.Arguments.arguments;

@Disabled
public class TrainingsCsvParsingTest {

    private static SparkSession sparkSession;

    private static Stream<Arguments> fileNameSource() throws IOException {
        Path dirPath = Path.of("target/test-classes/testdata/input/training");
        return Files.list(dirPath).map(filePath -> arguments(filePath, filePath.getFileName().toString()));
    }

    @BeforeAll
    public static void initializeSparkSession() {
        sparkSession = SparkStandaloneSessionSingleton.getSession();
    }

    @AfterAll
    public static void closeSparkSession() {
        sparkSession.close();
    }

    @Disabled //TODO adjust to new format
    @DisplayName("Parameterized test for files in testdata/input/training")
    @ParameterizedTest(name="file = {1}")
    @MethodSource("fileNameSource")
    public void shouldParseAnExampleCsvFile(Path inputFilePath, String fileName, @TempDir Path tempDir) throws IOException {

        // when
        TrainingsPreprocSparkJob.processTrainings(
                null,
                inputFilePath.toString(),
                tempDir.toAbsolutePath().toString());
        final CSVParser parsed = CsvParserFactory.createCsvParser(tempDir);

        // then
        Truth.assertThat(parsed.getRecords().size()).isEqualTo(5);
        Truth.assertThat(parsed.getHeaderNames()).containsExactly(ExpectedCsvHeaders.HEADERS);
        for (String h : ExpectedCsvHeaders.HEADERS) {
            for (CSVRecord record : parsed.getRecords()) {
                Truth.assertThat(record.isMapped(h)).isTrue();
                Truth.assertThat(record.get(h)).isNotEmpty();
            }
        }
    }
}
