package pl.psnc.eosc.rs.preprocessor.job.training;


import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.storage.StorageLevel;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;
import pl.psnc.eosc.rs.preprocessor.spark.SparkStandaloneSessionSingleton;

import java.nio.file.Path;
import java.util.Set;
import java.util.stream.Collectors;

import static org.apache.spark.sql.functions.*;
import static org.junit.jupiter.api.Assertions.*;

@Disabled //TODO
class TrainingsTransformationTest {

    static SparkSession sparkSession;

    @BeforeAll
    public static void createStandaloneSparkSession(){
        sparkSession = SparkStandaloneSessionSingleton.getSession();
    }

    @AfterAll
    public static void closeSparkSession() { sparkSession.close(); }

    @Test
    void processTrainings(@TempDir Path outputDir) {

        //Read input files
        Path filePath = Path.of("target/test-classes/testdata/transformation/trainings.json").toAbsolutePath();
        Dataset<Row> inputData = TrainingsPreprocSparkJob.readJsonData(filePath.toString());

        //Process data
        Dataset<Row> processedData = TrainingsPreprocSparkJob.processData(inputData);

            //Assertions
            processedData.persist(StorageLevel.MEMORY_ONLY());
            assertEquals(2, processedData.count());

        Set<String> mainTitleTokens = processedData.select(explode(flatten(col("mainTitles")))).collectAsList().stream().map(row -> row.getString(0)).collect(Collectors.toSet());
        assertEquals(Set.of("<EMAIL>", "word1:", "word2", "word5:", "word6", "<URL>", "123"), mainTitleTokens);

        Set<String> descriptionTokens = processedData.select(explode(flatten(col("descriptions")))).collectAsList().stream().map(row -> row.getString(0)).collect(Collectors.toSet());
        assertEquals(Set.of("word3", "word4(", "word7:", "word8", "123"), descriptionTokens);

        Set<String> keywords = processedData.select(explode(flatten(col("keywords")))).collectAsList().stream().map(row -> row.getString(0)).collect(Collectors.toSet());
        assertEquals(Set.of("keyword1", "keyword2","keyword3", "keyword4"), keywords);


        // checking if the file is saved without exceptions
//        TrainingsPreprocSparkJob.saveData(outputDir.resolve("out.csv").toString(), processedData);



        }
}
