package pl.psnc.eosc.rs.preprocessor.job.training;

public class ExpectedCsvHeaders {
    public static final String BESTACCESSRIGHT = "bestAccessRight";
    public static final String USAGE_COUNTS_DOWNLOADS = "usageCountsDownloads";
    public static final String USAGE_COUNTS_VIEWS = "usageCountsViews";
    static public String[] HEADERS = new String[]{
            "id",
            "mainTitles",
            "descriptions",
            "keywords",
            "keywordsMerged",
            BESTACCESSRIGHT,
            "languages",
            "authors",
            "resourceTypes",
            "contentTypes",
            "levelOfExpertise",
            "targetGroups",
            USAGE_COUNTS_DOWNLOADS,
            USAGE_COUNTS_VIEWS,
            "languagePrediction"
    };
}
