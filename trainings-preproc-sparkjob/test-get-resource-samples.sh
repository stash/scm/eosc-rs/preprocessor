#!/bin/bash

CURRENT_DIR=$(readlink -f .)
DESTINATION_DIR=${CURRENT_DIR}/src/test/resources/testdata/input
NUMBER_OF_RECORDS=10

( cd ../operations && ./dump-get-sample.sh 2023-02-27 training ${DESTINATION_DIR} ${NUMBER_OF_RECORDS} )