# V1Api

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**dumpsGetV1DumpsGet**](V1Api.md#dumpsGetV1DumpsGet) | **GET** /v1/dumps | Returns available dumps



## dumpsGetV1DumpsGet

> DumpResults dumpsGetV1DumpsGet(cursor, rows)

Returns available dumps

Returns available dumps

### Example

```java
// Import classes:
import pl.psnc.eosc.rs.preprocessor.common.pitocr.invoker.ApiClient;
import pl.psnc.eosc.rs.preprocessor.common.pitocr.invoker.ApiException;
import pl.psnc.eosc.rs.preprocessor.common.pitocr.invoker.Configuration;
import pl.psnc.eosc.rs.preprocessor.common.pitocr.invoker.auth.*;
import pl.psnc.eosc.rs.preprocessor.common.pitocr.invoker.models.*;
import pl.psnc.eosc.rs.preprocessor.common.pitocr.api.V1Api;

public class Example {
    public static void main(String[] args) {
        ApiClient defaultClient = Configuration.getDefaultApiClient();
        defaultClient.setBasePath("http://localhost");
        
        // Configure HTTP bearer authorization: HTTPBearer
        HttpBearerAuth HTTPBearer = (HttpBearerAuth) defaultClient.getAuthentication("HTTPBearer");
        HTTPBearer.setBearerToken("BEARER TOKEN");

        V1Api apiInstance = new V1Api(defaultClient);
        String cursor = "cursor_example"; // String | 
        Integer rows = 56; // Integer | 
        try {
            DumpResults result = apiInstance.dumpsGetV1DumpsGet(cursor, rows);
            System.out.println(result);
        } catch (ApiException e) {
            System.err.println("Exception when calling V1Api#dumpsGetV1DumpsGet");
            System.err.println("Status code: " + e.getCode());
            System.err.println("Reason: " + e.getResponseBody());
            System.err.println("Response headers: " + e.getResponseHeaders());
            e.printStackTrace();
        }
    }
}
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cursor** | **String**|  | [optional]
 **rows** | **Integer**|  | [optional]

### Return type

[**DumpResults**](DumpResults.md)

### Authorization

[HTTPBearer](../README.md#HTTPBearer)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **200** | OK |  -  |
| **400** | Bad request |  -  |
| **401** | Unauthorized |  -  |
| **403** | Forbidden |  -  |
| **422** | Validation Error |  -  |

