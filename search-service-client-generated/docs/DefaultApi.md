# DefaultApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**recommendPostInternalRecommendGet**](DefaultApi.md#recommendPostInternalRecommendGet) | **GET** /internal/recommend | Recommend Post
[**searchGetInternalSearchGet**](DefaultApi.md#searchGetInternalSearchGet) | **GET** /internal/search | Search Get



## recommendPostInternalRecommendGet

> Object recommendPostInternalRecommendGet(q, collection, rsPrefix)

Recommend Post

Do a search against the specified collection, pass results to RS

### Example

```java
// Import classes:
import pl.psnc.eosc.rs.preprocessor.common.pitocr.invoker.ApiClient;
import pl.psnc.eosc.rs.preprocessor.common.pitocr.invoker.ApiException;
import pl.psnc.eosc.rs.preprocessor.common.pitocr.invoker.Configuration;
import pl.psnc.eosc.rs.preprocessor.common.pitocr.invoker.models.*;
import pl.psnc.eosc.rs.preprocessor.common.pitocr.api.DefaultApi;

public class Example {
    public static void main(String[] args) {
        ApiClient defaultClient = Configuration.getDefaultApiClient();
        defaultClient.setBasePath("http://localhost");

        DefaultApi apiInstance = new DefaultApi(defaultClient);
        String q = "q_example"; // String | Query string
        String collection = "collection_example"; // String | Collection
        String rsPrefix = "http://localhost:9080/"; // String | RS instance prefix
        try {
            Object result = apiInstance.recommendPostInternalRecommendGet(q, collection, rsPrefix);
            System.out.println(result);
        } catch (ApiException e) {
            System.err.println("Exception when calling DefaultApi#recommendPostInternalRecommendGet");
            System.err.println("Status code: " + e.getCode());
            System.err.println("Reason: " + e.getResponseBody());
            System.err.println("Response headers: " + e.getResponseHeaders());
            e.printStackTrace();
        }
    }
}
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **q** | **String**| Query string |
 **collection** | **String**| Collection |
 **rsPrefix** | **String**| RS instance prefix |

### Return type

**Object**

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **200** | Successful Response |  -  |
| **422** | Validation Error |  -  |


## searchGetInternalSearchGet

> Object searchGetInternalSearchGet(q, collection)

Search Get

Do a search against the specified collection

### Example

```java
// Import classes:
import pl.psnc.eosc.rs.preprocessor.common.pitocr.invoker.ApiClient;
import pl.psnc.eosc.rs.preprocessor.common.pitocr.invoker.ApiException;
import pl.psnc.eosc.rs.preprocessor.common.pitocr.invoker.Configuration;
import pl.psnc.eosc.rs.preprocessor.common.pitocr.invoker.models.*;
import pl.psnc.eosc.rs.preprocessor.common.pitocr.api.DefaultApi;

public class Example {
    public static void main(String[] args) {
        ApiClient defaultClient = Configuration.getDefaultApiClient();
        defaultClient.setBasePath("http://localhost");

        DefaultApi apiInstance = new DefaultApi(defaultClient);
        String q = "q_example"; // String | Query string
        String collection = "collection_example"; // String | Collection
        try {
            Object result = apiInstance.searchGetInternalSearchGet(q, collection);
            System.out.println(result);
        } catch (ApiException e) {
            System.err.println("Exception when calling DefaultApi#searchGetInternalSearchGet");
            System.err.println("Status code: " + e.getCode());
            System.err.println("Reason: " + e.getResponseBody());
            System.err.println("Response headers: " + e.getResponseHeaders());
            e.printStackTrace();
        }
    }
}
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **q** | **String**| Query string |
 **collection** | **String**| Collection |

### Return type

**Object**

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **200** | Successful Response |  -  |
| **422** | Validation Error |  -  |

