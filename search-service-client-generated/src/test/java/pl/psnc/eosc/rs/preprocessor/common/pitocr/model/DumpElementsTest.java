/*
 * Search Service
 * EOSC Search Service
 *
 * The version of the OpenAPI document: 1.0.0-alpha1
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */


package pl.psnc.eosc.rs.preprocessor.common.pitocr.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonTypeName;
import com.fasterxml.jackson.annotation.JsonValue;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;


/**
 * Model tests for DumpElements
 */
public class DumpElementsTest {
    private final DumpElements model = new DumpElements();

    /**
     * Model tests for DumpElements
     */
    @Test
    public void testDumpElements() {
        // TODO: test DumpElements
    }

    /**
     * Test the property 'name'
     */
    @Test
    public void nameTest() {
        // TODO: test name
    }

    /**
     * Test the property 'referenceType'
     */
    @Test
    public void referenceTypeTest() {
        // TODO: test referenceType
    }

    /**
     * Test the property 'reference'
     */
    @Test
    public void referenceTest() {
        // TODO: test reference
    }

}
