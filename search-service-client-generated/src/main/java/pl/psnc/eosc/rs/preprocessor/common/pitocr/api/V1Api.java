package pl.psnc.eosc.rs.preprocessor.common.pitocr.api;

import pl.psnc.eosc.rs.preprocessor.common.pitocr.invoker.ApiClient;

import pl.psnc.eosc.rs.preprocessor.common.pitocr.model.BadRequest;
import pl.psnc.eosc.rs.preprocessor.common.pitocr.model.DumpResults;
import pl.psnc.eosc.rs.preprocessor.common.pitocr.model.Forbidden;
import pl.psnc.eosc.rs.preprocessor.common.pitocr.model.HTTPValidationError;
import pl.psnc.eosc.rs.preprocessor.common.pitocr.model.Unauthorized;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;

@javax.annotation.Generated(value = "org.openapitools.codegen.languages.JavaClientCodegen", date = "2022-01-30T13:54:59.900940+01:00[Europe/Berlin]")
@Component("pl.psnc.eosc.rs.preprocessor.common.pitocr.api.V1Api")
public class V1Api {
    private ApiClient apiClient;

    public V1Api() {
        this(new ApiClient());
    }

    @Autowired
    public V1Api(ApiClient apiClient) {
        this.apiClient = apiClient;
    }

    public ApiClient getApiClient() {
        return apiClient;
    }

    public void setApiClient(ApiClient apiClient) {
        this.apiClient = apiClient;
    }

    /**
     * Returns available dumps
     * Returns available dumps
     * <p><b>200</b> - OK
     * <p><b>400</b> - Bad request
     * <p><b>401</b> - Unauthorized
     * <p><b>403</b> - Forbidden
     * <p><b>422</b> - Validation Error
     * @param cursor  (optional)
     * @param rows  (optional)
     * @return DumpResults
     * @throws RestClientException if an error occurs while attempting to invoke the API
     */
    public DumpResults dumpsGetV1DumpsGet(String cursor, Integer rows) throws RestClientException {
        return dumpsGetV1DumpsGetWithHttpInfo(cursor, rows).getBody();
    }

    /**
     * Returns available dumps
     * Returns available dumps
     * <p><b>200</b> - OK
     * <p><b>400</b> - Bad request
     * <p><b>401</b> - Unauthorized
     * <p><b>403</b> - Forbidden
     * <p><b>422</b> - Validation Error
     * @param cursor  (optional)
     * @param rows  (optional)
     * @return ResponseEntity&lt;DumpResults&gt;
     * @throws RestClientException if an error occurs while attempting to invoke the API
     */
    public ResponseEntity<DumpResults> dumpsGetV1DumpsGetWithHttpInfo(String cursor, Integer rows) throws RestClientException {
        Object postBody = null;
        
        String path = apiClient.expandPath("/v1/dumps", Collections.<String, Object>emptyMap());

        final MultiValueMap<String, String> queryParams = new LinkedMultiValueMap<String, String>();
        final HttpHeaders headerParams = new HttpHeaders();
        final MultiValueMap<String, String> cookieParams = new LinkedMultiValueMap<String, String>();
        final MultiValueMap<String, Object> formParams = new LinkedMultiValueMap<String, Object>();

        queryParams.putAll(apiClient.parameterToMultiValueMap(null, "cursor", cursor));
        queryParams.putAll(apiClient.parameterToMultiValueMap(null, "rows", rows));

        final String[] localVarAccepts = { 
            "application/json"
         };
        final List<MediaType> localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);
        final String[] contentTypes = {  };
        final MediaType contentType = apiClient.selectHeaderContentType(contentTypes);

        String[] authNames = new String[] { "HTTPBearer" };

        ParameterizedTypeReference<DumpResults> returnType = new ParameterizedTypeReference<DumpResults>() {};
        return apiClient.invokeAPI(path, HttpMethod.GET, queryParams, postBody, headerParams, cookieParams, formParams, localVarAccept, contentType, authNames, returnType);
    }
}
