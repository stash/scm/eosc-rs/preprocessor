package pl.psnc.eosc.rs.preprocessor.common.pitocr.api;

import pl.psnc.eosc.rs.preprocessor.common.pitocr.invoker.ApiClient;

import pl.psnc.eosc.rs.preprocessor.common.pitocr.model.HTTPValidationError;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;

@javax.annotation.Generated(value = "org.openapitools.codegen.languages.JavaClientCodegen", date = "2022-01-30T13:54:59.900940+01:00[Europe/Berlin]")
@Component("pl.psnc.eosc.rs.preprocessor.common.pitocr.api.DefaultApi")
public class DefaultApi {
    private ApiClient apiClient;

    public DefaultApi() {
        this(new ApiClient());
    }

    @Autowired
    public DefaultApi(ApiClient apiClient) {
        this.apiClient = apiClient;
    }

    public ApiClient getApiClient() {
        return apiClient;
    }

    public void setApiClient(ApiClient apiClient) {
        this.apiClient = apiClient;
    }

    /**
     * Recommend Post
     * Do a search against the specified collection, pass results to RS
     * <p><b>200</b> - Successful Response
     * <p><b>422</b> - Validation Error
     * @param q Query string (required)
     * @param collection Collection (required)
     * @param rsPrefix RS instance prefix (required)
     * @return Object
     * @throws RestClientException if an error occurs while attempting to invoke the API
     */
    public Object recommendPostInternalRecommendGet(String q, String collection, String rsPrefix) throws RestClientException {
        return recommendPostInternalRecommendGetWithHttpInfo(q, collection, rsPrefix).getBody();
    }

    /**
     * Recommend Post
     * Do a search against the specified collection, pass results to RS
     * <p><b>200</b> - Successful Response
     * <p><b>422</b> - Validation Error
     * @param q Query string (required)
     * @param collection Collection (required)
     * @param rsPrefix RS instance prefix (required)
     * @return ResponseEntity&lt;Object&gt;
     * @throws RestClientException if an error occurs while attempting to invoke the API
     */
    public ResponseEntity<Object> recommendPostInternalRecommendGetWithHttpInfo(String q, String collection, String rsPrefix) throws RestClientException {
        Object postBody = null;
        
        // verify the required parameter 'q' is set
        if (q == null) {
            throw new HttpClientErrorException(HttpStatus.BAD_REQUEST, "Missing the required parameter 'q' when calling recommendPostInternalRecommendGet");
        }
        
        // verify the required parameter 'collection' is set
        if (collection == null) {
            throw new HttpClientErrorException(HttpStatus.BAD_REQUEST, "Missing the required parameter 'collection' when calling recommendPostInternalRecommendGet");
        }
        
        // verify the required parameter 'rsPrefix' is set
        if (rsPrefix == null) {
            throw new HttpClientErrorException(HttpStatus.BAD_REQUEST, "Missing the required parameter 'rsPrefix' when calling recommendPostInternalRecommendGet");
        }
        
        String path = apiClient.expandPath("/internal/recommend", Collections.<String, Object>emptyMap());

        final MultiValueMap<String, String> queryParams = new LinkedMultiValueMap<String, String>();
        final HttpHeaders headerParams = new HttpHeaders();
        final MultiValueMap<String, String> cookieParams = new LinkedMultiValueMap<String, String>();
        final MultiValueMap<String, Object> formParams = new LinkedMultiValueMap<String, Object>();

        queryParams.putAll(apiClient.parameterToMultiValueMap(null, "q", q));
        queryParams.putAll(apiClient.parameterToMultiValueMap(null, "collection", collection));
        queryParams.putAll(apiClient.parameterToMultiValueMap(null, "rs_prefix", rsPrefix));

        final String[] localVarAccepts = { 
            "application/json"
         };
        final List<MediaType> localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);
        final String[] contentTypes = {  };
        final MediaType contentType = apiClient.selectHeaderContentType(contentTypes);

        String[] authNames = new String[] {  };

        ParameterizedTypeReference<Object> returnType = new ParameterizedTypeReference<Object>() {};
        return apiClient.invokeAPI(path, HttpMethod.GET, queryParams, postBody, headerParams, cookieParams, formParams, localVarAccept, contentType, authNames, returnType);
    }
    /**
     * Search Get
     * Do a search against the specified collection
     * <p><b>200</b> - Successful Response
     * <p><b>422</b> - Validation Error
     * @param q Query string (required)
     * @param collection Collection (required)
     * @return Object
     * @throws RestClientException if an error occurs while attempting to invoke the API
     */
    public Object searchGetInternalSearchGet(String q, String collection) throws RestClientException {
        return searchGetInternalSearchGetWithHttpInfo(q, collection).getBody();
    }

    /**
     * Search Get
     * Do a search against the specified collection
     * <p><b>200</b> - Successful Response
     * <p><b>422</b> - Validation Error
     * @param q Query string (required)
     * @param collection Collection (required)
     * @return ResponseEntity&lt;Object&gt;
     * @throws RestClientException if an error occurs while attempting to invoke the API
     */
    public ResponseEntity<Object> searchGetInternalSearchGetWithHttpInfo(String q, String collection) throws RestClientException {
        Object postBody = null;
        
        // verify the required parameter 'q' is set
        if (q == null) {
            throw new HttpClientErrorException(HttpStatus.BAD_REQUEST, "Missing the required parameter 'q' when calling searchGetInternalSearchGet");
        }
        
        // verify the required parameter 'collection' is set
        if (collection == null) {
            throw new HttpClientErrorException(HttpStatus.BAD_REQUEST, "Missing the required parameter 'collection' when calling searchGetInternalSearchGet");
        }
        
        String path = apiClient.expandPath("/internal/search", Collections.<String, Object>emptyMap());

        final MultiValueMap<String, String> queryParams = new LinkedMultiValueMap<String, String>();
        final HttpHeaders headerParams = new HttpHeaders();
        final MultiValueMap<String, String> cookieParams = new LinkedMultiValueMap<String, String>();
        final MultiValueMap<String, Object> formParams = new LinkedMultiValueMap<String, Object>();

        queryParams.putAll(apiClient.parameterToMultiValueMap(null, "q", q));
        queryParams.putAll(apiClient.parameterToMultiValueMap(null, "collection", collection));

        final String[] localVarAccepts = { 
            "application/json"
         };
        final List<MediaType> localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);
        final String[] contentTypes = {  };
        final MediaType contentType = apiClient.selectHeaderContentType(contentTypes);

        String[] authNames = new String[] {  };

        ParameterizedTypeReference<Object> returnType = new ParameterizedTypeReference<Object>() {};
        return apiClient.invokeAPI(path, HttpMethod.GET, queryParams, postBody, headerParams, cookieParams, formParams, localVarAccept, contentType, authNames, returnType);
    }
}
