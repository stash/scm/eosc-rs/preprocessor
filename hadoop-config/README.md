# Hadoop configuration

# This directory

Move hadoop configuration (.xml) files to this directory.
All files will be copied to the Docker image.

# Subdirectories

Subdirectories are to support working with mutliple hadoop configurations.
Create subdirectories freely.
