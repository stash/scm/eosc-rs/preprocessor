#!/bin/bash
dockerfile="$1"
shift
docker run --rm -i hadolint/hadolint:2.8.0-alpine hadolint "$@" - < "$dockerfile"
