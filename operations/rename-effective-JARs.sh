#!/usr/bin/env bash

source .load-settings.sh

if [[ -z "$1" ]]; then
    echo "Usage: $0 <JAR_PREFIX> <LOCAL_FILE>"
    exit 1
fi

JAR_PREFIX=$1

./hadoop.sh fs -mv \
  preproc/jars/jobs/marketplace-resources-preproc-sparkjob-1.0-SNAPSHOT-uber.jar \
  preproc/jars/jobs/${JAR_PREFIX}_marketplace-resources-preproc-sparkjob-1.0-SNAPSHOT-uber.jar
./hadoop.sh fs -mv \
  preproc/jars/jobs/oag-resources-preproc-sparkjob-1.0-SNAPSHOT-uber.jar \
  preproc/jars/jobs/${JAR_PREFIX}_oag-resources-preproc-sparkjob-1.0-SNAPSHOT-uber.jar
./hadoop.sh fs -mv \
  preproc/jars/jobs/trainings-preproc-sparkjob-1.0-SNAPSHOT-uber.jar \
  preproc/jars/jobs/${JAR_PREFIX}_trainings-preproc-sparkjob-1.0-SNAPSHOT-uber.jar


echo "Done - prefix ${JAR_PREFIX} was added."
