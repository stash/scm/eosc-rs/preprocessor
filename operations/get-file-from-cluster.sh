#!/usr/bin/env bash

source .load-settings.sh

if [[ -z "$1" ||  -z "$2" ]]; then
    echo "Usage: $0 <REMOTE_FILE> <LOCAL_FILE>"
    echo "For example: $0 \"preproc/input/id.csv\" \"input/id.csv\""
    exit 1
fi

REMOTE_FILE=$1
LOCAL_FILE=$2


./hadoop.sh fs -get ${REMOTE_FILE} ${LOCAL_FILE}

echo "Done - now you can check it at ${LOCAL_FILE}"
