#!/bin/bash

if [[ -z "$1" ]]; then
    echo "Usage: $0 <DUMP_NAME>"
    echo "For example: $0 2022-11-28"
    exit 1
fi

DUMP_NAME=$1

# File names are created based on the resources array.
resources=("dataset" "other_rp" "publication" "software" "training" "service" "datasource")
ANALYSIS_DIR=../doc/resource-dump-analysis/
TMPDIR="$(mktemp -d)"

header_file_overwrite () {
   printf 'Dump: %s\n' "$DUMP_NAME" > $1
}

append_record_counter () {
   printf 'Sample size (lines): %s\n```` json\n' "$LINE_COUNTER" >> $1
}

append_footer () {
  printf "````" >> $1
}

for resource in ${resources[@]}; do
  FILE=0_${resource}.json
  # Fetch sample file for the resource.
  ./dumps-get-file.sh "${DUMP_NAME}/${resource}/${FILE}" "${TMPDIR}/${FILE}"

  # Check if file exists
  if [ -f "${TMPDIR}/${FILE}" ]; then
    echo "File ${FILE} exists."
    echo "Processing for ${resource} has started."

    LINE_COUNTER=$(wc -l < ${TMPDIR}/${FILE})

    # Names of all attributes (based on the first record).
    header_file_overwrite ${ANALYSIS_DIR}${resource}_avaliable_attributes.md
    echo "````" >> ${ANALYSIS_DIR}${resource}_avaliable_attributes.md
    head -n 1 ${TMPDIR}/${FILE} \
      | jq 'keys[]' >> ${ANALYSIS_DIR}${resource}_avaliable_attributes.md
    echo "````" >> ${ANALYSIS_DIR}${resource}_avaliable_attributes.md

    # Attribute names with count for not-null/not-empty values. Null/empty values were skipped.
    header_file_overwrite ${ANALYSIS_DIR}${resource}_not_null_attributes.md
    append_record_counter ${ANALYSIS_DIR}${resource}_not_null_attributes.md
    jq 'with_entries(select(.value | .!=null and . != "" and . != [])) | [{name: (keys)[]}]' ${TMPDIR}/${FILE}\
      | jq -s 'flatten | group_by (.name)[] | {name: .[0].name, count: length}' \
      | jq -s 'sort_by(.name)'  \
      >> ${ANALYSIS_DIR}${resource}_not_null_attributes.md
    append_footer ${ANALYSIS_DIR}${resource}_not_null_attributes.md

    # Attribute names with count for null/empty values. Filtered by max number in counters.
    header_file_overwrite ${ANALYSIS_DIR}${resource}_null_attributes.md
    append_record_counter ${ANALYSIS_DIR}${resource}_null_attributes.md
    jq 'with_entries(select(.value | .==null or . == "" or . == [])) | [{name: (keys)[]}]' ${TMPDIR}/${FILE}\
      | jq -s 'flatten | group_by (.name)[] | {name: .[0].name, count: length}' \
      | jq -s '([ .[].count ] | max) as $m| map(select(.count== $m)) | sort_by(.name)' \
      >> ${ANALYSIS_DIR}${resource}_null_attributes.md
    append_footer ${ANALYSIS_DIR}${resource}_null_attributes.md

  else
    echo "${FILE} does not exist!"
    echo "Resource ${resource} could not be processed!"
  fi
done

trap 'rm -rf -- "$TMPDIR"' EXIT