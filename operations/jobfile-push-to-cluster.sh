#!/usr/bin/env bash

source .load-settings.sh

if [[ -z "$1" ]]; then
    echo "Usage: $0 <LOCAL_JAR_FILE_PATH>"
    echo "For example: $0 ../s3-to-hdfs-transfer-sparkjob/target/s3-to-hdfs-transfer-sparkjob-1.0-SNAPSHOT-uber.jar"
    exit 1
fi

LOCAL_JAR_FILE_PATH=$1
JAR_FILE_NAME=$(basename $LOCAL_JAR_FILE_PATH)

REMOTE_JAR_FILE_PATH=${CLUSTER_JOBS_DIR}/${JAR_FILE_NAME}

./hadoop.sh fs -put ${LOCAL_JAR_FILE_PATH} ${REMOTE_JAR_FILE_PATH}

WEBVIEW="${CLUSTER_WEB_VIEW_URLBASE}${CLUSTER_JOBS_DIR}"
echo "Done - now you can check it on ${WEBVIEW}"
