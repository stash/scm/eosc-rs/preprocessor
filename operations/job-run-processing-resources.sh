#!/usr/bin/env bash

if [[ -z "$1" ]] || [[ -z "$2" ]] || [[ -z "$3" ]]; then
  CMD=`basename $0`
  echo "Usage: $CMD <INPUT_FILES> <TARGET_DIR> <RESOURCE_TYPE> optional<PREVIOUS_DUMP_DIR>"
  echo "For example: $CMD \"28-11-2022/oag/dataset/*\" \"28-11-2022/oag/software\""
  echo "will process all files from '<CLUSTER_DUMPS_DIR>/28-11-2022/oag/dataset' and produce output to '<CLUSTER_OUTPUT_DIR>/28-11-2022/oag/software'"
  echo "Notice the asterisk '*' and the quotation '\"' characters."
  exit 1
fi

source .load-settings.sh

INPUT_FILES=$1
TARGET_DIR=$2
RESOURCE_TYPE=$3

ENDPOINT="${PREPROCESSOR_RESTAPI_BASE}/run-oag-preproc-job"
PARAM_1="?inputFiles=${CLUSTER_DUMPS_DIR}/${INPUT_FILES}"
PARAM_2="&outputDir=${CLUSTER_OUTPUT_DIR}/${TARGET_DIR}"
PARAM_3="&resourceType=${RESOURCE_TYPE}"

if [[ -z "$4" ]]; then
  FULL_URL="${ENDPOINT}${PARAM_1}${PARAM_2}${PARAM_3}"
else
  PREVIOUS_DUMP_DIR=$4
  PARAM_4="&previousDumpDir=${CLUSTER_OUTPUT_DIR}/${PREVIOUS_DUMP_DIR}"
  FULL_URL="${ENDPOINT}${PARAM_1}${PARAM_2}${PARAM_3}${PARAM_4}"
fi

FULL_URL="${ENDPOINT}${PARAM_1}${PARAM_2}${PARAM_3}${PARAM_4}"

echo "Sending a 'process $RESOURCE_TYPE resources' request to the Preprocessor"
echo "Request URL: ${FULL_URL}"
curl -X POST "${FULL_URL}"
