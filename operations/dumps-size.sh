#!/usr/bin/env bash

if [[ -z "$1" ]]; then
  echo "Usage: $0 <S3_SOURCE_DIR>"
  echo "For example: $0 oag-6/openaire_prod_20220513/v3/dataset/"
  echo "Remember the '/' at the end of the path!"
  exit 1
fi

source .load-settings.sh

FOLDER=$1

aws s3 ls \
  "s3://${S3_BUCKET_NAME}/${FOLDER}" \
  --human-readable \
  --summarize \
  --recursive \
  --endpoint-url ${S3_ENDPOINT_URL}
