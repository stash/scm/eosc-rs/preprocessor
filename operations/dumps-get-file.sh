#!/usr/bin/env bash

if [[ -z "$1" ]] || [[ -z "$2" ]]; then
  CMD=`basename $0`
  echo "Usage: $CMD <REMOTE_FILE_PATH> <LOCAL_FILE_PATH>"
  exit 1
fi

source .load-settings.sh

REMOTE_FILE_PATH=$1
LOCAL_FILE_PATH=$2

echo "Getting a single file from '${S3_BUCKET_NAME}' bucket:"

aws s3api get-object \
  --bucket ess-mock-dumps \
  --endpoint-url ${S3_ENDPOINT_URL} \
  --key ${REMOTE_FILE_PATH} ${LOCAL_FILE_PATH}
