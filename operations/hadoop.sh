#!/usr/bin/env bash

source .load-settings.sh

HADOOP_CONFIG_DIR=$(realpath ../hadoop-config/${HADOOP_SELECTED_CONFIG_DIR})
echo "Loading Hadoop configuration from: ${HADOOP_CONFIG_DIR}"

CMD="$HADOOP_HOME/bin/hadoop --config ${HADOOP_CONFIG_DIR} $@"
echo "$CMD"
eval "$CMD"
