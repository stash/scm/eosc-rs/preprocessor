#!/bin/bash

if [ ! -f id_and_localization.csv ]; then
  echo "At least one of required file was not found!"
  exit 1
fi

read -s -p "Psql password for user preproc: " password
export PGPASSWORD=$password

echo "The extracting phase has started"

grep "dataset" id_and_localization.csv | awk -F ';' '{print "3;"$1";0;0"}' > dataset_id.csv
grep "publication" id_and_localization.csv | awk -F ';' '{print "2;"$1";0;0"}' > publication_id.csv
grep "software" id_and_localization.csv | awk -F ';' '{print "1;"$1";0;0"}' > software_id.csv
grep "other_rp" id_and_localization.csv | awk -F ';' '{print "5;"$1";0;0"}' > other_id.csv
grep "training" id_and_localization.csv | awk -F ';' '{print "4;"$1";0;0"}' > training_id.csv
grep "service" id_and_localization.csv | awk -F ';' '{print "0;"$1";0;0"}' > service_id.csv
grep "datasource" id_and_localization.csv | awk -F ';' '{print "7;"$1";0;0"}' > datasource_id.csv


read -p "Are you sure to DELETE all data from base.stats_total_table? [Y/N]" -n 1 -r
if [[ $REPLY =~ ^[Yy]$ ]]
then
	echo "The cleaning phase has started"
	psql -h chara-159.man.poznan.pl -U preproc -d preproc_db -c "TRUNCATE TABLE base.stats_total_table;"

	echo "The initialization phase has started"
	echo "Copying datasets..."
	psql -h chara-159.man.poznan.pl -U preproc -d preproc_db -c "\copy base.stats_total_table(resource_type,resource_id,orders,visits) FROM 'dataset_id.csv' with (format csv,header false, delimiter ';');"
	echo "Copying publications..."
	psql -h chara-159.man.poznan.pl -U preproc -d preproc_db -c "\copy base.stats_total_table(resource_type,resource_id,orders,visits) FROM 'publication_id.csv' with (format csv,header false, delimiter ';');"
	echo "Copying software..."
	psql -h chara-159.man.poznan.pl -U preproc -d preproc_db -c "\copy base.stats_total_table(resource_type,resource_id,orders,visits) FROM 'software_id.csv' with (format csv,header false, delimiter ';');"
	echo "Copying other research products..."
	psql -h chara-159.man.poznan.pl -U preproc -d preproc_db -c "\copy base.stats_total_table(resource_type,resource_id,orders,visits) FROM 'other_id.csv' with (format csv,header false, delimiter ';');"
	echo "Copying trainings..."
	psql -h chara-159.man.poznan.pl -U preproc -d preproc_db -c "\copy base.stats_total_table(resource_type,resource_id,orders,visits) FROM 'training_id.csv' with (format csv,header false, delimiter ';');"
	echo "Copying services..."
	psql -h chara-159.man.poznan.pl -U preproc -d preproc_db -c "\copy base.stats_total_table(resource_type,resource_id,orders,visits) FROM 'service_id.csv' with (format csv,header false, delimiter ';');"
	echo "Copying datasources..."
	psql -h chara-159.man.poznan.pl -U preproc -d preproc_db -c "\copy base.stats_total_table(resource_type,resource_id,orders,visits) FROM 'datasource_id.csv' with (format csv,header false, delimiter ';');"
fi
