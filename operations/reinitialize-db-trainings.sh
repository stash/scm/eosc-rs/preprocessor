#!/bin/bash

if [[ -z "$1" ]]; then
  echo "Usage: $0 <API_URL>"
  echo "For example: $0 https://beta.providers.eosc-portal.eu"
  exit 1
fi

source .load-settings.sh

API_URL=$1

read -s -p "Psql password for user preproc: " password
export PGPASSWORD=$password

echo "The extracting phase has started"

curl -X GET --header 'Accept: application/xml' ${API_URL}'/api/public/trainingResource/all?catalogue_id=all&suspended=false' --silent |
  grep -oP '(?<=<id>).*?(?=</id>)' | awk -F ';' '{print "4;"$0";0;0"}' > training_id.csv

read -p "Are you sure to DELETE all data from base.stats_total_table? [Y/N]" -n 1 -r
if [[ $REPLY =~ ^[Yy]$ ]]
then
	echo "The cleaning phase has started"
	psql -h ${PSQL_URL} -U preproc -d preproc_db -c "TRUNCATE TABLE base.stats_total_table_training;"

	echo "The initialization phase has started"
	echo "Copying trainings..."
	psql -h ${PSQL_URL} -U preproc -d preproc_db -c "\copy base.stats_total_table(resource_type,resource_id,orders,visits) FROM 'training_id.csv' with (format csv,header false, delimiter ';');"
fi