#!/usr/bin/env bash

if [[ -z "$1" ]] || [[ -z "$2" ]]; then
  echo "Usage: $0 <S3_SOURCE_DIR> <CLUSTER_TARGET_DIR>"
  echo "For example: $0 2022-11-28/software 28-11-2022/oag/software"
  echo "will transfer all files from the '2022-11-28/software' S3 directory to the '<CLUSTER_DUMPS_DIR>/28-11-2022/oag/software' directory on HDFS."
  exit 1
fi

source .load-settings.sh

FROM=$1
TO=$2

ENDPOINT="${PREPROCESSOR_RESTAPI_BASE}/run-s3-to-hdfs-job"
PARAM_1="?sourceDir=s3a://${S3_BUCKET_NAME}/${FROM}"
PARAM_2="&destDir=${CLUSTER_DUMPS_DIR}/${TO}"
FULL_URL="${ENDPOINT}${PARAM_1}${PARAM_2}"

echo "Sending a 'transfer' request to the Preprocessor"
echo "Request URL: ${FULL_URL}"
curl -X POST "${FULL_URL}"
