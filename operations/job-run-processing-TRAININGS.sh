#!/usr/bin/env bash

if [[ -z "$1" ]] || [[ -z "$2" ]]; then
  CMD=`basename $0`
  echo "Usage: $CMD <INPUT_FILES> <TARGET_DIR> optional<PREVIOUS_DUMP_DIR>"
  echo "For example: $CMD \"28-11-2022/oag/training/*\" \"28-11-2022/oag/training\""
  echo "will process all files from '<CLUSTER_DUMPS_DIR>/28-11-2022/oag/training' and produce output to '<CLUSTER_OUTPUT_DIR>/28-11-2022/oag/training'"
  echo "Notice the asterisk '*' and the quotation '\"' characters."
  exit 1
fi

source .load-settings.sh

INPUT_FILES=$1
TARGET_DIR=$2

ENDPOINT="${PREPROCESSOR_RESTAPI_BASE}/run-trainings-preproc-job"
PARAM_1="?inputFiles=${CLUSTER_DUMPS_DIR}/${INPUT_FILES}"
PARAM_2="&outputDir=${CLUSTER_OUTPUT_DIR}/${TARGET_DIR}"

if [[ -z "$3" ]]; then
  FULL_URL="${ENDPOINT}${PARAM_1}${PARAM_2}"
else
  PREVIOUS_DUMP_DIR=$3
  PARAM_3="&previousDumpDir=${PREVIOUS_DUMP_DIR}"
  FULL_URL="${ENDPOINT}${PARAM_1}${PARAM_2}${PARAM_3}"
fi

echo "Sending a 'process trainings' request to the Preprocessor"
echo "Request URL: ${FULL_URL}"
curl -X POST "${FULL_URL}"
