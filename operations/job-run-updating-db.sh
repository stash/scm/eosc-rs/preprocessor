#!/usr/bin/env bash

if [[ -z "$1" ]] || [[ -z "$2" ]]; then
  echo "Usage: $0 <DUMP_DIR> <RESOURCE_TYPE>"
  echo "For example: $0 preproc/output/2023-06-11 SOFTWARE "
  echo "relative to preproc/output/2023-06-11 will reflect changes in db."
  exit 1
fi

source .load-settings.sh

DUMP_DIR=$1
RESOURCE_TYPE=$2

ENDPOINT="${PREPROCESSOR_RESTAPI_BASE}/run-db-updater-job"
PARAM_1="?dumpDir=${DUMP_DIR}"
PARAM_2="&resourceType=${RESOURCE_TYPE}"
FULL_URL="${ENDPOINT}${PARAM_1}${PARAM_2}"

echo "Sending a 'db update' request to the Preprocessor"
echo "Request URL: ${FULL_URL}"
curl -X POST "${FULL_URL}"
echo ""
