#!/usr/bin/env bash

settings_file=.settings
settings_file_template=.settings.template
if [[ ! -f $settings_file ]]; then
    echo -e "IMPORTANT NOTICE"
    echo "Initializing a settings file '$settings_file' from template..."
    cp $settings_file_template $settings_file
    echo -e "Initialized with the following contents:\n"
    cat $settings_file | sed 's/^/    /'
    echo -e "\n\nPlease edit the '$settings_file' file and re-run the script."
    echo -e "\n"
    exit 1
fi

echo "Loading settings from: $(realpath $settings_file)"
source $settings_file

echo "Settings loaded successfully"
