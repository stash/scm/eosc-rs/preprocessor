#!/bin/bash

if [[ -z "$1" ]]; then
    echo "Usage: $0 <DUMP_NAME> <RESOURCE> <DESTINATION_DIR> <NUMBER_OF_LINES>"
    echo "For example: $0 2022-11-28 resource"
    exit 1
fi

DUMP_NAME=$1
RESOURCE=$2
FILE=0_${RESOURCE}.json
DESTINATION_DIR=$3
DESTINATION_FILE=${DESTINATION_DIR}/${RESOURCE}/${DUMP_NAME}_${FILE}
NUMBER_OF_LINES=$4

SOURCE_FILE=0_${RESOURCE}.json
# Fetch sample file for the resource.
./dumps-get-file.sh "${DUMP_NAME}/${RESOURCE}/${SOURCE_FILE}" "${DESTINATION_FILE}"

echo "$(head -5 ${DESTINATION_FILE})" > ${DESTINATION_FILE}