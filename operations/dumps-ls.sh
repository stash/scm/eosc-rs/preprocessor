#!/usr/bin/env bash

source .load-settings.sh

echo "Listing only directories in the '${S3_BUCKET_NAME}' bucket:"

aws s3api list-objects \
  --bucket ess-mock-dumps \
  --output text \
  --query "Contents[].{Key: Key}" \
  --endpoint-url ${S3_ENDPOINT_URL} \
  > .tmp/dumps-ls-all.txt

cat .tmp/dumps-ls-all.txt | xargs dirname | sort | uniq

echo "For more details please refer to .tmp/dumps-ls-all.txt"
