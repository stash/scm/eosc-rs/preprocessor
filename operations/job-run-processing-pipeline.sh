#!/usr/bin/env bash

if [[ -z "$1" ]] || [[ -z "$2" ]]; then
  echo "Usage: $0 <SOURCE_DIR> <DEST_DIR> optional<PREVIOUS_DUMP_DIR>"
  echo "For example: $0 2023-08-16 2023-08-16 2023-06-11 "
  echo "will transfer all files from the '2023-08-16' S3 directory to the '<cluster_dumps_input_dir>/2023-08-16' directory on HDFS,"
  echo "process the files and save results to  '<cluster_dumps_output_dir>/2023-08-16' directory on HDFS"
  echo "and reflect changes to the db ."
  exit 1
fi

source .load-settings.sh

FROM=$1
TO=$2

ENDPOINT="${PREPROCESSOR_RESTAPI_BASE}/transfer-and-process-all-resources"
PARAM_1="?sourceDir=${FROM}"
PARAM_2="&destDir=${TO}"

if [[ -z "$3" ]]; then
  FULL_URL="${ENDPOINT}${PARAM_1}${PARAM_2}"
else
  PREVIOUS_DUMP_DIR=$3
  PARAM_3="&previousDumpDir=${PREVIOUS_DUMP_DIR}"
  FULL_URL="${ENDPOINT}${PARAM_1}${PARAM_2}${PARAM_3}"
fi

echo "Sending a 'transfer and process' request to the Preprocessor"
echo "Request URL: ${FULL_URL}"
curl -X POST "${FULL_URL}"
echo ""
