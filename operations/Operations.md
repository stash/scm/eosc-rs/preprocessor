# Operating the running Preprocessor

## Table of Contents
1. [Introduction](#introduction)
2. [Setting-up](#setting-up)
3. [Listing S3 dumps](#listing-s3-dumps)
4. [Processing dump files](#processing-dump-files)
5. [Reinitialization DB](#reinitialization-db)

## Introduction

In this document, we describe necessary actions needed for preprocessing data with use of Hadoop computing cluster and reinitialization DB (`base.stats_total_table`).

In particular, the following is covered:
- Setting-up a local environment to work with remote Hadoop cluster
- Listing available dump files
- Transferring the dump files to Hadoop cluster
- Processing the files with Hadoop jobs
- Internals of the provided shell utility scripts

### Dump files

The Preprocessor is downloading metadata about available dumps from an S3-compliant storage (currently, as temporary solution). However, in future it will look for the metadata in a REST service named "Search Service", as soon as it is operationally ready.

The dumps are stored on an S3-compliant storage server. 

Processing dumps is a manual multistep process:
- Available dumps needs to be listed first
- Selected dumps needs to be transferred to Hadoop cluster
- Processing jobs needs to be started

## Setting-up

### Using the provided utility scripts

For every kind of operation, there is a shell utility script provided. 

The utility scripts are based on shared settings stored in the `.settings` file. The file is not tracked by
git. The file is initialized automatically during a first run of any utility script.

All scripts are checking input parameters and print usage information if the parameters are not correct.


### Setting-up access to S3 storage

The S3 storage is accessed via the AWS S3 API. The access is configured via the AWS CLI tool. The tool is available for all major operating systems. The tool is configured via the `~/.aws/credentials` file. The file should contain the following:

    [default]
    aws_access_key_id = <access key id>
    aws_secret_access_key = <access key id>


### Setting-up access to the Hadoop cluster

Step 1: Install Hadoop on your local machine

- Use hadoop version: 3.3.2
- Get Hadoop distribution from: https://archive.apache.org/dist/hadoop/common/

Step 2: Put your cluster configuration in the `../hadoop-config` directory
        - please refer to [its README file](../hadoop-config/README.md).

Step 3: Initialize a Hadoop wrapper utility by calling `./hadoop.sh`

Step 4: Verify your configuration by calling: `./hadoop.sh fs -ls`

The expected output should be similar to:

    ./hadoop.sh fs -ls           
    Loading settings from: /home/maciej/Src/eosc/preprocessor/operations/.settings
    Found 7 items
    drwxrwxr-x+  - hadoop supergroup          0 2022-11-30 02:12 .sparkStaging
    drwxr-xr-x   - hadoop supergroup          0 2022-11-28 10:20 2022-11-24
    drwxrwxr-x+  - hadoop supergroup          0 2022-06-22 18:12 cache_pretrained
    drwxrwxr-x+  - ubuntu supergroup          0 2022-11-02 15:04 embeddings
    drwxrwxr-x+  - hadoop supergroup          0 2022-09-15 12:59 nn-finder
    drwxrwxr-x   - hadoop supergroup          0 2022-09-29 08:38 offline-ml-data
    drwxrwxr-x+  - hadoop supergroup          0 2022-07-04 15:32 preproc


Now you should be able to access your Hadoop cluster via the wrapper utility by calling `./hadoop.sh 
<YOUR_PARAMS_HERE>`. The script wraps original `${HADOOP_HOME}/bin/hadoop` script and passes all arguments.


### Working with remote file system

Call `./hadoop.sh fs` to know more operation on files, such as `rm`, `mkdir`, `put`, `get`, etc.


## Listing S3 dumps

    ./dumps-ls.sh

Getting a size of a dump-folder:

    ./dumps-size.sh


## Processing dump files

The first/initial processing is called `full processing`
and the subsequent processing is called `differential processing`.

To perform full processing instead of differential processing,
make sure the `base.stats_total_table` table is empty or clear it and
do not specify the `PREVIOUS_DUMP_DIR` parameter in the following call.

Differential processing requires the `PREVIOUS_DUMP_DIR` parameter.

Running the processing pipeline:

 ```
./job-run-processing-pipeline.sh <SOURCE_DIR> <DEST_DIR> optional<PREVIOUS_DUMP_DIR>
 ```

Instead of the above, it is possible to perform each step separately. 
The processing pipeline consists of: 
1. [downloading data](#job-for-transferring-oarg-dumps-from-s3-to-hdfs), 
2. [processing](#job-for-processing-resources),
3. [updating the database](#updating-db).


### Deploying job definitions into the Hadoop cluster

Job implementations are not propagated automatically to the cluster and for that reason they need to be transferred before use, especially if their implementation's changed.

    ./jobfile-push-to-cluster.sh


### Running jobs

Jobs are launched by calling a Preprocessor's REST API. BASH scripts are already in place to facilitate these calls.


### Job for transferring OARG dumps from S3 to HDFS

    ./job-run-transferring-dumps.sh


### Job for processing resources

This job is applicable to **data sets**, **publications**, **software** and **other research products** and needs a separate launch for each of them.

For example:

    job-run-processing-DATASETS.sh "2022-11-28/dataset/*" processed-on-friday/datasets
    job-run-processing-OTHER_RP.sh "2022-11-28/other_rp/*" processed-on-friday/other_rps
    job-run-processing-PUBLICATIONS.sh "2022-11-28/publication/*" processed-on-friday/publications
    job-run-processing-SOFTWARE.sh "2022-11-28/software/*" processed-on-friday/softwares

All of those above scipts are simple aliases calling the base script:

    job-run-processing-resources.sh <INPUT> <OUTPUT> <RESOURCE_TYPE>


### Job for processing trainings

    job-run-processing-trainings.sh


### Updating DB
    
To update db and reflect changes from differential processing:

    job-run-updating-db.sh <DUMP_DIR> <RESOURCE_TYPE>


## Update dump statistics - only for analytics data - not required
To update dump statistic - directory `../doc/resource-dump-analysis`:
1. Run:
 ```
./dump-statistics.sh [DUMP_NAME]
 ```
For example:
 ```
./dump-statistics.sh 2023-01-26
 ```

2. Commit the files (`../doc/resource-dump-analysis/*`)

## Re-initialization of the training table (API->PSQL)
Run: 
 ```
./reinitialize-db-trainings.sh https://beta.providers.eosc-portal.eu
 ```