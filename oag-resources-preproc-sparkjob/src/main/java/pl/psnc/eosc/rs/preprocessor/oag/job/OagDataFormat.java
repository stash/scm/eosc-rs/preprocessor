package pl.psnc.eosc.rs.preprocessor.oag.job;

import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.types.DataTypes;
import org.apache.spark.sql.types.StructType;

import static org.apache.spark.sql.functions.*;
import static org.apache.spark.sql.functions.col;

public class OagDataFormat {

    static protected final StructType INPUT_SCHEMA = new StructType()
            .add("id", DataTypes.StringType)
            .add("title", DataTypes.StringType,true)
            .add("keywords", DataTypes.createArrayType(DataTypes.StringType))
            .add("author_names", DataTypes.createArrayType(DataTypes.StringType))
            .add("description", DataTypes.createArrayType(DataTypes.StringType))
            .add("language", DataTypes.createArrayType(DataTypes.StringType))
            .add("usage_counts_downloads", DataTypes.StringType)
            .add("usage_counts_views", DataTypes.StringType)
            .add("fos", DataTypes.createArrayType(DataTypes.StringType))
            .add("relations_long", DataTypes.createArrayType(DataTypes.StringType));

    static protected Dataset<Row> preSelector(Dataset<Row> ds) {
        return ds.select(col("id"),
                array(col("title")).as("mainTitles"),
                col("keywords").as("keywords"),
                col("author_names").as("authors"),
                col("description").as("descriptions"),
                col("language").as("languages"),
                col("fos").as("scientificDomains"),
                col("relations_long").as("relationsExtended"),
                col("usage_counts_downloads").as("downloadsCount"),
                col("usage_counts_views").as("viewsCount"));
    }

    static protected Dataset<Row> postSelector(Dataset<Row> ds) {
        return ds.select(
                col("id").as("id"),
                array_join(transform(col("mainTitles"), col -> array_join(col," ")),";").as("mainTitles"),
                array_join(array_sort(array_distinct(flatten(col("keywords"))))," ").as("keywordsMerged"),
                array_join(transform(col("descriptions"), col -> array_join(col," ")),";").as("descriptions"),
                array_join(array_sort(col("authors")),";").as("authors"),
                array_join(col("languages"), ";").as("languages"),
                array_join(array_sort(col("scientificDomains")), ";").as("scientificDomains"),
                array_join(col("relationsExtended"), ";").as("relationsExtended"),
                col("downloadsCount").as("orderCounter"),
                col("viewsCount").as("visitCounter"));
                // languagePrediction is added before save
//                array_join(col("languagePrediction"), ";").as("languagePrediction"));
    }
}
