package pl.psnc.eosc.rs.preprocessor.oag.job;

import org.apache.commons.lang3.StringUtils;
import org.apache.hadoop.conf.Configuration;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import pl.psnc.eosc.rs.preprocessor.diff.DiffProcessor;
import pl.psnc.eosc.rs.preprocessor.diff.DiffWriter;
import pl.psnc.eosc.rs.preprocessor.diff.model.DiffAggregated;
import pl.psnc.eosc.rs.preprocessor.shared.SummaryWriter;

import java.time.LocalDate;
import java.util.List;

import static org.apache.spark.sql.functions.*;
import static pl.psnc.eosc.rs.preprocessor.diff.CsvDigestReader.readCsvEffectiveData;
import static pl.psnc.eosc.rs.preprocessor.shared.SparkSharedResourcesPreprocessing.*;

public class OagResourcesPreprocSparkJob {

    public static final String APP_NAME = "OagPreproc";


    public static void main(String[] args) {
        if(args.length != 3) throw new IllegalArgumentException(
                "Exactly 3 parameters required! [previous_dump, input_files, output_dir]");
        String previousDump = args[0];
        String inputFiles = args[1];
        String outputDir = args[2];

        final SparkConf sparkConf = new SparkConf()
                .setAppName(APP_NAME);

        final JavaSparkContext javaSparkContext = new JavaSparkContext(sparkConf);

        final SparkSession sparkSession = SparkSession
                .builder()
                .sparkContext(javaSparkContext.sc())
                .appName(APP_NAME)
                .getOrCreate();

        processResources(previousDump, inputFiles,outputDir);

        Configuration hadoopConfig = sparkSession.sparkContext().hadoopConfiguration();
        SummaryWriter summaryWriter = new SummaryWriter(hadoopConfig);
        summaryWriter.printSummary(StringUtils.substring(inputFiles, 0, inputFiles.length() - 1));
        summaryWriter.printSummary(outputDir+"data/");
    }

    public static void processResources(String inputFilesPreviousDump, String inputFilesCurrentDump,
                                        String outputDir){
        Dataset<Row> prevDumpDigest = null;
        Dataset<Row> currentDumpDs = readJsonl(inputFilesCurrentDump);
        Dataset<Row> preprocessedCurrentDumpDs = processData(currentDumpDs);
        preprocessedCurrentDumpDs = OagDataFormat.postSelector(preprocessedCurrentDumpDs);

        if(StringUtils.isNotBlank(inputFilesPreviousDump)){
            prevDumpDigest = readCsvEffectiveData(inputFilesPreviousDump);
        }

        List<DiffAggregated> diff = DiffProcessor.processDump(prevDumpDigest,preprocessedCurrentDumpDs);
        DiffWriter.saveDiffToCsvWithLangDetection(diff,outputDir);
    }

    public static Dataset<Row> processData(Dataset<Row> ds) {

        ds = defaultPreprocessing(ds,true,true);
        return ds;
    }

    public static Dataset<Row> readJsonl(String inputFiles){
        SparkSession sparkSession = SparkSession.getDefaultSession().get();
        // Read json to dataframe
        Dataset<Row> ds = sparkSession.read().schema(OagDataFormat.INPUT_SCHEMA).json(inputFiles).toDF();
        return OagDataFormat.preSelector(ds);
    }

}
