package pl.psnc.eosc.rs.preprocessor.oag.job;


import com.google.common.truth.Truth;
import lombok.Getter;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.io.FileUtils;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.storage.StorageLevel;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.parallel.Execution;
import org.junit.jupiter.api.parallel.ExecutionMode;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import pl.psnc.eosc.rs.preprocessor.spark.SparkStandaloneSessionSingleton;
import pl.psnc.eosc.rs.preprocessor.testing.csv.CsvParserFactory;

import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.OffsetDateTime;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.apache.spark.sql.functions.*;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static pl.psnc.eosc.rs.preprocessor.oag.job.OagResourcesPreprocSparkJobTest.ResourceType.*;

public class OagResourcesPreprocSparkJobTest {

    public enum ResourceType{
        DATASET("dataset"),
        PUBLICATION("publication"),
        SOFTWARE("software"),
        OTHER_RESEARCH_PRODUCT("other_research_product");

        @Getter
        String name;

        public static Optional<ResourceType> getByName(String name){
            return Arrays.stream(ResourceType.values()).filter(e -> e.getName().equals(name)).findFirst();
        }

        ResourceType(String name){
            this.name = name;
        }
    }

    private static SparkSession sparkSession;

    static Path outputDir;

    @BeforeAll
    public static void initializeTestEnvironment() throws IOException {
        // Configure spark in standalone mode
        sparkSession = SparkStandaloneSessionSingleton.getSession();
        // create a new temp directory for test output
        outputDir = Paths.get("target/test-output/" + OffsetDateTime.now().toInstant().toEpochMilli());
        FileUtils.deleteDirectory(outputDir.toFile());
        FileUtils.forceMkdirParent(outputDir.toFile());
    }



    /**
     * The arguments are implicitly provided by {@linkplain OagResourcesPreprocSparkJobTest#shouldProcessTitles()}.
     * @see MethodSource
     */
    @Execution(ExecutionMode.SAME_THREAD)
    @ParameterizedTest
    @MethodSource
    void shouldProcessTitles(Path inputFilePath, ResourceType resourceType) {
        //Read input files
        Dataset<Row> inputData = loadFromJsonLines(inputFilePath.toAbsolutePath().toString());

        inputData.show();

        //Process data
        Dataset<Row> processedData = OagResourcesPreprocSparkJob.processData(inputData);

        //Assertions
        processedData.persist(StorageLevel.MEMORY_ONLY());
        Truth.assertThat(processedData.count()).isAtLeast(5);
        Set<String> mainTitleTokens = processedData.select(explode(flatten(col("mainTitles")))).collectAsList().stream().map(row -> row.getString(0)).collect(Collectors.toSet());
        assertFalse(mainTitleTokens.contains("and"),"'and' should be removed by stopwords remover");
        assertFalse(mainTitleTokens.contains("or"),"'and' should be removed by stopwords remover");
        assertFalse(mainTitleTokens.contains("123"),"'123' should be removed filtered");
        // todo: does the assertion below make sense?
//        assertTrue(mainTitleTokens.containsAll(Set.of("<EMAIL>","<URL>")));
    }

    static private Stream<Arguments> shouldProcessTitles() {
        return createTestArguments();
    }

    private static Dataset<Row> loadFromJsonLines(String absolutePath) {
        return OagResourcesPreprocSparkJob.readJsonl(absolutePath.toString());
    }


    /**
     * The arguments are implicitly provided by {@linkplain OagResourcesPreprocSparkJobTest#shouldProduceNonEmptyCsvFiles()}.
     * @see MethodSource
     */
    @Disabled //TODO adjust to new format
    @Execution(ExecutionMode.SAME_THREAD)
    @ParameterizedTest
    @MethodSource
    void shouldProduceNonEmptyCsvFiles(Path inputFilePath, ResourceType resourceType) throws IOException {

        // given
        Dataset<Row> inputData = loadFromJsonLines(inputFilePath.toAbsolutePath().toString());
        Dataset<Row> processedData = OagResourcesPreprocSparkJob.processData(inputData);
        processedData.persist(StorageLevel.MEMORY_ONLY());
        final Path outDirectory = outputDir.resolve("out-" + UUID.randomUUID());

        // when
//        OagResourcesPreprocSparkJob.saveData(outDirectory.toString(),processedData);

        // then
        final List<Path> files = CustomAssertions.assertFilesAreGenerated(outDirectory, 4);  // csv, csv.crc, _SUCCESS and ._SUCCESS.crc
        CustomAssertions.assertThatFileExists(files, "_SUCCESS");
        final Path csvFilePath = CustomAssertions.assertFileExists(files, ".csv");

        // and file has 6 lines
        Truth.assertThat(Files.readAllLines(csvFilePath)).hasSize(6); // header and 5 records

        // and CSV is parsable
        Reader in = new FileReader(csvFilePath.toFile());
        final List<CSVRecord> records = CsvParserFactory.createCsvFormatter().parse(in).getRecords();
        Truth.assertThat(records).hasSize(5);

        // and recors contain non-empty fields
        CustomAssertions.assertAllCollumnsHaveValues(resourceType, records);

        // copying input file to output directory
        Files.copy(inputFilePath, outDirectory.resolve("input-" + resourceType.name() + ".jsonl"));
    }

    private static Stream<Arguments> shouldProduceNonEmptyCsvFiles() {
        return createTestArguments();
    }

    /**
     * Verifies if all dump files are passed as input parameters
     */
    @Test
    public void shouldGenerateProperTestArguments() {
        Truth.assertThat(createTestArguments().count()).isAtLeast(4); // now 4, but will be more in future
        Truth.assertThat(createTestArguments().filter(args -> args.get()[1] == DATASET).count()).isAtLeast(1);
        Truth.assertThat(createTestArguments().filter(args -> args.get()[1] == OTHER_RESEARCH_PRODUCT).count()).isAtLeast(1);
        Truth.assertThat(createTestArguments().filter(args -> args.get()[1] == PUBLICATION).count()).isAtLeast(1);
        Truth.assertThat(createTestArguments().filter(args -> args.get()[1] == SOFTWARE).count()).isAtLeast(1);
    }

    private static Stream<Arguments> createTestArguments() {
        String pathRoot = "target/test-classes/testdata/input";
        return Stream.of(
                Arguments.of(pathRoot + "/dataset", DATASET),
                Arguments.of(pathRoot + "/publication", PUBLICATION),
                Arguments.of(pathRoot + "/other_rp", OTHER_RESEARCH_PRODUCT),
                Arguments.of(pathRoot + "/software", ResourceType.SOFTWARE)
        ).flatMap(args -> {
            try {
                final String arg0 = (String) args.get()[0];
                final Path directory = Paths.get(arg0);
                return Files.list(directory).map(file -> Arguments.of(file, args.get()[1]));
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        });
    }
}
