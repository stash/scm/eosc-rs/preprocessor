package pl.psnc.eosc.rs.preprocessor.oag.job;

import com.google.common.truth.Truth;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.lang3.StringUtils;
import pl.psnc.eosc.rs.preprocessor.oag.csv.ExpectedCsvHeaders;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static pl.psnc.eosc.rs.preprocessor.oag.job.OagResourcesPreprocSparkJobTest.ResourceType.*;

public class CustomAssertions {
    static void assertAllCollumnsHaveValues(OagResourcesPreprocSparkJobTest.ResourceType resourceType, List<CSVRecord> records) {
        HashSet<String> collumnsWithoutValue = new HashSet<>();
        collumnsWithoutValue.addAll(Arrays.asList(ExpectedCsvHeaders.HEADERS));

        collumnsWithoutValue.remove(ExpectedCsvHeaders.SCIENTIFIC_DOMAIN); // TODO remove when values are present

        if (resourceType == OTHER_RESEARCH_PRODUCT) {
            collumnsWithoutValue.remove(ExpectedCsvHeaders.BEST_ACCESS_RIGHT); // empty column is accepted in this test as it's always empty for ORP
            collumnsWithoutValue.remove(ExpectedCsvHeaders.USAGE_COUNTS_DOWNLOADS); // empty column is accepted in this test as it's always empty for ORP
            collumnsWithoutValue.remove(ExpectedCsvHeaders.USAGE_COUNTS_VIEWS); // empty column is accepted in this test as it's always empty for ORP
            collumnsWithoutValue.remove(ExpectedCsvHeaders.RELATIONS);
            collumnsWithoutValue.remove(ExpectedCsvHeaders.RELATIONS_EXTENDED);
        } else if (resourceType == SOFTWARE) {
            collumnsWithoutValue.remove(ExpectedCsvHeaders.KEYWORDS);
            collumnsWithoutValue.remove(ExpectedCsvHeaders.KEYWORDS_MERGED);
        } else if (resourceType == PUBLICATION || resourceType == DATASET) {
            collumnsWithoutValue.remove(ExpectedCsvHeaders.DOI);
        }

        for (String header : ExpectedCsvHeaders.HEADERS) {
            for (CSVRecord record : records) {
                if (StringUtils.isNotBlank(stripSafely(record.get(header)))) {
                    collumnsWithoutValue.remove(header);
                }
            }
        }
        Truth.assertWithMessage("columns without any value: " + collumnsWithoutValue)
                .that(collumnsWithoutValue).isEmpty();
    }

    static List<Path> assertFilesAreGenerated(Path outDirectory, int expectedFilesNumber) throws IOException {
        final List<Path> files = Files.list(outDirectory).collect(Collectors.toList());
        Truth.assertThat(files.size()).isEqualTo(expectedFilesNumber);
        return files;
    }

    static Path assertFileExists(List<Path> files, String suffix) {
        final Optional<Path> optionalCsv = files.stream().filter(path -> path.getFileName().toString().endsWith(suffix)).findFirst();
        Truth.assertThat(optionalCsv.isPresent()).isTrue();
        final Path csvPath = optionalCsv.get();
        return csvPath;
    }

    static void assertThatFileExists(List<Path> files, String success) {
        final Path successFileName = Path.of(success);
        final Stream<Path> successFileStream = files.stream().filter(path -> path.getFileName().equals(successFileName));
        Truth.assertThat(successFileStream.count()).isEqualTo(1);
    }

    private static String stripSafely(String inputString) {
        if (inputString == null) {
            return null;
        }
        return inputString.strip();
    }
}
