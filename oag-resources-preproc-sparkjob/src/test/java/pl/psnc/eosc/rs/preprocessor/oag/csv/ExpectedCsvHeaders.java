package pl.psnc.eosc.rs.preprocessor.oag.csv;

public class ExpectedCsvHeaders {
    public static final String BEST_ACCESS_RIGHT = "bestAccessRight";
    public static final String USAGE_COUNTS_DOWNLOADS = "usageCountsDownloads";
    public static final String USAGE_COUNTS_VIEWS = "usageCountsViews";
    public static final String RELATIONS = "relations";
    public static final String RELATIONS_EXTENDED = "relationsExtended";
    public static final String SCIENTIFIC_DOMAIN = "scientificDomains";
    public static final String KEYWORDS = "keywords";
    public static final String KEYWORDS_MERGED = "keywordsMerged";
    public static final String DOI = "doi";
    static public String[] HEADERS = new String[]{
            "authors",
            BEST_ACCESS_RIGHT,
            "descriptions",
            "id",
            DOI,
            KEYWORDS,
            KEYWORDS_MERGED,
            "languages",
            "mainTitles",
            "publicationDate",
            USAGE_COUNTS_DOWNLOADS,
            USAGE_COUNTS_VIEWS,
            RELATIONS,
            RELATIONS_EXTENDED,
            SCIENTIFIC_DOMAIN,
            "languagePrediction"
    };
}
