package pl.psnc.eosc.rs.preprocessor.job.marketplace.csv;

public class ExpectedCsvHeaders {
    public static final String BESTACCESSRIGHT = "bestAccessRight";
    public static final String USAGE_COUNTS_DOWNLOADS = "usageCountsDownloads";
    public static final String USAGE_COUNTS_VIEWS = "usageCountsViews";
    static public String[] HEADERS = new String[]{
            BESTACCESSRIGHT,
            "descriptions",
            "id",
            "languages",
            "mainTitles",
            "publicationDate",
            USAGE_COUNTS_DOWNLOADS,
            USAGE_COUNTS_VIEWS,
            "relations",
            "relationsExtended",
            "scientificDomains",
            "categories",
            "tags",
            "accessModes",
            "accessTypes",
            "isOpenAccess",
            "resourceType",
            "languagePrediction"
    };
}
