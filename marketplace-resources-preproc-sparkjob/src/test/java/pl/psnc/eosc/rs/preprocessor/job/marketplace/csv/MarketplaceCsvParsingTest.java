package pl.psnc.eosc.rs.preprocessor.job.marketplace.csv;

import com.google.common.truth.Truth;
import lombok.Getter;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.apache.spark.sql.SparkSession;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.io.TempDir;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import pl.psnc.eosc.rs.preprocessor.job.marketplace.MarketplaceResourcesPreprocSparkJob;
import pl.psnc.eosc.rs.preprocessor.spark.SparkStandaloneSessionSingleton;
import pl.psnc.eosc.rs.preprocessor.testing.csv.CsvParserFactory;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Optional;
import java.util.stream.Stream;

import static org.junit.jupiter.params.provider.Arguments.arguments;

public class MarketplaceCsvParsingTest {

    public enum ResourceType{
        DATASOURCE("datasource"),
        SERVICE("service");

        @Getter
        String name;

        public static Optional<ResourceType> getByName(String name){
            return Arrays.stream(ResourceType.values()).filter(e -> e.getName().equals(name)).findFirst();
        }

        ResourceType(String name){
            this.name = name;
        }
    }

    private static SparkSession sparkSession;

    private static Stream<Arguments> fileNameSource() {
        String pathRoot = "target/test-classes/testdata/input";
        return Stream.of(
                Arguments.of(pathRoot + "/datasource", ResourceType.DATASOURCE),
                Arguments.of(pathRoot + "/service", ResourceType.SERVICE)
        ).flatMap(args -> {
            try {
                final String arg0 = (String) args.get()[0];
                final Path dirPath = Paths.get(arg0);
                return Files.list(dirPath).map(filePath -> arguments(filePath, filePath.getFileName().toString(), args.get()[1]));
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        });
    }

    @BeforeAll
    public static void initializeSparkSession() {
        sparkSession = SparkStandaloneSessionSingleton.getSession();
    }


    @Disabled //TODO adjust to new format
    @DisplayName("Parameterized test for files in testdata/input")
    @ParameterizedTest(name="{2} file = {1}")
    @MethodSource("fileNameSource")
    public void shouldParseAnExampleCsvFile(Path inputFilePath, String fileName, ResourceType resourceType, @TempDir Path tempDir) throws IOException {

        // when
        MarketplaceResourcesPreprocSparkJob.processResources(
                null,
                inputFilePath.toString(),
                tempDir.toAbsolutePath().toString());
        final CSVParser parsed = CsvParserFactory.createCsvParser(tempDir);

        // then
        Truth.assertThat(parsed.getRecords().size()).isEqualTo(5);
        Truth.assertThat(parsed.getHeaderNames()).containsExactly(ExpectedCsvHeaders.HEADERS);
        for (String h : ExpectedCsvHeaders.HEADERS) {
            for (CSVRecord record : parsed.getRecords()) {
                Truth.assertThat(record.isMapped(h)).isTrue();
                Truth.assertThat(record.get(h)).isNotEmpty();
            }
        }
    }
}
