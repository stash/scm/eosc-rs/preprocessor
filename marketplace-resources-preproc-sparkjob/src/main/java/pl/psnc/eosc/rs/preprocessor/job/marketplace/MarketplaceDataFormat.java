package pl.psnc.eosc.rs.preprocessor.job.marketplace;

import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.types.DataTypes;
import org.apache.spark.sql.types.StructType;

import static org.apache.spark.sql.functions.*;
import static org.apache.spark.sql.functions.array_join;

public class MarketplaceDataFormat {

    static protected final StructType INPUT_SCHEMA = new StructType()
            .add("id", DataTypes.StringType)
            .add("title",DataTypes.StringType)
            .add("description", DataTypes.createArrayType(DataTypes.StringType))
            .add("language",DataTypes.createArrayType(DataTypes.StringType))
            .add("tag_list", DataTypes.createArrayType(DataTypes.StringType))
            .add("categories", DataTypes.createArrayType(DataTypes.StringType))
            .add("scientific_domains", DataTypes.createArrayType(DataTypes.StringType))
            .add("best_access_right",DataTypes.StringType)
            .add("access_modes",DataTypes.StringType)
            .add("access_types",DataTypes.StringType)
            .add("open_access",DataTypes.BooleanType)
            .add("publication_date",DataTypes.StringType)
            .add("type",DataTypes.StringType)
            .add("rating",DataTypes.StringType)
            .add("usage_counts_downloads",DataTypes.StringType)
            .add("usage_counts_views",DataTypes.StringType)
            .add("relations", DataTypes.createArrayType(DataTypes.StringType))
            .add("relations_long", DataTypes.createArrayType(DataTypes.StringType));

    static protected Dataset<Row> preSelector(Dataset<Row> ds) {
        return ds.select(col("id"),
                array(col("title")).as("mainTitles"),
                col("description").as("descriptions"),
                col("best_access_right").as("bestAccessRight"),
                col("language").as("languages"),
                col("tag_list").as("tags"),
                col("categories").as("categories"),
                col("access_modes").as("accessModes"),
                col("access_types").as("accessTypes"),
                col("open_access").as("isOpenAccess"),
                col("publication_date").as("publicationDate"),
                col("type").as("resourceType"),
                col("scientific_domains").as("scientificDomains"),
                col("relations").as("relations"),
                col("relations_long").as("relationsExtended"),
                col("usage_counts_downloads").as("downloadsCount"),
                col("usage_counts_views").as("viewsCount"));
    }

    static protected Dataset<Row> postSelector(Dataset<Row> ds) {
        return ds.select(
                col("id").as("id"),
                array_join(transform(col("mainTitles"), col -> array_join(col," ")),";").as("mainTitles"),
                array_join(transform(col("descriptions"), col -> array_join(col," ")),";").as("descriptions"),
                array_join(col("languages"),";").as("languages"),
                array_join(col("categories"),";").as("categories"),
                array_join(col("tags"),";").as("tags"),
                col("accessModes").as("accessModes"),
                col("accessTypes").as("accessTypes"),
                col("isOpenAccess").as("isOpenAccess"),
                col("publicationDate").as("publicationDate"),
                col("bestAccessRight").as("bestAccessRight"),
                col("resourceType").as("resourceType"),
                array_join(col("scientificDomains"),";").as("scientificDomains"),
                array_join(col("relations"), ";").as("relations"),
                array_join(col("relationsExtended"), ";").as("relationsExtended"),
                col("downloadsCount").as("orderCounter"),
                col("viewsCount").as("visitCounter"));
//                array_join(col("languagePrediction"), ";").as("languagePrediction"));
    }

}
