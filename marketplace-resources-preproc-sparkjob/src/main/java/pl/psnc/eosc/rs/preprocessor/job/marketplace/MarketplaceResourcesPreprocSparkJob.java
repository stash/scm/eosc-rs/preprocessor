package pl.psnc.eosc.rs.preprocessor.job.marketplace;

import org.apache.commons.lang3.StringUtils;
import org.apache.hadoop.conf.Configuration;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.sql.*;
import pl.psnc.eosc.rs.preprocessor.diff.DiffProcessor;
import pl.psnc.eosc.rs.preprocessor.diff.DiffWriter;
import pl.psnc.eosc.rs.preprocessor.diff.model.DiffAggregated;
import pl.psnc.eosc.rs.preprocessor.shared.SummaryWriter;

import java.util.List;

import static pl.psnc.eosc.rs.preprocessor.diff.CsvDigestReader.readCsvEffectiveData;
import static pl.psnc.eosc.rs.preprocessor.shared.SparkSharedResourcesPreprocessing.*;

public class MarketplaceResourcesPreprocSparkJob {

    public static final String APP_NAME = "MarketplacePreproc";

    public static void main(String[] args) {
        if(args.length != 3) throw new IllegalArgumentException(
                "Exactly 3 parameters required! [previous_dump, input_files, output_dir]");
        String previousDump = args[0];
        String inputFiles = args[1];
        String outputDir = args[2];

        final SparkConf sparkConf = new SparkConf()
                .setAppName(APP_NAME);

        final JavaSparkContext javaSparkContext = new JavaSparkContext(sparkConf);

        final SparkSession sparkSession = SparkSession
                .builder()
                .sparkContext(javaSparkContext.sc())
                .appName(APP_NAME)
                .getOrCreate();

        processResources(previousDump,inputFiles,outputDir);

        Configuration hadoopConfig = sparkSession.sparkContext().hadoopConfiguration();
        SummaryWriter summaryWriter = new SummaryWriter(hadoopConfig);
        summaryWriter.printSummary(StringUtils.substring(inputFiles, 0, inputFiles.length() - 1));
        summaryWriter.printSummary(outputDir+"data/");
    }

    public static void processResources(String inputFilesPreviousDump, String inputFilesCurrentDump,
                                        String outputDir){

        Dataset<Row> prevDumpDigest = null;
        Dataset<Row> currentDumpDs = readJsonData(inputFilesCurrentDump);
        currentDumpDs = MarketplaceDataFormat.preSelector(currentDumpDs);
        Dataset<Row> preprocessedCurrentDumpDs = processData(currentDumpDs);
        preprocessedCurrentDumpDs = MarketplaceDataFormat.postSelector(preprocessedCurrentDumpDs);

        if(StringUtils.isNotBlank(inputFilesPreviousDump)){
            prevDumpDigest = readCsvEffectiveData(inputFilesPreviousDump);
        }

        List<DiffAggregated> diff = DiffProcessor.processDump(prevDumpDigest,preprocessedCurrentDumpDs);
        DiffWriter.saveDiffToCsvWithLangDetection(diff,outputDir);
    }

    public static Dataset<Row> processData(Dataset<Row> ds) {
        return defaultPreprocessing(ds,false,false);
    }

    public static Dataset<Row> readJsonData(String inputFiles) {
        SparkSession sparkSession = SparkSession.getDefaultSession().get();
        return sparkSession.read().schema(MarketplaceDataFormat.INPUT_SCHEMA).json(inputFiles).toDF();
    }
}
