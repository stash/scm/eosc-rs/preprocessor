#!/bin/bash

CURRENT_DIR=$(readlink -f .)
echo "$CURRENT_DIR"
( cd ../operations && ./dump-get-sample.sh 2023-02-27 service ${CURRENT_DIR}/src/test/resources/testdata/input 5 )
( cd ../operations && ./dump-get-sample.sh 2023-02-27 datasource ${CURRENT_DIR}/src/test/resources/testdata/input 5 )