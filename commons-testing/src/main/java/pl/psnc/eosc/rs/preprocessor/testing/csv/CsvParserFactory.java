package pl.psnc.eosc.rs.preprocessor.testing.csv;

import com.google.common.truth.Truth;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.QuoteMode;
import org.apache.commons.io.FilenameUtils;

import java.io.*;
import java.nio.file.Path;
import java.util.Objects;

// todo: move this class to a new maven module 'commons-testing'
public class CsvParserFactory {

    static public CSVParser createCsvParser(Path tempDir) throws IOException {
        File sourceFile = null;
        for(File file : Objects.requireNonNull(tempDir.resolve("data").toFile().listFiles())){
            if(FilenameUtils.getExtension(file.getName()).equals("csv")){
                sourceFile = file;
            }
        }
        Truth.assertThat(sourceFile).isNotNull();
        Reader in = new FileReader(sourceFile);

        return createCsvFormatter().parse(in);
    }

    static public CSVFormat createCsvFormatter() {
        return CSVFormat.DEFAULT // Standard Comma Separated Value format, as for RFC4180 but allowing empty lines
                .builder() // builder mode needed to adjust other settings
                .setHeader() // column headers are auto-detected
                .setDelimiter(';')
                .setEscape('\\')
                .setQuote('"')
                .setQuoteMode(QuoteMode.ALL)
                .build();
    }
}
