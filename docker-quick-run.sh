#!/bin/bash
set -x

# Use when the newest jar has been already generated (skip mvn package)

docker compose build
docker compose up