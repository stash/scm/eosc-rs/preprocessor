FROM openjdk:11.0.15-jdk

ENV HADOOP_HOME=/opt/hadoop \
    HADOOP_CONF_DIR=/opt/hadoop/etc/hadoop \
    SPARK_HOME=/opt/spark

RUN echo "preproc:x:1001070000:0:::" >> /etc/passwd

RUN \
  wget --progress=dot:giga https://dlcdn.apache.org/hadoop/common/hadoop-3.3.1/hadoop-3.3.1.tar.gz && \
  tar -xzf hadoop-3.3.1.tar.gz && \
  rm hadoop-3.3.1.tar.gz && \
  mv hadoop-3.3.1 $HADOOP_HOME && \
  echo "PATH=$PATH:$HADOOP_HOME/bin" >> ~/.bashrc

RUN \
    wget --progress=dot:giga https://archive.apache.org/dist/spark/spark-3.2.2/spark-3.2.2-bin-hadoop3.2.tgz && \
    tar xvf spark-3.2.2-bin-hadoop3.2.tgz && \
    rm spark-3.2.2-bin-hadoop3.2.tgz && \
    mv spark-3.2.2-bin-hadoop3.2 $SPARK_HOME && \
    echo "export SPARK_HOME=$SPARK_HOME" >> ~/.bashrc && \
    echo "export PATH=$PATH:$SPARK_HOME/bin:$SPARK_HOME/sbin" >> ~/.bashrc

COPY ./run.sh /app/
COPY ./application/target/application.jar /app/target/application.jar

WORKDIR /app
CMD ["./run.sh"]
