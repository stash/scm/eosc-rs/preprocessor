package pl.psnc.eosc.rs.preprocessor.shared;

public enum ResourceType {
    SERVICE((short) 0, "service"),
    SOFTWARE((short) 1, "software"),
    PUBLICATION((short) 2, "publication"),
    DATASET((short) 3, "dataset"),
    TRAINING((short) 4, "training"),
    OTHER_RP((short) 5, "other_rp"),
    MISMATCHED((short) 6, "mismatched"),
    DATASOURCE((short) 7, "datasource");

    public Short getId() {
        return id;
    }

    public String getSubDir() {
        return subDir;
    }

    private final Short id;

    private final String subDir;

    ResourceType(short id, String subDir) {
        this.id = id;
        this.subDir = subDir;
    }
}