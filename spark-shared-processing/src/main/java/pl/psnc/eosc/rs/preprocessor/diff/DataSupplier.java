package pl.psnc.eosc.rs.preprocessor.diff;

import lombok.AllArgsConstructor;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import pl.psnc.eosc.rs.preprocessor.diff.model.DiffData;
import pl.psnc.eosc.rs.preprocessor.diff.model.DiffDigest;

import java.util.ArrayList;
import java.util.List;

@AllArgsConstructor
public class DataSupplier {

    Dataset<Row> processedDump;

    public List<DiffData> match(List<DiffDigest> diffDigests) {
        DiffDigest addedRowsDigest = FindUtils.findDiggestByType(diffDigests,DiffType.ADDED);
        DiffDigest changedContentRowsDigest = FindUtils.findDiggestByType(diffDigests,DiffType.CHANGED_CONTENT);

        List<DiffData> diffData = new ArrayList<>();
        if(addedRowsDigest!=null) diffData.add(matchRows(addedRowsDigest));
        if(changedContentRowsDigest!=null) diffData.add(matchRows(changedContentRowsDigest));
        return diffData;
    }

    private DiffData matchRows(DiffDigest diffDigest){
        Dataset<Row> rows = diffDigest.getDigestRows();
        Dataset<Row> result = processedDump.join(
                rows,
                processedDump.col("id").equalTo(rows.col("id")),
                "leftsemi");
        return new DiffData(diffDigest.getType(),result);
    }

}
