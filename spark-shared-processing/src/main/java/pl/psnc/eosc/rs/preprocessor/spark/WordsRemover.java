package pl.psnc.eosc.rs.preprocessor.spark;

import org.apache.spark.ml.Transformer;
import org.apache.spark.ml.param.Param;
import org.apache.spark.ml.param.ParamMap;
import org.apache.spark.ml.util.DefaultParamsReader;
import org.apache.spark.ml.util.DefaultParamsWritable;
import org.apache.spark.ml.util.Identifiable;
import org.apache.spark.ml.util.MLReader;
import org.apache.spark.sql.Column;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.functions;
import org.apache.spark.sql.types.StructType;
import scala.reflect.ClassTag;

import java.util.Arrays;

import static org.apache.spark.sql.functions.*;
import static scala.io.Codec.UTF8;
import static scala.io.Source.fromInputStream;

public class WordsRemover extends Transformer implements DefaultParamsWritable {

    private final String uid;
    private Param<String> column;
    private Param<String[]> stopWords;
    private Param<Boolean> nestedArrays;


    public WordsRemover() {
        this(Identifiable.randomUID("WordsRemover"));
    }

    public WordsRemover(String uid) {
        this.uid = uid;
        init();
    }

    @Override
    public String uid() {
        return uid;
    }

    @Override
    public Transformer copy(ParamMap extra) {
        return defaultCopy(extra);
    }

    /**
     * The below method is a work around, see:
     * https://issues.apache.org/jira/browse/SPARK-17048
     **/
    public static MLReader<WordsRemover> read() {
        return new DefaultParamsReader<>();
    }

    public Dataset<Row> transform(Dataset<?> dataset) {
        Dataset<Row> transformedDataset = dataset.toDF();
        StructType outputSchema = transformSchema(dataset.schema());
        String columnName = getColumn();
        final String[] stopWords = getStopWords();

        Column outputCol;
        if(getNestedArrays()){
            outputCol = functions.transform(col(columnName), kwds -> filter(kwds, kwd -> not(lower(kwd).isin(stopWords))));
        } else {
            outputCol = filter(col(columnName), kwd -> not(lower(kwd).isin(stopWords)));
        }

        transformedDataset = transformedDataset.withColumn(columnName, outputCol);

        return transformedDataset;
    }

    @Override
    public StructType transformSchema(StructType schema) {
        return schema;
    }

    private void init() {
        column = new Param(this, "columns", "input column names");
        stopWords = new Param(this, "stopWords", "the words to be filtered out");
        nestedArrays = new Param(this, "nestedArrays", "whether the input structure is nested");
        set(nestedArrays,false);
    }


    public WordsRemover loadStopWordsByLanguages(String[] languages){

        String[] words = Arrays.stream(languages).map(lang -> "/org/apache/spark/ml/feature/stopwords/" + lang + ".txt")
                .map(getClass()::getResourceAsStream)
                .map(is -> (String[]) fromInputStream(is, UTF8()).getLines().toStream().toArray(ClassTag.apply(String.class)))
                .flatMap(lines -> Arrays.stream(lines))
                .toArray(String[]::new);

        set(stopWords,words);
        return this;
    }
    public WordsRemover setStopWords(String[] value) {
        set(stopWords, value);
        return this;
    }
    public String[] getStopWords() {
        return getOrDefault(stopWords);
    }

    public WordsRemover setColumn(String value) {
        set(column, value);
        return this;
    }
    public String getColumn() {
        return getOrDefault(column);
    }

    public WordsRemover setNestedArrays(Boolean value) {
        set(nestedArrays, value);
        return this;
    }
    public Boolean getNestedArrays() {
        return getOrDefault(nestedArrays);
    }

    // Methods needed for spark (params)
    public Param stopWords() {
        return stopWords;
    }
    public Param column() {
        return column;
    }
    public Param nestedArrays() {
        return nestedArrays;
    }
}