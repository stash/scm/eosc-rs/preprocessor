package pl.psnc.eosc.rs.preprocessor.shared;

import java.util.Map;

public class SharedSparkProcessingConstants {

    public static String COL_SEPARATOR = ";";
    public static Map<String,String> DEFAULT_CSV_OUTPUT_FORMAT_OPTIONS = Map.of(
            "sep",COL_SEPARATOR,
            "encoding","UTF-8",
            "escape","\"",
            "header","true");

    public static String OAG_DUMPS_COL_SEPARATOR = "\u0001";
    public static String OAG_DUMPS_LIST_SEPARATOR = "\u0002";
    public static String OAG_DUMPS_KEY_VALUE_SEPARATOR = "\u0003";

}
