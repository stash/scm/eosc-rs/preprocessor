package pl.psnc.eosc.rs.preprocessor.spark;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.sql.SparkSession;

public class SparkStandaloneSessionSingleton {

    static JavaSparkContext javaSparkContext;

    private static final SparkConf DEFAULT_SPARK_CONF = new SparkConf()
            .setMaster("local[*]")
            .setAppName("testApp")
            .set("spark.driver.bindAddress", "127.0.0.1");

    private static void init(){
        javaSparkContext = new JavaSparkContext(DEFAULT_SPARK_CONF);
    }

    public static SparkSession getSession(){
        //Configure spark in standalone mode
        if(javaSparkContext==null || javaSparkContext.sc().isStopped()){
            init();
        }
        return SparkSession
                .builder()
                .sparkContext(javaSparkContext.sc())
                .getOrCreate();
    }

}
