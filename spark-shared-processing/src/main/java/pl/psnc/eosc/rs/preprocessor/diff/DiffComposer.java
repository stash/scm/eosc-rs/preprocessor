package pl.psnc.eosc.rs.preprocessor.diff;

import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import pl.psnc.eosc.rs.preprocessor.diff.model.DiffAggregated;
import pl.psnc.eosc.rs.preprocessor.diff.model.DiffData;
import pl.psnc.eosc.rs.preprocessor.diff.model.DiffDigest;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class DiffComposer {

    public static List<DiffAggregated> compose(List<DiffData> dataList, List<DiffDigest> digests){
        List<DiffType> types = Arrays.asList(DiffType.values());
        List<DiffAggregated> diffAggregatedList = new ArrayList<>();
        for(DiffType type: types) {
            diffAggregatedList.add(new DiffAggregated(
                    type,
                    extractDigestRows(digests,type),
                    extractDataRows(dataList,type)));

        }
        return diffAggregatedList;
    }

    private static Dataset<Row> extractDigestRows(List<DiffDigest> digests, DiffType type){
        DiffDigest digest = FindUtils.findDiggestByType(digests,type);
        if(digest==null) return null;
        return digest.getDigestRows();
    }

    private static Dataset<Row> extractDataRows(List<DiffData> dataList, DiffType type){
        DiffData data = FindUtils.findDataByType(dataList,type);
        if(data==null) return null;
        return data.getDataRows();
    }
}
