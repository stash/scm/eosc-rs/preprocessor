package pl.psnc.eosc.rs.preprocessor.diff;

import org.apache.spark.sql.Column;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.functions;
import scala.collection.JavaConversions;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import static org.apache.spark.sql.functions.*;
import static org.apache.spark.sql.functions.col;


public class DumpToDigestConverter {

    public static Dataset<Row> convert(Dataset<Row> currentDumpDs) {
        List<Column> columns = Arrays.stream(currentDumpDs.columns())
                .filter(c -> !Objects.equals(c, "id") && !Objects.equals(c, "visitCounter") && !Objects.equals(c, "orderCounter"))
                .map(functions::col).collect(Collectors.toList());
        return currentDumpDs
                .withColumn("digest", md5(concat_ws(";", JavaConversions.asScalaBuffer(columns))))
                .select(
                        col("id"),
                        col("digest"),
                        col("visitCounter"),
                        col("orderCounter")
                );
    }
}
