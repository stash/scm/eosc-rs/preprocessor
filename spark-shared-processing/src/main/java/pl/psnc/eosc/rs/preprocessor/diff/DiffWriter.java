package pl.psnc.eosc.rs.preprocessor.diff;

import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import pl.psnc.eosc.rs.preprocessor.diff.model.DiffAggregated;

import java.util.List;

import static pl.psnc.eosc.rs.preprocessor.diff.CsvWriter.saveData;
import static pl.psnc.eosc.rs.preprocessor.diff.CsvWriter.saveDigest;
import static pl.psnc.eosc.rs.preprocessor.shared.SparkSharedResourcesPreprocessing.detectLanguage;

public class DiffWriter {

    public static void saveDiffToCsvWithLangDetection(List<DiffAggregated> diffs, String outputDir) {
        for (DiffAggregated diff : diffs) {
            DiffType type = diff.getType();

            Dataset<Row> dsDigest = diff.getDigest();
            if(dsDigest!=null && !dsDigest.isEmpty()) {
                saveDigest(dsDigest, type, outputDir);
                if (type.equals(DiffType.ADDED) || type.equals(DiffType.CHANGED_CONTENT)) {
                    Dataset<Row> dsData = diff.getData();
                    Dataset<Row> dsDataWithLang = detectLanguage(dsData, "descriptions");
                    saveData(dsDataWithLang, type, outputDir);
                }
            }
        }

    }
}
