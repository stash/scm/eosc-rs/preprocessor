package pl.psnc.eosc.rs.preprocessor.diff;

import lombok.extern.slf4j.Slf4j;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.types.StructType;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static org.apache.spark.sql.functions.col;
import static pl.psnc.eosc.rs.preprocessor.shared.SharedSparkProcessingConstants.COL_SEPARATOR;

@Slf4j
public class CsvDigestReader {

    static StructType baseSchema = new StructType()
            .add("id", "string")
            .add("digest", "string");

    static StructType baseWithCountersSchema = new StructType()
            .add("id", "string")
            .add("digest", "string")
            .add("visitCounter", "string")
            .add("orderCounter", "string");

    static StructType unstableIdSchema = new StructType()
            .add("deletedId","string")
            .add("digest", "string")
            .add("addedId", "string")
            .add("visitCounter", "string")
            .add("orderCounter", "string");

    static StructType changedContentSchema = new StructType()
            .add("id", "string")
            .add("deletedDigest","string")
            .add("addedDigest", "string")
            .add("visitCounter", "string")
            .add("orderCounter", "string");


    public static Dataset<Row> readCsvEffectiveData(String inputDir) {
        String directory = inputDir+"/digest/";
        Dataset<Row> added = readCSV(baseWithCountersSchema,directory+DiffType.ADDED.name);
        Dataset<Row> unchanged = readCSV(baseWithCountersSchema,directory+DiffType.UNCHANGED.name);
        Dataset<Row> unstableId = readCSV(unstableIdSchema,directory+DiffType.UNSTABLE_ID.name);
        Dataset<Row> modifiedUnstableId = unstableId.select(col("addedId").as("id"),col("digest"),
                col("visitCounter"),col("orderCounter"));
        Dataset<Row> changedContent = readCSV(changedContentSchema,directory+DiffType.CHANGED_CONTENT.name);
        Dataset<Row> modifiedChangedContent = changedContent.select(col("id"),col("addedDigest").as("digest"),
                col("visitCounter"),col("orderCounter"));

        Dataset<Row> applicableDigest = added.union(unchanged).union(modifiedUnstableId).union(modifiedChangedContent);
        return applicableDigest;
    }

    public static Dataset<Row> readCsvAddedIdsStats(String inputDir) {
        String directory = inputDir+"/digest/";
        Dataset<Row> added = readCSV(baseWithCountersSchema,directory+DiffType.ADDED.name);
        Dataset<Row> addedId = added.select(col("id").as("id"),
                col("visitCounter"),col("orderCounter"));
        Dataset<Row> unstableId = readCSV(unstableIdSchema,directory+DiffType.UNSTABLE_ID.name);
        Dataset<Row> addedUnstableId = unstableId.select(col("addedId").as("id"),
                col("visitCounter"),col("orderCounter"));

        Dataset<Row> applicableDigest = addedId.union(addedUnstableId);
        return applicableDigest;
    }

    public static Dataset<Row> readCsvDeletedIds(String inputDir) {
        String directory = inputDir+"/digest/";
        Dataset<Row> deleted = readCSV(baseSchema,directory+ DiffType.DELETED.name);
        Dataset<Row> deletedId = deleted.select(col("id").as("id"));
        Dataset<Row> unstableId = readCSV(unstableIdSchema,directory+DiffType.UNSTABLE_ID.name);
        Dataset<Row> deletedUnstableId = unstableId.select(col("deletedId").as("id"));

        Dataset<Row> applicableDigest = deletedId.union(deletedUnstableId);
        return applicableDigest;
    }

    public static Dataset<Row> readCsvUpdateCounterIdsStats(String inputDir) {
        String directory = inputDir+"/digest/";
        Dataset<Row> unchanged = readCSV(baseWithCountersSchema,directory+ DiffType.UNCHANGED.name);
        Dataset<Row> unchangedStats = unchanged.select(col("id").as("id"),
                col("visitCounter"),col("orderCounter"));
        Dataset<Row> changedContent = readCSV(baseWithCountersSchema,directory+ DiffType.CHANGED_CONTENT.name);
        Dataset<Row> changedContentStats = changedContent.select(col("id").as("id"),
                col("visitCounter"),col("orderCounter"));

        Dataset<Row> applicableDigest = unchangedStats.union(changedContentStats);
        return applicableDigest;
    }

    public static Dataset<Row> readCsvAddedData(String inputDir) {
        String directory = inputDir+"/digest/";
        Dataset<Row> added = readCSV(baseSchema,directory+DiffType.ADDED.name);
        return added;
    }

    public static Dataset<Row> readCsvUnstableIdData(String inputDir) {
        String directory = inputDir+"/digest/";
        Dataset<Row> unstableId = readCSV(unstableIdSchema,directory+DiffType.UNSTABLE_ID.name);
        return unstableId;
    }

    public static Dataset<Row> readCsvDeletedData(String inputDir) {
        String directory = inputDir+"/digest/";
        Dataset<Row> deleted = readCSV(baseSchema,directory+DiffType.DELETED.name);
        return deleted;
    }

    public static Dataset<Row> readCSV(StructType schema, String files) {
        SparkSession sparkSession = SparkSession.getDefaultSession().get();
        Boolean dirExists = null;
        try {
            Path path = new Path(files);
            FileSystem fileSystem = FileSystem.get(sparkSession.sparkContext().hadoopConfiguration());
            dirExists = fileSystem.exists(path);
        } catch (IOException e){
            log.error("Exception during checking presence of the directory");
        }
        if(dirExists){
            Dataset<Row> ds = sparkSession.read()
                    .option("sep", COL_SEPARATOR)
                    .option("encoding", "UTF-8")
                    .option("header","true")
                    .schema(schema)
                    .csv(files)
                    .toDF();
            return ds;
        } else {
            List<Row> rows = new ArrayList<Row>();
            return sparkSession.createDataFrame(rows, schema);
        }
    }
}
