package pl.psnc.eosc.rs.preprocessor.diff;

import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import pl.psnc.eosc.rs.preprocessor.diff.model.DiffDigest;

import java.util.ArrayList;
import java.util.List;

import static org.apache.spark.sql.functions.*;

public class DiffFinder {

    private static List<DiffDigest> recognize(Dataset<Row> addedRowsDigest) {
        List<DiffDigest> diffDigests = new ArrayList<>();
        diffDigests.add(new DiffDigest(DiffType.ADDED,addedRowsDigest));
        return diffDigests;
    }

    public static List<DiffDigest> recognize(Dataset<Row> prevDumpDigest, Dataset<Row> currentDump) {
        // During initialization, all records are treated as added
        Dataset<Row> currentDumpDigest = DumpToDigestConverter.convert(currentDump);
        if(prevDumpDigest==null || prevDumpDigest.isEmpty()) return recognize(currentDumpDigest);

        Dataset<Row> addedRowsDigest = extractAddedRows(prevDumpDigest,currentDumpDigest);
        Dataset<Row> deletedRowsDigest = extractDeletedRows(prevDumpDigest,currentDumpDigest);
        Dataset<Row> unchangedRowsDigest = extractUnchangedRows(prevDumpDigest,currentDumpDigest);
        Dataset<Row> changedIdRowsDigest = findUnstableId(deletedRowsDigest,addedRowsDigest);
        Dataset<Row> changedContentRowsDigest = findContentChanges(deletedRowsDigest,addedRowsDigest);

        //exclude unstable rows
        addedRowsDigest = addedRowsDigest.join(
                changedContentRowsDigest,
                addedRowsDigest.col("id").equalTo(changedContentRowsDigest.col("id")),
                "leftanti");
        addedRowsDigest = addedRowsDigest.join(
                changedIdRowsDigest,
                addedRowsDigest.col("id").equalTo(changedIdRowsDigest.col("addedId")),
                "leftanti");
        deletedRowsDigest = deletedRowsDigest.join(
                changedContentRowsDigest,
                deletedRowsDigest.col("id").equalTo(changedContentRowsDigest.col("id")),
                "leftanti");
        deletedRowsDigest = deletedRowsDigest.join(
                changedIdRowsDigest,
                deletedRowsDigest.col("id").equalTo(changedIdRowsDigest.col("deletedId")),
                "leftanti");

        List<DiffDigest> diffDigests = new ArrayList<>();
        diffDigests.add(new DiffDigest(DiffType.UNCHANGED,unchangedRowsDigest.persist()));
        diffDigests.add(new DiffDigest(DiffType.ADDED,addedRowsDigest.persist()));
        diffDigests.add(new DiffDigest(DiffType.DELETED,deletedRowsDigest.persist()));
        diffDigests.add(new DiffDigest(DiffType.UNSTABLE_ID,changedIdRowsDigest.persist()));
        diffDigests.add(new DiffDigest(DiffType.CHANGED_CONTENT,changedContentRowsDigest.persist()));
        return diffDigests;
    }

    public static Dataset<Row> findUnstableId(Dataset<Row> deletedRows, Dataset<Row> addedRows) {
        deletedRows = deletedRows.select(col("id").as("deletedId"),col("digest").as("oldDigest"),
                col("visitCounter").as("deletedVisitCounter"),col("orderCounter").as("deletedOrderCounter"));
        addedRows = addedRows.select(col("id").as("addedId"),col("digest"),
                col("visitCounter").as("addedVisitCounter"),col("orderCounter").as("addedOrderCounter"));
        Dataset<Row> resultRows = deletedRows.join(addedRows, deletedRows.col("oldDigest").equalTo(addedRows.col("digest")));
        resultRows = resultRows.select(col("deletedId"),col("digest"),col("addedId"),
                col("addedVisitCounter").minus(col("deletedVisitCounter")).as("visitCounter"),
                col("addedOrderCounter").minus(col("deletedOrderCounter")).as("orderCounter"));
        return resultRows;
    }

    public static Dataset<Row> findContentChanges(Dataset<Row> deletedRows, Dataset<Row> addedRows) {
        deletedRows = deletedRows.select(col("id").as("deletedId"),col("digest").as("deletedDigest"),
                col("visitCounter").as("deletedVisitCounter"),col("orderCounter").as("deletedOrderCounter"));
        addedRows = addedRows.select(col("id"),col("digest").as("addedDigest"),
                col("visitCounter").as("addedVisitCounter"),col("orderCounter").as("addedOrderCounter"));
        Dataset<Row> resultRows = deletedRows.join(addedRows, deletedRows.col("deletedId").equalTo(addedRows.col("id")));
        resultRows = resultRows.select(col("id"),col("deletedDigest"),col("addedDigest"),
                col("addedVisitCounter").minus(col("deletedVisitCounter")).as("visitCounter"),
                col("addedOrderCounter").minus(col("deletedOrderCounter")).as("orderCounter"));
        return resultRows;
    }

    public static Dataset<Row> findUnchanged(Dataset<Row> oldRows, Dataset<Row> newRows) {
        oldRows = oldRows.select(col("id").as("oldId"),col("digest").as("oldDigest"),
                col("visitCounter").as("oldVisitCounter"),col("orderCounter").as("oldOrderCounter"));
        newRows = newRows.select(col("id"),col("digest"),
                col("visitCounter").as("newVisitCounter"),col("orderCounter").as("newOrderCounter"));
        Dataset<Row> resultRows = oldRows.join(newRows, oldRows.col("oldId").equalTo(newRows.col("id"))
                .and(oldRows.col("oldDigest").equalTo(newRows.col("digest"))));
        resultRows = resultRows.select(col("id"),col("digest"),
                col("newVisitCounter").minus(col("oldVisitCounter")).as("visitCounter"),
                col("newOrderCounter").minus(col("oldOrderCounter")).as("orderCounter"));
        return resultRows;
    }

    public static Dataset<Row> extractAddedRows(Dataset<Row> previousDump, Dataset<Row> currentDump) {
        Dataset<Row> resultRows = currentDump.join(previousDump, currentDump.col("id").equalTo(previousDump.col("id"))
                .and(currentDump.col("digest").equalTo(previousDump.col("digest"))),"leftanti");
        return resultRows;
    }

    public static Dataset<Row> extractDeletedRows(Dataset<Row> previousDump, Dataset<Row> currentDump) {
        Dataset<Row> resultRows = previousDump.join(currentDump, previousDump.col("id").equalTo(currentDump.col("id"))
                .and(previousDump.col("digest").equalTo(currentDump.col("digest"))),"leftanti");
        return resultRows;
    }

    public static Dataset<Row> extractUnchangedRows(Dataset<Row> previousDump, Dataset<Row> currentDump) {
        previousDump = previousDump.select(col("id").as("oldId"),col("digest").as("oldDigest"),
                col("visitCounter").as("oldVisitCounter"),col("orderCounter").as("oldOrderCounter"));
        currentDump = currentDump.select(col("id"),col("digest"),
                col("visitCounter").as("newVisitCounter"),col("orderCounter").as("newOrderCounter"));
        Dataset<Row> resultRows = previousDump.join(currentDump, previousDump.col("oldId").equalTo(currentDump.col("id"))
                .and(previousDump.col("oldDigest").equalTo(currentDump.col("digest"))));
        resultRows = resultRows.select(col("id"),col("digest"),
                col("newVisitCounter").minus(col("oldVisitCounter")).as("visitCounter"),
                col("newOrderCounter").minus(col("oldOrderCounter")).as("orderCounter"));
        return resultRows;
    }

}
