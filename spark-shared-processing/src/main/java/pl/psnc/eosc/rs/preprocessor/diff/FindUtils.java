package pl.psnc.eosc.rs.preprocessor.diff;

import pl.psnc.eosc.rs.preprocessor.diff.model.DiffData;
import pl.psnc.eosc.rs.preprocessor.diff.model.DiffDigest;

import java.util.Collection;
import java.util.function.Predicate;

public class FindUtils {
    private static <T> T findByProperty(Collection<T> collection, Predicate<T> filter) {
        return collection.stream().filter(filter).findFirst().orElse(null);
    }

    public static DiffDigest findDiggestByType(Collection<DiffDigest> diffDigests, DiffType type) {
        return FindUtils.findByProperty(diffDigests, diggest -> type.equals(diggest.getType()));
    }

    public static DiffData findDataByType(Collection<DiffData> diffData, DiffType type) {
        return FindUtils.findByProperty(diffData, data -> type.equals(data.getType()));
    }
}
