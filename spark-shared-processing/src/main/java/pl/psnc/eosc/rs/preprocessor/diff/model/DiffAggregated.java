package pl.psnc.eosc.rs.preprocessor.diff.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.storage.StorageLevel;
import pl.psnc.eosc.rs.preprocessor.diff.DiffType;

@Data
@AllArgsConstructor
public class DiffAggregated {
    DiffType type;
    Dataset<Row> digest;
    Dataset<Row> data;

    public void persistAll(){
        if(shouldPersistDigest())
        digest.persist(StorageLevel.MEMORY_AND_DISK());
        if(shouldPersistData())
            data.persist(StorageLevel.MEMORY_AND_DISK());
    }

    public void unpersistAll(){
        if(shouldPersistDigest())
            digest.unpersist();
        if(shouldPersistData())
            data.unpersist();
    }

    boolean shouldPersistDigest(){
        return digest!=null;
    }
    boolean shouldPersistData(){
        return (type.equals(DiffType.ADDED) || type.equals(DiffType.CHANGED_CONTENT)) && data!=null;
    }
}
