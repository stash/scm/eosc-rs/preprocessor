package pl.psnc.eosc.rs.preprocessor.diff;

import lombok.Getter;

public enum DiffType {

    ADDED("added"),
    DELETED("deleted"),
    UNCHANGED("unchanged"),
    UNSTABLE_ID("unstable_id"),
    CHANGED_CONTENT("changed_content");

    @Getter
    final String name;

    DiffType(String name){
        this.name = name;
    }

}
