package pl.psnc.eosc.rs.preprocessor.diff;

import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;

import static pl.psnc.eosc.rs.preprocessor.shared.SharedSparkProcessingConstants.DEFAULT_CSV_OUTPUT_FORMAT_OPTIONS;

public class CsvWriter {

    protected static void saveDigest(Dataset<Row> ds, DiffType dataDiffType, String outputDir) {
        if(ds==null || ds.isEmpty()) return;
        String outputSubDir = outputDir+"/digest/"+dataDiffType.getName();
        save(ds,outputSubDir);
    }
    protected static void saveData(Dataset<Row> ds, DiffType dataDiffType, String outputDir) {
        String outputSubDir = outputDir+"/data/"+dataDiffType.getName();
        save(ds,outputSubDir);
    }
    private static void save(Dataset<Row> ds, String outputDir) {
        ds.write()
                .options(DEFAULT_CSV_OUTPUT_FORMAT_OPTIONS)
                .csv(outputDir);
    }
}
