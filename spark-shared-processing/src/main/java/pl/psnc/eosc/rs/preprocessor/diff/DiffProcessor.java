package pl.psnc.eosc.rs.preprocessor.diff;

import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import pl.psnc.eosc.rs.preprocessor.diff.model.DiffAggregated;
import pl.psnc.eosc.rs.preprocessor.diff.model.DiffData;
import pl.psnc.eosc.rs.preprocessor.diff.model.DiffDigest;

import java.util.List;

public class DiffProcessor {

    public static List<DiffAggregated> processDump(Dataset<Row> prevDumpDigest, Dataset<Row> preprocessedCurrentDump){
        List<DiffDigest> digests = DiffFinder.recognize(prevDumpDigest,preprocessedCurrentDump);
        DataSupplier dataSupplier = new DataSupplier(preprocessedCurrentDump);
        List<DiffData> dataList = dataSupplier.match(digests);
        List<DiffAggregated> aggregatedDiffs = DiffComposer.compose(dataList,digests);
        return aggregatedDiffs;
    }
}
