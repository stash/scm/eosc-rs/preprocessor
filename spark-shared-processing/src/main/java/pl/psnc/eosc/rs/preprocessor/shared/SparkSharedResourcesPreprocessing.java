package pl.psnc.eosc.rs.preprocessor.shared;

import com.johnsnowlabs.nlp.pretrained.PretrainedPipeline;
import org.apache.spark.sql.Column;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import pl.psnc.eosc.rs.preprocessor.spark.WordsRemover;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;

import static org.apache.spark.sql.functions.*;

public class SparkSharedResourcesPreprocessing {

    public final static String URL_FINAL_REPLACEMENT = "<URL>";
    public final static String EMAIL_FINAL_REPLACEMENT = "<EMAIL>";

    public static Dataset<Row> defaultPreprocessing(Dataset<Row> ds, Boolean processKeywords, Boolean processAuthors) {
        List<String> columns = Arrays.asList(ds.columns());

        ds = defaultMainTitlesPreprocessing(ds, "mainTitles");
        ds = removeEnglishStopwords(ds, "mainTitles", true);

        ds = defaultDescriptionsPreprocessing(ds, "descriptions");
        ds = removeEnglishStopwords(ds, "descriptions", true);

        if(processKeywords) {
            ds = defaultKeywordPreprocessing(ds, "keywords");
            ds = removeEnglishStopwords(ds, "keywords", true);
        }

        if(processAuthors) {
            ds = defaultAuthorsPreprocessing(ds,"authors");
        }

        // Filter where mainTitles is "null" one more time
        ds = ds.filter(size(col("mainTitles")).plus(size(col("descriptions"))).gt(0));
        return ds;
    }

    public static Dataset<Row> detectLanguage(Dataset<Row> ds, String columnName){
        SparkSession spark = SparkSession.builder().getOrCreate();
        String modelPath = spark.sparkContext().getConf().get("spark.model.path","");

        PretrainedPipeline languageDetector;
        if(!modelPath.isBlank()){
            languageDetector = PretrainedPipeline.fromDisk(modelPath);
        } else {
            languageDetector = new PretrainedPipeline("detect_language_43", "xx");
        }


        Dataset<Row> transformedDs = ds.withColumn("text", col(columnName));
        return languageDetector.transform(transformedDs)
                .withColumn("languagePrediction", array_join(col("language.result"), ";")).as("languagePrediction")
                .drop("document","sentence","language","text");
    }

    public static Dataset<Row> defaultKeywordPreprocessing(Dataset<Row> ds, String columnName){
        // Process keywords (clean/split/etc)
        Column keywords = col(columnName);
        return ds.withColumn(columnName, defaultColumnProcessing(keywords,true));
    }

    public static Dataset<Row> defaultMainTitlesPreprocessing(Dataset<Row> ds, String columnName){
        //Process mainTitles
        Column mainTitles = col(columnName);
        return ds.withColumn(columnName, defaultColumnProcessing(mainTitles,false));
    }

    public static Dataset<Row> defaultDescriptionsPreprocessing(Dataset<Row> ds, String columnName){
        //Process descriptions
        Column descriptions = col(columnName);
        return ds.withColumn(columnName, defaultColumnProcessing(descriptions,false));
    }
    
    private static Column defaultColumnProcessing(Column column, Boolean useExtendedSplit){
        column = transform(column, col -> lower(col)); // to lowercase
        column = transform(column, col -> regexp_replace(col, "\\{\"references\":[^}]*\\}"," ")); // remove "references"
        column = transform(column, col -> regexp_replace(col, "doi:( ){0,1}[a-z0-9./]*"," ")); // remove "DOI"
        column = transform(column, col -> regexp_replace(col, "(\\&quot;|\\&amp;|\\&lt;|\\&gt;|\\&nbsp;|\\&li;)"," ")); // remove HTML entities
        column = transform(column, col -> regexp_replace(col, "\\&\\w{0,6};"," ")); // remove other HTML entities
        column = transform(column, col -> regexp_replace(col, "<[^>]+>"," ")); // remove HTML tags
        column = transform(column, col -> regexp_replace(col, "[0-z.]+@[0-z.]+\\.[A-z]{2,}", EMAIL_FINAL_REPLACEMENT)); // replace emails to final <EMAIL>
        column = transform(column, col -> regexp_replace(col, "(http|ftp|https)(:\\/\\/| )([\\w_-]+(?:(?:\\.[\\w_-]+)+))([\\w.,@?^=%&:\\/~+#-]*[\\w@?^=%&\\/~+#-])", URL_FINAL_REPLACEMENT)); // replace url's to final <URL>
        if(useExtendedSplit){
            column = transform(column, v1 -> split(v1, "([\\s_\\/]+)|(::)")); // split each keyword into tokens group
        } else {
            column = transform(column, col -> split(col, "[\\s_\\/]+")); // split by whitespaces into token groups, previous pattern for titles `([\s_\/]+)|(::)`
        }
        column = transform(column, col -> filter(col, v1 -> length(v1).gt(1).and(v1.isNotNull()))); // remove null and 1 char tokens
        column = filter(column, col -> size(col).gt(0)); // remove empty token groups
        return column;
    }

    public static Dataset<Row> defaultAuthorsPreprocessing(Dataset<Row> ds, String columnName){
        //Process descriptions
        Column authorsTransformation = col("authors");
        authorsTransformation = transform(authorsTransformation, desc -> lower(desc));
        authorsTransformation = filter(authorsTransformation, v1 -> length(v1).gt(1).and(v1.isNotNull()));
        ds = ds.withColumn("authors", authorsTransformation);
        return ds.withColumn(columnName, authorsTransformation);
    }

    public static Dataset<Row> cleanDateFormats(Dataset<Row> ds, String columnName){
        // Make Date Format Consistent
        String today = LocalDate.now().toString();
        Dataset<Row> resultDs = ds.withColumn(columnName,
                        when(col(columnName).rlike("[0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}:[0-9]{2}Z"),
                                to_timestamp(col(columnName), "yyyy-MM-dd'T'HH:mm:ss'Z'"))       //2014-08-01T01:58:24Z to timestamp
                                .when((length(col(columnName)).gt(10)), expr("substring(" + columnName + ", 1, 10)"))        //2019-07-15T16:14:28.636
                                .when((length(col(columnName)).equalTo(4)), expr("concat_ws('-'," + columnName + ",'01-01')"))   //2019
                                .otherwise(col(columnName))      //otherwise, remain the same
                );
        return resultDs;
    }

    public static Dataset<Row> filterTimestampIfGreater(Dataset<Row> ds, String columnName, LocalDate bound){
        return ds.withColumn(columnName,
                when(col(columnName).gt(bound.toString()), null)     //2558-01-01 -> null
                        .otherwise(col(columnName))     //timestamp to date
        );
    }

    public static Dataset<Row> removeEnglishStopwords(Dataset<Row> ds, String columnName){
        return removeEnglishStopwords(ds,columnName,false);
    }

    public static Dataset<Row> removeEnglishStopwords(Dataset<Row> ds, String columnName, Boolean nestedArrays){
        WordsRemover titleStopWordsRemover = new WordsRemover()
                .setColumn(columnName)
                .loadStopWordsByLanguages(new String[]{"english"})
                .setNestedArrays(nestedArrays);
        return titleStopWordsRemover.transform(ds);
    }

}
