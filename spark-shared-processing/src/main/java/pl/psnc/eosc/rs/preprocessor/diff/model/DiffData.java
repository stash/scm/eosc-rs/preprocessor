package pl.psnc.eosc.rs.preprocessor.diff.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import pl.psnc.eosc.rs.preprocessor.diff.DiffType;

@Data
@AllArgsConstructor
public class DiffData {

    DiffType type;
    Dataset<Row> dataRows;
}
