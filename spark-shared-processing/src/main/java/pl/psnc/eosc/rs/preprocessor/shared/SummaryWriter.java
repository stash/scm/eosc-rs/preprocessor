package pl.psnc.eosc.rs.preprocessor.shared;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.*;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class SummaryWriter {

    Path path;

    FileSystem fileSystem;

    Configuration hadoopConf;

    public SummaryWriter(Configuration hadoopConf) {
        this.hadoopConf = hadoopConf;
    }

    public void printSummary(String textPath){
        changePath(textPath);

        try {
            System.out.println("=================");
            System.out.println("Summary for: " + textPath);
            ContentSummary cs = fileSystem.getContentSummary(path);
            System.out.println("Total size:  "  + cs.getLength() + " B");
            System.out.println("Disk usage with replication factor: " + cs.getSpaceConsumed() + " B");
//            System.out.println("Number of records:  " + countLines());
            FileStatus[] statuses = fileSystem.listStatus(path);//TODO
            for (int i = 0; i < statuses.length; i++) {
                FileStatus currentFS = statuses[i];
                summarizeRecords(currentFS);
            }
            System.out.println("Files: " + cs.getFileCount());
            for (int i = 0; i < statuses.length; i++) {
                FileStatus currentFS = statuses[i];
                summarizeFileStatus(String.valueOf(i),currentFS);
            }
            System.out.println("=================");

        } catch (IOException e) {
            throw new RuntimeException("Error getting fileStatus for path: " + path, e);
        }
    }

    private void summarizeFileStatus(String id, FileStatus status) throws IOException {
        System.out.println(" * Directory " + id + "; Path: " + status.getPath());
        FileStatus[] subStatuses = fileSystem.listStatus(status.getPath());
        for (int i = 0; i < subStatuses.length; i++) {
            FileStatus currentFS = subStatuses[i];
            String enrichedId = id+"."+i;
            if(currentFS.isDirectory()){
                summarizeFileStatus(enrichedId,currentFS);
            } else {
                summarizeFile(enrichedId,currentFS);
            }
        }
    }
    private void summarizeFile(String id, FileStatus status){
        System.out.println("   * File " + id + "; Path: " + status.getPath() + "; Size: " + status.getLen() + " B");
    }

    private void changePath(String textPath){
        path = new Path(textPath);
        try {
            fileSystem = FileSystem.get(path.toUri(), hadoopConf);
        } catch (IOException e) {
            throw new RuntimeException("Error getting fileSystem for path: " + path, e);
        }
    }

    private void summarizeRecords(FileStatus status) throws IOException {
        FileStatus[] subStatuses = fileSystem.listStatus(status.getPath());
        for (int i = 0; i < subStatuses.length; i++) {
            FileStatus currentFS = subStatuses[i];
            if(currentFS.isDirectory()){
                System.out.println("Path "+currentFS.getPath()+" Number of records:  "+countLines(currentFS));
                summarizeRecords(currentFS);
            }
        }
    }

    private int countLines(FileStatus fileStatus) throws IOException {
        int count = 0;
        FileStatus[] subFS = fileSystem.listStatus(fileStatus.getPath());
        for(FileStatus f : subFS){
            if(f.isFile()){
                try {
                    FSDataInputStream inputStream = fileSystem.open(f.getPath());
                    BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));

                    String line = reader.readLine();
                    while(line!=null){
                        // Skip headers
                        if(!line.startsWith("id;")){
                            count++;
                        }
                        line = reader.readLine();
                    }

                    reader.close();
                } catch (IOException e) {
                    throw new RuntimeException("Error getting file for path: " + path, e);
                }
            }
        }
        return count;
    }
}
