package pl.psnc.eosc.rs.preprocessor.shared;

import lombok.Data;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import pl.psnc.eosc.rs.preprocessor.spark.SparkStandaloneSessionSingleton;

import java.sql.Date;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import static org.apache.spark.sql.functions.col;
import static org.apache.spark.sql.functions.explode;
import static org.junit.jupiter.api.Assertions.assertEquals;

class SparkSharedResourcesPreprocessingTest {

    static SparkSession sparkSession;

    @BeforeAll
    public static void createStandaloneSparkSession(){
        sparkSession = SparkStandaloneSessionSingleton.getSession();
    }

    @AfterAll
    public static void closeSparkSession(){
        sparkSession.close();
    }

    @Test
    public void defaultMainTitlesProcessing(){
        Dataset<Row> dataset = sparkSession.createDataFrame(List.of(
                        new TestRow(List.of("word1&quot;word2 email@email.com",
                                "https://website.com/path","123","word3 []{}:&amp;222",
                                "&quot; &amp; &lt; &gt; &nbsp; &li;", "&pound;",
                                "{\"references\": [\"\"]}word4{\"references\": " +
                                        "[\"Ouimet, R., Korboulousky, N., Bilger, I. 2023. " +
                                        "Soil Systems 7(2):39. https://doi.org/10.3390/soil9.\", " +
                                        "\"Forests 12, https:// doi.org/10.3390/f12050583\"]} word5",
                                "DOI: 10.11646/zootaxa.4588.1.1, DOI:10.3390/f12050583 DOI: 10.3390/soilsystems7020039")))
                , SparkSharedResourcesPreprocessingTest.TestRow.class);

        dataset = SparkSharedResourcesPreprocessing.defaultMainTitlesPreprocessing(dataset,"tokens");

        System.out.println(dataset.select(explode(col("tokens"))).collectAsList());

        Set<String> outputTokens = dataset.select(explode(col("tokens"))).collectAsList().stream().flatMap(row -> row.getList(0).stream().map(kw -> (String)kw)).collect(Collectors.toSet());

        assertEquals(Set.of("word1","word2","word3","word4","word5",
                "<EMAIL>","<URL>","[]{}:","123","222"),outputTokens);
    }

    @Test
    public void defaultDescriptionsProcessing(){
        Dataset<Row> dataset = sparkSession.createDataFrame(List.of(
                        new TestRow(List.of("word1 word2 email@email.com","https://website.com/path","123","word3 []{}: 222", "&quot; &amp; &lt; &gt; &nbsp; &li;")))
                , SparkSharedResourcesPreprocessingTest.TestRow.class);

        dataset = SparkSharedResourcesPreprocessing.defaultDescriptionsPreprocessing(dataset,"tokens");

        System.out.println(dataset.select(explode(col("tokens"))).collectAsList());

        Set<String> outputTokens = dataset.select(explode(col("tokens"))).collectAsList().stream().flatMap(row -> row.getList(0).stream().map(kw -> (String)kw)).collect(Collectors.toSet());

        assertEquals(Set.of("word1","word2","word3","<EMAIL>","<URL>","[]{}:","123","222"),outputTokens);
    }

    @Test
    public void defaultKeywordProcessing(){
        Dataset<Row> dataset = sparkSession.createDataFrame(List.of(
                        new TestRow(List.of("word1 word2 ","123","word3 []{}: 222")))
                , SparkSharedResourcesPreprocessingTest.TestRow.class);

        dataset = SparkSharedResourcesPreprocessing.defaultKeywordPreprocessing(dataset,"tokens");

        System.out.println(dataset.select(explode(col("tokens"))).collectAsList());

        Set<String> outputTokens = dataset.select(explode(col("tokens"))).collectAsList().stream().flatMap(row -> row.getList(0).stream().map(kw -> (String)kw)).collect(Collectors.toSet());

        assertEquals(Set.of("word1","word2","word3","[]{}:","123","222"),outputTokens);
    }

    @Test
    public void cleanDateFormats(){
        Dataset<Row> dataset = sparkSession.createDataFrame(List.of(
                        new TestRowDates("2014-08-01T01:58:24Z"),
                        new TestRowDates("2019-07-15T16:14:28.636"),
                        new TestRowDates("2019"),
                        new TestRowDates("2019-07-11"))
                , SparkSharedResourcesPreprocessingTest.TestRowDates.class);

        dataset = SparkSharedResourcesPreprocessing.cleanDateFormats(dataset,"date");
        dataset = dataset.withColumn("date",col("date").cast("date"));

        Set<LocalDate> outputDates = dataset.select(col("date")).collectAsList().stream().flatMap(row -> Optional.ofNullable(row.getDate(0)).map(Date::toLocalDate).stream()).collect(Collectors.toSet());

        assertEquals(Set.of(
                LocalDate.of(2019,7,15),
                LocalDate.of(2019,1,1),
                LocalDate.of(2014,8,1),
                LocalDate.of(2019,7,11)),outputDates);
    }

    @Data
    public static class TestRow {
        public List<String> tokens;

        public TestRow(List<String> tokens) {
            this.tokens = tokens;
        }
    }

    @Data
    public static class TestRowDates {
        public String date;

        public TestRowDates(String date) {
            this.date = date;
        }
    }

}