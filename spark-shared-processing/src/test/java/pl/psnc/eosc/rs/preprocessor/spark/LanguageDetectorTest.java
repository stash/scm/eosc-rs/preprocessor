package pl.psnc.eosc.rs.preprocessor.spark;

import lombok.Data;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.storage.StorageLevel;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import pl.psnc.eosc.rs.preprocessor.shared.SparkSharedResourcesPreprocessing;

import java.util.List;
import java.util.stream.Collectors;

import static org.apache.spark.sql.functions.col;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class LanguageDetectorTest {
    static SparkSession sparkSession;

    @BeforeAll
    public static void createStandaloneSparkSession(){
        sparkSession = SparkStandaloneSessionSingleton.getSession();
    }

    @AfterAll
    public static void closeSparkSession() { sparkSession.close(); }

    @Test
    public void predictLanguage(){
        Dataset<Row> dataset = sparkSession.createDataFrame(List.of(
                        new LanguageDetectorTest.TestRow("Word1;WORD2;word3"),
                        new LanguageDetectorTest.TestRow("Word1;word4"))
                , LanguageDetectorTest.TestRow.class);

        dataset = SparkSharedResourcesPreprocessing.detectLanguage(dataset,"descriptions");
        dataset.repartition(100);
        dataset.persist(StorageLevel.MEMORY_AND_DISK());

        dataset.show();
        assertEquals(2,dataset.columns().length);
        List<String> result = dataset.select(col("languagePrediction")).collectAsList().stream().map(row -> row.getString(0)).collect(Collectors.toList());
        assertEquals(2,result.size());
        assertEquals(result.get(0),"en");
        assertEquals(result.get(1),"en");
        dataset.unpersist();
        dataset.show();
        dataset.printSchema();
    }

    @Data
    public static class TestRow {
        public String descriptions;

        public TestRow(String descriptions) {
            this.descriptions = descriptions;
        }
    }
}
