package pl.psnc.eosc.rs.preprocessor.spark;

import lombok.Data;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.storage.StorageLevel;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static org.apache.spark.sql.functions.col;
import static org.apache.spark.sql.functions.explode;
import static org.junit.jupiter.api.Assertions.*;

class WordsRemoverTest {

    static SparkSession sparkSession;

    @BeforeAll
    public static void createStandaloneSparkSession(){
        sparkSession = SparkStandaloneSessionSingleton.getSession();
    }

    @AfterAll
    public static void closeSparkSession() { sparkSession.close(); }

    @Test
    public void transform(){
        Dataset<Row> dataset = sparkSession.createDataFrame(List.of(
                        new TestRow(List.of("Word1", "WORD2", "word3")),
                        new TestRow(List.of("Word1", "word4")))
                ,TestRow.class);

        dataset.show();

        // Remove words from keywords
        WordsRemover keywordsStopWordsRemover = new WordsRemover()
                .setColumn("keywords")
                .setStopWords(new String[]{"word1", "word2"})
                .setNestedArrays(false);

        dataset = keywordsStopWordsRemover.transform(dataset);
        dataset.persist(StorageLevel.MEMORY_ONLY());

        Set<String> keywordsFlattened = dataset.select(explode(col("keywords"))).collectAsList().stream().map(row -> row.getString(0)).collect(Collectors.toSet());

        assertEquals(2,keywordsFlattened.size());
        assertTrue(keywordsFlattened.contains("word3"));
        assertTrue(keywordsFlattened.contains("word4"));
        dataset.show();
    }

    @Data
    public static class TestRow {
        public List<String> keywords;

        public TestRow(List<String> keywords) {
            this.keywords = keywords;
        }
    }

}