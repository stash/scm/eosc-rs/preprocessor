package pl.psnc.eosc.rs.preprocessor.diff;

public class TestPath {
    static private final String BASE = "src/test/resources/diffprocessing";
    static private final String DUMP_CURRENT = BASE+"/current_dump";
    static private final String DUMP_PREVIOUS = BASE+"/previous_dump";
    static public final String CURRENT_BASE = DUMP_CURRENT+"/base.jsonl";
    static public final String CURRENT_EMPTY = DUMP_CURRENT+"/empty.jsonl";
    static public final String CURRENT_MIXED = DUMP_CURRENT+"/mixed.jsonl";
    static public final String PREVIOUS_ADDED = DUMP_PREVIOUS+"/added.csv";
    static public final String PREVIOUS_CHANGED_CONTENT = DUMP_PREVIOUS+"/changed_content.csv";
    static public final String PREVIOUS_DELETED = DUMP_PREVIOUS+"/deleted.csv";
    static public final String PREVIOUS_UNCHANGED = DUMP_PREVIOUS+"/unchanged.csv";
    static public final String PREVIOUS_UNSTABLE_ID = DUMP_PREVIOUS+"/unstable_id.csv";
    static public final String PREVIOUS_MIXED = DUMP_PREVIOUS+"/mixed.csv";
    static public final String PREVIOUS_STRUCTURED = BASE+"/previous_structured";
    
}