package pl.psnc.eosc.rs.preprocessor.diff;

import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.types.StructType;

import static pl.psnc.eosc.rs.preprocessor.shared.SharedSparkProcessingConstants.COL_SEPARATOR;

public class SimpleCsvReader {

    public static Dataset<Row> readCsvDigestData(String inputFiles) {
        SparkSession sparkSession = SparkSession.getDefaultSession().get();
        // Read csv to dataframe

        StructType schema = new StructType()
                    .add("id", "string")
                    .add("digest", "string")
                    .add("orderCounter", "string")
                    .add("visitCounter", "string");

        Dataset<Row> ds = sparkSession.read().option("sep", COL_SEPARATOR).option("encoding", "UTF-8").option("header","true").schema(schema).csv(inputFiles).toDF();

        return ds;
    }
}
