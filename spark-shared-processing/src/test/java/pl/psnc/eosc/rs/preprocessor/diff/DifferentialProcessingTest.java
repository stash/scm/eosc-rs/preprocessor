package pl.psnc.eosc.rs.preprocessor.diff;

import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import pl.psnc.eosc.rs.preprocessor.diff.model.DiffAggregated;
import pl.psnc.eosc.rs.preprocessor.spark.SparkStandaloneSessionSingleton;

import java.util.List;

import static com.google.common.truth.Truth.assertThat;
import static pl.psnc.eosc.rs.preprocessor.diff.SimpleJsonReader.readJsonData;
import static pl.psnc.eosc.rs.preprocessor.diff.SimpleCsvReader.readCsvDigestData;

public class DifferentialProcessingTest {

    static SparkSession sparkSession;

    @BeforeAll
    public static void createStandaloneSparkSession(){
        sparkSession = SparkStandaloneSessionSingleton.getSession();
    }

    @AfterAll
    public static void closeSparkSession(){
        sparkSession.close();
    }

    @Test
    public void initProcessing(){
        Dataset<Row> dataset = readJsonData(TestPath.CURRENT_BASE);
        List<DiffAggregated> result = DiffProcessor.processDump(null,dataset);

        assertThat(result.size()).isEqualTo(5);
        printResult(result);
        for (DiffAggregated agg: result) {
            if(agg.getType().equals(DiffType.ADDED)){
                assertThat(agg.getDigest().count()).isEqualTo(3);
                assertThat(agg.getData().count()).isEqualTo(3);
            } else {
                if (agg.getDigest() != null)
                    assertThat(agg.getDigest().count()).isEqualTo(0);
                if (agg.getData() != null)
                    assertThat(agg.getData().count()).isEqualTo(0);
            }
        }
//        DiffSaver saver = new DiffSaver("src/test/second");
//        saver.saveDiff(result);
    }

    @Test
    public void addedProcessing(){
        Dataset<Row> dataset = readJsonData(TestPath.CURRENT_BASE);
        Dataset<Row> previous = readCsvDigestData(TestPath.PREVIOUS_ADDED);
        List<DiffAggregated> result = DiffProcessor.processDump(previous,dataset);

        assertThat(result.size()).isEqualTo(5);
        printResult(result);
        for (DiffAggregated agg: result) {
            if(agg.getType().equals(DiffType.ADDED)){
                assertThat(agg.getDigest().count()).isEqualTo(3);
                assertThat(agg.getData().count()).isEqualTo(3);
            } else {
                if (agg.getDigest() != null)
                    assertThat(agg.getDigest().count()).isEqualTo(0);
                if (agg.getData() != null)
                    assertThat(agg.getData().count()).isEqualTo(0);
            }
        }
    }

    @Test
    public void unchangedProcessing(){
        Dataset<Row> dataset = readJsonData(TestPath.CURRENT_BASE);
        Dataset<Row> previous = readCsvDigestData(TestPath.PREVIOUS_UNCHANGED);
        dataset.show();
        previous.show();
        List<DiffAggregated> result = DiffProcessor.processDump(previous,dataset);

        assertThat(result.size()).isEqualTo(5);
        printResult(result);
        for (DiffAggregated agg: result) {
            if(agg.getType().equals(DiffType.UNCHANGED)){
//                assertThat(agg.getDigest().count()).isEqualTo(3);
            } else {
                if (agg.getDigest() != null) {
                    System.out.println("Type: " + agg.getType() + " " + agg.getDigest().count());
                    agg.getDigest().show(false);
                    System.out.println("~~~~~~~~~~~~~~~~~~~`");
                    assertThat(agg.getDigest().count()).isEqualTo(0);
                }
                if (agg.getData() != null)
                    assertThat(agg.getData().count()).isEqualTo(0);
            }
        }
    }

    @Test
    public void changedContentProcessing(){
        Dataset<Row> dataset = readJsonData(TestPath.CURRENT_BASE);
        Dataset<Row> previous = readCsvDigestData(TestPath.PREVIOUS_CHANGED_CONTENT);
        previous.show();
        List<DiffAggregated> result = DiffProcessor.processDump(previous,dataset);

        assertThat(result.size()).isEqualTo(5);
        printResult(result);
        for (DiffAggregated agg: result) {
            if(agg.getType().equals(DiffType.CHANGED_CONTENT)){
                assertThat(agg.getDigest().count()).isEqualTo(3);
                assertThat(agg.getData().count()).isEqualTo(3);
            } else {
                if (agg.getDigest() != null)
                    assertThat(agg.getDigest().count()).isEqualTo(0);
                if (agg.getData() != null)
                    assertThat(agg.getData().count()).isEqualTo(0);
            }
        }
    }

    @Test
    public void unstableIdProcessing(){
        Dataset<Row> dataset = readJsonData(TestPath.CURRENT_BASE);
        Dataset<Row> previous = readCsvDigestData(TestPath.PREVIOUS_UNSTABLE_ID);
        previous.show();
        List<DiffAggregated> result = DiffProcessor.processDump(previous,dataset);

        assertThat(result.size()).isEqualTo(5);
        printResult(result);
        for (DiffAggregated agg: result) {
            if(agg.getType().equals(DiffType.UNSTABLE_ID)){
                assertThat(agg.getDigest().count()).isEqualTo(3);
            } else {
                if (agg.getDigest() != null)
                    assertThat(agg.getDigest().count()).isEqualTo(0);
                if (agg.getData() != null)
                    assertThat(agg.getData().count()).isEqualTo(0);
            }
        }
    }

    @Test
    public void deletedProcessing(){
        Dataset<Row> dataset = readJsonData(TestPath.CURRENT_EMPTY);
        Dataset<Row> previous = readCsvDigestData(TestPath.PREVIOUS_DELETED);
        previous.show(false);
        List<DiffAggregated> result = DiffProcessor.processDump(previous,dataset);

        assertThat(result.size()).isEqualTo(5);
        printResult(result);
        for (DiffAggregated agg: result) {
            if(agg.getType().equals(DiffType.DELETED)){
                assertThat(agg.getDigest().count()).isEqualTo(3);
            } else {
                if (agg.getDigest() != null)
                    assertThat(agg.getDigest().count()).isEqualTo(0);
                if (agg.getData() != null)
                    assertThat(agg.getData().count()).isEqualTo(0);
            }
        }
    }

    @Test
    public void mixedProcessing(){
        Dataset<Row> dataset = readJsonData(TestPath.CURRENT_MIXED);
        Dataset<Row> previous = readCsvDigestData(TestPath.PREVIOUS_MIXED);
        dataset.show();
        previous.show();
        List<DiffAggregated> result = DiffProcessor.processDump(previous,dataset);

        assertThat(result.size()).isEqualTo(5);
        printResult(result);
        for (DiffAggregated agg: result) {
            assertThat(agg.getDigest().count()).isEqualTo(1);
            if (agg.getType() == DiffType.ADDED
                    || agg.getType() == DiffType.CHANGED_CONTENT)
                assertThat(agg.getData().count()).isEqualTo(1);
        }
    }

    @Test
    public void mixedStructuredProcessing(){
        Dataset<Row> dataset = readJsonData(TestPath.CURRENT_MIXED);
        Dataset<Row> previous = CsvDigestReader.readCsvEffectiveData(TestPath.PREVIOUS_STRUCTURED);
        previous.show();
        List<DiffAggregated> result = DiffProcessor.processDump(previous,dataset);

        assertThat(result.size()).isEqualTo(5);
        printResult(result);
        for (DiffAggregated agg: result) {
            assertThat(agg.getDigest().count()).isEqualTo(1);
            if (agg.getType() == DiffType.ADDED
                    || agg.getType() == DiffType.CHANGED_CONTENT)
                assertThat(agg.getData().count()).isEqualTo(1);
        }
    }

    private void printResult(List<DiffAggregated> result){
        for (DiffAggregated agg: result) {
            System.out.println(agg.getType());
            if(agg.getDigest()!=null)agg.getDigest().show(false);
            if(agg.getData()!=null)agg.getData().show();
            System.out.println("-----------");
        }
    }

}