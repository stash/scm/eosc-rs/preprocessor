package pl.psnc.eosc.rs.preprocessor.diff;

import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.types.DataTypes;
import org.apache.spark.sql.types.StructType;

import static org.apache.spark.sql.functions.*;
import static org.apache.spark.sql.functions.col;

public class SimpleJsonReader {

    public static Dataset<Row> readJsonData(String inputFiles) {
        SparkSession sparkSession = SparkSession.getDefaultSession().get();
        StructType schema = new StructType()
                .add("id", DataTypes.StringType)
                .add("title",DataTypes.StringType)
                .add("open_access",DataTypes.BooleanType)
                .add("relations", DataTypes.createArrayType(DataTypes.StringType))
                .add("orderCounter", DataTypes.StringType)
                .add("visitCounter", DataTypes.StringType);


        Dataset<Row> ds = sparkSession.read().schema(schema).json(inputFiles).toDF();


        ds = ds.select(
                col("id"),
                col("title"),
                col("open_access").as("isOpenAccess"),
                array_join(col("relations"), ";").as("relations"),
                col("orderCounter"),
                col("visitCounter")
        );

        return ds;
    }
}
