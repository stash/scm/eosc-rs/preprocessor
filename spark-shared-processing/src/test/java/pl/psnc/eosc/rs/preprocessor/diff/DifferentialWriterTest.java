package pl.psnc.eosc.rs.preprocessor.diff;

import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.io.TempDir;
import pl.psnc.eosc.rs.preprocessor.diff.model.DiffAggregated;
import pl.psnc.eosc.rs.preprocessor.spark.SparkStandaloneSessionSingleton;

import java.io.File;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import static com.google.common.truth.Truth.assertThat;
import static pl.psnc.eosc.rs.preprocessor.diff.SimpleCsvReader.readCsvDigestData;
import static pl.psnc.eosc.rs.preprocessor.diff.SimpleJsonReader.readJsonData;

public class DifferentialWriterTest {

    @TempDir
    File tmpDir;

    static SparkSession sparkSession;

    @BeforeAll
    public static void createStandaloneSparkSession(){
        sparkSession = SparkStandaloneSessionSingleton.getSession();
    }

    @AfterAll
    public static void closeSparkSession(){
        sparkSession.close();
    }

//    @Test
    public void checkDirStructure(){
        Dataset<Row> dataset = readJsonData(TestPath.CURRENT_MIXED);
        Dataset<Row> previous = readCsvDigestData(TestPath.PREVIOUS_MIXED);
        List<DiffAggregated> result = DiffProcessor.processDump(previous,dataset);

        assertThat(result.size()).isEqualTo(5);
        for (DiffAggregated agg: result) {
            assertThat(agg.getDigest().count()).isEqualTo(1);
            if (agg.getType() == DiffType.ADDED
                    || agg.getType() == DiffType.CHANGED_CONTENT)
                assertThat(agg.getData().count()).isEqualTo(1);
        }

        DiffWriter.saveDiffToCsvWithLangDetection(result, tmpDir.getPath());
        assertThat(tmpDir.list()).asList().containsExactlyElementsIn(List.of("data","digest"));
        for (File file: Objects.requireNonNull(tmpDir.listFiles())) {
            if(file.getName().equals("digest")){
                assertThat(file.list()).asList().containsExactlyElementsIn(
                        Arrays.stream(DiffType.values()).map(value->value.name).collect(Collectors.toList()));
            } else if(file.getName().equals("data")){
                assertThat(file.list()).asList().containsExactlyElementsIn(
                        List.of(DiffType.ADDED.name,DiffType.CHANGED_CONTENT.getName()));
            }
            checkIfSubdirContainsExpectedFiles(file.listFiles());
        }

    }

    private void checkIfSubdirContainsExpectedFiles(File[] files) {
        for (File file: Objects.requireNonNull(files)) {
            assertThat(file.list().length).isAtLeast(4);
        }
    }

}
