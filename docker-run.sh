#!/bin/bash
set -x

# Use to build mvn project, build image and run container

mvn package
docker compose build
docker compose up