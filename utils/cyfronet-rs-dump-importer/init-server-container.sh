#!/bin/bash

docker run -p 27017:27017 -v "$(realpath ./dump_dir):/dump_dir" --name eosc-cyfronet-rs-mongo -d mongo:latest
