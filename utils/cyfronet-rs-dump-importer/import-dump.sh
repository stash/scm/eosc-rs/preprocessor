#!/bin/bash

DB_NAME=cyfronet-rs
docker exec -i eosc-cyfronet-rs-mongo sh -c "mongorestore -d ${DB_NAME} --dir /dump_dir"
