# How-to import Cyfronet's MongoDb dump file

1. First, initialize a Docker container for latest version of the MongoDb server: `init-server-container.sh`
2. The brand new server container should be listening on port `27017`
3. Then, put the `*.json` and `*.bson` files into the `dump_dir` directory.
4. Finally, import data by calling the script: `import-dump.sh`
