#!/bin/sh

# This script starts a Swagger UI docker container with given JSON file mounted as a volume.

GIVEN_JSONFILE=`realpath $1`

if [ -z "${GIVEN_JSONFILE}" ];
then
  echo "How to run: $0 <swagger json file path>"
  exit 1
fi

echo "Starting the Swagger UI in container"
echo "Loaded file: ${GIVEN_JSONFILE}"
echo "Please open http://localhost"
echo "The container will be removed after stopped"

docker run \
-ti \
--rm \
-p 80:8080 \
-e SWAGGER_JSON=/mounted_doc/swagger.json \
-v "${GIVEN_JSONFILE}":/mounted_doc/swagger.json \
swaggerapi/swagger-ui

