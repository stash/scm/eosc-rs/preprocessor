# How to import data

1. First, put a dump file(s) into the folder `./docker-entrypoint-initdb.d/` (an example file name: `02_mp_db_dump_ano17022022.sql`). There can be more than one dump file placed there. Please keep the file naming convention to ensure proper order of files loading.
2. Then, call the `init-server-container.sh`.

As a result, a brand new PostgreSQL docker container is created and the data from the dump file(s) are restored automatically. 
