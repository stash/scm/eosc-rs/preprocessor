#!/bin/bash

docker run -it \
 -p 5432:5432 \
 -e POSTGRES_PASSWORD=somepass123 \
 -v ${PWD}/docker-entrypoint-initdb.d:/docker-entrypoint-initdb.d \
 --name eosc-mp-postgres \
 postgres:14
