#!/bin/bash
set -x

# Use to build mvn project, build image (no-cache) and recreate container

mvn clean package
docker compose build --no-cache
docker compose up --force-recreate --no-deps