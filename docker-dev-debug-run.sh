#!/bin/bash
set -x

# Use to build mvn project, build image and run container with active 'dev' and 'debug' profiles.

mvn package
docker compose build
docker compose --profile dev --profile debug up